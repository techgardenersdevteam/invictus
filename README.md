# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Invictus Stand Alone bundle


### VERSIONS

TAG: 3.0.X => Sf 2.4 supports
TAG: 3.1.X => Sf 2.7 supports
TAG: 3.2.X => Sf 2.8 supports
TAG: 3.3.X => Sf 3.0 Supports

### How do I get set up? ###

install bundle

            composer require invictus/cms-core dev-master


enable bundles:

            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new Bazinga\Bundle\JsTranslationBundle\BazingaJsTranslationBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),

            new Invictus\CmsBundle\InvictusCmsBundle(),
            new Invictus\AdminBundle\InvictusAdminBundle(),
            new Invictus\NewsBundle\InvictusNewsBundle(),
            new Invictus\ProjectBundle\InvictusProjectBundle(),
            new Invictus\PageBundle\InvictusPageBundle(),
            new Invictus\WidgetBundle\InvictusWidgetBundle(),
            new Invictus\PrototypeBundle\InvictusPrototypeBundle(),            
            //new Invictus\UserBundle\InvictusUserBundle(),
            //new Invictus\PathBundle\InvictusPathBundle(),
            new Invictus\TemplateBundle\InvictusTemplateBundle(),
            new Invictus\ServiceBundle\InvictusServiceBundle(),
            new Invictus\FaqBundle\InvictusFaqBundle(),
            new Invictus\GuideBundle\InvictusGuideBundle(),
            new Invictus\TestimonialBundle\InvictusTestimonialBundle(),
            new Invictus\FrontEndBundle\InvictusFrontEndBundle(),
           
            // NOTE
            //If you need to use other modules of CMS enabled its here

insert parameters:

			mailer_port: null
			cookie_lifetime: 0
			invictus_folder: invictus
			invictus_session_lifetime: 3600
            invictus_superadmin_mail: yuo@yourdomain.ext
			
	
add routes


            _liip_imagine:
                resource: "@LiipImagineBundle/Resources/config/routing.xml"
            
            _bazinga_jstranslation:
                resource: "@BazingaJsTranslationBundle/Resources/config/routing/routing.yml"
            
            fos_js_routing:
                resource: "@FOSJsRoutingBundle/Resources/config/routing/routing.xml"
            
            
            invictus_admin:
                resource: "@InvictusAdminBundle/Resources/config/routing.yml"
                prefix:   /%invictus_folder%
            
            invictus_widget:
                resource: "@InvictusWidgetBundle/Resources/config/routing.yml"
                prefix:   /%invictus_folder%
            
            invictus_cms:
                resource: "@InvictusCmsBundle/Resources/config/routing.yml"
                prefix:   /%invictus_folder%
            
            
            # Rotte base per il FE di INVICTUS
            invictus_fe:
                resource: "@InvictusFrontEndBundle/Resources/config/routing.yml"
                prefix:   /
				
				
				
				
Install assets

            // Already done if composer install run with no error            
            php app/console assets:install

Add security configuration

# To get started with security, check out the documentation:
# http://symfony.com/doc/current/book/security.html

    jms_security_extra:
        secure_all_services: false
        expressions: true

    security:
        encoders:
            Invictus\AdminBundle\Entity\Admin:
                algorithm: sha512
                iterations: 5000
                encode_as_base64: true
            FOS\UserBundle\Model\UserInterface: bcrypt
    
        role_hierarchy:
            ROLE_ADMIN:       ROLE_USER
            ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]
    
        providers:
            invictus:
                entity: {class: InvictusAdminBundle:Admin } # using a custom repository to login with username or email, details in AdminRepository.php
            fos_userbundle:
                id: fos_user.user_provider.username
    
        firewalls:
            invictus:
                pattern: ^/%invictus_folder%/
                provider: invictus
                anonymous: ~
                form_login:
                    check_path: /%invictus_folder%/login_check
                    login_path: /%invictus_folder%/login
                    default_target_path: /%invictus_folder%/dashboard
                    username_parameter: invictus_username
                    password_parameter: invictus_password
                logout:
                    path: /%invictus_folder%/logout
                    target: /%invictus_folder%/login
    
        access_control:
            - { path: /%invictus_folder%/login, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: /%invictus_folder%/.+, role: ROLE_ADMIN }
            - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/admin/, role: ROLE_ADMIN }
            - { path: ^/it_IT/webapp, role: ROLE_USER }
            - { path: ^/en_GB/webapp, role: ROLE_USER }
            - { path: ^/, role: ROLE_USER }
    
        access_denied_url: ^/login

Add configuration on config.yml

    # Parameters IS already present !!!
    parameters:
        locale: it_IT
        liip_imagine.data_root: %kernel.root_dir%/../web/uploads

    bazinga_js_translation:
        locale_fallback:    "%locale%"
        

    liip_imagine:
        loaders:
            default:
                filesystem:
                    data_root: "%liip_imagine.data_root%"
        resolvers:
           profile_photos:
              web_path:
                  cache_prefix:         /media
        filter_sets:
            invictus_medialibrary:
                quality: 80
                filters:
                    thumbnail: { size: [60, 60], mode: inbound }
            invictus_instant_preview:
                quality: 80
                filters:
                    thumbnail: { size: [800, 600], mode: inset }
            invictus_thumb:
                quality: 80
                filters:
                    thumbnail: { size: [40, 40], mode: inbound }
            desktop_bg_full:
                quality: 80
                filters:
                    thumbnail: { size: [1440, 900], mode: inset }
            tablet_bg_full:
                quality: 80
                filters:
                    thumbnail: { size: [1024, 750], mode: inset }
            phone_bg_full:
                quality: 80
                filters:
                    thumbnail: { size: [640, 1100], mode: inset }
            blog_thumb:
                quality: 80
                filters:
                    thumbnail: { size: [200, 190], mode: outbound, allow_upscale: true }
            blog_medium:
                quality: 80
                filters:
                    thumbnail: { size: [600, 450], mode: outbound, allow_upscale: true }
            blog_zoom:
                quality: 80
                filters:
                    thumbnail: { size: [1000, 1000], mode: outbound }
            related_post_thumb:
                quality: 80
                filters:
                    thumbnail: { size: [200, 200], mode: outbound, allow_upscale: true }
            media_zoom:
                quality: 80
                filters:
                    thumbnail: { size: [500, 700], mode: inset, allow_upscale: true }
            fb_img:
                quality: 80
                filters:
                    thumbnail: { size: [600, 450], mode: outbound, allow_upscale: true }
            fixed_width_360:
                quality: 80
                filters:
                    thumbnail: { size: [360, 3000], mode: inset }
            fixed_width_600:
                quality: 80
                filters:
                    thumbnail: { size: [360, 3000], mode: inset }
            team:
                quality: 80
                filters:
                    thumbnail: { size: [480, 960], mode: outbound }
            test:
                quality: 80
                filters:
                    thumbnail: { size: [400, 300], mode: outbound, allow_upscale: true }        