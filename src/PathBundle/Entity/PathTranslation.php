<?php

namespace Invictus\PathBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PathTranslation
 *
 * @ORM\Table(name="path_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_path_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id"})
 * @ORM\Entity(repositoryClass="Invictus\PathBundle\Entity\PathTranslationRepository")
 */
class PathTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Path
     *
     * @ORM\ManyToOne(targetEntity="Path", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_path_id", referencedColumnName="id")
     * })
     */
    private $fkBase;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\PathBundle\Entity\Path $fkBase
     * @return PathTranslation
     */
    public function setFkBase(\Invictus\PathBundle\Entity\Path $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\PathBundle\Entity\Path
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}