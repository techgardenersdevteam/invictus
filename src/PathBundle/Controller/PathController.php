<?php

namespace Invictus\PathBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Response;
    use Invictus\CmsBundle\Controller\InvictusController;
    use Symfony\Component\Finder\Finder;


class PathController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        //$config['appId'] = 'vs';
        //$config['languageId'] = 'en_GB';
        //$config['moduleId'] = 4;
        $config['orderBy'] = array(
            'translation.label' => 'translation.label ASC'
        );
        $this->tableManager->addConfig($config);
        $this->tableManager->enablePredefinedToggle('enabled');

        //$this->tableManager->enableAction('edit');
        //$this->tableManager->disableAction('delete');
    }


    public function importAction()
    {

        $kmlDir = $this->get('kernel')->getRootDir().'/../web/kml';

        $finder = new Finder();
        $finder->files()->in($kmlDir.'/percorsi/linee')->name('*.kml');
        $polylines = array();
        foreach ($finder as $file) {
            $path = $kmlDir.'/percorsi/linee/'.$file->getRelativePathname();
            $polylines[$file->getRelativePathname()] = $path;
            /*
            // Print the absolute path
            print $file->getRealpath()."\n";

            // Print the relative path to the file, omitting the filename
            print $file->getRelativePath()."\n";

            // Print the relative path to the file
            print $file->getRelativePathname()."\n";
            */
        }

        $finder = new Finder();
        $finder->files()->in($kmlDir.'/percorsi/punti')->name('*.kml');
        $markers = array();
        foreach ($finder as $file) {
            $path = $kmlDir.'/percorsi/punti/'.$file->getRelativePathname();
            $markers[$file->getRelativePathname()] = $path;
            /*
            // Print the absolute path
            print $file->getRealpath()."\n";

            // Print the relative path to the file, omitting the filename
            print $file->getRelativePath()."\n";

            // Print the relative path to the file
            print $file->getRelativePathname()."\n";
            */
        }

        return $this->render('N2ktagFEBundle:Import:path.html.twig', array(
                'polylines' => $polylines,
                'markers' => $markers
            ));

    }


    public function importPolylinesAction($pathId, $filename, $path = 'uploads/')
    {

        set_time_limit(7200);
        $dbHost = $this->container->getParameter('database_host');
        $dbName = $this->container->getParameter('database_name');
        $dbUser = $this->container->getParameter('database_user');
        $dbPassword = $this->container->getParameter('database_password');

        $conn = new \PDO("mysql:host=$dbHost;dbname=$dbName;charset=utf8", $dbUser, $dbPassword);

        $filePath = $this->get('kernel')->getRootDir().'/../web/'.$path.$filename;

        if(file_exists($filePath)){

            $kml = simplexml_load_file($filePath);

            if ($kml === false) {
                echo "Failed loading KML\n";
                foreach(libxml_get_errors() as $error) {
                    echo "\t", $error->message;
                }
                die();
            }

            $sql = "DELETE
                      FROM relation

                      WHERE
                      fk_module_id_a = 55
                      AND fk_item_id_a = $pathId
                      AND fk_module_id_b = 53
                      AND slot = 1";
            $query = $conn->prepare($sql);
            if(!$query->execute()){
                echo '<br>Query error: '.$sql;
            }


            $i = 0;
            $errors = 0;
            foreach($kml->Document->Folder->Placemark as $placemark){

                $pointsCoordinates = $placemark->MultiGeometry->LineString->coordinates;

                $points = explode(' ', $pointsCoordinates);
                array_shift($points);

                foreach($points as $point){

                    $i++;

                    $coordinates = explode(',', $point);

                    if(count($coordinates) == 3){

                        $point = array();
                        $point['latitude'] = $coordinates[1];
                        $point['longitude'] = $coordinates[0];
                        $point['enabled'] = true;
                        $point['deleted'] = false;

                        /*
                        echo "<pre>";
                        echo $i;
                        print_r($point);
                        echo "</pre>";
                        echo "<br><br>";
                        */

                        $sql = "INSERT INTO geo_point( lat, lng, enabled, deleted )
                                    values ( :latitude, :longitude, :enabled, :deleted )";
                        $query = $conn->prepare($sql);
                        $success = $query->execute($point);

                        if(!$success)
                        {
                            $errors++;
                            echo "<pre>";
                            print_r($query->errorInfo());
                            echo "</pre>";
                            echo "<br><br>";
                        }else{
                            $lastInsertId = $conn->lastInsertId();
                            $sql = "INSERT INTO relation( fk_module_id_a, fk_item_id_a, fk_module_id_b, fk_item_id_b, slot )
                                    values ( 55, $pathId, 53, $lastInsertId, 1)";
                            $query = $conn->prepare($sql);
                            if(!$query->execute()){
                                echo '<br>'.$sql;
                            }
                        }

                    }

                }
            }

            echo " - - - - - [ $i punti ] - - - - - -<br>";

            echo " - - - - - [ $errors errori ] - - - - - -";

            die();

        }else{
            echo "$filename non esiste";
        }

        die(' - - - ');
    }


    public function importMarkersAction($pathId, $filename)
    {

        set_time_limit(7200);

        $dbHost = $this->container->getParameter('database_host');
        $dbName = $this->container->getParameter('database_name');
        $dbUser = $this->container->getParameter('database_user');
        $dbPassword = $this->container->getParameter('database_password');

        $conn = new \PDO("mysql:host=$dbHost;dbname=$dbName;charset=utf8", $dbUser, $dbPassword);

        $filePath = "kml/$filename";

        if(file_exists($filePath)){

            $kml = simplexml_load_file($filePath);

            if ($kml === false) {
                echo "Failed loading KML\n";
                foreach(libxml_get_errors() as $error) {
                    echo "\t", $error->message;
                }
                die();
            }

            $i = 0;
            $errors = 0;
            foreach($kml->Document->Folder->Placemark as $placemark){

                $pointsCoordinates = $placemark->MultiGeometry->LineString->coordinates;

                $points = explode(' ', $pointsCoordinates);
                array_shift($points);

                foreach($points as $point){

                    $i++;

                    $coordinates = explode(',', $point);

                    $point = array();
                    $point['latitude'] = $coordinates[0];
                    $point['longitude'] = $coordinates[1];
                    $point['enabled'] = true;
                    $point['deleted'] = false;

                    /*
                    echo "<pre>";
                    echo $i;
                    print_r($point);
                    echo "</pre>";
                    echo "<br><br>";
                    */

                    $sql = "INSERT INTO geo_point( lat, lng, enabled, deleted )
                                    values ( :latitude, :longitude, :enabled, :deleted )";
                    $query = $conn->prepare($sql);
                    $success = $query->execute($point);

                    if(!$success)
                    {
                        $errors++;
                        echo "<pre>";
                        print_r($query->errorInfo());
                        echo "</pre>";
                        echo "<br><br>";
                    }else{
                        $lastInsertId = $conn->lastInsertId();
                        $sql = "INSERT INTO relation( fk_module_id_a, fk_item_id_a, fk_module_id_b, fk_item_id_b, slot )
                                    values ( 55, $pathId, 53, $lastInsertId, 1)";
                        $query = $conn->prepare($sql);
                        if(!$query->execute()){
                            echo '<br>'.$sql;
                        }
                    }
                }
            }

            echo " - - - - - [ $i punti ] - - - - - -<br>";

            echo " - - - - - [ $errors errori ] - - - - - -";

            die();

        }else{
            echo "$filename non esiste";
        }

        die(' - - - ');

    }


    public function getPathByIdAction($_locale, $id, $lat = 45.438936, $lng = 12.583466, $radius = 500)
    {
        $cm = $this->get('invictus.contentManager');

        $pathsConfig = array(
            'moduleId' => 55,
            'languageId' => $_locale,
            /*
            'joins' => array(
                'visibility' => false
            )*/
            'orderBy' => array(
                'base.id' => 'base.id DESC'
            )
        );

        if($id != 0){
            $pathsConfig['where'] = array(
                'base.id' => "base.id = $id"
                //'base.id' => "base.id IN($id)"
            );
        }

        if($id == 'moretto'){
            die('incompetenza');
        }

        if($id == 'poile'){
            die('Dio');
        }

        $paths = $cm->getData($pathsConfig)['data'];

        /*
        var_dump($paths);
        die();
        */

        $data = array();

        $pathIds = array();

        foreach($paths as $path){
            $data[$path->getId()]['id'] = $path->getId();
            //$data[$path->getId()]['codice'] = $path->getCod();
            //$data[$path->getId()]['idCategoria'] = $path->getFkCategory()->getId();
            //$data[$path->getId()]['categoria'] = $path->getFkCategory()->getTranslations()[0]->getLabel();
            $data[$path->getId()]['label'] = $path->getTranslations()[0]->getLabel();
            $data[$path->getId()]['points'] = array();
            $data[$path->getId()]['markers'] = array();
            $data[$path->getId()]['polygons'] = array();

            $pathIds[] = $path->getId();

            //var_dump($pathIds);die();
            $pathPoints = $this->getDoctrine()
                ->getRepository('InvictusWidgetBundle:GeoPoint')
                ->getPathPointsById($path->getId());

            //print_r($polygons);die();
            foreach($pathPoints as $key => $point){
                $data[$key]['points'] = $point;
            }

            $pathMarkers = $this->getDoctrine()
                ->getRepository('InvictusWidgetBundle:GeoPoint')
                ->getPathMarkersById($_locale, $path->getId(), $lat, $lng, $radius);

            //print_r($polygons);die();
            foreach($pathMarkers as $key => $marker){
                $data[$key]['markers'] = $marker;
            }

            /*
            $pathsPolygons = $this->getDoctrine()
                ->getRepository('InvictusPathBundle:Path')
                ->getPolygonsByPathIds($path->getId());

            //var_dump($pathsPolygons);die();
            foreach($pathsPolygons as $key => $polygon){
                $data[$path->getId()]['polygons'][$key] = $polygon;
            }
            */

        }

        return new JsonResponse(array_values($data));
    }


    public function getMarkersByHabitatIdsAction($ids, $_locale)
    {
        $ids = explode(',', $ids);
        $points = $this->getDoctrine()
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->getGeoPointsByHabitatIds($ids, $_locale);

        return new JsonResponse(array_values($points));
    }


    public function getMarkersByDatasheetIdsAction($ids, $_locale)
    {
        $ids = explode(',', $ids);
        $points = $this->getDoctrine()
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->getGeoPointsByDatasheetIds($ids, $_locale);

        return new JsonResponse(array_values($points));
    }


    protected function preUpdateItem($item){
        try{
            $length = rand(1,15);
            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
            $item->setAbstract($randomString);
        }catch (Exception $e){

        }

        return $item;
    }

}
