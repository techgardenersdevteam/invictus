

function POIGmapPinPointerInitialize(){

    var map;
    var mapOptions = {
        zoom: 11,
        center: new google.maps.LatLng(45.379161,12.458496),
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        mapTypeControl: true,
        mapTypeControlOptions:{
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        navigationControl: true,
        scrollwheel: false,
        streetViewControl: false,
        autofit: {}
    };
    map = new google.maps.Map($('#{{ tab.widgets.gmapPinPointer.mapId }}')[0], mapOptions);

    /*
     var weatherLayer = new google.maps.weather.WeatherLayer({
     temperatureUnits: google.maps.weather.TemperatureUnit.CELSIUS
     });
     weatherLayer.setMap(map);

     var cloudLayer = new google.maps.weather.CloudLayer();
     cloudLayer.setMap(map);
     */

    /*
     var panoramioLayer = new google.maps.panoramio.PanoramioLayer();
     //panoramioLayer.setTag('gabbiano');
     //panoramioLayer.setTag('burano');
     panoramioLayer.setMap(map);


     //var photoPanel = document.getElementById('photo-panel');
     //map.controls[google.maps.ControlPosition.RIGHT_TOP].push(photoPanel);

     google.maps.event.addListener(panoramioLayer, 'click', function(photo) {
     var li = document.createElement('li');
     var link = document.createElement('a');
     link.innerHTML = photo.featureDetails.title + ': ' +
     photo.featureDetails.author;
     link.setAttribute('href', photo.featureDetails.url);
     li.appendChild(link);
     //photoPanel.appendChild(li);
     //photoPanel.style.display = 'block';
     });
     */

    /**
     * Global marker object that holds all markers.
     * @type {Object.<string, google.maps.LatLng>}
     */
    var markers = {};

    var draggedMarker = false;

    /**
     * Concatenates given lat and lng with an underscore and returns it.
     * This id will be used as a key of marker to cache the marker in markers object.
     * @param {!number} lat Latitude.
     * @param {!number} lng Longitude.
     * @return {string} Concatenated marker id.
     */
    var getMarkerUniqueId= function(lat, lng) {
        return lat + '_' + lng;
    }

    /**
     * Creates an instance of google.maps.LatLng by given lat and lng values and returns it.
     * This function can be useful for getting new coordinates quickly.
     * @param {!number} lat Latitude.
     * @param {!number} lng Longitude.
     * @return {google.maps.LatLng} An instance of google.maps.LatLng object
     */
    var getLatLng = function(lat, lng) {
        return new google.maps.LatLng(lat, lng);
    };

    /**
     * Binds click event to given map and invokes a callback that appends a new marker to clicked location.
     */
    var addMarker = google.maps.event.addListener(map, 'rightclick', function(e){
        var lat = e.latLng.lat(); // lat of clicked point
        var lng = e.latLng.lng(); // lng of clicked point

        $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker-id']").val('0');
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker .fieldbox.translation input").val('');
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker .fieldbox.translation textarea").val('');
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lat]']").val(lat);
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker em.lat").text(lat);
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lng]']").val(lng);
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker em.lng").text(lng);
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker").addClass('enabled');
    });

    /**
     * Binds right click event to given marker and invokes a callback function that will remove the marker from map.
     * @param {!google.maps.Marker} marker A google.maps.Marker instance that the handler will binded.
     */
    var bindMarkerEvents = function(marker) {

        google.maps.event.addListener(marker, 'click', function() {
            console.dir(marker.data);
            $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker-id']").val(marker.id);
            //$(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='markertitle']").val(marker.title);
            //$(".widget-gmap-pin-pointer.{{ key }} .add-marker textarea[name='marker-description']").val(marker.description.replace(/<br\s*[\/]?>/gi, ""));
            $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lat]']").val(marker.position.ob);
            $(".widget-gmap-pin-pointer.{{ key }} .add-marker em.lat").val(marker.position.ob);
            $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lng]']").val(marker.position.pb);
            $(".widget-gmap-pin-pointer.{{ key }} .add-marker em.lng").val(marker.position.pb);
            for(var translation in marker.data.translations){
                locale = marker.data.translations[translation].fkLanguage.id;
                label = marker.data.translations[translation].label;
                body = marker.data.translations[translation].body;
                //console.info(locale + label + body);
                $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[translations][" + locale + "][label]']").val(label);
                $(".widget-gmap-pin-pointer.{{ key }} .add-marker textarea[name='marker[translations][" + locale + "][body]']").val(body);
            }
            $(".widget-gmap-pin-pointer.{{ key }} .add-marker").addClass('enabled');
        });

        google.maps.event.addListener(marker, "rightclick", function (point){
            var markerId = getMarkerUniqueId(point.latLng.lat(), point.latLng.lng()); // get marker id by using clicked point's coordinate
            var marker = markers[markerId]; // find marker
            removeMarker(marker, markerId); // remove it

            updateMarkersToSubmit(markers);
        });

        google.maps.event.addListener(marker, "dragstart", function (point) {
            //console.log('start on ' + point.latLng.lat() + point.latLng.lng());
            var markerId = getMarkerUniqueId(point.latLng.lat(), point.latLng.lng()); // get marker id by using clicked point's coordinate
            draggedMarker = markerId;
        });

        google.maps.event.addListener(marker, "dragend", function (point) {
            var markerId = getMarkerUniqueId(point.latLng.lat(), point.latLng.lng()); // get marker id by using clicked point's coordinate
            markers[draggedMarker].id = markerId;
            markers[markerId] = markers[draggedMarker];
            delete markers[draggedMarker]; // delete marker instance from markers object

            updateMarkersToSubmit(markers);
        });
    };


    {% spaceless %}
    var storedMarkers = {{ invictusKernel.serviceProxy('widget.geo_point', 'getGeoPointsByModuleId', [52, 4, 'array'])|raw }};
{% endspaceless %}

var initMarkers = function(storedMarkers){
    console.log('initMarkers');

    if($.isEmptyObject(storedMarkers)){
        return true;
    }

    var bounds = new google.maps.LatLngBounds ();

    i = 0;
    for(var key in storedMarkers){

        title = getCorrectTitleTranslation(storedMarkers[key]);


        marker = addMarker(storedMarkers[key]);

        bounds.extend(marker.position);
        markers[markerId] = marker; // cache marker in markers object
        bindMarkerEvents(marker); // bind right click event to marker
        i++;
    }
    map.fitBounds(bounds);

    // Se c'è un solo marker setto lo zoom al default impostato in mapOptions
    // perchè fitBounds imposta la mappa allo zomm massimo per contenere tutti i marker
    if(i = 1){
        log("Setto lo zoom a " + mapOptions.zoom);
        map.setZoom(mapOptions.zoom);
    }
};

/**
 * Removes given marker from map.
 * @param {!google.maps.Marker} marker A google.maps.Marker instance that will be removed.
 * @param {!string} markerId Id of marker.
 */
var removeMarker = function(marker, markerId) {
    marker.setMap(null); // set markers setMap to null to remove it from map
    delete markers[markerId]; // delete marker instance from markers object
};

var updateMarkersToSubmit = function(markers) {
    console.log('updateMarkersForm ');
    $('.widget-gmap-pin-pointer.{{ key }} .geo-points-fields').empty();
    var j = 0;
    for(var key in markers){
        markerInputs = markerToInputsToSubmit(markers[key], j);
        $('.widget-gmap-pin-pointer.{{ key }} .geo-points-fields').append(markerInputs);
        j++;
    }
    console.log('- - - - - - - - - - - - - ');
};

$(".widget-gmap-pin-pointer.{{ key }} .add-marker").on("click", ".close", function(){
    $(".widget-gmap-pin-pointer.{{ key }} .add-marker").removeClass('enabled');
});

$('#cms-edit .marker-form .locale-selector').html($('#cms-edit .heading .locale-selector').html());

$('#cms-edit .marker-form .locale-selector').on('click', 'a', function(event)
{
    event.preventDefault();
    $this = $(this);
    locale = $this.attr('class');
    $('#cms-edit .marker-form .locale-selector a').parent().removeClass('sel');
    $this.parent().addClass('sel');
    $('#cms-edit .marker-form .fieldbox.translation').addClass('hidden');
    $('#cms-edit .marker-form .fieldbox.' + locale).removeClass('hidden');

});

$('#cms-edit .marker-form .locale-selector li.sel a').eq(0).trigger('click');

$(".widget-gmap-pin-pointer.{{ key }} .marker-form").addClass('sel');

$(".widget-gmap-pin-pointer.{{ key }} .gmap").css({background:'none'});

$(".widget-gmap-pin-pointer.{{ key }} .add-marker input[type='submit']").on("click", function(e){

    e.preventDefault();

    var markerData = addMarkerFormToObject();

    console.log('---------submit-----------');
    console.dir(markerData);
    console.log('---------submit-----------');

    id = $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker-id']").val();
    lat = $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lat]']").val();
    lng = $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lng]']").val();

    log('Submit: marker-id value:' + id);

    if(id != '0'){
        log("Modifica del marker con id" + id);
        var markerId =  id;
        //console.dir(markers);
        removeMarker(markers[id], id);
    }else{
        log("Nuovo marker");
        var markerId =  getMarkerUniqueId(lat, lng);
    }

    if(markers[markerId] != undefined){
        CMS.notify({message:'Esiste già un marker a questa latitudine (' + lat + ') e longitudine  (' + lng + ')'});
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker").removeClass('enabled');
        return false;
    }

    var marker = new google.maps.Marker({
        position: getLatLng(lat, lng),
        map: map,
        id: markerId,
        //title: getCorrectTitleTranslation(markerData),
        data: markerData,
        animation: google.maps.Animation.DROP,
        options: {
            draggable: true
        }
    });
    markers[markerId] = marker; // cache marker in markers object
    bindMarkerEvents(marker); // bind right click event to marker

    $(".widget-gmap-pin-pointer.{{ key }} .add-marker").removeClass('enabled');

    updateMarkersToSubmit(markers);
});


initMarkers(storedMarkers);
updateMarkersToSubmit(markers);

}

function clearAddMarkerForm()
{
    $(".widget-gmap-pin-pointer.{{ key }} input[name='marker[lat]']").val('');
    $(".widget-gmap-pin-pointer.{{ key }} input[name='marker[lng]']").val('');
    $(".widget-gmap-pin-pointer.{{ key }} .fieldbox.translation input").val('');
    $(".widget-gmap-pin-pointer.{{ key }} .fieldbox.translation textarea").val('');
}

function objectToAddMarkerForm(object)
{
    //object = {id: 100, lat: '45.123213', lng: '12.3456456', translations:[{label:'cippo label', body: 'cippo body', fkLanguage:{id:'it_IT'}}]};
    //if(object.lat == undefined){alert('niet');}
    $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lat]']").val(object.data.position.ob);
    $(".widget-gmap-pin-pointer.{{ key }} .add-marker em.lat").text(object.data.position.ob);
    $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[lng]']").val(object.data.position.pb);
    $(".widget-gmap-pin-pointer.{{ key }} .add-marker em.lng").text(object.data.position.pb);

    for(var translation in object.translations){
        locale = object.data.translations[translation].fkLanguage.id;
        label = object.data.translations[translation].label;
        body = object.data.translations[translation].body;
        //console.info(locale + label + body);
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker input[name='marker[translations][" + locale + "][label]']").val(label);
        $(".widget-gmap-pin-pointer.{{ key }} .add-marker textarea[name='marker[translations][" + locale + "][body]']").val(body);
    }
}

function addMarkerFormToObject()
{
    object = $(".widget-gmap-pin-pointer.{{ key }} .add-marker input, .widget-gmap-pin-pointer.{{ key }} .add-marker textarea").toJSON();

    console.log('---------addMarkerFormToObject-----------');
    console.dir(object);
    console.log('---------addMarkerFormToObject-----------');

    return object;
}

function markerToInputsToSubmit(marker, j)
{
    markerInputs = '';
    markerInputs = '<input type="hidden" name="geoPoints[{{ key }}][' + j + '][title]" value="' + marker.title + '" />';
    markerInputs += '<input type="hidden" name="geoPoints[{{ key }}][' + j + '][description]" value="' + marker.description + '" />';
    markerInputs += '<input type="hidden" name="geoPoints[{{ key }}][' + j + '][lat]" value="' + marker.position.ob + '" />';
    markerInputs += '<input type="hidden" name="geoPoints[{{ key }}][' + j + '][lng]" value="' + marker.position.pb + '" />';

    return markerInputs;
}

function getCorrectTitleTranslation(marker)
{
    console.log('---------getCorrectTitleTranslation-----------');
    console.dir(marker);
    console.log('---------getCorrectTitleTranslation-----------');

    var title = false;

    $.each(marker.translations, function(key, value){
        console.log(key + ": " );
    });

    /*
     for(var translation in marker.translations){
     console.warn('ooooooo');
     if(marker.translations[translation].fkLanguage.id == '{{ invictusKernel.UILanguageId }}'){
     title = marker.translations[translation].label;
     }
     }
     */
    if(!title){
        title = marker.translations[0].label;
    }

    return title;
}

function log(string){
    $('.map-logger').prepend(string + '<br><br>');
}

var addMarker = function(markerData){

    var markerId = getMarkerUniqueId(markerData.lat, markerData.lng);
    var marker = new google.maps.Marker({
        position: getLatLng(markerData.lat, markerData.lng),
        map: map,
        id: markerId,
        data: markerData,
        title: title,
        animation: google.maps.Animation.DROP,
        options: {
            draggable: true
        }
    });
}

//google.maps.event.addDomListenerOnce(document.getElementById('{{ tab.widgets.gmapPinPointer.mapId }}'), 'mouseover', POIGmapPinPointerInitialize);

