<?php

namespace Invictus\WidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * GeoPointTranslation
 *
 * @ORM\Table(name="geo_point_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_geo_point_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_geo_point_id", "fk_language_id"})
 * @ORM\Entity(repositoryClass="Invictus\WidgetBundle\Entity\GeoPointTranslationRepository")
 */
class GeoPointTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \GeoPoint
     *
     * @ORM\ManyToOne(targetEntity="GeoPoint", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_geo_point_id", referencedColumnName="id")
     * })
     */
    private $fkBase;


    /**
     * Set fkBase
     *
     * @param \Invictus\WidgetBundle\Entity\GeoPoint $fkBase
     * @return GeoPointTranslation
     */
    public function setFkBase(\Invictus\WidgetBundle\Entity\GeoPoint $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\WidgetBundle\Entity\GeoPoint 
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}