<?php

namespace Invictus\WidgetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * GeoPoint
 *
 * @ORM\Table(name="geo_point")
 * @ORM\Entity(repositoryClass="Invictus\WidgetBundle\Entity\GeoPointRepository")
 */
class GeoPoint extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", nullable=false)
     */
    private $lat = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", nullable=false)
     */
    private $lng = 0;

    /**
     * @ORM\OneToMany(targetEntity="GeoPointTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkGeoPointItem")
     */
    protected $visibilities;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkSchedaItem")
     */
    //protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkSchedaItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        //$this->metadatas = new ArrayCollection();
        //$this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->lat.'-'.$this->lng;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return GeoPoint
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    
        return $this;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     * @return GeoPoint
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    
        return $this;
    }

    /**
     * Get lng
     *
     * @return string 
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Add translations
     *
     * @param \Invictus\WidgetBundle\Entity\GeoPointTranslation $translations
     * @return GeoPoint
     */
    public function addTranslation(\Invictus\WidgetBundle\Entity\GeoPointTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\WidgetBundle\Entity\GeoPointTranslation $translations
     */
    public function removeTranslation(\Invictus\WidgetBundle\Entity\GeoPointTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return GeoPoint
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }
}