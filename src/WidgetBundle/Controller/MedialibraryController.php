<?php

namespace Invictus\WidgetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Invictus\CmsBundle\Controller\InvictusController;


class MedialibraryController extends InvictusController
{

    public function medialibraryAction($_locale, $moduleId, $itemId)
    {
        $attachments = $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:AttachmentTypology')
            //->setInvictusKernel($this->invictusKernel)
            ->getData(array(
                    //'appId' => '',
                    'languageId' => $_locale,
                    'moduleId' => 5,
                    'joins' => array(
                        'visibility' => false,
                        'position' => false,
                        'metadata' => false
                    ),
                    'orderBy' => array(
                        'translation.label ASC'
                    )
                )
            );

        //var_dump($attachments['DQL']);die();

        return $this->render(
            'InvictusWidgetBundle:Medialibrary:index.html.twig', array(
                'locale' => $_locale,
                'moduleId' => $moduleId,
                'itemId' => $itemId,
                'attachments' => $attachments['data']
            )
        );
    }


    public function getResultsAction(Request $request, $_locale)
    {
        $moduleId = $request->request->get('moduleId', false);
        $typeId =$request->request->get('typeId', false);
        $text = $request->request->get('text', false);

        $config = array(
            //'appId' => '',
            'languageId' => $_locale,
            'moduleId' => 6,
            'joins' => array(
                'visibility' => false,
                'position' => false,
                'metadata' => false
            ),
            'orderBy' => array(
                'base.fkModule ASC',
                'translation.label ASC'
            ),
            'params' => array(
                'base.enabled' => null
            )
        );

        if($typeId){
            $config['where']['base.fkAttachmentTypology'] = 'base.fkAttachmentTypology = :typeId';
            $config['params']['typeId'] = $typeId;
        }

        if($text){
            $config['where']['text'] = '(translation.label LIKE :text OR base.filename LIKE :text OR translation.link LIKE :text)';
            $config['params']['text'] = "%$text%";
        }

        $attachments = $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Attachment')
            //->setInvictusKernel($this->invictusKernel)
            ->getData($config);

        //var_dump($attachments);die();

        return $this->render(
            'InvictusWidgetBundle:Medialibrary:results.html.twig', array(
                'attachments' => $attachments['data']
            )
        );
    }


    public function connectAction(Request $request, $moduleId, $itemId)
    {
        $this->init();

        $attachments = $request->request->get('attachments', false);

        //$attachments = array(113, 112, 111, 110);

        $module = $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Module')
            ->find($moduleId);

        $moduleAttachment = $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Module')
            ->find(6);

        foreach($attachments as $attachmentId){

            $attachment = $this->getDoctrine()
                ->getRepository('InvictusCmsBundle:Attachment')
                ->find($attachmentId);

            $clonedAttachment = clone $attachment;
            $clonedAttachment->setFkModule($module);
            $clonedAttachment->setFkItemId($itemId);

            $this->em->persist($clonedAttachment);

            $attachmentTranslations = $this->getDoctrine()
                ->getRepository('InvictusCmsBundle:AttachmentTranslation')
                ->findByFkBase($attachmentId);

            //var_dump($attachmentTranslations);die();

            foreach($attachmentTranslations as $t){
                $clonedAttachmentTranslation = clone $t;
                $clonedAttachmentTranslation->setFkBase($clonedAttachment);
                $this->em->persist($clonedAttachmentTranslation);
                $clonedAttachment->addTranslation($clonedAttachmentTranslation);
            }

            $this->em->flush();

            $visibilities = $this->getDoctrine()
                ->getRepository('InvictusCmsBundle:Visibility')
                ->findBy(array('fkItemId' => $attachmentId, 'fkModule' => 6 ));

            foreach($visibilities as $v){

                $appId = $v->getFkApp()->getId();
                $languageId = $v->getFkLanguage()->getId();
                $moduleId = 6;
                $visibilityItemId = $clonedAttachment->getId();

                $sql = "
                    INSERT INTO visibility
                    (fk_app_id, fk_language_id, fk_module_id, fk_item_id)
                    VALUES ('$appId', '$languageId', $moduleId, $visibilityItemId)
                ";
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
                /*
                $clonedVisibilty = clone $v;
                $clonedVisibilty->setFkModule($moduleAttachment);
                $clonedVisibilty->setFkItemId($clonedAttachmentId);
                //var_dump($clonedVisibilty);die();
                $this->em->persist($clonedVisibilty);
                $clonedAttachment->addVisibility($clonedVisibilty);
                */
            }

        }

        $this->em->flush();

        $info = array(
            'error' => 0,
            'type' => 'success',
            'message' => 'Allegati associati correttamente'
        );

        return new JsonResponse($info);
    }

}
