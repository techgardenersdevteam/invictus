<?php

namespace Invictus\WidgetBundle\GeoPoint;

use Invictus\CmsBundle\Extensions\InvictusKernel;
use Invictus\WidgetBundle\Entity\GeoPoint;
use Invictus\WidgetBundle\Entity\GeoPointTranslation;

class GeoPointManager
{

    protected $invictusKernel;

    public function __construct(InvictusKernel $invictusKernel, $doctrine)
    {
        $this->invictusKernel = $invictusKernel;
        $this->doctrine = $doctrine;
        $this->em = $this->doctrine->getManager();
    }

    public function getGeoPointsByModuleId($moduleId, $itemId, $resultType)
    {
        $geoPoints = $this->doctrine
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->setInvictusKernel($this->invictusKernel)
            ->getGeoPointsByModuleId($moduleId, $itemId, $resultType);

        /*
        foreach($geoPoints as $geoPoint){
            echo $geoPoint->getId();
        }
        */

        //echo "<pre>";\Doctrine\Common\Util\Debug::dump($geoPoints);echo "/<pre>";

        $json = json_encode($geoPoints);

        return $json;
    }

    public function getOtherGeoPointsByModuleId($moduleId, $itemId, $resultType)
    {
    $geoPoints = $this->doctrine
        ->getRepository('InvictusWidgetBundle:GeoPoint')
        ->setInvictusKernel($this->invictusKernel)
        ->getOtherGeoPointsByModuleId($moduleId, $itemId, $resultType);

    /*
    foreach($geoPoints as $geoPoint){
        echo $geoPoint->getId();
    }
    */

    //echo "<pre>";\Doctrine\Common\Util\Debug::dump($geoPoints);echo "/<pre>";

    $json = json_encode($geoPoints);

    return $json;
}

    public function getAllGeoPoints($resultType)
    {
        $geoPoints = $this->doctrine
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->setInvictusKernel($this->invictusKernel)
            ->getAllGeoPoints($resultType);

        /*
        foreach($geoPoints as $geoPoint){
            echo $geoPoint->getId();
        }
        */

        //echo "<pre>";\Doctrine\Common\Util\Debug::dump($geoPoints);echo "/<pre>";

        $json = json_encode($geoPoints);

        return $json;
    }

    public function saveGeoPoints($itemId, $data)
    {

        $this->doctrine
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->deleteGeoPointsByModuleId($this->invictusKernel->moduleId, $itemId);

        // Potrei cancellare le relazione dei punti settati come deleted = 1 per fare pulizia in relation ma così facendo,
        // in caso volessi riattivare un punto eliminato, dovrei ricreare la relazione oltre a settare deleted = 0
        /*
        $this->doctrine
            ->getRepository('InvictusCmsBundle:Relation')
            ->deleteRelations(array(
                'fk_module_id_a' => $this->invictusKernel->moduleId,
                'fk_item_id_a' => $itemId,
                'fk_module_id_b' => 53,
                'slot' => 1
            ));
        */
        if($data){

            foreach($data as $widgetInstance){

                foreach($widgetInstance as $point){
                    $geoPoint = new GeoPoint();

                    $geoPoint->setLat($point['lat']);
                    $geoPoint->setLng($point['lng']);
                    $geoPoint->setEnabled(true);

                    foreach($point['translations'] as $key => $value){

                        $geoPointTranslation = new GeoPointTranslation();

                        $geoPointTranslation->setLabel($value['label']);
                        $geoPointTranslation->setBody($value['body']);
                        $language = $this->invictusKernel->getLanguage(array('id' => $key));
                        $geoPointTranslation->setFkLanguage($language);
                        $this->em->persist($geoPointTranslation);

                        $geoPoint->addTranslation($geoPointTranslation);
                    }

                    $this->em->persist($geoPoint);

                    $this->em->flush();

                    $this->doctrine
                        ->getRepository('InvictusCmsBundle:Relation')
                        ->addRelation(array(
                            'fk_module_id_a' => $this->invictusKernel->moduleId,
                            'fk_item_id_a' => $itemId,
                            'fk_module_id_b' => 53,
                            'fk_item_id_b' => $geoPoint->getId(),
                            'slot' => 1,
                            'position' => 0
                        ));
                }

            }
        }
    }

    public function savePathMarkers($itemId, $data)
    {
        $this->doctrine
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->deleteMarkersByPathId($itemId);

        $datasheets = explode(',', $data['datasheets']);

        foreach($datasheets as $datasheet){
            $relation = array();
            $relation['fk_module_id_a'] = 55;
            $relation['fk_item_id_a'] = $itemId;
            $relation['fk_module_id_b'] = 53;
            $relation['fk_item_id_b'] = $datasheet;
            $relation['slot'] = 2;
            $this->doctrine
                ->getRepository('InvictusCmsBundle:Relation')
                ->addRelation($relation);
        }

        $habitats = explode(',', $data['habitats']);

        foreach($habitats as $habitat){
            $relation = array();
            $relation['fk_module_id_a'] = 55;
            $relation['fk_item_id_a'] = $itemId;
            $relation['fk_module_id_b'] = 53;
            $relation['fk_item_id_b'] = $habitat;
            $relation['slot'] = 2;
            $this->doctrine
                ->getRepository('InvictusCmsBundle:Relation')
                ->addRelation($relation);
        }

        return true;
    }

    public function getPathMarkersById($_locale, $id){
        $geoPoints = $this->doctrine
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->setInvictusKernel($this->invictusKernel)
            ->getPathMarkersById($_locale, $id);

        /*
        foreach($geoPoints as $geoPoint){
            echo $geoPoint->getId();
        }
        */

        //echo "<pre>";\Doctrine\Common\Util\Debug::dump($geoPoints);echo "/<pre>";

        $json = json_encode($geoPoints);

        return $json;
    }

}
