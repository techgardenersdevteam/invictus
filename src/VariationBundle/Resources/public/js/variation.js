Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('code', {}, 'variation'),
    Translator.trans('label'),
    Translator.trans('category'),
    Translator.trans('price', {}, 'variation'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:80, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:45, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'code', index:'base.code', width:100, fixed: true, sortable: true, search: true, align: 'left'},
    {name:'label', index:'translation.label', width:300, search:true, align: 'left'},
    {name:'category', index:'category_translation.label', width:150, sortable: false, search: false, align: 'left'},
    {name:'price', index:'base.price', width:150, fixed: true, sortable: true, search: true, align: 'left'},
    {name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'news_label': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'code', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'category', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'price', '', {'text-align':'left'});
    }
});