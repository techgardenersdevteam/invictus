<?php
  
namespace Invictus\VariationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VariationTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'title',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('selectText', 'text', array(
                'label' => 'selectText',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'variation'
            )
        );

        $builder->add('descriptionText', 'text', array(
                'label' => 'descriptionText',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'variation'
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'content',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                )
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\VariationBundle\Entity\Variation'
            )
        );

    }

    public function getName()
    {
        return 'variation_translation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\VariationBundle\Entity\VariationTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
