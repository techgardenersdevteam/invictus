<?php

namespace Invictus\VariationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class VariationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('fkCategory', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Category'
            )
        );

        $builder->add('code', 'text', array(
                'label' => 'code',
                'required' => true,
                'attr'   =>  array(
                ),
                'translation_domain' => 'variation'
            )
        );

        $builder->add('price', 'text', array(
                'label' => 'price',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'variation'
            )
        );

        $builder->add('discount', 'text', array(
                'label' => 'discount',
                'required' => false,
                'attr'   =>  array(
                ),
                'label_attr'   =>  array(
                    'raw' => true
                ),
                'translation_domain' => 'variation'
            )
        );

        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new VariationTranslationType() ));

        //$builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'variation';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\VariationBundle\Entity\Variation',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
