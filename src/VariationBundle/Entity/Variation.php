<?php

namespace Invictus\VariationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Variation
 *
 * @ORM\Table(name="variation")
 * @ORM\Entity(repositoryClass="Invictus\VariationBundle\Entity\VariationRepository")
 */
class Variation extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category_id", referencedColumnName="id")
     * })
     */
    private $fkCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", scale=2, length=255, nullable=true)
     */
    private $price = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="integer", length=255, nullable=true)
     */
    private $discount = 0;

    /**
     * @ORM\OneToMany(targetEntity="VariationTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkVariationItem")
     */
    //protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Position", mappedBy="fkVariationItem")
     */
    protected $positions;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkVariationItem")
     */
    //protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkVariationItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->metadatas = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->tag;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add translation
     *
     * @param \Invictus\VariationBundle\Entity\VariationTranslation $translation
     * @return Variation
     */
    public function addTranslation(\Invictus\VariationBundle\Entity\VariationTranslation $translation)
    {
        $translation->setFkBase($this);
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \Invictus\VariationBundle\Entity\VariationTranslation $translation
     */
    public function removeTranslation(\Invictus\VariationBundle\Entity\VariationTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }


    /**
     * Add visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     * @return Variation
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility[] = $visibility;

        return $this;
    }

    /**
     * Remove visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility->removeElement($visibility);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     * @return Variation
     */
    public function addAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    
        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     * @return Variation
     */
    public function addMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $metadata->setFkItemId($this->getId());
        $this->metadatas[] = $metadata;
    
        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     */
    public function removeMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $this->metadatas->removeElement($metadata);
    }

    /**
     * Get metadatas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Variation
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Variation
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Variation
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get colorCode
     *
     * @return string
     */
    public function getColorCode()
    {
        return substr($this->code, 0, 2);
    }

    /**
     * Get modelCode
     *
     * @return string
     */
    public function getModelCode()
    {
        return substr($this->code, 2, 2);
    }

    /**
     * Get modelCode
     *
     * @return string
     */
    public function getModelType()
    {
        return substr($this->code, 2, 2) == 'BA' ? 'low' : 'high';
    }

    /**
     * Set discount
     *
     * @param string $discount
     * @return Variation
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return string 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Variation
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get salePrice
     *
     * @return string
     */
    public function getSalePrice()
    {
         $salePrice = ($this->price/100)*(100-$this->discount);

        return number_format((float)$salePrice, 2, '.', '');
    }



    /**
     * Set fkCategory
     *
     * @param \Invictus\CmsBundle\Entity\Category $fkCategory
     * @return Variation
     */
    public function setFkCategory(\Invictus\CmsBundle\Entity\Category $fkCategory = null)
    {
        $this->fkCategory = $fkCategory;
    
        return $this;
    }

    /**
     * Get fkCategory
     *
     * @return \Invictus\CmsBundle\Entity\Category 
     */
    public function getFkCategory()
    {
        return $this->fkCategory;
    }
}