<?php

namespace Invictus\VariationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * VariationTranslation
 *
 * @ORM\Table(name="variation_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_variation_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_variation_id", "fk_language_id"})
 * @ORM\Entity()
 */
class VariationTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Variation
     *
     * @ORM\ManyToOne(targetEntity="Variation", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_variation_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionText", type="string", length=255, nullable=false)
     */
    private $descriptionText;

    /**
     * @var string
     *
     * @ORM\Column(name="selectText", type="string", length=255, nullable=false)
     */
    private $selectText;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\VariationBundle\Entity\Variation $fkBase
     * @return VariationTranslation
     */
    public function setFkBase(\Invictus\VariationBundle\Entity\Variation $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\VariationBundle\Entity\Variation
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Set descriptionText
     *
     * @param string $descriptionText
     * @return VariationTranslation
     */
    public function setDescriptionText($descriptionText)
    {
        $this->descriptionText = $descriptionText;
    
        return $this;
    }

    /**
     * Get descriptionText
     *
     * @return string 
     */
    public function getDescriptionText()
    {
        return $this->descriptionText;
    }

    /**
     * Set selectText
     *
     * @param string $selectText
     * @return VariationTranslation
     */
    public function setSelectText($selectText)
    {
        $this->selectText = $selectText;
    
        return $this;
    }

    /**
     * Get selectText
     *
     * @return string 
     */
    public function getSelectText()
    {
        return $this->selectText;
    }
}