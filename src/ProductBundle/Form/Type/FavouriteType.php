<?php

namespace Invictus\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;
use Doctrine\ORM\EntityRepository;

class FavouriteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('fkShoe', 'entity' , array(
            'class' => 'InvictusProductBundle:Product',
            'property' => 'translations[0].label',
            'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('base')
                        ->select('base, translation')
                        ->innerJoin('base.translations', 'translation', 'WITH', 'translation.fkLanguage = :locale')
                        ->innerJoin('translation.fkLanguage', 'lang', 'WITH', 'lang.id = :locale')
                        ->andWhere('base.enabled = 1')
                        ->andWhere('base.deleted = 0')
                        ->andWhere('base.fkCategory = :category')
                        ->orderBy('base.id', 'ASC')
                        ->setParameter('locale', 'en_GB')
                        ->setParameter('category', 21);
                    return $qb;
                },
            'empty_value' => 'chooseShoe',
            'label' => 'shoe',
            'translation_domain' => 'favourite',
            'required' => true,
            'attr' =>  array(
                'class' => 'select2',
                )
            )
        );

        $builder->add('fkCover', 'entity' , array(
                'class' => 'InvictusProductBundle:Product',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) {
                        $categories = array(22,23,24,25,26);
                        $qb = $er->createQueryBuilder('base')
                            ->select('base, translation')
                            ->innerJoin('base.translations', 'translation', 'WITH', 'translation.fkLanguage = :locale')
                            ->innerJoin('translation.fkLanguage', 'lang', 'WITH', 'lang.id = :locale')
                            ->andWhere('base.enabled = 1')
                            ->andWhere('base.deleted = 0')
                            ->andWhere('base.fkCategory IN (:categories)')
                            ->addOrderBy('base.fkCategory', 'ASC')
                            ->addOrderBy('base.id', 'ASC')
                            ->setParameter('locale', 'en_GB')
                            ->setParameter('categories', $categories);
                        return $qb;
                    },
                'empty_value' => 'chooseCover',
                'label' => 'cover',
                'translation_domain' => 'favourite',
                'required' => true,
                'attr' =>  array(
                    'class' => 'select2',
                )
            )
        );

        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new FavouriteTranslationType() ));

    }

    public function getName()
    {
        return 'favourite';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\ProductBundle\Entity\Favourite',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
