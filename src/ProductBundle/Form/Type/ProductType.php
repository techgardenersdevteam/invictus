<?php

namespace Invictus\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;
use Doctrine\ORM\EntityRepository;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('fkModel', 'entity' , array(
            'class' => 'InvictusVariationBundle:Variation',
            'property' => 'translations[0].label',
            'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('base')
                        ->select('base, translation')
                        ->innerJoin('base.translations', 'translation', 'WITH', 'translation.fkLanguage = :locale')
                        ->innerJoin('translation.fkLanguage', 'lang', 'WITH', 'lang.id = :locale')
                        ->andWhere('base.enabled = 1')
                        ->andWhere('base.deleted = 0')
                        ->andWhere('base.fkCategory = :category')
                        ->orderBy('base.id', 'ASC')
                        ->setParameter('locale', 'en_GB')
                        ->setParameter('category', 17);
                    return $qb;
                },
            'empty_value' => 'chooseModel',
            'label' => 'model',
            'translation_domain' => 'product',
            'required' => false,
            'attr' =>  array(
                'class' => 'select2',
                )
            )
        );

        $builder->add('fkSole', 'entity' , array(
                'class' => 'InvictusVariationBundle:Variation',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) {
                        $qb = $er->createQueryBuilder('base')
                            ->select('base, translation')
                            ->innerJoin('base.translations', 'translation', 'WITH', 'translation.fkLanguage = :locale')
                            ->innerJoin('translation.fkLanguage', 'lang', 'WITH', 'lang.id = :locale')
                            ->andWhere('base.enabled = 1')
                            ->andWhere('base.deleted = 0')
                            ->andWhere('base.fkCategory = :category')
                            ->orderBy('base.id', 'ASC')
                            ->setParameter('locale', 'en_GB')
                            ->setParameter('category', 19);
                        return $qb;
                    },
                'empty_value' => 'chooseSole',
                'label' => 'sole',
                'translation_domain' => 'product',
                'required' => false,
                'attr' =>  array(
                    'class' => 'select2',
                )
            )
        );

        $builder->add('code', 'text', array(
                'label' => 'code',
                'required' => true,
                'attr'   =>  array(
                ),
                'translation_domain' => 'variation'
            )
        );

        $builder->add('price', 'text', array(
                'label' => 'price',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'variation'
            )
        );

        $builder->add('discount', 'text', array(
                'label' => 'discount',
                'required' => false,
                'attr'   =>  array(
                ),
                'label_attr'   =>  array(
                    'raw' => true
                ),
                'translation_domain' => 'variation'
            )
        );
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkCategory', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Category'
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new ProductTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'product';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\ProductBundle\Entity\Product',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
