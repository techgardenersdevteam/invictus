Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('code', {}, 'product'),
    Translator.trans('label'),
    Translator.trans('category'),
    Translator.trans('price', {}, 'product'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:80, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:45, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'code', index:'base.code', width:100, fixed: true, sortable: true, search: true, align: 'left'},
    {name:'label', index:'translation.label', width:300, search:true, align: 'left'},
    {name:'category', index:'category_translation.label', width:150, sortable: false, search: false, align: 'left'},
    {name:'price', index:'base.price', width:150, fixed: true, sortable: true, search: true, align: 'left'},
    {name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'news_label': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'});
    }
});


$(document).bind({
    /* 'loadFormSuccess' : setVariationsVisibility, */ // Per qualche ragione non va, ho dovuto ripetere il codice in setCategoryChange
    'loadFormSuccess' : setCategoryChange
});


function setVariationsVisibility(){
    var category = $('#product_fkCategory option:selected').val();
    console.info('cat:' + category);
    if(category == 21){
        $('#product_fkModel').parent().css({display:'block'});
        $('#product_fkSole').parent().css({display:'block'});
        $( "input[id*='descriptionText']" ).parent().css({display:'none'});
        $('#product_code').prop('disabled', true);
    }else{
        $('#product_fkModel').parent().css({display:'none'});
        $('#product_fkSole').parent().css({display:'none'});
        $( "input[id*='descriptionText']" ).parent().css({display:'block'});
        $('#product_code').prop('disabled', false);
    }
}

function setCategoryChange(){
    $('#product_fkCategory').on('change', function(){
        console.log('Change');
        setVariationsVisibility();
    });

    var category = $('#product_fkCategory option:selected').val();
    console.info('cat:' + category);
    if(category == 21){
        $('#product_fkModel').parent().css({display:'block'});
        $('#product_fkSole').parent().css({display:'block'});
        $('#product_code').prop('disabled', true);
        $( "input[id*='descriptionText']" ).parent().css({display:'none'});
    }else{
        $('#product_fkModel').parent().css({display:'none'});
        $('#product_fkSole').parent().css({display:'none'});
        $('#product_code').prop('disabled', false);
        $( "input[id*='descriptionText']" ).parent().css({display:'block'});
    }

}