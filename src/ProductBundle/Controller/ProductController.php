<?php

namespace Invictus\ProductBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Invictus\CmsBundle\Controller\InvictusController;


class ProductController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        $config['orderBy'] = array(
            'base.fkCategory' => 'base.fkCategory ASC',
            'position.position' => 'position.position ASC'
        );
        $config['joins'] = array(
            'position' => true
        );
        $this->tableManager->addConfig($config);

        //$this->tableManager->disableAction('attach');
        $this->tableManager->enablePredefinedToggle('enabled');
    }

    protected function preInsertItem($item)
    {
        if($item->getFkCategory()->getId() == 21){
            $code = $item->getFkModel()->getCode().$item->getFkSole()->getCode();
            $item->setCode($code);
        }

        return $item;
    }
    protected function preUpdateItem($item)
    {
        if($item->getFkCategory()->getId() == 21){
            $code = $item->getFkModel()->getCode().$item->getFkSole()->getCode();
            $item->setCode($code);
        }

        return $item;
    }

}
