<?php

namespace Invictus\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Favourite
 *
 * @ORM\Table(name="favourite")
 * @ORM\Entity(repositoryClass="Invictus\ProductBundle\Entity\FavouriteRepository")
 */
class Favourite extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Invictus\ProductBundle\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_shoe_id", referencedColumnName="id")
     * })
     */
    private $fkShoe;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Invictus\ProductBundle\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_cover_id", referencedColumnName="id")
     * })
     */
    private $fkCover;

    /**
     * @ORM\OneToMany(targetEntity="FavouriteTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Position", mappedBy="fkFavouriteItem")
     */
    protected $positions;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkFavouriteItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->positions = new ArrayCollection();
        //$this->attachments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkShoe
     *
     * @param \Invictus\ProductBundle\Entity\Product $fkShoe
     * @return Favourite
     */
    public function setFkShoe(\Invictus\ProductBundle\Entity\Product $fkShoe = null)
    {
        $this->fkShoe = $fkShoe;
    
        return $this;
    }

    /**
     * Get fkShoe
     *
     * @return \Invictus\ProductBundle\Entity\Product 
     */
    public function getFkShoe()
    {
        return $this->fkShoe;
    }

    /**
     * Set fkCover
     *
     * @param \Invictus\ProductBundle\Entity\Product $fkCover
     * @return Favourite
     */
    public function setFkCover(\Invictus\ProductBundle\Entity\Product $fkCover = null)
    {
        $this->fkCover = $fkCover;
    
        return $this;
    }

    /**
     * Get fkCover
     *
     * @return \Invictus\ProductBundle\Entity\Product 
     */
    public function getFkCover()
    {
        return $this->fkCover;
    }

    /**
     * Add translations
     *
     * @param \Invictus\ProductBundle\Entity\FavouriteTranslation $translations
     * @return Favourite
     */
    public function addTranslation(\Invictus\ProductBundle\Entity\FavouriteTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\ProductBundle\Entity\FavouriteTranslation $translations
     */
    public function removeTranslation(\Invictus\ProductBundle\Entity\FavouriteTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Favourite
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }
}