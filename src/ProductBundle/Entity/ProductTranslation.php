<?php

namespace Invictus\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ProductTranslation
 *
 * @ORM\Table(name="product_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_product_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_product_id", "fk_language_id"})
 * @ORM\Entity()
 */
class ProductTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_product_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionText", type="string", length=255, nullable=true)
     */
    private $descriptionText;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\ProductBundle\Entity\Product $fkBase
     * @return ProductTranslation
     */
    public function setFkBase(\Invictus\ProductBundle\Entity\Product $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\ProductBundle\Entity\Product
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Set descriptionText
     *
     * @param string $descriptionText
     * @return VariationTranslation
     */
    public function setDescriptionText($descriptionText)
    {
        $this->descriptionText = $descriptionText;

        return $this;
    }

    /**
     * Get descriptionText
     *
     * @return string
     */
    public function getDescriptionText()
    {
        return $this->descriptionText;
    }

}