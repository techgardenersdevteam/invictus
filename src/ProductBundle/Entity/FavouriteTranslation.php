<?php

namespace Invictus\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * FavouriteTranslation
 *
 * @ORM\Table(name="favourite_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_favourite_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_favourite_id", "fk_language_id"})
 * @ORM\Entity()
 */
class FavouriteTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Favourite
     *
     * @ORM\ManyToOne(targetEntity="Favourite", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_favourite_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\ProductBundle\Entity\Favourite $fkBase
     * @return FavouriteTranslation
     */
    public function setFkBase(\Invictus\ProductBundle\Entity\Favourite $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\ProductBundle\Entity\Favourite 
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}