<?php

namespace Invictus\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="Invictus\ProductBundle\Entity\ProductRepository")
 */
class Product extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category_id", referencedColumnName="id")
     * })
     */
    private $fkCategory;

    /**
     * @var \Variation
     *
     * @ORM\ManyToOne(targetEntity="Invictus\VariationBundle\Entity\Variation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_model_id", referencedColumnName="id")
     * })
     */
    private $fkModel;

    /**
     * @var \Variation
     *
     * @ORM\ManyToOne(targetEntity="Invictus\VariationBundle\Entity\Variation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_sole_id", referencedColumnName="id")
     * })
     */
    private $fkSole;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", scale=2, length=255, nullable=true)
     */
    private $price = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="integer", length=255, nullable=true)
     */
    private $discount = 0;

    /**
     * @ORM\OneToMany(targetEntity="ProductTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkProductItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Position", mappedBy="fkProductItem")
     */
    protected $positions;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkProductItem")
     */
    protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkProductItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->metadatas = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->tag;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add translation
     *
     * @param \Invictus\ProductBundle\Entity\ProductTranslation $translation
     * @return Product
     */
    public function addTranslation(\Invictus\ProductBundle\Entity\ProductTranslation $translation)
    {
        $translation->setFkBase($this);
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \Invictus\ProductBundle\Entity\ProductTranslation $translation
     */
    public function removeTranslation(\Invictus\ProductBundle\Entity\ProductTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }


    /**
     * Add visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     * @return Product
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility[] = $visibility;

        return $this;
    }

    /**
     * Remove visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility->removeElement($visibility);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     * @return Product
     */
    public function addAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    
        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     * @return Product
     */
    public function addMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $metadata->setFkItemId($this->getId());
        $this->metadatas[] = $metadata;
    
        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     */
    public function removeMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $this->metadatas->removeElement($metadata);
    }

    /**
     * Get metadatas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Product
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Product
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Product
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get colorCode
     *
     * @return string
     */
    public function getColorCode()
    {
        return substr($this->code, 0, 2);
    }

    /**
     * Get modelCode
     *
     * @return string
     */
    public function getSoleCode()
    {
        return substr($this->code, 4, 2);
    }

    /**
     * Get modelCode
     *
     * @return string
     */
    public function getModelCode()
    {
        return substr($this->code, 2, 2);
    }

    /**
     * Get modelCode
     *
     * @return string
     */
    public function getModelType()
    {
        return substr($this->code, 2, 2) == 'BA' ? 'low' : 'high';
    }



    /**
     * Set price
     *
     * @param string $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Product
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Get salePrice
     *
     * @return string
     */
    public function getSalePrice()
    {
        $salePrice = ($this->price/100)*(100-$this->discount);

        return number_format((float)$salePrice, 2, '.', '');
    }

    /**
     * Set fkCategory
     *
     * @param \Invictus\CmsBundle\Entity\Category $fkCategory
     * @return Product
     */
    public function setFkCategory(\Invictus\CmsBundle\Entity\Category $fkCategory = null)
    {
        $this->fkCategory = $fkCategory;
    
        return $this;
    }

    /**
     * Get fkCategory
     *
     * @return \Invictus\CmsBundle\Entity\Category 
     */
    public function getFkCategory()
    {
        return $this->fkCategory;
    }

    /**
     * Set fkModel
     *
     * @param \Invictus\VariationBundle\Entity\Variation $fkModel
     * @return Product
     */
    public function setFkModel(\Invictus\VariationBundle\Entity\Variation $fkModel = null)
    {
        $this->fkModel = $fkModel;
    
        return $this;
    }

    /**
     * Get fkModel
     *
     * @return \Invictus\VariationBundle\Entity\Variation 
     */
    public function getFkModel()
    {
        return $this->fkModel;
    }

    /**
     * Set fkSole
     *
     * @param \Invictus\VariationBundle\Entity\Variation $fkSole
     * @return Product
     */
    public function setFkSole(\Invictus\VariationBundle\Entity\Variation $fkSole = null)
    {
        $this->fkSole = $fkSole;
    
        return $this;
    }

    /**
     * Get fkSole
     *
     * @return \Invictus\VariationBundle\Entity\Variation 
     */
    public function getFkSole()
    {
        return $this->fkSole;
    }
}