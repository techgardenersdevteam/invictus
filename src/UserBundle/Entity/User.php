<?php 

namespace Invictus\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use N2ktag\WebappBundle\Entity\User as WebappUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Invictus\UserBundle\Entity\UserRepository")
 */
class User extends WebappUser
{

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $fkApp = null;

    /**
     * Get fkApp
     *
     * @return integer
     */
    public function getFkApp()
    {
        return $this->fkApp;
    }

    /**
     * Set fkApp
     *
     * @return integer
     */
    public function setFkApp($fkApp)
    {
        return $this->fkApp = $fkApp;
    }
}