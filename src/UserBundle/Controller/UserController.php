<?php

namespace Invictus\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Response;

class UserController extends InvictusController
{

    public function tableListAjaxAction()
    {

        $this->init();

        //var_dump($users);die();

        if(method_exists($this, 'configureTableManager')) {
            $this->configureTableManager();
        }

        $currentPage = $this->invictusKernel->getGETParam('page', 1);
        $pageSize = $this->invictusKernel->getGETParam('rows', 10);

        $sidx = $this->invictusKernel->getGETParam('sidx', false); // campo di ordinamento
        $sord = $this->invictusKernel->getGETParam('sord', false); // tipo di ordinamento (asc|desc)

        if($sidx){
        }

        $sql = "SELECT
                    fos_user.*

                    FROM fos_user
                    ";
        $stmt = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare($sql);
        $stmt->execute();
        $records = count($stmt->fetchAll());

        $start = ($currentPage - 1) * $pageSize;
        $sql = "SELECT
                    fos_user.*

                    FROM fos_user

                    LIMIT $start, $pageSize
                    ";
        $stmt = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare($sql);

        $stmt->execute();
        $result = array();
        $result['data'] = $stmt->fetchAll();
        $result['info']['records'] = $records;
        $result['info']['pages'] = ceil($records/$pageSize);

        $response = new Response();
        $response->headers->set('Content-type', 'application/json; charset=utf-8');
        return $this->render($this->invictusKernel->getCurrentModule()->getBundleView().':table-list-ajax.json.twig',
            array(
                'data' => $result['data'],
                'page' => $currentPage,
                'total' => $result['info']['pages'],
                'records' => $result['info']['records']
            ),
            $response
        );
    }

    /*
    protected function configureTableList(){

        $this->fields[] = 'user.username';
        $this->sorts[] = 'asc';

        if($this->invictusKernel->getGETParam('id')){
            $this->where[] = ' AND id LIKE :id';
            $this->parameters['id'] = $this->request->query->get('id');
        }

        if($this->invictusKernel->getGETParam('label')){
            $this->where[] = ' AND label LIKE :label';
            $this->parameters['label'] = '%'.$this->request->query->get('label').'%';
        }

        //$this->where['base.delete'] = null;
        //$this->parameters['deleted'] = '0';

        $this->tableListConfig['bools'] = array(
            array(
                'table' => 'user',
                'field' => 'is_active',
                'icons' => array(
                    0 => 'icomoon16-minus-3',
                    1 => 'icomoon16-checkmark-3'
                ),
                'translation' => array(
                    0 => 'disabled',
                    1 => 'enabled'
                ),
                'css' => array(
                    0 => 'disabled',
                    1 => 'enabled'
                )
            )
        );

        $this->tableListConfig['actions'] = array(
            'attachments' => false,
            'edit' => true,
            'delete' => false
        );
    }


    protected function preInsertItem($item){

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($item);
        $hashedPassword = $encoder->encodePassword($item->getPassword(), $item->getSalt());
        $item->setPassword($hashedPassword);
        return $item;
    }


    protected function preUpdateItem($item){
        // prendo la password da $_POST.
        // Se usassi $item->getPassword() riceverei la vecchia password già hashata
        $password = $this->invictusKernel->getPOSTParam('user')['password'];
        if(!empty($password)){
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($item);
            $hashedPassword = $encoder->encodePassword($password, $item->getSalt());
            $item->setPassword($hashedPassword);
        }
        return $item;
    }
    */

}
