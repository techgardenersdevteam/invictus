<?php

namespace Invictus\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('isActive', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('firstName', 'text', array(
                'translation_domain' => 'admin',
                'label' => 'firstName',
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('lastName', 'text', array(
                'label' => 'lastName',
                'translation_domain' => 'admin',
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('email', 'email', array(
                'label' => 'email',
                'attr'   =>  array(
                )
            )
        );

        $builder->add('username', 'text', array(
                'label' => 'username',
                'attr'   =>  array(
                )
            )
        );

        $builder->add('password', 'password', array(
                'label' => 'password',
                'required' => (in_array('update', $options['validation_groups'])) ? false : true,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'attr'   =>  array(
                )
            )
        );

    }

    public function getName()
    {
        return 'user';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\UserBundle\Entity\User',
            'validation_groups' => array(),
            'invictusKernel' => null
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('')
        );
    }

}