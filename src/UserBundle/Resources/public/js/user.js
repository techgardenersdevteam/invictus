Table.jqGridOptions.colNames =[
        //Translator.trans('actions'),
        Translator.trans('id'),
        Translator.trans('name'),
        Translator.trans('username'),
        Translator.trans('email'),
        Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
        //{name:'actions', index:'actions', fixed: true, width:70, sortable: false, search: false, align: 'center'},
        {name:'id', index:'admin.id', fixed: true, width:80, sortable: true, search: true, align: 'center'},
        {name:'name', index:'admin.name', width:20, sortable: false, search: true, align: 'left'},
        {name:'username', index:'admin.username', width:20, sortable: true, search: true, align: 'left'},
        {name:'email', index:'admin.email', width:20, sortable: true, search: true, align: 'left'},
        {name:'enabled', index:'admin.is_active', fixed: true, width:100, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'admin_email': "Translation.validation.required",
            'admin_username': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            //.jqGrid ('setLabel', 'actions', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'lastName', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'firstName', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'email', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'name', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'username', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});