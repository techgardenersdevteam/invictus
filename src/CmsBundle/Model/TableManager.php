<?php

namespace Invictus\CmsBundle\Model;

use Invictus\CmsBundle\Extensions\InvictusKernel;

class TableManager
{

    protected $contentManager;
    protected $totalItems = 0;
    protected $totalPages = 1;
    protected $predefinedToggles = array(
        'highlighted' => array(
            'field' => 'highlighted',
            'icons' => array(
                0 => 'icomoon16-star',
                1 => 'icomoon16-star-3'
            ),
            'translation' => array(
                0 => 'notHighlighted',
                1 => 'highlighted'
            ),
            'css' => array(
                0 => 'highlighted',
                1 => 'unhighlighted'
            ),
            'unique' => true
        ),
        'enabled' => array(
            'field' => 'enabled',
            'icons' => array(
                0 => 'icomoon16-minus-3',
                1 => 'icomoon16-checkmark-3'
            ),
            'translation' => array(
                0 => 'notPublished',
                1 => 'published'
            ),
            'css' => array(
                0 => 'disabled',
                1 => 'enabled'
            )
        )
    );
    protected $toggles = array();
    protected $actions = array(
        'attach' => true,
        'edit' => true,
        'delete' => true
    );
    protected $config = array();
    public $debug = false;


    public function __construct(ContentManager $contentManager)
    {
        $this->contentManager = $contentManager;
    }


    public function initBE(){
        // nelle tabelle del CMS i filtri su abilitazione e visibilità vanno sempre esclusi
        $config = array(
            'joins' => array(
                'visibility' => false,
                'metadata' => array(
                    'type' => 'LEFT'
                )
            ),
            'where' => array(
                'base.enabled' => null
            ),
            'params' => array(
                'enabled' => null
            )
        );
        $this->addConfig($config);
    }


    public function setCurrentPage($currentPage = 1)
    {
        $config = array(
            'paginator' => array(
                'currentPage' => $currentPage
            )
        );
        $this->config = array_replace_recursive($this->config, $config);

        return $currentPage;
    }


    public function setPageSize($pageSize = 10){
        $config = array(
            'paginator' => array(
                'pageSize' => $pageSize
            )
        );
        $this->config = array_replace_recursive($this->config, $config);

        return $pageSize;
    }


    public function enableAction($actionName)
    {
        $this->actions[$actionName] = true;
    }


    public function disableAction($actionName)
    {
        $this->actions[$actionName] = false;
    }


    public function addToggle($toggle)
    {
        /* toggle di esempio
         array(
            'table' => 'project',
            'field' => 'enabled',
            'icons' => array(
                0 => 'icomoon16-minus-3',
                1 => 'icomoon16-checkmark-3'
            ),
            'translation' => array(
                0 => 'notPublished',
                1 => 'published'
            ),
            'css' => array(
                0 => 'disabled',
                1 => 'enabled'
            )
        )
        */
        $this->toggles[] = $toggle;
    }


    public function removeToggle($toggleName)
    {
        unset($this->toggles[$toggleName]);
    }


    public function enablePredefinedToggle($toggleName)
    {
        $this->addToggle($this->predefinedToggles[$toggleName]);
    }


    public function addConfig($config)
    {
        $this->config = array_replace_recursive($this->config, $config);
    }


    public function removeConfig($configName)
    {
        unset($this->config[$configName]);
    }


    public function getConfig()
    {
        return $this->config;
    }


    public function fetchItems()
    {
        return $this->contentManager->getData($this->config, $this->debug);
    }


    public function getJqGridArray()
    {
        $result = $this->contentManager->getData($this->config, $this->debug);

        $data['page'] = $result['info']['currentPage'];
        $data['records'] = $result['info']['totalItems'];
        $data['total'] = $result['info']['totalPages'];
        $data['rows'] = $result['data'];
        $data['actions'] = $this->actions;
        $data['toggles'] = $this->toggles;

        return $data;
    }

}
