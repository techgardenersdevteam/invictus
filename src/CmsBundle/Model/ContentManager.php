<?php

namespace Invictus\CmsBundle\Model;

use Invictus\CmsBundle\Extensions\InvictusKernel;
use Invictus\CmsBundle\Entity\Log;

class ContentManager
{

    protected $invictusKernel;


    public function __construct(InvictusKernel $invictusKernel, $doctrine)
    {
        $this->invictusKernel = $invictusKernel;
        $this->app = $this->invictusKernel->getCurrentApp();
        $this->language = $this->invictusKernel->getCurrentLanguage();
        $this->module = $this->invictusKernel->getCurrentModule();

        $this->doctrine = $doctrine;
        $this->em = $this->doctrine->getManager();
    }


    /**
     * @param $itemId
     * @param array $config
     *
     * $config = array(
     *      'appId' => 'vs',
     *      'languageId' => 'it_IT',
     *      'moduleId' => 11
     * );
     */
    public function getData($config = array(), $debug = false)
    {
        $data = $this->em
            ->getRepository($this->module->getBundleEntity())
            ->setInvictusKernel($this->invictusKernel)
            ->getData($config, $debug);
        return $data;
    }


    public function getCurrentItem($config = array(), $debug = false)
    {
        $data = $this->em
            ->getRepository($this->module->getBundleEntity())
            ->setInvictusKernel($this->invictusKernel)
            ->getCurrentItem($config, $debug);

        return $data;
    }


    public function getAttachments($config = array(), $debug = false)
    {

        $data = $this->em
            ->getRepository($this->module->getBundleEntity())
            ->setInvictusKernel($this->invictusKernel)
            ->getAttachments($config, $debug);

        return $data;
    }


    public function getRelatedIdsFromConfig($config = array(), $debug = false)
    {
        $data = $this->em
            ->getRepository('InvictusCmsBundle:Relation')
            ->setInvictusKernel($this->invictusKernel)
            ->getRelatedIdsFromConfig($config, $debug);

        return $data;
    }


    public function getRelatedElementsFromConfig($config = array(), $debug = false)
    {
        $data = $this->em
            ->getRepository('InvictusCmsBundle:Relation')
            ->setInvictusKernel($this->invictusKernel)
            ->getRelatedElementsFromConfig($config, $debug);

        return $data;
    }

}
