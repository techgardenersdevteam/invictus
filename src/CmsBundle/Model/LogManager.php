<?php

namespace Invictus\CmsBundle\Model;

use Invictus\CmsBundle\Extensions\InvictusKernel;
use Invictus\CmsBundle\Entity\Log;
use Symfony\Component\Validator\Constraints\DateTime;

class LogManager
{

    protected $invictusKernel;

    private $levelToLog = 0;

    public function __construct(InvictusKernel $invictusKernel, $doctrine)
    {
        $this->invictusKernel = $invictusKernel;
        $this->doctrine = $doctrine;
        $this->em = $this->doctrine->getManager();
    }

    public function setLogLevel($level)
    {
        $this->levelToLog = $level;
    }

    public function log($data = array(), $level = 1)
    {

        if($level < $this->levelToLog){
            return false;
        }

        $default = array(
            'appId' => $this->invictusKernel->appId,
            'languageId' => $this->invictusKernel->languageId,
            'moduleId' => $this->invictusKernel->moduleId,
            'itemId' => null,
            'label' => '...',
            'description' => '...',
            'action' => '-',
            'level' => $level,
            'user_agent' => '-'
        );

        $data = array_merge($default, $data);

        if($data['moduleId'] != 8){
            $log = new Log();
            $log->setFkApp($this->invictusKernel->getApp(array('id' => $data['appId'])));
            $log->setFkLanguage($this->invictusKernel->getLanguage(array('id' => $data['languageId'])));
            $log->setFkModule($this->invictusKernel->getModule(array('id' => $data['moduleId'])));
            $log->setFkAdmin($this->invictusKernel->user);
            $log->setItemId($data['itemId']);
            $log->setLabel($data['label']);
            $log->setBody($data['description']);
            $log->setAction($data['action']);
            $log->setLevel($data['level']);
            $log->setDatetime(new \DateTime());
            $log->setIp($_SERVER['SERVER_ADDR']);
            $log->setUserAgent($_SERVER['HTTP_USER_AGENT']);

            $this->em->persist($log);

            $this->em->flush();
        }
    }

}
