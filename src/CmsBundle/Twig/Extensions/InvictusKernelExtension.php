<?php
/*
 * This file is part of the INVICTUS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace Invictus\CmsBundle\Twig\Extensions;

use Invictus\CmsBundle\Extensions\InvictusKernel;
use Invictus\CmsBundle\Extensions\CMSConfigurations;

class InvictusKernelExtension extends \Twig_Extension
{
    protected $invictusKernel;

    function __construct(InvictusKernel $invictusKernel) {
        $this->invictusKernel = $invictusKernel;
    }

    public function getGlobals() {
        return array(
            'invictusKernel' => $this->invictusKernel,
        );
    }

    public function getName()
    {
        return 'invictusKernel';
    }

}