<?php

namespace Invictus\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use Invictus\CmsBundle\Controller\InvictusController;
use Invictus\CmsBundle\Entity\Attachment;
use Invictus\CmsBundle\Form\Type\AttachmentType;


class AttachmentController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        $config['orderBy'] = array(
            'base.fkAttachmentTypology' => 'base.fkAttachmentTypology ASC'
        );

        if($this->invictusKernel->parentModuleId AND $this->invictusKernel->parentItemId){
            $config['where']['base.fkModule'] = 'base.fkModule = :fkModule';
            $config['where']['base.fkItemId'] = 'base.fkItemId = :fkItemId';

            $config['params']['fkModule'] = $this->invictusKernel->parentModuleId;
            $config['params']['fkItemId'] = $this->invictusKernel->parentItemId;
        }

        $this->tableManager->addConfig($config);

        $this->tableManager->disableAction('attach');
        $this->tableManager->enablePredefinedToggle('enabled');
    }


    public function singleUploadAction()
    {
        $attachment = new Attachment();
        $uploadDir = $attachment->getUploadDir();
        $uploadDir = '';

        $form = $this->createForm(new AttachmentType(), $attachment);
        if ($this->getRequest()->isMethod('POST')){
            $form->handleRequest($this->getRequest());

            if ($form->isValid()){

                $newFilename = $attachment->upload(false, $this->getToken());

                if( strstr($attachment->getFile()->getClientMimeType(),'image') ) {
                    // RedirectResponse object
                    $imagemanagerResponse = $this->container
                        ->get('liip_imagine.controller')
                        ->filterAction(
                            $this->getRequest(),
                            $uploadDir.'/'.$newFilename,
                            'invictus_instant_preview'              // filter defined in config.yml
                        );

                    // string to put directly in the "src" of the tag <img>
                    $cacheManager = $this->container->get('liip_imagine.cache.manager');
                    $src = $cacheManager->getBrowserPath($uploadDir.'/'.$newFilename, 'invictus_instant_preview');


                    // RedirectResponse object
                    $imagemanagerResponse = $this->container
                        ->get('liip_imagine.controller')
                        ->filterAction(
                            $this->getRequest(),
                            $uploadDir.'/'.$newFilename,
                            'invictus_thumb'              // filter defined in config.yml
                        );

                    // string to put directly in the "src" of the tag <img>
                    $cacheManager = $this->container->get('liip_imagine.cache.manager');
                    $cacheManager->getBrowserPath($uploadDir.'/'.$newFilename, 'invictus_thumb');
                }else{
                    $src = false;
                }

                $file = array(
                    'oldFilename' => $attachment->getFile()->getClientOriginalName(),
                    'filename' => $newFilename,
                    'type' => $attachment->getFile()->getClientMimeType(),
                    'size' => $attachment->getFile()->getClientSize(),
                    'ext' => $ext = pathinfo($attachment->sanitizeFilename($attachment->getFile()->getClientOriginalName()), PATHINFO_EXTENSION),
                    'error' => $attachment->getFile()->getError(),
                    'src' => $src,
                    'uploadDir' => $this->container->getParameter('liip_imagine.data_root')
                );
                return new JsonResponse($file);
            }

        }

        $file = array(
            'oldFilename' => '...',
            'filename' => '...',
            'type' => 'undefined',
            'size' => '0',
            'error' => true,
            'message' => 'Invalid form or not a POST request'
        );
        return new JsonResponse($file);
    }


    protected function preInsertItem($item){

        return $item;
    }


    protected function preUpdateItem($item){

        return $item;
    }


    protected function postFlushAdd($item)
    {
        //$this->updateAttachmentFkItemId($item->getId());
    }


    protected function postFlushUpdate($item)
    {
        //$this->updateAttachmentFkItemId($item->getId());
    }


    private function updateAttachmentFkItemId($id){
        $fkItemId = ($this->invictusKernel->parentItemId) ? $this->invictusKernel->parentItemId : 0 ;
        $sql = "UPDATE attachment SET fk_item_id = $fkItemId WHERE id = $id";
        $conn = $this->get('database_connection');
        $conn->exec($sql);
    }


    private function getUniquePath($path, $token = false)
    {
        if(file_exists($path)){

            $pathinfo = pathinfo($path);

            if($token){

                $path = $pathinfo['dirname'].'/'.$pathinfo['filename']."_$token.".$pathinfo['extension'];
                if(file_exists($path)){
                    $token = time();
                    $path = $pathinfo['dirname'].'/'.$pathinfo['filename']."_$token.".$pathinfo['extension'];
                }

            }else{
                $token = time();
                $path = $pathinfo['dirname'].'/'.$pathinfo['filename']."_$token.".$pathinfo['extension'];
            }

        }

        $pathInfo = pathinfo($path);
        $pathInfo['fullPath'] = $path;

        return $pathInfo;
    }


    public function getToken()
    {
        return $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Attachment')
            ->getLastId();
    }



}
