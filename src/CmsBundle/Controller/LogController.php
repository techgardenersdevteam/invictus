<?php

namespace Invictus\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class LogController extends InvictusController
{

    protected function configureTableList(){

        $this->fields[] = 'log.datetime';
        $this->sorts[] = 'desc';

        if($this->invictusKernel->getGETParam('id')){
            $this->where[] = ' AND id LIKE :id';
            $this->parameters['id'] = $this->request->query->get('id');
        }

        if($this->invictusKernel->getGETParam('label')){
            $this->where[] = ' AND label LIKE :label';
            $this->parameters['label'] = '%'.$this->request->query->get('label').'%';
        }

        $this->where[] = ' AND deleted = :deleted';
        $this->parameters['deleted'] = '0';

        $this->tableListConfig['bools'] = array(
        );

        $this->tableListConfig['actions'] = array(
            'attachments' => false,
            'edit' => false,
            'delete' => false
        );
    }

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        //$config['appId'] = 'vs';
        //$config['languageId'] = 'en_GB';
        //$config['moduleId'] = 4;
        $config['orderBy'] = array(
            'datetime' => 'base.datetime DESC'
        );
        $config['joins'] = array(
            'position' => false,
            'visibility' => false,
            'metadata' => false
        );
        $this->tableManager->addConfig($config);

        $this->tableManager->disableAction('edit');
        $this->tableManager->disableAction('delete');
        $this->tableManager->disableAction('attach');
    }

}
