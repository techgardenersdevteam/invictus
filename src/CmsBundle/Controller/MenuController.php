<?php

namespace Invictus\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class MenuController extends InvictusController
{

    protected function configureTableList(){

        $this->fields[] = 'menu.id';
        $this->sorts[] = 'asc';

        if($this->invictusKernel->getGETParam('id')){
            $this->where[] = ' AND id LIKE :id';
            $this->parameters['id'] = $this->request->query->get('id');
        }

        if($this->invictusKernel->getGETParam('tag')){
            $this->where[] = ' AND tag LIKE :tag';
            $this->parameters['tag'] = '%'.$this->request->query->get('tag').'%';
        }

        $this->where[] = ' AND deleted = :deleted';
        $this->parameters['deleted'] = '0';

        $this->tableListConfig['bools'] = array(
            array(
                'table' => 'menu',
                'field' => 'enabled',
                'icons' => array(
                    0 => 'icomoon16-minus-2',
                    1 => 'icomoon16-checkmark-2'
                ),
                'translation' => array(
                    0 => 'notPublished',
                    1 => 'published'
                ),
                'css' => array(
                    0 => 'enabled',
                    1 => 'disabled'
                )

            )
        );

        $this->tableListConfig['actions'] = array(
            'attachments' => false,
            'edit' => true,
            'delete' => true
        );
    }

}
