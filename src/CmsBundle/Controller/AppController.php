<?php

namespace Invictus\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class AppController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        //$config = array();
        //$config['appId'] = 'vs';
        //$config['languageId'] = 'en_GB';
        //$config['moduleId'] = 4;

        //$this->tableManager->addConfig($config);

        //$this->tableManager->enableAction('edit');
        $this->tableManager->disableAction('attach');
        $this->tableManager->enablePredefinedToggle('highlighted');
        $this->tableManager->enablePredefinedToggle('enabled');
    }


    protected function setAppLanguageRelations($item, $relations)
    {
        $relations = $this->invictusKernel->getPOSTParam($relations);
        if($relations){

            foreach($relations as $relation){

                $sql = "DELETE FROM relation
                    WHERE
                    fk_app_id = '{$item->getId()}'
                    AND fk_module_id_a IS NULL
                    AND fk_item_id_a IS NULL
                    AND fk_module_id_b IS NULL
                    AND fk_item_id_b IS NULL
                    AND slot = 1
                    ";
                $stmt = $this->em
                    ->getConnection()
                    ->prepare($sql);

                $stmt->execute();

                if(isset($relation['fk_language_id'])){
                    $position = 1;
                    foreach($relation['fk_language_id'] as $locale){

                        $sql = "INSERT INTO relation
                        (fk_app_id, fk_language_id, position, slot)
                        VALUES('{$item->getId()}', '$locale', $position, 1)";
                        $stmt = $this->em
                            ->getConnection()
                            ->prepare($sql);

                        $stmt->execute();

                        $position++;
                    }
                }

            }
        }
    }

}
