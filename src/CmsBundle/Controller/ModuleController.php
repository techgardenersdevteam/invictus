<?php

namespace Invictus\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class ModuleController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        $config['orderBy'] = array(
            'base.id' => 'base.id ASC'
        );
        $config['joins'] = array(
            'position' => false
        );
        $this->tableManager->addConfig($config);

        $this->tableManager->disableAction('attach');
        $this->tableManager->enablePredefinedToggle('enabled');
    }


    protected function setModuleAppLanguageRelations($item, $visibilities)
    {
        $sql = "DELETE
                FROM relation
                WHERE
                fk_module_id_a = 4
                AND fk_item_id_a = {$item->getId()}
                AND fk_module_id_b IS NULL
                AND fk_item_id_b IS NULL
        ";

        $conn = $this->get('database_connection');
        $conn->exec($sql);


        $visibilities = $this->invictusKernel->getPOSTParam($visibilities, false);
        if($visibilities){
            foreach($visibilities['apps'] as $appId => $app){
                foreach($app['languages'] as $languageId => $value ){
                    $sql = "INSERT
                            INTO relation
                            (fk_app_id, fk_language_id, fk_module_id_a, fk_item_id_a, position, slot)
                            VALUES ('$appId', '$languageId', 4, {$item->getId()}, 0, 0)
                    ";

                    $conn = $this->get('database_connection');
                    $conn->exec($sql);
                }
            }
        }
    }

}
