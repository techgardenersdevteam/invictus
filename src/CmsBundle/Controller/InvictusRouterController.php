<?php
/*
 * This file is part of the INVICTUS project
 *
 * (c) Matteo Poile <matteo.poile@devsign.it>
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace Invictus\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class InvictusRouterController extends Controller
{

    private $invictusKernel;

    public function actionAction(Request $request, array $path = array() )
    {
        $this->invictusKernel = $this->get('invictus.kernel');
        $ajaxSuffix = ($request->isXmlHttpRequest()) ? 'Ajax' : '' ;
        //$ajaxSuffix = "";
        return $this->forward(
          $this->invictusKernel->getCurrentModule()->getBundleController().':'.$request->attributes->get('action').$ajaxSuffix,
          $path
        );
    }
    
    
    public function actionIdAction(Request $request)
    {
        return $this->actionAction($request, array(
                                    'app' => $request->attributes->get('app'),
                                    'locale' => $request->attributes->get('locale'),
                                    'module' => $request->attributes->get('moduleTag'),
                                    'action' => $request->attributes->get('action'),
                                    'id' => $request->attributes->get('id')
                                  ));
    }    
    
}
