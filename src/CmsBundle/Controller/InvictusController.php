<?php
/*
 * This file is part of the INVICTUS project
 *
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Invictus\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Invictus\CmsBundle\Entity\Metadata;
use Invictus\CmsBundle\Model\TableManager;


class InvictusController extends Controller
{

    protected $request = NULL;
    protected $em = NULL;
    protected $where = array();
    protected $messages = array();
    protected $UILocale = null;
    protected $invictusKernel = null;
    protected $formTypeClass = null;
    protected $logger = null;
    protected $parameters = array();
    protected $fields = array();
    protected $sorts = array();
    protected $tableListConfig = array();
    protected $tableManager = null;


    protected function init()
    {
        $this->request = $this->getRequest();
        $this->em = $this->getDoctrine()->getManager();
        $this->UILocale = $this->get('security.context')->getToken()->getUser()->getUILanguage()->getId();
        $this->request->setLocale($this->UILocale);
        $this->get('session')->set('_locale', $this->UILocale);
        $this->invictusKernel = $this->get('invictus.kernel');
        //$this->formTypeClass = $this->invictusKernel->getCurrentModule()->getBundleNameSpacePath()."\\Form\\Type\\".$this->invictusKernel->getCurrentModule()->getEntity()."Type";
        $this->formTypeClass = $this->invictusKernel->getCurrentModule()->getFormType();
        $this->logger = $this->get('invictus.logger');
    }
    
    
    public function indexAction()
    {   
        $this->init();

        $this->logger->log(array('action' => 'view'), 1);

        return $this->render($this->invictusKernel->getCurrentModule()->getBundleView().':module.html.twig');
    }

    
    public function addAjaxAction()
    {
        $this->init();

        $this->logger->log(array('action' => 'add', 'label' => 'Show add form'), 1);

        $config = json_decode($this->invictusKernel->getCurrentModule()->getConfig());

        $item = $this->invictusKernel->getCurrentModule()->getModuleEntity();

        if($config->module->MA){
            $item->setFkApp($this->invictusKernel->getCurrentApp());
        }


        if(isset($config->metadata) and $config->metadata){
            $item = $this->prepareAddMetadatas($item, $config->metadata);
        }

        //Se l'item è settata come multilingua creo la traduzione vuota e l'aggiungo
        if($config->module->ML){
            $item = $this->prepareAddTranslations($item);
        }

        $form = $this->createForm(new $this->formTypeClass(),
                                    $item,
                                    array(
                                        'validation_groups' => array('add'),
                                        'invictusKernel' => $this->invictusKernel
                                    )
        );

        if ($this->request->isMethod('POST')){

            $form->handleRequest($this->request);

            if ($form->isValid()){

                // Potrei evitare "$this->em->persist($item);" mettendo @ORM\ManyToOne(targetEntity="EntityBase", cascade={"persist"}) in EntityTranslation.php.
                // Preferisco però lasciare $this->em->persist($item); per renderlo più esplicito e usare cascade={"persist"} solo per i vari fkLanguage e fkApp.
                // Da notare che cascade={"persist"} va messo solo nell'owning side che è sempre l'Entity che ha il ManyToOne in quanto gestisce la foreign key tramite il campo fk_nomemodulo_id.
                // Questo crea confusione perchè ci si ritrova ad usare qualcosa simile a $translation->setFkItem().
                $this->em->persist($item);

                if($config->module->ML){
                    foreach ($item->getTranslations() as $translation) {

                        /* Di solito si dovrebbe fare qualcosa di simile a $translation->setFkItem($item) in quanto è translation l'owning side.
                           Con un piccolo workaround è possibile fare il molto più chiaro $item->addTranslation($translation); con una piccola modifica a $item->addTranslation():

                           public function addTranslation(\Invictus\CmsBundle\Entity\ModuleTranslation $translations)
                            {
                            --> $translations->setFkModule($this); <--
                                $this->translations[] = $translations;
                                return $this;
                            }

                            Senza questa modifica si può usare:

                            $setMainEntity = 'setFk'.ucfirst($this->invictusKernel->getCurrentModule()->getTag());
                            $translation->$setMainEntity($item);
                            $this->em->persist($translation);
                        */
                        $translation = $this->preInsertTranslation($translation);
                        $item->addTranslation($translation);
                        $this->em->persist($translation);
                    }
                }

                if(isset($config->metadata) and $config->metadata){
                    //$item = $this->prepareAddMetadatas($item, $config->metadata);
                }


                $item = $this->preInsertItem($item);
                //echo "<pre>";\Doctrine\Common\Util\Debug::dump($item->getMetadatas());echo "</pre>";

                $notice = array(
                    'message' => $this->get('translator')->trans('success.addItem'),
                    'type' => 'success'
                );

                //Basta il flush nel try ma lo lascio qui per generare un errore utile al debug
                $this->em->flush();
                try {
                    $this->em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {

                    $this->logger->log(array('action' => 'add', 'label' => 'Save error'), 6);

                    $notice = array(
                        'message' => $this->get('translator')->trans('error.addItem'),
                        'type' => 'alert'
                    );
                }

                $this->logger->log(array('action' => 'add', 'label' => 'Save success'), 1);

                $this->postFlushAdd($item);

                if($this->invictusKernel->getPOSTParam('widget', false)){
                    foreach($this->invictusKernel->getPOSTParam('widget') as $method => $argument){
                        $this->$method($item, $argument);
                    }
                }

            }else{

                print_r($form->getErrorsAsString());
                print_r($form->getErrors());

                $this->logger->log(array('action' => 'add', 'label' => 'Form data are invalid'), 5);

                $notice = array(
                    'message' => $this->get('translator')->trans('error.invalidForm'),
                    'type' => 'error'
                );
            }
            
            // Risposta json con messaggi dopo salvataggio form
            return new JsonResponse($notice);
        }
        
        // Risposta html alla richiesta del form vuoto
        //return $this->render($this->invictusKernel->getCurrentModule()->getBundleController().':form.html.twig',
        return $this->render('InvictusCmsBundle::form.html.twig',
            array(
                'form' => $form->createView(),
                'item' => $item
            )
        );

        
    }
    
    
    public function updateAjaxAction($id)
    {
        
        $this->init();

        $this->logger->log(array('action' => 'update', 'label' => 'Show update form'), 1);

        $config = json_decode($this->invictusKernel->getCurrentModule()->getConfig());

        $item = $this->getDoctrine()
            ->getRepository($this->invictusKernel->getCurrentModule()->getBundleEntity())
            //->setInvictusKernel($this->invictusKernel)
            ->findOneById($id);

        if (!$item) {

            $this->logger->log(array('action' => 'update', 'label' => 'No item found with id $id'), 6);

            $notice = array(
                'message' => "No item found with id $id",
                'type' => 'error'
            );
            return new JsonResponse($notice);
        }

        if(isset($config->metadata) and $config->metadata){
            // resetto eventuali metadata che potrebbero venir lazy loaded
            // resetMetadatas() non funziona se inserito in prepareUpdateMetadatas()
            $item->resetMetadatas();
            // delego la gestione dei metadatas a prepareUpdateMetadatas()
            $item = $this->prepareUpdateMetadatas($item, $config->metadata);
        }

        if($config->module->ML){
            $item->resetTranslations();
            $item = $this->prepareUpdateTranslations($item);
        }

        //$form = $this->createForm(new $this->formTypeClass(), $item, array('validation_groups' => array('update'), 'appId' => $this->invictusKernel->appId, 'languageId' => $this->invictusKernel->languageId, 'UILanguageId' => $this->invictusKernel->UILanguageId ));
        $form = $this->createForm(new $this->formTypeClass(), $item, array('validation_groups' => array('update'), 'invictusKernel' => $this->invictusKernel ));

        if ($this->request->isMethod('POST')) {
            //var_dump(array_unique($this->invictusKernel->getPOSTParam('relations', false)));
            $form->handleRequest($this->request);

            if ($form->isValid()){
                $item = $this->preUpdateItem($item);
                $this->em->persist($item);

                $notice = array(
                    'message' => $this->get('translator')->trans('success.updateItem'),
                    'type' => 'success'
                );

                //Basta il flush nel try ma lo lascio qui per debug
                //$this->invictusKernel->doctrineDump($item->getMetadatas());
                $this->em->flush();
                try {
                    $this->em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {

                    $this->logger->log(array('action' => 'update', 'label' => 'Update error'), 6);

                    $notice = array(
                        'message' => $this->get('translator')->trans('error.updateItem'),
                        'type' => 'alert'
                    );
                }

                $this->logger->log(array('action' => 'update', 'label' => 'Update success'), 6);

                if($this->invictusKernel->getPOSTParam('widget', false)){
                    foreach($this->invictusKernel->getPOSTParam('widget') as $method => $argument){
                        $this->$method($item, $argument);
                    }
                }

                if($this->invictusKernel->getPOSTParam('widgets', false)){
                    $this->manageWidgets($item->getId(), $this->invictusKernel->getPOSTParam('widgets'));
                }

            }else{
                print_r($form->getErrorsAsString());

                $this->logger->log(array('action' => 'update', 'label' => 'Form data are invalid'), 5);

                $notice = array(
                    'message' => $this->get('translator')->trans('error.invalidForm'),
                    'type' => 'error'
                );
            }

            // Risposta json con messaggi dopo salvataggio form
            return new JsonResponse($notice);
        }

        return $this->render('InvictusCmsBundle::form.html.twig',
            array(
                'form' => $form->createView(),
                'item' => $item
            )
        );
    }

    
    public function deleteAjaxAction($id)
    {
        $this->init();

        $item = $this->getDoctrine()
            ->getRepository($this->invictusKernel->getCurrentModule()->getBundleEntity())
            ->findOneById($id);

        if (!$item){

            $this->logger->log(array('action' => 'delete', 'label' => "No item with id $id"), 5);

            $notice = array(
                'message' => $this->get('translator')->trans('error.delete'),
                'type' => 'alert'
            );
        }else{

            $skip_modules_tag = array(
                "app",
                "language"
            );
            if(preg_match('/^('.implode('|', $skip_modules_tag).')/', $this->invictusKernel->getCurrentModule()->getTag())){
                $item->setId($item->getId().'_'.time());
            }
            $item->setDeleted(true);

            $this->em->persist($item);
            $this->em->flush();

            $notice = array(
                'message' => $this->get('translator')->trans('success.deleteItem'),
                'type' => 'success'
            );
                    
            try {
                $this->em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {

                $this->logger->log(array('action' => 'delete', 'label' => "Delete error"), 6);

                $notice = array(
                    'message' => $this->get('translator')->trans('error.deleteItem'),
                    'type' => 'alert'
                );
            }

            $this->logger->log(array('action' => 'delete', 'label' => "Delete success"), 1);
            
        }
        
        return new JsonResponse($notice);
    }
    
    
    public function tableListAjaxAction()
    {
        $this->init();

        $this->tableManager = $this->get('invictus.tableManager');
        $this->tableManager->initBE();

        if(method_exists($this, 'configureTableManager')) {
            $this->configureTableManager();
        }

        $currentPage = $this->tableManager->setCurrentPage($this->invictusKernel->getGETParam('page', 1));
        $pageSize = $this->tableManager->setPageSize($this->invictusKernel->getGETParam('rows', 10));

        $sidx = $this->invictusKernel->getGETParam('sidx', false); // campo di ordinamento
        $sord = $this->invictusKernel->getGETParam('sord', false); // tipo di ordinamento (asc|desc)

        if($sidx){
            // prima resetto l'ordinamento di default
            $this->tableManager->removeConfig('orderBy');

            $sord = ($sord) ? $sord : 'ASC';
            $config['orderBy'] = array(
                'jqGrid' => "$sidx $sord"
            );
            $this->tableManager->addConfig($config);
        }

        $data = $this->tableManager->getJqGridArray();

        //echo "<pre>";print_r($this->tableManager->getConfig());echo "</pre>";die();
        //unset($data['rows']);echo "<pre>";print_r($data);echo "</pre>";die();

        $response = new Response();
        $response->headers->set('Content-type', 'application/json; charset=utf-8');
        return $this->render($this->invictusKernel->getCurrentModule()->getBundleView().':table-list-ajax.json.twig',
            array(
                'data' => $data
            ),
            $response
        );
    }


    public function oldTableListAjaxAction()
    {
        $this->init();

        // valori di default sovrascritto se necessario in seguito tramite $this->configureTableList();
        $this->tableListConfig['bools'] = false;
        $this->tableListConfig['actions'] = false;

        if(method_exists($this, 'configureTableList')) {
            $this->configureTableList();
        }

        $page = $this->invictusKernel->getGETParam('page', 1); // numero di pagina
        $rpp = $this->invictusKernel->getGETParam('rows', '10'); // record per pagina


        $fields = $this->invictusKernel->getGETParam('sidx', $this->fields); // campo di ordinamento
        $sorts = $this->invictusKernel->getGETParam('sord', $this->sorts); // tipo di ordinamento (asc|desc)

        $records = $this->em->getRepository($this->invictusKernel->getCurrentModule()->getBundleEntity())
            ->getTotalRecords($this->where, $this->parameters);
        $total = ceil($records/$rpp); // numero di pagine
        $page = ($page > $total) ? $total : $page;
        $page = ($page < 1) ? 1 : $page;

        $rows = $this->em->getRepository($this->invictusKernel->getCurrentModule()->getBundleEntity())
            ->setInvictusKernel($this->invictusKernel)
            ->findAllOrdered($this->where, $this->parameters, $fields, $sorts, $page, $rpp);

        $this->tableListConfig['page'] = $page;
        $this->tableListConfig['records'] = $records;
        $this->tableListConfig['total'] = $total;
        $this->tableListConfig['rows'] = $rows;

        $response = new Response();
        $response->headers->set('Content-type', 'application/json; charset=utf-8');
        return $this->render($this->invictusKernel->getCurrentModule()->getBundleView().':table-list-ajax.json.twig',
            array(
                'data' => $this->tableListConfig
            ),
            $response
        );
    }


    public function treeListAjaxAction()
    {
        $this->init();

        $idParent = $this->invictusKernel->getGETParam('idParent', 1);
        /*
        $rows = $this->em->getRepository($this->invictusKernel->getCurrentModule()->getBundleController())
            ->findByIdParent($idParent);

        $data['rows'] = $rows;
        */
        $data['actions'] = false; // valore di default sovrascritto se necessario in seguito
        $data['actions'] = array(
            'attachments' => false,
            'modify' => true,
            'delete' => true
        );

        $response = new Response();
        $response->headers->set('Content-type', 'application/json; charset=utf-8');
        return $this->render($this->invictusKernel->getCurrentModule()->getBundleController().':tree-list-ajax.json.twig',
            array(
                'data' => $data
            ),
            $response);


    }
        
    
    public function setBoolAction($moduleId, $itemId, $field, $unique = false)
    {
        $this->logger = $this->get('invictus.logger');
        $this->em = $this->getDoctrine()->getManager();
        $module = $this->em->getRepository('InvictusCmsBundle:Module')
            ->findOneById($moduleId);
        $bundleEntity = $module->getBundleEntity();

        $item = $this->em->getRepository($bundleEntity)
                        ->findOneById($itemId);
        
        $fieldForMethod = str_replace(' ', '', ucwords(str_replace('_', ' ', $field)));
        $setField = 'set'.$fieldForMethod;
        $getField = 'get'.$fieldForMethod;

        $newBoolValue  = !$item->$getField();

        $item->$setField($newBoolValue);

        $this->em->persist($item);

        $notice = array(
                    'message' => $this->get('translator')->trans('success.update'),
                    'type' => 'success'
                );
        if($unique){
            $table = $module->getDbTable();
            $sql = "UPDATE $table SET $field = 0";
            $conn = $this->get('database_connection');
            $conn->exec($sql);
        }

        try {
            $this->em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {

            $this->logger->log(array(
                    'action' => 'set bool',
                    'label' => "Set bool error on field '<i>$field</i>'",
                    'moduleId' => $moduleId,
                    'itemId' => $itemId
                ),
                    6);

            $notice = array(
                    'message' => $this->get('translator')->trans('error.update'),
                    'type' => 'alert'
                );
        }

        $this->logger->log(array('action' => 'set bool', 'label' => "Set bool success on field '<i>$field</i>'", 'moduleId' => $moduleId, 'itemId' => $itemId), 6);

      /*
        if($moduleId == 54){
            $to = $item->getEmail();
            $from = 'info@n2ktag.com';
            $subject = 'La sua registrazione a N2K tag / Your account N2K tag';
            $username = $item->getUsername();
            if($newBoolValue){
                $email_tpl = 'N2ktagWebappBundle:Email:user_enabled_email.html.twig';
            }else{
                $email_tpl = 'N2ktagWebappBundle:Email:user_disabled_email.html.twig';
            }

            $messaggio = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($from)
                ->setTo($to)
                ->setBody(
                    $this->renderView(
                        $email_tpl,
                        array('username' => $username)
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($messaggio);

        }
      */

        return new JsonResponse($notice);
        
    }


    public function addToSessionAction($key, $value)
    {

        $session = $this->get('session');
        $session->set($key, $value);

        return new Response('Post parameters setted in session');

    }


    public function getJsTreeChildrenAction()
    {
        $this->init();
        $parentId = $this->request->request->get('parentId');
        $parentId = empty($parentId) ? null : $parentId;
        $appId = $this->request->request->get('appId');
        $appId = empty($appId) ? null : $appId;
        $languageId = $this->request->request->get('languageId');
        $languageId = empty($languageId) ? null : $languageId;
        $repository = $this->request->request->get('repository');

        $children = $this->getDoctrine()
            ->getRepository($repository)
            ->setInvictusKernel($this->invictusKernel)
            ->getJsTreeChildren($parentId, $appId, $languageId, $json = false);

        return new JsonResponse($children);
    }


    public function orderJsTreeChildrenAction()
    {
        $this->init();
        $parentId = $this->request->request->get('parentId', null);
        $ids = $this->request->request->get('ids');
        $appId = $this->request->request->get('appId');
        $appId = empty($appId) ? null : $appId;
        $languageId = $this->request->request->get('languageId');
        $languageId = empty($languageId) ? null : $languageId;
        $repository = $this->request->request->get('repository');

        $result = $this->getDoctrine()
            ->getRepository($repository)
            ->setInvictusKernel($this->invictusKernel)
            ->orderJsTreeChildren($parentId, $ids, $appId, $languageId);

        return new JsonResponse($result);
    }


    protected function setMetadatas($item, $metadatas)
    {
        $translations = $item->getTranslations();

        $i = 1;
        foreach($item->getMetadatas() as $metadata) {

            $slug = $metadata->getSlug();

            if(empty($slug)){
                $metadataLanguage = $metadata->getFkLanguage()->getId();
                foreach($translations as $translation){
                    if($translation->getFkLanguage()->getId() == $metadataLanguage){
                        $slug = $translation->getLabel();
                        break;
                    }
                }
            }

            if(empty($slug)){
                $slug = $translations[0]->getLabel();
            }

            $metadata->setFkItemId($item->getId());
            $metadata->setSlug($this->invictusKernel->slugify($slug));
            $item->addMetadata($metadata);
            $this->em->persist($metadata);
            /*
            echo $i.") <br>";
            echo $metadata->getFkApp()->getId()." - ";
            echo $metadata->getFkLanguage()->getId()." - ";
            echo $metadata->getFkModule()->getId()." - ";
            echo $metadata->getFkItemId()." - ";
            echo $metadata->getSlug()." <br><br> ";
            $i++;
            */
        }
        $this->em->flush();
    }


    protected function setVisibilities($item, $visibilities)
    {
        $visibilities = $this->invictusKernel->getPOSTParam($visibilities, false);
        $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Visibility')
            ->setInvictusKernel($this->invictusKernel)
            ->saveVisibilities($this->invictusKernel->moduleId, $item->getId(), $visibilities);
    }


    protected function setAllVisibilities($item, $visibilities)
    {
        $visibilities = $this->invictusKernel->getPOSTParam($visibilities, false);
        $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Visibility')
            ->setInvictusKernel($this->invictusKernel)
            ->saveAllVisibilities($this->invictusKernel->moduleId, $item->getId(), $visibilities);
    }


    protected function setPositions($item, $positions)
    {
        $positions = $this->invictusKernel->getPOSTParam($positions, false);
        $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Position')
            ->setInvictusKernel($this->invictusKernel)
            ->savePositions($this->invictusKernel->moduleId, $positions);
    }


    protected function setRelations($item, $relations)
    {
        $relations = $this->invictusKernel->getPOSTParam($relations);
        //$relations = array_reverse($relations);
        foreach($relations as $relation){

            $position = 1;

            $knownField = $relation['side'] == 'owning' ? 'fk_item_id_a' : 'fk_item_id_b';
            $relatedField = $relation['side'] == 'owning' ? 'fk_item_id_b' : 'fk_item_id_a' ;

            $constraints = array(
                'fk_app_id' => $relation['fk_app_id'],
                'fk_language_id' => $relation['fk_language_id'],
                'fk_module_id_a' => $relation['fk_module_id_a'],
                'fk_module_id_b' => $relation['fk_module_id_b'],
                $knownField => $item->getId(),
                'slot' => $relation['slot']
            );

            $this->getDoctrine()
                ->getRepository('InvictusCmsBundle:Relation')
                ->setInvictusKernel($this->invictusKernel)
                ->deleteRelations($constraints);

            if(!isset($relation[$relatedField])){
                continue;
            }

            foreach($relation[$relatedField] as $id){
                $data = $constraints;
                $data[$relatedField] = $id;
                //echo $data['fk_app_id'] . " " . $data['fk_language_id'] . " $id $position - ";
                $data['position'] = $position;

                $this->getDoctrine()
                    ->getRepository('InvictusCmsBundle:Relation')
                    ->setInvictusKernel($this->invictusKernel)
                    ->addRelation($data);

                $position++;
            }

        }
    }


    protected function manageWidgets($itemId, $widgets)
    {
        foreach($widgets as $widget){
            // Controllo che il widget non sia esplicitamente disabilitato.
            // Alcuni widget potrebbero venire attivati su richiesta (vedi mouseover di GMap),
            // è necessario sapere se eseguire il metodo del servizio (che potrebeb cancellare dati)
            // o se non eseguirlo perchè il widget non è stato abilitato
            if(!isset($widget['enabled']) || $widget['enabled']){
                $data = $this->invictusKernel->getPOSTParam($widget['argument']);
                $service = $this->get($widget['service']);
                $service->$widget['method']($itemId, $data);
            }
        }
    }


    protected function preInsertItem($item){
        return $item;
    }
    protected function preUpdateItem($item){
        return $item;
    }
    protected function preInsertTranslation($translation){
        return $translation;
    }
    protected function preUpdateTranslation($translation){
        return $translation;
    }
    protected function postFlushAdd($item){
        return false;
    }


    protected function prepareAddMetadatas($item, $config)
    {
        if($config->ML){
            foreach($this->invictusKernel->getUserLanguages($this->invictusKernel->moduleTag) as $language){
                $metadata = new Metadata();
                $metadata->setFkModule($this->invictusKernel->getModule(array('id' => $this->invictusKernel->moduleId)));
                $metadata->setFkItemId($item->getId());
                if($config->MA){
                    $metadata->setFkApp($this->invictusKernel->getApp(array('id' => $this->invictusKernel->appId)));
                }
                $metadata->setFkLanguage($this->invictusKernel->getLanguage(array('id' => $language['id'])));
                $this->em->persist($metadata);
                $item->addMetadata($metadata);
            }
        }else if(!$config->ML){
            $metadata = new Metadata();
            $metadata->setFkModule($this->invictusKernel->getModule(array('id' => $this->invictusKernel->moduleId)));
            $metadata->setFkItemId($item->getId());
            if($config->MA){
                $metadata->setFkApp($this->invictusKernel->getApp(array('id' => $this->invictusKernel->appId)));
            }
            $metadata->setFkLanguage(null);
            $this->em->persist($metadata);
            $item->addMetadata($metadata);
        }

        return $item;
    }


    protected function prepareUpdateMetadatas($item, $config)
    {
        if($config->ML){
            foreach( $this->invictusKernel->getUserLanguages($this->invictusKernel->moduleTag) as $language){
                $criteria = array(
                    'fkModule' => $this->invictusKernel->moduleId,
                    'fkItemId' => $item->getId(),
                    'fkApp' => null,
                    'fkLanguage' => $language['id']
                );

                if($config->MA){
                    $criteria['fkApp'] = $this->invictusKernel->appId;
                }
                $metadata = $this->getDoctrine()
                    ->getRepository('InvictusCmsBundle:Metadata')
                    ->findOneBy($criteria);

                if(!$metadata){
                    $metadata = new Metadata();
                    $metadata->setFkModule($this->invictusKernel->getModule(array('id' => $this->invictusKernel->moduleId)));
                    $metadata->setFkItemId($item->getId());
                    $metadata->setFkLanguage($this->invictusKernel->getLanguage(array('id' => $language['id'])));
                    if($config->MA){
                        $metadata->setFkApp($this->invictusKernel->getApp(array('id' => $this->invictusKernel->appId)));
                    }
                }
                $this->em->persist($metadata);
                $item->addMetadata($metadata);
            }
        }else if(!$config->ML){
            $criteria = array(
                'fkModule' => $this->invictusKernel->moduleId,
                'fkItemId' => $item->getId(),
                'fkApp' => null,
                'fkLanguage' => null
            );
            if($config->MA){
                $criteria['fkApp'] = $this->invictusKernel->appId;
            }
            $metadata = $this->getDoctrine()
                ->getRepository('InvictusCmsBundle:Metadata')
                ->findOneBy($criteria);

            if(!$metadata){
                $metadata = new Metadata();
                $metadata->setFkModule($this->invictusKernel->getModule(array('id' => $this->invictusKernel->moduleId)));
                $metadata->setFkItemId($item->getId());
                if($config->MA){
                    $metadata->setFkApp($this->invictusKernel->getApp(array('id' => $this->invictusKernel->appId)));
                }
            }
            $this->em->persist($metadata);
            $item->addMetadata($metadata);
        }

        return $item;
    }


    protected function prepareAddTranslations($item)
    {
        foreach( $this->invictusKernel->getUserLanguages($this->invictusKernel->moduleTag) as $language){
            $fkLanguage = $this->getDoctrine()
                ->getRepository('InvictusCmsBundle:Language')
                ->findOneById($language['id']);

            $translation = $this->invictusKernel->getCurrentModule()->getModuleEntityTranslation();

            $translation->setFkLanguage($fkLanguage);
            $item->addTranslation($translation);
        }

        return $item;
    }


    protected function prepareUpdateTranslations($item)
    {
        $module = $this->invictusKernel->getCurrentmodule();
        foreach( $this->invictusKernel->getUserLanguages($this->invictusKernel->moduleTag) as $language){
            $criteria = array(
                'fkBase' => $item->getId(),
                'fkLanguage' => $language['id']
            );
            $translation = $this->getDoctrine()
                ->getRepository($module->getBundleEntityTranslation())
                ->findOneBy($criteria);

            if(!$translation){

                $trans = $this->invictusKernel->getCurrentModule()->getModuleEntityTranslation();

                $translation = $this->invictusKernel->getCurrentModule()->getModuleEntityTranslation();
                $translation->setFkBase($item);
                $translation->setFkLanguage($this->invictusKernel->getLanguage(array('id' => $language['id'])));

            }
            $this->em->persist($translation);
            $item->addTranslation($translation);
        }

        return $item;
    }


    public function forceDownloadAction($fileName)
    {
        //$d = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);
        //$response->headers->set('Content-Disposition', $d);

        $filePath = __DIR__.'/../../../../web/uploads/'.$fileName;
        $response = new BinaryFileResponse($filePath);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);

        return $response;
    }

}
