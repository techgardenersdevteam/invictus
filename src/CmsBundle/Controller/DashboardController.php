<?php

namespace Invictus\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class DashboardController extends InvictusController
{

    public function indexAction()
    {
        $this->init();

        return $this->render('InvictusCmsBundle:Dashboard:module.html.twig');
    }

}
