Table.jqGridOptions.colNames =[
        Translator.trans('actions'),
        Translator.trans('id'),
        Translator.trans('label'),
        Translator.trans('country'),
        Translator.trans('icon', {}, 'language'),
        Translator.trans('enabled')
    ];
Table.jqGridOptions.colModel = [
        {name:'actions', index:'actions', fixed: true, width:60, sortable: false, search: false, align: 'center'},
        {name:'path', index:'base.id', width:70, fixed: true, sortable: false, search: true, align: 'center'},
	    {name:'label', index:'base.label', width:40, search:true, align: 'left'},
        {name:'country', index:'base.country', width:40, search:true, align: 'left'},
        {name:'icon', index:'base.icon', fixed: true, width:70, sortable: false, search: false, align: 'center'},
	    {name:'enabled', index:'base.enabled', fixed: true, width:80, sortable: true, search: false, align: 'center'}
	];

Module.tinyMCE = {
    base: {},
    simple: {
        settings:{
            language: 'en'
        }
    },
    test: {
        target: '#pippo',
        settings:{
            language: 'de'
        }
    }
}

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'app_label': "Translation.validation.required",
	        'app_path': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");



$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'enabled', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'country', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});

