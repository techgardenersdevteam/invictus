$(document).ready(function(){
    
/* CMS TOPBAR - CMS TOPBAR - CMS TOPBAR - CMS TOPBAR - CMS TOPBAR - CMS TOPBAR */
    
    $('#cms-menu-toggler').click(function(event){
        event.preventDefault();
        $(document).trigger('toggleMenu');
    });

    
    $('#app-selector-toggler').click(function(event){
        event.preventDefault();
        $(document).trigger('toggleAppSelector');
    });
    
    
    $('#app-selector').on('click', 'a', function(event){
        event.preventDefault();
        $this = $(this);
        if(!$this.parent().hasClass('sel')){    
            $('#app-selector .sel').removeClass(); 
            $this.parent().addClass('sel');

            var currAppText = $('#app-selector li.sel').text();
            var currAppImg = $('#app-selector li.sel img').prop('src');
            //$('#selected-app').html('<span class="text"><span class="icon"><img src="' + currAppImg + '" alt="' + currAppText + '" /></span>&nbsp;' + currAppText + '</span>');
            $('#selected-app').html('<span class="label">' + currAppText + '</span>');
        
            url = $this.prop('href').split('/').reverse();
            //dir(url);
            
            // needed for later comparison
            oldLocale = CMS.locale;
                     
            CMS.app = url[2];
            CMS.locale = url[1]; // siamo sicuri che sia sempre alla posizione 1 quando è presente #app-selector
            //Module.tag = url[0]; già settato in invictus.html.twig
            Item.id = false;

            if(CMS.userMALTree[Module.tag]['apps'][CMS.app]['languages'][CMS.locale] == undefined)
            {
                CMS.notify({"message":Translator.get('info.forcedLocaleSwitch', { "app": currAppText, "oldLocale": oldLocale, "locale": CMS.locale }), "type":"info", "timeout":10000});
            }

            var currAppLogo = CMS.userMALTree[Module.tag]['apps'][CMS.app]['logo'];
            $('#logo').prop('src', '/bundles/invictuscms/images/apps/' + currAppLogo);

            var localeSelectorLis = '';
            $.each(CMS.userMALTree[Module.tag]['apps'][CMS.app]['languages'], function(key, value) {
                var selLocale = (value.id == CMS.locale) ? 'class="sel"' : '' ;
                localeSelectorLis += '<li ' + selLocale + '><a href="/' + CMS.invictusFolder + '/' + CMS.app + '/' + value.id + '/' + Module.tag + '" class="' + value.id + '"><img src="/bundles/invictuscms/images/flags/' + value.icon + '" alt="' + value.label + '"></a></li>';
            });
            $('#cms-topbar .locale-selector ul').html(localeSelectorLis);
            if(!$('#cms-topbar .locale-selector ul li.sel').length)
            {   
                $('#cms-topbar .locale-selector ul li:eq(0)').addClass('sel');
                CMS.locale = $('#cms-topbar .locale-selector ul li:eq(0) a').prop('href').split('/').reverse()[1];
            }

            $('#cms-edit .locale-selector ul').html(localeSelectorLis);
            if(!$('#cms-edit .locale-selector ul li.sel').length)
            {
                $('#cms-edit .locale-selector ul li:eq(0)').addClass('sel');
            }
            
            $('#cms-edit .heading .button.new a').prop('href', $('#cms-topbar .locale-selector ul li.sel a').prop('href') + '/add');

            $('#app-selector a').each(function(){
                appItemUrl = this.href.split('/').reverse();
                this.href = '/' + CMS.invictusFolder + '/' + appItemUrl[2] + '/' + CMS.locale + '/' + Module.tag;
            });

            $(document).trigger('renderMenu');
            
            if($("#jqGrid").length){
                $("#jqGrid").jqGrid('setGridParam',{url: $('#cms-topbar .locale-selector ul li.sel a').prop('href') + '/tableList'});
                $(document).trigger("reloadTable");
            }else if($("#cms-tree").length){
                $("#cms-tree .jstree").refresh();
            }
            
            $('#cms-edit').removeClass();
            $('#cms-edit .wrapper').empty();

            History.pushState(null, null, $('#app-selector .sel a').attr('href'));
            CMS.setPageTitle();
        }
        
        $('#cms-topbar #app-selector-toggler').trigger('click');
        
    });


    $('#cms-topbar .locale-selector').on('click', 'a', function(event){

        event.preventDefault();
        $this = $(this);

        if(!$this.parent().hasClass('sel')){

            $('#cms-topbar .locale-selector li.sel').removeClass('sel');
            $this.parent().addClass('sel');

            url = $this.prop('href').split('/').reverse();
            //console.dir(url);

            //CMS.app = url[2]; non serve risettare l'app al cambio lingua, viene settato già al cambio app
            CMS.locale = url[1]; // siamo sicuri che sia sempre alla posizione 1 quando è #cms-table .locale-selector
            //Module.tag = url[0]; già settato in invictus.html.twig
            Item.id = false;

            $('#cms-edit .heading .button.new a').prop('href', $this.prop('href') + '/add');

            $('#cms-menu a').each(function(){
                url = $(this).prop('href').split('/').reverse();
                $(this).attr('href', '/invictus/' + CMS.app + '/' + CMS.locale + '/' + url[0] );
            });


            if($("#jqGrid").length){
                $("#jqGrid").jqGrid('setGridParam',{url: $this.prop('href') + '/tableList'});
                $(document).trigger("reloadTable");
                $(document).trigger('renderMenu');
            }else if($("#cms-tree").length){
                var tree = jQuery.jstree._reference("#cms-tree .wrapper");
                tree.refresh();
                $(document).trigger('renderMenu');
            }
        }

        History.pushState(null, null, $('#cms-topbar .locale-selector .sel a').attr('href'));
        CMS.setPageTitle();
        return false;

    });


    $('#cms-table-toggler').click(function(event){
        event.preventDefault();
        $(document).trigger('toggleTable');
    });

/* CMS TOPBAR - CMS TOPBAR - CMS TOPBAR - CMS TOPBAR - CMS TOPBAR - CMS TOPBAR */


/* TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE */

    $('#cms-tree .locale-selector').on('click', 'a', function(event)
    {
        event.preventDefault();
        $this = $(this);
        if(!$this.parent().hasClass('sel'))
        {
            $('#cms-tree .locale-selector li.sel').removeClass('sel');
            $this.parent().addClass('sel');

            url = $this.prop('href').split('/').reverse();
            //console.dir(url);

            //CMS.app = url[2]; non serve risettare l'app al cambio lingua, viene settato già al cambio app
            CMS.locale = url[1]; // siamo sicuri che sia sempre alla posizione 1 quando è presente #cms-tree .locale-selector
            //Module.tag = url[0]; già settato in invictus.html.twig
            Item.id = false;

            $('#cms-edit .heading .button.new a').prop('href', $this.prop('href') + '/add');

            $('#cms-menu a').each(function(){
                url = $(this).prop('href').split('/').reverse();
                $(this).attr('href', '/invictus/' + CMS.app + '/' + CMS.locale + '/' + url[0] );
            });

            var tree = jQuery.jstree._reference("#cms-tree .wrapper");
            tree.refresh();
            $(document).trigger('renderMenu');
        }

        History.pushState(null, null, $('#cms-tree .locale-selector .sel a').attr('href'));
        CMS.setPageTitle();
        return false;
    });

/* TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE - TREE */

    
/* CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT */
        
    $('#cms-edit .heading .button.new').click(function(event){
        event.preventDefault();
        Form.url = $(this).children('a').prop('href');
        $(document).trigger('loadForm');
        $(document).trigger('loadAddForm');
    });

    $('#cms-edit .heading .button.medialibrary').click(function(event){
        event.preventDefault();
        Form.url = $(this).children('a').prop('href');
        $(document).trigger('loadMediaLibrary');
    });

    $('#cms-edit .heading .button.save').click(function(){
        $(document).trigger('saveItem');
        return false;
    });

    $('#cms-edit').on('click', '.action.modify', function(event)
    {
        event.prevenDefault();
        url = this.href();
        var arguments = {
            url : url
        };
        $(document).trigger('loadUpdateForm', [arguments]);
    });

    $('#cms-edit .heading .locale-selector').on('click', 'a', function(event)
    {

        event.preventDefault();
        $this = $(this);
        locale = $this.attr('class');
        if($this.parent().hasClass('sel')){
            $this.parent().removeClass('sel');
            $('#cms-edit .fieldbox.' + locale).not('.widget-wrapper .fieldbox').addClass('hidden');
        }else{
            $this.parent().addClass('sel');
            $('#cms-edit .fieldbox.' + locale).not('.widget-wrapper .fieldbox').removeClass('hidden');
        }

    });
    
    $('#cms-edit').on('click', '.submit', function(event){ 
        $(document).trigger('saveItem');
    });

    $('#cms-edit').on('click', 'input.datepicker', function() {
        //workaround per datepicker 'live'
        //alert('datepicker');
        $(this).datepicker({
            dateFormat: "yy/mm/dd",
            showAnim: "slideDown",
            showOn:'focus'
        }).focus();
    });

    /*
    
    $('.relation .quick-select .suggest input, #visibility-box .quick-select .filter input').live('keypress', function(e){
        if(e.which == 13){
            $(this).parent().find('.rel-search').trigger('click');
            return false;
        }
    });

    $('.relation .results .fieldbox').live('click', function() {
    
        $this = $(this);
        id = $this.prop('id');
        value = $this.prop('rel');
        text = $this.find('a').text();
        moduleId = $this.parent().parent().prop('rel');
        
        //alert(id + '|' + value + '|' + text + '|' + moduleId);
        
        var relationType = 'relations';
        if($this.parent().parent().hasClass('reverse')){
            relationType = 'reverseRelations';
            //alert(relationType);
        }
        
        $this.animate({opacity:0}, 200, function(){
            $this.parent().parent().find('.sortable')
                .append('<li style="border-radius:3px" id="' + id + '">'
                            + '<span class="position">?°</span><span class="label" title="' + text + ' [id:' + value + ']">' + text + '</span>'
                            + '<img class="unselect" title="deseleziona" alt="deseleziona" src="../../../admin/img/small_close.png">'
                            + '<input type="hidden" value="' + value + '" name="' + relationType + '[' + moduleId + '][]" class="hidden">'
                           + '</li>');
            $('#rel-css').append('.results #' + id + '{display:none} ');
            $this.remove();
        });
            
    });
    
    $('.relation .selected .unselect').live('click', function() {
    
        $this = $(this).parent();
        id = $this.prop('id');
        value = $this.find('input').val();
        text = $this.find('span.label').text();
        moduleId = $this.parent().parent().parent().parent().prop('rel');
        //alert(id + '|' + value + '|' + text + '|' + moduleId);
        
        $this.animate({opacity:0}, 500, function(){
            $this.parent().parent().parent().find('.results')
                .append('<div class="fieldbox" id=" + id + " rel="' + value + '">'
                                + '<a href="javascript:void(0)" title="' + text + ' [id:' + value + ']">' + text + '</a>'
                                + '<img src="../../../admin/img/silky/bullet_go.png" alt="add" title="add to selected" />'
                        + '</div>');
            $('#rel-css').append('.results #' + id + '{display:block} ');
            $this.remove();
        });
            
    });
    
    $('.relation .quick-select .selectAll').live('click', function() {
        
        $resultsFieldbox = $(this).parent().parent().find('.results').find('.fieldbox:visible');
        $resultsFieldbox.each(function(index) {
            var addSlow = setTimeout("$resultsFieldbox.eq("+index+").trigger('click');", (750 * index));
        });
        
    });

    $('.relation .quick-select .suggest .rel-search').live('click', function() {
            
            $results = $(this).parent().parent().parent().find('.results');
            
            $('#rel-css').empty();
            $('.sortable li').each(function(){
                //alert('.results #' + $(this).prop('id') + '{display:none} ');
                $('#rel-css').append('.results #' + $(this).prop('id') + '{display:none} ');
            });
            
            $results.css({'background-position': "50px 50px"});
            var searchVal = $(this).parent().find('input').val();
            var app = $(this).data('app');
            var locale = $(this).data('locale');
            var dbTable = $(this).data('db-table');
            $results.animate({opacity:0}, 500, function(){
            $results.empty();            
            $results.css({opacity:1}).html('<i>' + Translator.get('msg.loading') + '</i>');
                $.post('suggestRelations.php', {'search': searchVal, 'app': app, 'locale': locale, 'dbTable': dbTable}, function(data){
                    $results.css({opacity:0}).html(data).animate({opacity:1}, 350);
                });
            });
            
    });
    
    $('.checkbox-list .fieldbox.sel').live('click', function(){
        $(this).removeClass('sel').children('.checkbox').prop('checked', false);
        return false;
    });
    
    $('.checkbox-list .fieldbox:not(.sel)').live('click', function(){
        $(this).addClass('sel').children('.checkbox').prop('checked', true);
        return false;
    });
    
    $('#cms-edit .bool').live('click', function() {
    
        var onValue = ($(this).prop('data-on-value') != undefined) ? $(this).prop('data-on-value') : 1;
        var offValue = ($(this).prop('data-off-value') != undefined) ? $(this).prop('data-off-value') : 0;
                
        if($(this).hasClass('checked')){
            $(this).removeClass('checked').find('input').val(offValue);
        }else{
            $(this).addClass('checked').find('input').val(onValue);
        }
        
        //alert('on:"' + onValue + '" | off:"' + offValue + '" | current:"' + $(this).find('input').val() + '"');
        
    });

    $("#cms-edit input.autocomplete").live("keydown.autocomplete", function(){
        $(this).autocomplete({
            source: $(this).prop('rel'),
            minLength: 2,
            autoFocus: true
        });
    });
*/
    
/* CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT - CMS EDIT */
    
}); // # # # # # #   fine $(document).ready();


$(window).resize(function() {
    $(document).trigger('setTableAutoWidth');
    $(document).trigger('setMenuHeight');
});