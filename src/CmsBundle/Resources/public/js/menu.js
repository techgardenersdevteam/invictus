Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('tag'),
    Translator.trans('menuItem', {}, 'menu'),
    Translator.trans('parent', {}, 'menu'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:70, sortable: false, search: false, align: 'center'},
    {name:'id', index:'menu.id', width:50, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'tag', index:'menu.tag', width:20, search:true, align: 'left'},
    {name:'label', index:'menu.label', width:35, search: true, align: 'left'},
    {name:'parent_label', index:'parent.label', width:20, search: false, sortable: false, align: 'left'},
    {name:'enabled', index:'menu.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Tree.repository = "InvictusCmsBundle:Menu";

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        //'menu_tag': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'tag', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});


Tree.jstreeOptions.contextmenu.items.attach = false;

$(document).bind({
    'loadFormSuccess' : setVisibleFormElements,
    'saveItem' : setNodeToRefresh,
    'itemSaved' : refreshTree
});


function setVisibleFormElements(){
    setNodeToRefresh();
    if($('#menu_parent option:selected').val() == ''){
        $('.fieldbox.link, .fieldbox.itemAttributes, .fieldbox.linkAttributes, .fieldbox.childrenAttributes').css({display:'none'});
        $('.fieldbox.label label').text(Translator.get('menu:menuName'));
    }
    $('#menu_parent').change(function(){
        if($('#menu_parent option:selected').val() == ''){
            $('.fieldbox.link, .fieldbox.itemAttributes, .fieldbox.linkAttributes, .fieldbox.submenuAttributes').css({display:'none'});
            $('.fieldbox.label label').text(Translator.get('menu:menuName'));
        }else{
            $('.fieldbox.link, .fieldbox.itemAttributes, .fieldbox.linkAttributes, .fieldbox.submenuAttributes').css({display:'block'});
            $('.fieldbox.label label').text(Translator.get('menu:menuItem'));
        }
    });
}

function setNodeToRefresh(){
    idToRefresh = $('#menu_parent option:selected').val();
    if(idToRefresh == ''){
        nodeId = false;
    }else{
        nodeId = idToRefresh
    }
    Tree.nodeIdsToRefresh.push(nodeId);
}

function refreshTree(){
    //console.dir(Tree.nodeIdsToRefresh);
    //node = !Tree.nodeIdToRefresh ? '' : $("#cms-tree #m-" + Tree.nodeIdToRefresh).parent().parent();
    var tree = jQuery.jstree._reference("#cms-tree .wrapper");
    tree.refresh($("#cms-tree #m-" + Tree.nodeIdsToRefresh[0]));
    if(Tree.nodeIdsToRefresh[0] != Tree.nodeIdsToRefresh[1]){
        tree.refresh($("#cms-tree #m-" + Tree.nodeIdsToRefresh[1]));
    }
    Tree.nodeIdsToRefresh = new Array();
}
