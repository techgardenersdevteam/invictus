Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('label'),
    Translator.trans('tag'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:70, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:50, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'label', index:'translation.label', width:80, search:true, align: 'left'},
    {name:'tag', index:'base.tag', width:20, search:true, align: 'left'},
    {name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'module_tag': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'tag', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'type', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});


$(document).bind({
    'loadFormSuccess' : setCodemirror
});

