Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('tag'),
    Translator.trans('label'),
    Translator.trans('bundle', {}, 'module'),
    Translator.trans('dbTable', {}, 'module'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:70, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:50, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'tag', index:'base.tag', width:25, search:true, align: 'left'},
    {name:'label', index:'translation.label', width:25, search:true, align: 'left'},
    {name:'bundle', index:'base.bundle', width:25, search:true, align: 'left'},
    {name:'dbTable', index:'base.dbTable', width:25, search:true, align: 'left'},
    {name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Module.tinyMCE = {
    base: {},
    simple: {
        settings:{
            language: 'it'
        }
    },
    test: {
        target: '#pippo',
        settings:{
            language: 'de'
        }
    }
}

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
        'module_tag': "Translation.validation.required"
    };
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'tag', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'bundle', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'dbTable', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});


$(document).bind({
    'loadFormSuccess' : setCodemirror
});

function setCodemirror(){
    $('#module_config').focus(function(){
        var module_config = CodeMirror.fromTextArea(document.getElementById("module_config"), {
            lineNumbers: true,
            mode: {name: "javascript", json: true},
            json: true,
            gutters: ["CodeMirror-lint-markers"],
            lintWith: CodeMirror.jsonValidator
        });
        module_config.on("change", function(cm, change) {
            $('#module_config').text(cm.getValue());
        });
    });

    $('#module_tabsConfig').focus(function(){
        var module_tabsConfig = CodeMirror.fromTextArea(document.getElementById("module_tabsConfig"), {
            lineNumbers: true,
            mode: "application/json",
            gutters: ["CodeMirror-lint-markers"],
            lintWith: CodeMirror.jsonValidator
        });
        module_tabsConfig.on("change", function(cm, change) {
            $('#module_tabsConfig').text(cm.getValue());
        });
    });

}

