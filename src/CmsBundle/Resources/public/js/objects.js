$body = $('body');
$cmsTopbar = $('#cms-topbar');
$cmsContainer = $('#cms-container');
$cmsTable = $('#cms-table');
$cmsEdit = $('#cms-edit');
$cmsSidebar = $('#cms-sidebar');
$cmsMenu = $('#cms-menu');
$cmsTree = $('#cms-tree');
$cmsFooter = $('#cms-footer');


var CMS = {
    app: false,
    locale: false,
    _locale: false,
    action: false,
    sessionLifetime: null, // (sec) sovrascritto in invictus.html.twig nel blocco phpToJs
    reminderLifetime: 120, // (sec)
    tinyMCE: {
        base: {
            target: '.mce',
            settings: {
                language: 'it',
                themes: 'advanced',
                disk_cache: true,
                debug: false,
                mode: "specific_textareas",
                editor_selector: "mceEditor",
                button_tile_map: true,
                theme: "advanced",
                skin: "o2k7",
                skin_variant: "silver",
                width: "100%",
                entity_encoding: "raw",
                statusbar : false,
                plugins: "fullscreen,paste,tabfocus,template,autoresize", //,preview,autoresize,inlinepopups,tabfocus,template,",
                // theme_advanced_buttons1: "bold,italic,link,unlink,forecolor,charmap,styleselect,|,bullist,numlist,undo,redo,|,pastetext,pasteword,|,code,fullscreen",
                theme_advanced_blockformats : "h1,h2,h3,h4",
                theme_advanced_buttons1: "formatselect,bold,italic,link,unlink,bullist,numlist,forecolor,charmap,|,undo,redo,|,pasteword,|,code,fullscreen, syntaxhl", /*,pastetext*/
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                statusbar : false,
                theme_advanced_statusbar_location : "none",
                valid_elements: "a[name|href|class|target|title|rel]" +
                    ",address[style|id|class|rel]" +
                    ",b[style|id|class|rel]" +
                    ",br[style|id|class|rel]" +
                    ",div[style|id|class|rel]" +
                    ",em[style|id|class|rel]" +
                    ",h1[style|id|class|rel]" +
                    ",h2[style|id|class|rel]" +
                    ",h3[style|id|class|rel]" +
                    ",h4[style|id|class|rel]" +
                    ",hr[class|width|size|noshade]" +
                    ",img[class|src|id|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]" +
                    ",li[style|id|class|rel]" +
                    ",ol[style|id|class|rel]" +
                    ",p[style|id|class|rel]" +
                    ",span[class|align|style]" +
                    ",strong[style|id|class|rel]" +
                    ",ul[style|id|class|rel]" +
                    ",iframe[style|id|class|rel|src|frameborder|allowfullscreen|width|height]",
                dialog_type: "modal",
                document_base_url: "/",
                convert_urls: false,
                content_css : "/bundles/invictuscms/css/mce.css",
                style_formats: [
                    {title: 'h1', inline: 'h1'},
                    {title: 'h2', block: 'h2'},
                    {title: 'h3', inline: 'h3'}
                ]
            }
        },
        simple: {
            target: '.simple-mce',
            settings: {
                language: 'it', // usare CMS.locale?
                themes: 'advanced',
                disk_cache: true,
                debug: false,
                mode: "specific_textareas",
                editor_selector: "mce",
                button_tile_map: true,
                theme: "advanced",
                skin: "o2k7",
                skin_variant: "silver",
                width: "100%",
                entity_encoding: "raw",
                plugins: "fullscreen,paste,tabfocus,template,autoresize", //,preview,autoresize,inlinepopups,tabfocus,template,",
                // theme_advanced_buttons1: "bold,italic,link,unlink,forecolor,charmap,styleselect,|,bullist,numlist,undo,redo,|,pastetext,pasteword,|,code,fullscreen",
                theme_advanced_buttons1: "bold,italic,link,unlink,|,undo,redo,|,pasteword",/*,pastetext*/
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location : "none",
                valid_elements: "a[name|href|class|target|title|rel]" +
                    ",address[style|id|class|rel]" +
                    ",b[style|id|class|rel]" +
                    ",br[style|id|class|rel]" +
                    ",div[style|id|class|rel]" +
                    ",em[style|id|class|rel]" +
                    ",h1[style|id|class|rel]" +
                    ",h2[style|id|class|rel]" +
                    ",h3[style|id|class|rel]" +
                    ",h4[style|id|class|rel]" +
                    ",hr[class|width|size|noshade]" +
                    ",img[class|src|id|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]" +
                    ",li[style|id|class|rel]" +
                    ",ol[style|id|class|rel]" +
                    ",p[style|id|class|rel]" +
                    ",span[class|align|style]" +
                    ",strong[style|id|class|rel]" +
                    ",ul[style|id|class|rel]" +
                    ",iframe[style|id|class|rel|src|frameborder|allowfullscreen|width|height]",
                dialog_type: "modal",
                document_base_url: "/",
                content_css : "/bundles/invictuscms/css/mce.css",
                convert_urls: false
            }
        },
        singleLine: {
            target: '.single-line-mce',
            settings: {
                language: 'it', // usare CMS.locale?
                themes: 'advanced',
                disk_cache: true,
                debug: false,
                mode: "specific_textareas",
                editor_selector: "single-line-mce",
                button_tile_map: true,
                theme: "advanced",
                skin: "o2k7",
                skin_variant: "silver",
                width: "100%",
                height: 30,
                force_br_newlines: false,
                force_p_newlines: false,
                force_root_block: false,
                entity_encoding: "raw",
                plugins: "paste,tabfocus,template", //,preview,autoresize,inlinepopups,tabfocus,template,",
                // theme_advanced_buttons1: "bold,italic,link,unlink,forecolor,charmap,styleselect,|,bullist,numlist,undo,redo,|,pastetext,pasteword,|,code,fullscreen",
                theme_advanced_buttons1: "bold,italic,|,undo,redo,|,pasteword",/*,pastetext*/
                theme_advanced_buttons2: "",
                theme_advanced_buttons3: "",
                theme_advanced_toolbar_location: "top",
                theme_advanced_toolbar_align: "left",
                theme_advanced_statusbar_location : "none",
                theme_advanced_resizing_min_height: 30,
                setup: function(ed){
                    // Prevent the return key from starting a new paragraph
                    ed.onKeyDown.add(function(ed, event) {
                        if (event.keyCode == 13)  {
                            event.preventDefault();
                            event.stopPropagation();
                            return false;
                        }
                    });
                },
                init_instance_callback: function( ed ){
                // Work around for TinyMCE's hardcoded min-height
                    $('#'+ed.editorContainer).find('td.mceIframeContainer iframe, table.mceLayout').height(30);
                },
                valid_elements: "strong/b,em/i",
                dialog_type: "modal",
                document_base_url: "/",
                content_css : "/bundles/invictuscms/css/single-line-mce.css",
                convert_urls: false
            }
        }
    },
    initAppSelector: function () {
        console.info('CMS.initAppSelector');

        if ($('#app-selector').length) {
            offset = $('#selected-app').offset();
            w = $('#selected-app').outerWidth();
            bw = $(window).width();
            //console.info(bw);
            right = bw - offset.left - w - 39;
            //$('#selected-app').css({'min-width': $('#app-selector').width() - 25});
            $('#app-selector').css({right: right});
        }

    },
    switchApp: function (event, arguments) {
        console.info(event.type + ' | CMS.switchApp');

        CMS.app = arguments.app;
    },
    toggleAppSelector: function(event){
        console.info(event.type + ' | CMS.toggleAppSelector');
        //alert(parseInt( $('#app-selector').css('top')));
        if( parseInt( $('#app-selector').css('top') ) > 0 ){
            $(document).trigger('hideAppSelector');
        }else{
            $(document).trigger('showAppSelector');
        }
    },
    hideAppSelector: function(){
        console.info('CMS.hideAppSelector');
        apt = $('#app-selector-toggler').children('span');
        $('#app-selector').animate({top: '-' + ($('#app-selector').height() + 10)}, 350, 'easeInOutExpo', function(){
            var arrow = apt.removeClass().addClass('icomoon14-caret-down');
        });
    },
    showAppSelector : function(){
        console.info('CMS.hideAppSelector');
        apt = $('#app-selector-toggler').children('span');
        topbarHeight = $('#cms-topbar').outerHeight();
        $('#app-selector').animate({top: topbarHeight}, 350, 'easeInOutExpo', function(){
            var arrow = apt.removeClass().addClass('icomoon14-caret-up');
        });
    },
    switchLocale: function (event, arguments) {
        console.info(event.type + ' | CMS.switchLocale');

        CMS.locale = arguments.locale;
    },
    notify: function (config) {
        console.info('CMS.notify');

        defaultConfig = {
            'message': 'Notification message not set',
            'type': 'alert', // (success|alert|error)
            'layout': 'top',
            'timeout': 6000
        }
        config = $.extend(defaultConfig, config);

        switch(config.type){
            case 'info':
                icon = 'icomoon14-info-sign'
                break;
            case 'success':
                icon = 'icomoon14-ok-sign'
                break;
            case 'alert':
                icon = 'icomoon14-warning-sign'
                break;
            case 'error':
                icon = ' icomoon14-remove-sign'
                break;
        }

        if(false === config.icon){
            icon = false;
        }

        iconSpan = (icon) ? '<span class="' + icon + '" aria-hidden="true"></span> &nbsp;' : '';

        $.noty.clearQueue();
        $.noty.closeAll();
        noty({
            "text": iconSpan + config.message,
            "layout": config.layout,
            "type": config.type,
            "textAlign": "left",
            "easing": "easeInOutCubic",
            "animateOpen": {"height": "toggle"},
            "animateClose": {"height": "toggle"},
            "speed": 500,
            "timeout": config.timeout,
            "closable": true,
            "closeOnSelfClick": true
        });
    },
    openNestedModule: function(url){

        if($('body').hasClass('opened-menu')){
            rightMargin = 175;
        }else if($('body').hasClass('collapsed-menu')){
            rightMargin = 61;
        }else{
            rightMargin = 25;
        }

        $.fancybox.open(
            [
                {
                    'href': url
                }
            ],
            {
                'type': 'iframe',
                'padding': 0,
                'margin': [26,25,0,rightMargin],
                'autoSize': false,
                'autoCenter': false,
                'scrolling': 'no',
                'width': '100%',
                'height': 430,
                'transitionIn': 'elastic',
                'transitionOut': 'fade',
                'titleShow': false,
                'overlayOpacity': 0.2,
                'hideOnOverlayClick': true
            }
        );
    },
    setFancyIframeHeight: function (height) {
        console.info('CMS.setFancyIframeHeight');
        $('.fancybox-inner').stop().animate({height: height}, 500);
    },
    openFancyImage: function (src, caption) {
        $.fancybox.open(
            [
                {
                    'href': src
                }
            ],
            {
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    overlay : {
                        showEarly : false
                    }
                },
                'padding': 2,
                'autoSize': false,
                'autoCenter': false,
                'scrolling': 'no',
                'width': '100%',
                'height': 430,
                'transitionIn': 'elastic',
                'transitionOut': 'fade',
                'titleShow': true,
                'overlayOpacity': 0.2,
                'hideOnOverlayClick': true
            }
        );
    },
    initLogoutCountdown: function () {
        console.info('CMS.initLogoutCountdown');

        window.clearTimeout(CMS.reminderTimeout);
        delete CMS.reminderTimeout;
        window.clearTimeout(CMS.logoutTimeout);
        delete CMS.logoutTimeout;

        sessionLifetime_ms = CMS.sessionLifetime * 1000;
        reminderLifetime_ms = CMS.reminderLifetime * 1000;
        //vedi https://developer.mozilla.org/en-US/docs/DOM/window.clearTimeout#Example
        CMS.reminderTimeout = setTimeout(function () {
            CMS.remindLogoutCountdown()
        }, (sessionLifetime_ms - reminderLifetime_ms));
        CMS.logoutTimeout = setTimeout(function () {
            CMS.logout()
        }, (sessionLifetime_ms));
    },
    remindLogoutCountdown: function () {
        console.info('CMS.remindLogoutCountdown');

        CMS.notify({"message": Translator.trans('warning.sessionExpiring', {user: User.username}), "timeout": CMS.reminderLifetime * 1000});
    },
    logout: function () {
        console.info('CMS.logout');

        //window.location.href = window.location;
        CMS.notify({"message": Translator.trans('warning.sessionExpired'), "timeout": 9999});
    },
    addToSession: function (param, val) {
        console.info('CMS.addToSession');

        $.post('/' + CMS.invictusFolder + '/add-to-session/' + param + "/" + val);
    },
    redirectFrom: function(event, arguments){
        console.info(event.type + ' | CMS.redirectFrom');

        config = {
            'message' : Translator.trans('warning.redirectFrom', {"from": arguments.from, "to": window.location.pathname }),
            'timeout' : 10000
        }
        CMS.notify(config);
    },
    setPageTitle: function(action){
        title = 'INVICTUS | ' + Module.label;
        if(CMS.app != '' || CMS.locale != '' ){
            title += ' ( ' + CMS.app + ' ' + CMS.locale + ' )';
        }
        if(action != undefined){
            title += ' | ' + action;
        }
        $('title').text(title);

        return title;
    }
};
$(document).bind({
    'initAppSelector' : CMS.initAppSelector,
    'switchApp' : CMS.switchApp,
    'initLocaleSelector' : CMS.initLocaleSelector,
    'switchLocale' : CMS.switchLocale,
    'initLogoutCountdown' : CMS.initLogoutCountdown,
    'redirectFrom' : CMS.redirectFrom,
    'toggleAppSelector' : CMS.toggleAppSelector,
    'hideAppSelector' : CMS.hideAppSelector,
    'showAppSelector' : CMS.showAppSelector
});
//console.dir(CMS);


var Module = {
    id: false,
    tag: false,
    parent: false
};
$(document).bind({
});
//console.dir(Module);


var User = {
    id : null,
    firstName : null,
    lastName : null,
    username : null,
    email: null
};
$(document).bind({
});
//console.dir(User);


var Menu = {
    getStatus: function(){
        console.info('Menu.getStatus');

        return $.jCookies({ get : 'menu' }).status;
    },
    init: function(){
        console.info('Menu.init');

        Table.fadeOut();
        switch(Menu.getStatus())
        {
            case 'hidden':
                $(document).trigger('hideMenu');
                break;
            case 'collapsed':
                $(document).trigger('collapseMenu');
                break;
            default:
                $(document).trigger('openMenu');
        }
        setTimeout(function(){$(document).trigger('setTableAutoWidth');Table.fadeIn();}, 500);
    },
    toggle: function(event){
        console.info(event.type + ' | Menu.toggle');

        Table.fadeOut();
        switch(Menu.getStatus())
        {
            case 'hidden':
                $(document).trigger('collapseMenu');
                break;
            case 'collapsed':
                $(document).trigger('openMenu');
                break;
            default:
                $(document).trigger('hideMenu');
        }
        setTimeout(function(){$(document).trigger('setTableAutoWidth');Table.fadeIn();}, 500);
    },
    hide: function(){
        console.info('Menu.hide');
        $body.removeClass('opened-menu').removeClass('collapsed-menu').addClass('hidden-menu');
        $.jCookies({
            name : 'menu',
            value : {status:"hidden"}
        });
        CMS.addToSession('menuStatus', 'hidden');
    },
    collapse : function(){
        console.info('Menu.collapse');
        $body.removeClass('hidden-menu').removeClass('collapsed-menu').addClass('collapsed-menu');
        $.jCookies({
            name : 'menu',
            value : {status:"collapsed"}
        });
        CMS.addToSession('menuStatus', 'collapsed');
        Menu.setTooltip();
    },
    open : function(){
        console.info('Menu.open');
        $body.removeClass('hidden-menu').removeClass('collapsed-menu').addClass('opened-menu');
        $.jCookies({
            name : 'menu',
            value : {status:"opened"}
        });
        CMS.addToSession('menuStatus', 'opened');
    },
    setHeight: function(){
        console.info('Menu.setHeight');

        height = $(window).height() - $('#logo').height();
        $('#cms-menu nav').height(height);
        //console.warn(height + " - " + $(window).height() + " - " + $('#logo').height() + " - " + $('#cms-topbar').height() );
    },
    render: function(){
        console.info('Menu.render');

        i = 1;
        menuItems = new Array();
        sel = (Module.tag == 'dashboard') ? 'class="sel"' : '' ;
        menuItems[0] = '<li id="menu-1" ' + sel + '><a href="/' + CMS.invictusFolder + '/dashboard"><span class="tipsy-menu icomoon16-home-2" aria-hidden="true" rel="Dashboard"></span><span class="effect" data-hover="Dashboard">Dashboard</span></a></li>';

        /*
        $.each(CMS.userAppsLanguagesModulesTree[CMS.app]['languages'][CMS.locale]['modules'], function(key, value) {
            id = value.id;
            tag = value.tag;
            app = CMS.app;
            locale = CMS.locale;
            assetsFolder = value.assets_folder;
            icon = value.icon;
            label = value['translations']['it_IT']['label'];
            sel = (Module.tag == tag) ? 'class="sel"' : '' ;
            menuItems[i] = '<li id="menu-' + id + '" ' + sel + '><a href="/invictus/' + app + '/' + locale + '/' + tag + '" class="tipsy-menu" rel="' + label + '" title="' + label + '"><span class="' + icon + '" aria-hidden="true"></span>' + label + '</a></li>';
            i++;
        });
        */

        $.each(CMS.userMALTree, function(key, value) {
            if(value.config.module == undefined){
                var notice = {
                    "message" : 'Il modulo “' + value.tag + '” non compare a menu a causa di un\'incompleta configurazione principale'
                }
                CMS.notify(notice);
            }else{
                id = value.id;
                tag = value.tag;
                app = CMS.app;
                locale = CMS.locale;
                assetsFolder = value.assets_folder;
                icon = value.icon;
                config = value.config;
                label = value['translations'][CMS._locale]['label'];
                sel = (Module.tag == tag) ? 'class="sel"' : '' ;
                menuItems[i] = '<li id="menu-' + id + '" ' + sel + '><a href="/' + CMS.invictusFolder;
                menuItems[i] += (config.module.MA) ? ( CMS.app != '') ? '/' + CMS.app : '/' + CMS.lastUsedApp  : '';
                menuItems[i] += (config.module.ML) ? ( CMS.locale != '') ? '/' + CMS.locale : '/' + CMS.lastUsedLocale : '';
                menuItems[i] +=  '/' + tag + '"><span class="tipsy-menu ' + icon + '" aria-hidden="true" rel="' + label + '"></span><span class="effect" data-hover="' + label + '">' + label + '</span></a></li>';
                i++;
                //console.info(value.tag);
            }
        });

        $('#cms-menu ul').html(menuItems.join(''));
    },
    setTooltip: function(){
        $('#cms-menu .tipsy-menu').tipsy({
            className:      'tipsy-menu',
            delayIn: 0,      // delay before showing tooltip (ms)
            delayOut: 50,     // delay before hiding tooltip (ms)
            fade: false,     // fade tooltips in/out?
            fallback: '???',    // fallback text to use when no tooltip text
            gravity: 'w',    // gravity
            html: false,     // is tooltip content HTML?
            live: true,     // use live event support?
            offset: -5,       // pixel offset of tooltip from element
            opacity: 1,    // opacity of tooltip
            title: 'rel',  // attribute/callback containing tooltip text
            trigger: 'hover' // how tooltip is triggered - hover | focus | manual
        });
    }
}
$(document).bind({
    'initMenu' : Menu.init,
    'toggleMenu' : Menu.toggle,
    'openMenu' : Menu.open,
    'collapseMenu' : Menu.collapse,
    'hideMenu' : Menu.hide,
    'setMenuHeight' : Menu.setHeight,
    'renderMenu' : Menu.render
});
//console.dir(Menu);


var Table = {
    simpleSearch: true,
    jqGridOptions: {
        url: '/' + CMS.invictusFolder + '/-app-/-locale-/-module-/tableList',
        datatype: 'json',
        mtype: 'GET',
        gridviev: true, // If set to true we can not use treeGrid, subGrid, or afterInsertRow event.
        autowidth: true,
        forceFit: true,
        shrinkToFit: true,
        height: '100%',
        rownumWidth: 30,
        ignoreCase: true,
        rownumbers: false,
        sortable: false,
        cellLayout: 11,
        autoencode: false,
        pager: '#pager',
        rowNum: 10,
        sortorder: '', //non invio un sortorder di default, l'ordinamento verrà poi gestito dal metodo configureTableList di ogni singolo controller con $this->defaultSortOrder = 'asc|desc';
        altRows: true,
        altclass: 'odd',
        rowList: [10, 25, 50, 100],
        viewrecords: true,
        viewsortcols: [true, 'horizontal', false],/*
        loadtext: Translator.trans('loadText'),
        pgtext: Translator.trans('pgText'),
        recordtext: Translator.trans('recordText'),
        emptyrecords: Translator.trans('emptyRecords'),*/
        beforeSelectRow: function(rowId, e) {
            //alert(e.target);
            if($(e.target).is("span")){
                return false;
            }
            if($(e.target).is("a")){
                return false;
            }
            return true;
        },
        //ondblClickRow: function(rowid){
        onSelectRow: function(rowid){
            Form.url = jQuery('#jqGrid tr#' + rowid + ' .action.edit').prop('href') ;
            if(Form.url != undefined){
                $(document).trigger('loadUpdateForm');
                $(document).trigger('loadForm');
            }else{
                console.warn('Form.url is undefined - object.js line 574');
            }
        },
        loadError: function(xhr, textStatus, errorThrown)  {
            CMS.notify({"message": "Error loading table data", "timeout": 10000});
        }
    },
    getStatus: function(){
        console.info('Table.getStatus');
        status =  $.jCookies({ get : 'table' }).status||'opened';
        console.log(status);
        return status;
    },
    initJqGrid: function(){
        console.info('Table.initJqGrid');

        if($("#jqGrid").length){
            $("#jqGrid")
            .jqGrid(Table.jqGridOptions)
                /*
            .navGrid(
                '#pager',
                {view: false, del: false, edit: false, add: false, search: false, refresh: false},
                {}, // use default settings for edit
                {}, // use default settings for add
                 $('..tipsy').tipsy({
                 delayIn: 150,      // delay before showing tooltip (ms)
                 delayOut: 200,     // delay before hiding tooltip (ms)
                 fade: true,     // fade tooltips in/out?
                 fallback: '???',    // fallback text to use when no tooltip text
                 gravity: 'se',    // gravity
                 html: true,     // is tooltip content HTML?
                 live: true,     // use live event support?
                 offset: 0,       // pixel offset of tooltip from element
                 opacity: 1,    // opacity of tooltip
                 title: 'title',  // attribute/callback containing tooltip text
                 trigger: 'hover' // how tooltip is triggered - hover | focus | manual
                 });          {},  // delete instead that del:false we need this
                {multipleSearch : true} // enable the advanced searching
            )
            */
            .jqGrid('filterToolbar');

            if(CMS.jqSimpleSearch){
                $("#jqGrid").jqGrid(
                    'navButtonAdd',
                    "#pager",
                    {
                        caption: "",
                        title:"Cerca",
                        buttonicon: "ui-icon-search",
                        onClickButton: function(){
                            $("#jqGrid")[0].toggleToolbar();
                        }
                    }
                );
            }

            $("#jqGrid")[0].toggleToolbar();

            // in admin css #cms-table .wrapper ha l'altezza impostata a 270px per migliorare il rendering della pagina.
            // dopo aver inserito jqGrid lo rimetto su auto in modo che possa cambiare in base ai record visualizzati
            //$('#cms-table .wrapper').css({'height':'auto'});
        }
    },
    toggle: function(event){
        console.info(event.type + ' | Table.toggle');
        switch(Table.getStatus())
        {
            case 'collapsed':
                $(document).trigger('openTable');
                break;
            default:
                $(document).trigger('collapseTable');
        }
    },
    open: function(event){
        console.info(event.type + ' | Table.open');

        //$('#cms-table div.wrapper').slideDown(500, 'easeOutCubic');
        $('#cms-table').removeClass().addClass('opened-table');
        $('#cms-topbar .toggler span').removeClass().addClass('icomoon14-chevron-up');
        $.jCookies({
            name : 'table',
            value : { status : "opened"}
        });
    },
    collapse: function(event){
        console.info(event.type + ' | Table.collapse');

        $('#cms-table').removeClass().addClass('collapsed-table');
        $('#cms-topbar .toggler span').removeClass().addClass('icomoon14-chevron-down');
        $.jCookies({
            name : 'table',
            value : { status : "collapsed"}
        });
    },
    fadeIn: function(){
        $("#gbox_jqGrid").css({visibility:'visible'});
    },
    fadeOut: function(){
        $("#gbox_jqGrid").css({visibility:'hidden'});
    },
    reload: function(event){
        console.info(event.type + ' | Table.reload');
        jQuery("#jqGrid").trigger("reloadGrid");
    },
    setAutoWidth: function(event){
        //console.info(event.type + ' | Table.setAutoWidth');
        if($('body').hasClass('table')){
            $("#jqGrid").setGridWidth( $('#cms-table .wrapper').width());
        }
    }
};
$(document).bind({
    'initJqGrid' : Table.initJqGrid,
    'toggleTable' : Table.toggle,
    'openTable' : Table.open,
    'collapseTable' : Table.collapse,
    'reloadTable' : Table.reload,
    'setTableAutoWidth' : Table.setAutoWidth
});
//console.dir(Table);


var Tree = {
    repository: false,
    nodeIdsToRefresh: new Array(),
    themesFolder: "/bundles/invictuscms/libs/jstree/themes/",
    jstreeOptions: {
        core: {
            html_titles: true,
            animation: 350
        },
        plugins: [ "themes", "json_data", "ui", "hotkeys", "cookies", "dnd", "contextmenu", "types" ],
        json_data: {
            "ajax" : {
                // the URL to fetch the data
                "url" : '/' + CMS.invictusFolder + '/get-jstree-children',
                // the `data` function is executed in the instance's scope
                // the parameter is the node being loaded
                // (may be -1, 0, or undefined when loading the root nodes)
                "type": "POST",
                "data" : function (n) {
                    // the result is fed to the AJAX request `data` option
                    return {
                        "repository": function(){
                            if(!Tree.repository){
                                alert("Please set “Tree.repository” var in " + Module.tag + ".js");
                            }

                            return Tree.repository;
                        },
                        "parentId": n.attr ? n.attr("data-id") : '',
                        "appId": CMS.app,
                        'languageId': CMS.locale
                    };
                }
            }
            /*,
            "data" : [
                {
                    "attr" : { "id" : "p-1", "rel": "root" },
                    "data" : "Home",
                    "children": [
                        {
                            "attr" : { "id" : "p-4", "rel": "page" },
                            "data" : "Chi"
                        },
                        {
                            "attr" : { "id" : "p-5", "rel": "page" },
                            "data" : "Cosa"
                        },
                        {
                            "attr" : { "id" : "p-2", "rel": "page" },
                            "data" : {
                                "title" : "Prodotti",
                                "attr" : { "href" : "http://www.google.it" }
                            },
                            "children": [
                                {
                                    "attr" : { "id" : "p-15", "rel": "cat" },
                                    "data" : "Tavoli"
                                },
                                {
                                    "attr" : { "id" : "p-25", "rel": "cat" },
                                    "data" : "Lampade",
                                    "children": [
                                        {
                                            "attr" : { "id" : "p-45", "rel": "prod" },
                                            "data" : "Aura"
                                        },
                                        {
                                            "attr" : { "id" : "p-65", "rel": "prod" },
                                            "data" : "Bell e bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            "progressive_render" : true,
            //"progressive_unload" : true,
            */
        },
        themes: {
            //"url":  "{{ asset('/bundles/invictuscms/libs/jstree/themes/default/style.css') }}",
            "theme" : "invictus",
            "dots" : true,
            "icons" : true
        },
        types: {
            "valid_children" : [ "root" ],
            "types" : {
                "root": {
                    "icon": {
                        "image": "/bundles/invictuscms/images/icons/folder.png",
                        "position": "0 2px"
                    },
                    "valid_children": [ "page", "page_disabled" ],
                    "max_children": -1,
                    "hover_node": false,
                    "select_node": function () {return false;}
                },
                "root_disabled": {
                    "icon": {
                        "image": "/bundles/invictuscms/images/icons/folder_disabled.png",
                        "position": "0 2px"
                    },
                    "valid_children": [ "page", "page_disabled" ],
                    "max_children": -1,
                    "hover_node": false,
                    "select_node": function () {return false;}
                },
                "page": {
                    "icon": {
                        "image": "/bundles/invictuscms/images/icons/layout.png",
                        "position": "0 2px"
                    },
                    "valid_children": [ "page", "page_disabled" ],
                    "max_children": -1,
                    "select_node": function () {return false;}
                },
                "page_disabled": {
                    "icon": {
                        "image": "/bundles/invictuscms/images/icons/layout_disabled.png",
                        "position": "0 2px"
                    },
                    "valid_children": [ "page", "page_disabled" ],
                    "max_children": -1,
                    "select_node": function () {return false;}
                }
            }
        },
        contextmenu: {
            "items": {
                "create" : false,
                "rename" : false,
                "remove" : false,
                "ccp": false,
                "modify" : {
                    // The item label
                    "label"				: "Modify",
                    // The function to execute upon a click
                    "action"			: function (obj) {
                        if(obj.attr('data-id') != undefined){
                            Form.url = CMS.modulePath + '/update/' + obj.attr('data-id');
                            $(document).trigger('loadUpdateForm');
                        }
                    },
                    // All below are optional
                    "_disabled"			: false,		// clicking the item won't do a thing
                    "_class"			: "class",	// class is applied to the item LI node
                    "separator_before"	: false,	// Insert a separator before the item
                    "separator_after"	: true,		// Insert a separator after the item
                    // false or string - if does not contain `/` - used as classname
                    "icon"				: "/bundles/invictuscms/images/icons/bullet_edit.png"
                },
                "attach" : {
                    // The item label
                    "label"				: "Attach",
                    // The function to execute upon a click
                    "action"			: function (obj) { alert("Allega") },
                    // All below are optional
                    "_disabled"			: false,		// clicking the item won't do a thing
                    "_class"			: "class",	// class is applied to the item LI node
                    "separator_before"	: false,	// Insert a separator before the item
                    "separator_after"	: true,		// Insert a separator after the item
                    // false or string - if does not contain `/` - used as classname
                    "icon"				: "/bundles/invictuscms/images/icons/attach.png"
                },
                "delete" : {
                    // The item label
                    "label"				: "Delete",
                    // The function to execute upon a click
                    "action"			: function (obj) {
                        console.dir(obj);
                        Item.confirmRemove(obj.attr('data-id'))
                    },
                    // All below are optional
                    "_disabled"			: false,		// clicking the item won't do a thing
                    "_class"			: "class",	// class is applied to the item LI node
                    "separator_before"	: false,	// Insert a separator before the item
                    "separator_after"	: false,		// Insert a separator after the item
                    // false or string - if does not contain `/` - used as classname
                    "icon"				: "/bundles/invictuscms/images/icons/bullet_cross.png"
                }
            }
        }
    },
    initJstree: function(){
        console.info('Tree.initJstree');

        $.jstree._themes = Tree.themesFolder;
        jQuery("#cms-tree .wrapper")
            .bind("move_node.jstree", function (e, data) {
                /*
                 data.rslt contains:
                 .o - the node being moved
                 .r - the reference node in the move
                 .ot - the origin tree instance
                 .rt - the reference tree instance
                 .p - the position to move to (may be a string - "last", "first", etc)
                 .cp - the calculated position to move to (always a number)
                 .np - the new parent
                 .oc - the original node (if there was a copy)
                 .cy - boolen indicating if the move was a copy
                 .cr - same as np, but if a root node is created this is -1
                 .op - the former parent
                 .or - the node that was previously in the position of the moved node
                 */
                //alert(data.rslt.np.attr("data-id"));

                var parent = '#cms-tree #' + data.rslt.np.attr('id');
                if(parent == '#cms-tree #undefined'){
                    parent = '#cms-tree .jstree';
                    var parentId = null;
                }else{
                    var parentId = $(parent).attr('data-id');
                }
                ids = new Array();
                $(parent + ' > ul > li').each(function(i){
                    ids[i] = $(this).attr('data-id');
                });
                //alert(parent + ids.join('|'));

                $.post('/' + CMS.invictusFolder + '/order-jstree-children', {"repository" : Tree.repository, "parentId": parentId, "ids": ids.join('|'), "appId": CMS.app, "languageId": CMS.locale },function(data){
                    if(data.success){
                        /*
                        if(parent == '#undefined'){
                            node = $('#cms-tree .jstree');
                        }else{
                            node = $('#cms-tree ' + parent);
                        }
                        var tree = jQuery.jstree._reference("#cms-tree .wrapper");
                        tree.refresh(node);
                        */
                    }else{
                        var note = {
                            "type" : "alert",
                            "message" : Translator.trans("error.position"),
                            "timeout" : 6000
                        }
                        CMS.notify(note);
                    }
                })
            })
            .jstree(Tree.jstreeOptions)
            .delegate("a","dblclick", function(e) {
                e.preventDefault();
                $(this).prev().trigger('click');
            })
            .delegate("a","click", function(e) {
                e.preventDefault();
                var idParent = $(this).parent().parent().parent().attr("data-id");
                var id = $(this).parent().attr("data-id");
                //alert(id + ' ' + idParent);
                if(id != undefined){
                    Form.url = CMS.modulePath + '/update/' + id;
                    $(document).trigger('loadUpdateForm');
                }
            });
    }
};
$(document).bind({
    'initJstree' : Tree.initJstree
});
//console.dir(Tree);


var Form = {
    url: false,
    action: false,
    setAction: function(){
       console.info('Form.setAction'); 
       
       Form.action = $('#cms-edit form').prop('action');
    },
    load: function(event){
        console.info(event.type + ' | form.load');
    },
    loadAdd: function( event ){
        console.info(event.type + ' | form.loadAdd');
        
        CMS.action = 'add';        
        $('#cms-edit').addClass('loading');
        
        $('#cms-edit .wrapper').slideUp(300, 'easeOutCubic', function(){
            $('#cms-edit').removeClass().addClass('add loading');
            $.ajax({
                url: Form.url,
                type: 'GET'
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.getResponseHeader("INVICTUS-page") == 'Login'){
                    config = {
                        "message" : Translator.trans("warning.sessionExpired"),
                        "timeout" : 0
                    }
                    CMS.notify(config);
                    $('#cms-edit').removeClass();
                }else{
                    $('#cms-edit .wrapper').html(data);
                    $(document).trigger('loadFormSuccess');

                    $('#cms-edit').removeClass('loading');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                config = {
                    "message" : Translator.trans("error.newItem"),
                    "type": "error"
                }
                CMS.notify(config);
                $('#cms-edit').removeClass();
            })
            .always(function(){
                CMS.initLogoutCountdown();
            });
        });
        
    },
    loadUpdate: function(event){
        console.info(event.type + ' | form.loadUpdate');
        
        CMS.action = 'update';
        $('#cms-edit').addClass('loading');
        $('#cms-edit .wrapper').slideUp(300, 'easeOutCubic', function(){
            $('#cms-edit').removeClass().addClass('update loading');
            $.ajax({
                url: Form.url,
                type: 'GET'
            })
            .done(function(data, textStatus, jqXHR){
                if(jqXHR.getResponseHeader("INVICTUS-page") == 'Login'){
                    config = {
                        "message" : Translator.trans("warning.sessionExpired"),
                        "timeout" : 0
                    }
                    CMS.notify(config);
                    $('#cms-edit').removeClass();
                }else{
                    $('#cms-edit .wrapper').html(data);
                    $(document).trigger('loadFormSuccess');

                    $('#cms-edit').removeClass('loading');
                }
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                config = {
                    "message" : Translator.trans("error.loadingItem"),
                    "type": "error"
                }
                CMS.notify(config);
                $('#cms-edit').removeClass();
            })
            .always(function(){
                CMS.initLogoutCountdown();
            });
            /*
            $.get(Form.url, function(data) {
                if(data){
                    $('#cms-edit .wrapper').html(data);
                    $(document).trigger('loadFormSuccess');
                    Form.setEditingInfo();
                }else{
                    notify(Translator.trans('error.loadingItem'));
                }
                $('#cms-edit').removeClass('loading');
            });
            */
        });
        
    },
    loadSuccess: function(event){
        console.info(event.type + ' | Form.loadSuccess');

        $('#cms-edit .wrapper').slideDown(750, 'easeOutCubic');
        $('#cms-edit input[type="text"]').eq(0).focus();

        if($('#cms-edit .locale-selector').length){
            $('#cms-edit .locale-selector li').removeClass('sel');
            $('#cms-edit .fieldbox.translation').addClass('hidden');
            $('#cms-edit .locale-selector li a.' + CMS.locale).trigger('click');
        }

        Form.setTabs();
        Form.setSelect2();
        Form.setEditingInfo();
        Form.setICheck();
        Form.setTinyMCE();
        Form.setPositionSortable();
        Form.validate();
        //$('#cms-edit .chosen').chosen();
        
    },
    loadMediaLibrary: function( event ){
        console.info(event.type + ' | form.loadMediaLibrary');

        CMS.action = 'add';
        $('#cms-edit').addClass('loading');

        $('#cms-edit .wrapper').slideUp(300, 'easeOutCubic', function(){
            $('#cms-edit').removeClass().addClass('add medialibrary loading');
            $.ajax({
                url: Form.url,
                type: 'GET'
            })
                .done(function(data, textStatus, jqXHR){
                    if(jqXHR.getResponseHeader("INVICTUS-page") == 'Login'){
                        config = {
                            "message" : Translator.trans("warning.sessionExpired"),
                            "timeout" : 0
                        }
                        CMS.notify(config);
                        $('#cms-edit').removeClass();
                    }else{
                        $('#cms-edit .wrapper').html(data);
                        $(document).trigger('loadFormSuccess');

                        $('#cms-edit').removeClass('loading');
                    }
                })
                .fail(function(jqXHR, textStatus, errorThrown){
                    config = {
                        "message" : Translator.trans("error.newItem"),
                        "type": "error"
                    }
                    CMS.notify(config);
                    $('#cms-edit').removeClass();
                })
                .always(function(){
                    CMS.initLogoutCountdown();
                });
        });

    },
    setTabs: function(){
        console.info(' | Form.setTabs');

        $('#cms-edit .wrapper').prepend('<ul class="tabs"></ul>');
        $('#cms-edit fieldset legend').each(function(){
            target = $(this).parent().prop('id');
            text = $(this).text();
            $('#cms-edit .wrapper .tabs').append('<li><a href="#' + target + '">' + text + '</a></li>');
        });
        $('#cms-edit fieldset legend').remove();
        // check if tabs() has already been called
        if ($('#cms-edit .wrapper').data("ui-tabs")){
            $('#cms-edit .wrapper').tabs('destroy');
        }
        $('#cms-edit .wrapper').tabs({fx: { height: 'show', opacity: 'show' }});
        //$('#cms-edit .tabs li a:eq(0)').trigger('click');
    },
    setEditingInfo: function(){
        var modifyText = new Array();
        if($('#cms-edit form :not(hidden) input.modifying-text:visible').length){
            $('#cms-edit form :not(hidden) input.modifying-text:visible').each(function(index){
                modifyText[index] = $(this).val();
            });
        }else{
            modifyText[0] = $('#cms-edit input[type="text"]:visible').eq(0).val();
        }
        $('#cms-edit .button.editing-info .label em').remove();
        $('#cms-edit .button.editing-info .label').append('<em>“' + modifyText.join(' ') + '”</em>');
    },
    setSelect2: function(){

        function formatSelection(object, container) {
            var originalOption = $(object.element);
            return '<div class="' + originalOption.prop('class') + '"><span class="position">' + originalOption.data('position') + '</span> | ' +  object.text + '</div>';
        }

        $('#cms-edit .select2')
            .css({width:'100%'}) // va messo direttamente inline nel tag select per per permettere al js select2 di ridimensionarsi in percentuale
            .select2({
                minimumResultsForSearch: 10,
                allowClear: true
            })

        $('#cms-edit .select2Sortable')
            .css({width:'100%'}) // va messo direttamente inline nel tag select per per permettere al js select2 di ridimensionarsi in percentuale
            .select2({
                minimumResultsForSearch: 10, // numero minimo di opzioni per attivare il search
                allowClear: true,
                formatSelection: formatSelection,
                escapeMarkup: function(m) { return m; }
            })

        $('#cms-edit .select2Sortable')
            .select2Sortable({
                bindOrder: 'sortableStop',
                sortableOptions: {
                    containment: 'parent',
                    items: "li.select2-search-choice",
                    placeholder: '',
                    scroll: true,
                    scrollSensitivity: 1,
                    axis: 'y',
                    revert: false,
                    opacity: 0.5
                }
            });
        $('#widget-relation-selector header, #widget-position-sorter header').on('click', 'img.language', function(){
            var $this = $(this);
            var target = $this.attr('rel');
            var fieldbox = $this.closest('.fieldbox');
            fieldbox.find('img.language').removeClass('sel');
            $this.addClass('sel');
            fieldbox.find('.sorter').css({display:'none'});
            fieldbox.find('.sorter.' + target).css({display:'block'});
        })
    },
    setICheck: function(){
        $('#cms-edit fieldset:not(#visibility) input.checkbox, #cms-edit fieldset:not(#visibility) input.radio').each(function(){
            var self = $(this),
            label = self.prev(),
            label_text = label.text();

            label.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_line-green',
                radioClass: 'iradio_line-blue',
                insert: '<div class="icheck_line-icon"></div>' + label_text
            });
        });
        $('#cms-edit fieldset#visibility input.checkbox, #cms-edit fieldset#visibility input.radio').each(function(){
            var self = $(this),
                label = self.prev(),
                label_text = label.text();

            //label.remove();
            self.iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                //insert: '<div class="icheck_line-icon"></div>' + label_text
            });
        });
        $('#cms-edit fieldset#visibility #widget-visibility-selector').on('click', 'strong', function(){
            $(this).next().find('input').iCheck('check');
        });
    },
    setTinyMCE: function(){
        console.info('Form.initTinyMCE');

        if(Module.tinyMCE){
            for (var config in Module.tinyMCE) {
                if(CMS.tinyMCE[config] != 'undefined'){
                    var data = $.extend(true, CMS.tinyMCE[config], Module.tinyMCE[config]);
                }else{
                    var data = Module.tinyMCE[config];
                }
                $('#cms-edit form ' + data.target ).tinymce(data.settings);
            }
        }else{
            for (var config in CMS.tinyMCE) {
                var data = CMS.tinyMCE[config];
                $('#cms-edit form ' + data.target ).tinymce(data.settings);
            }
        }
    },
    setPositionSortable: function(){
        $('#widget-position-sorter ul.sortable').sortable({
            items: 'li',
            containment: 'parent',
            scroll: true,
            scrollSensitivity: 1,
            axis: 'y',
            revert: true,
            opacity: 0.5,
            tolerance: 'pointer'
        });
        $('#widget-position-sorter ul.sortable').on('click', '.up', function(){
            //alert($(this).prev().length);
            if($(this).parent().prev().length){
                //alert($(this).closest('li')[0].outerHTML);
                $($(this).closest('li')[0].outerHTML).insertBefore($(this).parent().prev());
                $(this).parent().remove();
            }
        });
        $('#widget-position-sorter ul.sortable').on('click', '.down', function(){
            //alert($(this).prev().length);
            if($(this).parent().next().length){
                $($(this).closest('li')[0].outerHTML).insertAfter($(this).parent().next());
                $(this).parent().remove();
            }
        });
    },
    switchTab: function(event){
        console.info(event.type + ' | Form.switchTab');

    },
    validate: function(){
        console.info('Form.validate');

        var validator = $("#cms-edit form").validate({
            //permette di specificare i campi da ignorare
            ignore: '.fieldbox.hidden input, .fieldbox.hidden textarea, .fieldbox.hidden select',
            //evita di prendere il messaggio di errore dall'attributo title
            ignoreTitle: true,
            rules: Form.getValidateRules(),
            messages: Form.getValidateMessages(),
            focusCleanup: false,
            onkeyup: false,
            // settato a false perchè gestito manualmente con highlight()
            focusInvalid: false,
            errorClass: "error",
            highlight: function(element, errorClass){
                $(element).closest('.fieldbox').addClass(errorClass);
              },
            unhighlight: function(element, errorClass){
                $(element).closest('.fieldbox').removeClass(errorClass);
            },
            errorPlacement: function(error, element){
                $(element).closest('.fieldbox').find('ul.errors').remove();
                  $(element).closest('.fieldbox').append("<ul class='errors'><li>" + error.text() + "</li></ul>");
              },
            invalidHandler: function(form){
                var firstErrorFieldbox = $('#cms-edit .fieldbox.error').eq(0);
                var firstErrorTab = firstErrorFieldbox.closest('fieldset').prop('id');
                if($('#cms-edit ul.tabs li').length > 1){
                    $('#cms-edit ul.tabs a[href=#' + firstErrorTab + ']').trigger('click');
                }
                setTimeout(function(){
                    $('html, body').animate({
                        scrollTop: $('#cms-edit .fieldbox.error').eq(0).offset().top - 80
                    }, 500);
                    firstErrorFieldbox.find('input').focus();
                }, 100);
                return false;
            },
            debug:true
        });
    },
    getValidateRules: function(){
        console.info('Form.getValidateRules');
    // function to be overwritten in module.js in Bundle
    },
    getValidateMessages: function(){
        console.info('Form.getValidateMessages');
    // function to be overwritten in module.js in Bundle
    }
};
$(document).bind({
    'loadForm' : Form.load,
    'loadAddForm' : Form.loadAdd,
    'loadUpdateForm' : Form.loadUpdate,
    'loadFormSuccess' : Form.loadSuccess,     
    'switchFormTab' : Form.switchTab,
    'initTinyMCE' : Form.initTinyMCE,
    'loadMediaLibrary' : Form.loadMediaLibrary,
});
//console.dir(Form);


var Item = {
    id: false,
    parent: false,
    save: function(event){ 
        console.info(event.type + ' | Item.save'); 
        
        if(!$('#cms-edit form').valid()){
            return false;
        }
        //event.preventDefault();
        $('#cms-edit').addClass('saving');
        var formData = $('#cms-edit form').toJSON();
        console.dir(formData);
        $(document).trigger('itemDataSent');
        $('#cms-edit form .submit').prop('disabled','disabled').css({opacity:.3}).val(Translator.trans('saving'));
        $.post($('#cms-edit form').prop('action'), formData, function(data) {
            $(document).trigger('itemResponseReceived');
            if(data.type){
                if(data.type == "success"){
                    //$('#cms-edit .wrapper').slideUp(300, 'easeOutCubic', function(){
                        jQuery("#jqGrid").trigger("reloadGrid");
                        //$('#cms-edit').removeClass();
                    //});
                }
                CMS.notify(data);
                $(document).trigger('itemSaved');
            }else{
                data = {
                    'message': Translator.trans('warning.noResponse'),
                    'type': 'alert'
                }
                CMS.notify(data);
                $(document).trigger('itemNotSaved');
            }
            $('#cms-edit .submit').prop('disabled','').css({opacity:1}).val(Translator.trans('save'));
            $('#cms-edit').removeClass('saving');
        });
    },
    add: function(event){ 
        console.info(event.type + ' | Item.confirmRemove');        
    },
    update: function(event){
        console.info(event.type + ' | Item.confirmRemove'); 
        
    },
    confirmRemove: function(id){
        console.info('Item.confirmRemove | ' + id);

        buttons = {};
        buttons[Translator.trans('delete')] = function() {
            Item.remove(id);
        };
        buttons[Translator.trans('cancel')] = function() {
                $(this).dialog("close");
            };
        
        $("#dialog-confirm").dialog({
            resizable: true,
            width:400,
            modal: true,
            position: ['center',180],
            buttons: buttons
        });
        
    },
    remove: function(id){
        console.info('Item.remove | ' + id);

        Item.id = id;

        $("#dialog-confirm").dialog("close");
        jQuery("#jqGrid tr#" + id).animate({opacity:0.3}, 700);

        $.ajax({
            //url: jQuery("#jqGrid tr#" + id + ' a.action.delete').prop('href'),
            url: CMS.modulePath + '/delete/' + id,
            type: 'GET'
        })
        .done(function(data, textStatus, jqXHR){
            if(jqXHR.getResponseHeader("INVICTUS-page") == 'Login'){
                config = {
                    "message" : Translator.trans("warning.sessionExpired"),
                    "timeout" : 0
                }
                CMS.notify(config);
                if(CMS.listType == 'table'){
                    jQuery("#jqGrid tr#" + Item.id).animate({opacity:1}, 700);
                }
            }else{
                if(CMS.listType == 'table'){
                    jQuery("#jqGrid tr#" + Item.id).animate({opacity:0}, 250, function(){
                        $(this).remove();
                    });
                    jQuery("#jqGrid").trigger("reloadGrid");
                }else if(CMS.listType == 'tree'){
                    var tree = jQuery.jstree._reference("#cms-tree .wrapper");
                    tree.refresh();
                }
                CMS.notify(data);
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown){
            config = {
                "message" : Translator.trans("error.deleteItem"),
                "type": "error"
            }
            CMS.notify(config);
        })
        .always(function(){
            CMS.initLogoutCountdown();
        });

        /*
         $.post('/invictus/' + CMS.app + '/' + CMS.locale + '/' + Module.tag +  '/delete/' + id, function(data) {
         if(data.type){
         jQuery("#jqGrid tr#" + id).animate({opacity:0}, 250, function(){
         $(this).remove();
         });
         jQuery("#jqGrid").trigger("reloadGrid");
         $(document).trigger('removeItem');
         CMS.notify(data);
         }else{
         data = {
         'message': Translator.trans('warning.noResponse'),
         'type': 'alert'
         }
         CMS.notify(data);
         jQuery("#jqGrid tr#" + arguments.id).animate({opacity:1}, 350);
         }
         }, 'json');
         */
    },
    loadAttachments: function(event, arguments){
        console.info(event.type + ' | Item.loadAttachments');
        
        Item.arguments.moduleId = arguments.moduleId;
        Item.arguments.itemId = arguments.itemId;
        Item.arguments.label = arguments.label;
        
        $.fancybox({
                    'overlayOpacity':   0.6,
                    'padding':            5,
                    'autoScale':        false,
                    'transitionIn':        'elastic',
                    'transitionOut':     'fade',
                    'overlayColor':     '#333',
                    'hideOnOverlayClick': false,
                    'type':                'iframe',
                    'width':            '100%',
                    'height':            437,
                    'href':                '../attachment/index.php?parentModuleId=' + module + '&amp;parentItemId=' + itemId + '&amp;label=' + label
                });
    },
    setBool: function(bundle, entity, id, field){
        console.info('Item.setBool');
        //alert(url);
        
        $.post('/' + CMS.invictusFolder + '/set-bool/' + bundle + '/' + entity + '/' + id + '/' + field , function(data) {
            if(data.type == 'success'){
                $(document).trigger('reloadTable');
            }else{
                CMS.notify(Translator.trans('error.setBool'), 'error');
            }
        });
    },
    cloneItem: function(itemId){ 
    
    $("#dialog-clone").dialog({
        resizable: true,
        width: 400,
        height: 180,
        modal: true,
        buttons: {
            "Clona": function() {
                
                var moduleId =  $('#cloneModuleId').val();
                var app = 'sys'
                var label = $('#cloneLabel').val();
                
                $.post('controller.php', {action: 'clone', cloneModuleId: moduleId, cloneItemId: itemId, cloneApp: app, cloneLabel: label}, function(data){
                    
                    reloadGrid();
                    notify(data.msg, data.type);
                    
                }, 'json');
                 
                $(this).dialog("close");
                               
            },
            "Annulla": function() {
                $(this).dialog("close");
            }
        }
    });
    
}
};
$(document).bind({
    'saveItem' : Item.save,
    'addItem' : Item.add,
    'updateItem' : Item.update,
    'confirmRemoveItem' : Item.confirmRemove,
    'removeItem' : Item.remove,
    'loadAttachments' : Item.loadAttachments
});
