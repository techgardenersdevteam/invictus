$(document).ready(function(){

    if($('body').hasClass('standard')){
        $(document).trigger('initMenu');
        $(document).trigger('renderMenu');
        $(document).trigger('setMenuHeight');
        $(document).trigger('initAppSelector');
        Menu.setTooltip();
    }

    if($('body').hasClass('table')){
        $(document).trigger('initJqGrid');
    }

    if($('body').hasClass('tree')){
        $(document).trigger('initJstree');
    }

    $(document).trigger('initLogoutCountdown');
    $(document).trigger('initTinyMCE'); // Usefull for non ajax insert/update request
});