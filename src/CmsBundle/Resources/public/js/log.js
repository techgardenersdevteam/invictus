Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('module'),
    Translator.trans('item'),
    Translator.trans('level', {}, 'log'),
    Translator.trans('action'),
    Translator.trans('text'),
    Translator.trans('adminUser'),
    Translator.trans('app'),
    Translator.trans('language'),
    Translator.trans('date')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:60, sortable: false, search: false, align: 'center'},
    {name:'module', index:'base.fkModule', width:50, sortable: true, search: true, align: 'center'},
    {name:'item', index:'base.fkItemId', width:30, sortable: true, search: true, align: 'center'},
    {name:'level', index:'base.level', width:50, search:true, align: 'left'},
    {name:'action', index:'base.action', width:50, search:true, align: 'left'},
    {name:'label', index:'base.label', width:80, sortable: true, search: true, align: 'left'},
    {name:'admin', index:'base.fkAdmin', width:50, sortable: true, search: false, align: 'left'},
    {name:'app', index:'base.fkApp', width:50, sortable: true, search: false, align: 'left'},
    {name:'language', index:'base.fkLanguage', width:50, sortable: true, search: false, align: 'left'},
    {name:'date', index:'base.datetime', width:50, sortable: true, search: false, align: 'left'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'app_label': "Translation.validation.required",
	        'app_path': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");



$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'level', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'action', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'admin', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'app', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'language', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'datetime', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});

