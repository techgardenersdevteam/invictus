Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('label'),
    Translator.trans('name'),
    Translator.trans('typology', {}, 'attachment'),
    Translator.trans('preview'),
    Translator.trans('enabled')
];

Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:70, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:45, fixed: true, sortable: true, search: true, align: 'center'},
    {name:'label', index:'translation.label', width:40, search:true, align: 'left'},
    {name:'filename', index:'base.filename', width:40, search:true, align: 'left'},
    {name:'typology', index:'base.fkAttachmentTypology', width:40, search:true, align: 'left'},
    {name:'preview', index:'preview', width:95, fixed: true, sortable: false, search:false, align: 'center'},
    {name:'enabled', index:'base.enabled', fixed: true, width:85, sortable: true, search: false, align: 'center'}
];

Module.tinyMCE = {
    base: {},
    simple: {
        settings:{
            language: 'en'
        }
    },
    test: {
        target: '#pippo',
        settings:{
            language: 'de'
        }
    }
}

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'module_tag': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'id', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'tag', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'filename', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'typology', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'preview', '', {'text-align':'center'})
            .jqGrid ('setLabel', 'enabled', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});


$(document).bind({
    'loadFormSuccess' : setVisibleFormElements
});


function setVisibleFormElements(){
    type = fileTypes[$('#attachment_fkAttachmentTypology option:selected').val()];
    $('form').removeClass().addClass(type);
    Form.setEditingInfo();
    setFileUpload();
    $('#attachment_fkAttachmentTypology').on('change', function(){
        // vedi views/Attachment/module.html.twig
        type = fileTypes[$('#attachment_fkAttachmentTypology option:selected').val()];
        $('form').removeClass().addClass(type);
        //resetUploadeFile();
    })
}

function setFileUpload()
{
    $('#preview').click(function(){
        $('#attachment_file').trigger('click'); //focus()?
    });

    $('#preview .close').click(function(e){
        e.stopPropagation();
        resetUploadeFile();
    });

    $('#preview .preview').click(function(e){
        e.stopPropagation();
        //alert($('#attachment_ext').val());
        ext = $('#attachment_ext').val();
        filePath = $(this).prop('href');
        if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif'){
            e.preventDefault();
            CMS.openFancyImage(filePath);
        }
    });

    $(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });

    $(document).bind('dragover', function (e) {
        var dropZone = $('#preview'),
            timeout = window.dropZoneTimeout;
        if (!timeout) {
            dropZone.addClass('in');
        } else {
            clearTimeout(timeout);
        }
        var found = false,
            node = e.target;
        do {
            if (node === dropZone[0]) {
                found = true;
                break;
            }
            node = node.parentNode;
        } while (node != null);
        if (found) {
            dropZone.addClass('hover');
        } else {
            dropZone.removeClass('hover');
        }
        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
        }, 100);
    });

    $('#attachment_file')
        .bind('fileuploadadd', function (e, data) {
            $('#preview').addClass('loading').removeClass('loaded');
            $('#progress-bar').css({'width':0});

            //console.log('add');
            var jqXHR = data.submit()
                .success(function (result, textStatus, jqXHR) {
                    //console.log('xxx');
                    //console.log(result);

                    $('#attachment_oldFilename').val(result.oldFilename);
                    $('#oldFilename').val(result.oldFilename);

                    $('#attachment_filename').val(result.filename);
                    $('#filename').val(result.filename);

                    $('#attachment_size').val(result.size);
                    var sizeKB = Math.ceil(result.size/1024 * 100)/100;
                    var size = (sizeKB > 1024) ? Math.ceil(sizeKB/1024 * 100)/100 + ' MB' : sizeKB + ' KB';
                    //alert(result.size + ' - ' + sizeKB + ' - ' + size);
                    $('#size').val(size);

                    $('#attachment_mime').val(result.type);
                    $('#attachment_ext').val(result.ext);
                    $('#mime').val(result.type);
                    if(result.src){
                        $('#preview .preview').prop('href', result.src);
                        $('#preview').css({'background-image':'url(' + result.src + ')'}).prop('title',result.src);
                    }else{
                        $('#preview').css({'background-image':'none'}).prop('title',result.src);
                        $('#preview .preview').prop('href', '/uploads/' + result.filename);
                    }
                })
                .error(function (jqXHR, textStatus, errorThrown) {/* ... */})
                .complete(function (result, textStatus, jqXHR) {/* ... */});
        })
        .bind('fileuploadone', function (e, data) {
            $.each(data.files, function (index, file) {
                alert(file.name + file.size + file.type);
            });
            $('#progress-bar').css({'width':'100%'});
            console.log('done');
        })
        .bind('fileuploafail', function (e, data) {
            $('#progress-bar').css({'width':0});
            console.log('fail');
        })
        .bind('fileuploadalways', function (e, data) {
            $('#preview').removeClass('loading').addClass('loaded');
            $('#progress-bar').css({'width':0});
            console.log('always');
            console.log(data);
        });
    /*
        .bind('fileuploaprogress', function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress').css({'width': progress + '%'});
        });
        /*
        .bind('fileuploadsubmit', function (e, data) {})
        .bind('fileuploadsend', function (e, data) {})
        .bind('fileuploaddone', function (e, data) {})
        .bind('fileuploadfail', function (e, data) {})
        .bind('fileuploadalways', function (e, data) {})
        .bind('fileuploadprogress', function (e, data) {})
        .bind('fileuploadprogressall', function (e, data) {})
        .bind('fileuploadstart', function (e) {})
        .bind('fileuploadstop', function (e) {})
        .bind('fileuploadchange', function (e, data) {})
        .bind('fileuploadpaste', function (e, data) {})
        .bind('fileuploaddrop', function (e, data) {})
        .bind('fileuploaddragover', function (e) {})
        .bind('fileuploadchunksend', function (e, data) {})
        .bind('fileuploadchunkdone', function (e, data) {})
        .bind('fileuploadchunkfail', function (e, data) {})
        .bind('fileuploadchunkalways', function (e, data) {})
        */

    var upload = $('#attachment_file').fileupload({
        url: '/invictus/single-upload',
        type: 'POST',
        dataType: 'json',
        dropZone: $('#preview'),
        pasteZone: $('#preview'),
        formAcceptCharset: 'utf-8',
        sequentialUploads: 'false',
        progressInterval: 500,
        bitrateInterval: 1000,
        formData: {"attachment[_token]":$('#attachment__token').val()},
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress-bar').css({'width': progress + '%'});
        }
    });
}


function resetUploadeFile(){
    $('#preview').removeClass('loaded').css({'background-image':'url(/bundles/invictuscms/images/icons/instant_preview_add.png)'});
    $('#oldFilename').val('');
    $('#attachment_oldFilename').val('');
    $('#attachment_oldFilename').prop('title','');
    $('#attachment_filename').val('');
    $('#filename').val('');
    $('#size').val('');
    $('#attachment_size').val('');
    $('#attachment_ext').val('');
    $('#mime').val('');
    $('#attachment_mime').val('');
    $('#attachment_alt').val('');
    $('#attachment_title').val('');
}