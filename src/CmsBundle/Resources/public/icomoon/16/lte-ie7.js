/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon16\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icomoon16-switch' : '&#xe000;',
			'icomoon16-home' : '&#xe001;',
			'icomoon16-home-2' : '&#xe002;',
			'icomoon16-stack' : '&#xe003;',
			'icomoon16-locked' : '&#xe004;',
			'icomoon16-eye' : '&#xe005;',
			'icomoon16-layers' : '&#xe006;',
			'icomoon16-checkmark' : '&#xe007;',
			'icomoon16-check-alt' : '&#xe008;',
			'icomoon16-x' : '&#xe009;',
			'icomoon16-x-altx-alt' : '&#xe00a;',
			'icomoon16-plus' : '&#xe00b;',
			'icomoon16-plus-alt' : '&#xe00c;',
			'icomoon16-minus' : '&#xe00d;',
			'icomoon16-minus-alt' : '&#xe00e;',
			'icomoon16-list' : '&#xe00f;',
			'icomoon16-spinner' : '&#xe010;',
			'icomoon16-spinner-2' : '&#xe011;',
			'icomoon16-spinner-3' : '&#xe012;',
			'icomoon16-spinner-4' : '&#xe013;',
			'icomoon16-spinner-5' : '&#xe014;',
			'icomoon16-menu' : '&#xe015;',
			'icomoon16-star' : '&#xe016;',
			'icomoon16-star-2' : '&#xe017;',
			'icomoon16-star-3' : '&#xe018;',
			'icomoon16-heart' : '&#xe019;',
			'icomoon16-heart-2' : '&#xe01a;',
			'icomoon16-paragraph-justify' : '&#xe01b;',
			'icomoon16-plus-2' : '&#xe01c;',
			'icomoon16-minus-2' : '&#xe01d;',
			'icomoon16-cancel' : '&#xe01e;',
			'icomoon16-eye-2' : '&#xe01f;',
			'icomoon16-magnifier' : '&#xe020;',
			'icomoon16-star-4' : '&#xe021;',
			'icomoon16-checkmark-2' : '&#xe022;',
			'icomoon16-cancel-2' : '&#xe023;',
			'icomoon16-pencil' : '&#xe024;',
			'icomoon16-edit' : '&#xe025;',
			'icomoon16-compose' : '&#xe026;',
			'icomoon16-cancel-3' : '&#xe027;',
			'icomoon16-trash-alt' : '&#xe028;',
			'icomoon16-trash' : '&#xe029;',
			'icomoon16-cancel-circle' : '&#xe02a;',
			'icomoon16-locked-2' : '&#xe02b;',
			'icomoon16-locked-3' : '&#xe02c;',
			'icomoon16-disk' : '&#xe02d;',
			'icomoon16-floppy' : '&#xe02e;',
			'icomoon16-comments' : '&#xe030;',
			'icomoon16-chat' : '&#xe031;',
			'icomoon16-comments-2' : '&#xe02f;',
			'icomoon16-stack-2' : '&#xe032;',
			'icomoon16-icons' : '&#xe033;',
			'icomoon16-box' : '&#xe034;',
			'icomoon16-drawer' : '&#xe035;',
			'icomoon16-cabinet' : '&#xe036;',
			'icomoon16-puzzle' : '&#xe037;',
			'icomoon16-users' : '&#xe038;',
			'icomoon16-newspaper' : '&#xe039;',
			'icomoon16-calendar' : '&#xe03a;',
			'icomoon16-stack-3' : '&#xe03b;',
			'icomoon16-key' : '&#xe03c;',
			'icomoon16-attachment' : '&#xe03d;',
			'icomoon16-attachment-2' : '&#xe03e;',
			'icomoon16-paperclip' : '&#xe03f;',
			'icomoon16-pictures' : '&#xe040;',
			'icomoon16-pictures-2' : '&#xe041;',
			'icomoon16-pictures-3' : '&#xe042;',
			'icomoon16-file-pdf' : '&#xe043;',
			'icomoon16-file-zip' : '&#xe044;',
			'icomoon16-file-word' : '&#xe045;',
			'icomoon16-file-excel' : '&#xe046;',
			'icomoon16-file' : '&#xe047;',
			'icomoon16-vimeo' : '&#xe048;',
			'icomoon16-youtube' : '&#xe049;',
			'icomoon16-image' : '&#xe04a;',
			'icomoon16-film' : '&#xe04b;',
			'icomoon16-map' : '&#xe04d;',
			'icomoon16-play' : '&#xe04e;',
			'icomoon16-image-2' : '&#xe04f;',
			'icomoon16-vimeo2' : '&#xe050;',
			'icomoon16-vimeo-2' : '&#xe051;',
			'icomoon16-youtube-2' : '&#xe052;',
			'icomoon16-hyperlink' : '&#xe053;',
			'icomoon16-file-2' : '&#xe055;',
			'icomoon16-file-3' : '&#xe054;',
			'icomoon16-folder' : '&#xe056;',
			'icomoon16-link' : '&#xe057;',
			'icomoon16-file-4' : '&#xe058;',
			'icomoon16-file-5' : '&#xe059;',
			'icomoon16-music' : '&#xe05a;',
			'icomoon16-film-2' : '&#xe05b;',
			'icomoon16-popup' : '&#xe05c;',
			'icomoon16-zip' : '&#xe05d;',
			'icomoon16-picture' : '&#xe05e;',
			'icomoon16-camera' : '&#xe05f;',
			'icomoon16-cloud' : '&#xe060;',
			'icomoon16-equalizer' : '&#xe061;',
			'icomoon16-wrench' : '&#xe062;',
			'icomoon16-menu-2' : '&#xe063;',
			'icomoon16-arrow-up-alt1' : '&#xe064;',
			'icomoon16-attachment-3' : '&#xe065;',
			'icomoon16-info' : '&#xe067;',
			'icomoon16-info-2' : '&#xe068;',
			'icomoon16-layout' : '&#xe06a;',
			'icomoon16-layout-2' : '&#xe069;',
			'icomoon16-download' : '&#xe06b;',
			'icomoon16-tree' : '&#xe06c;',
			'icomoon16-globe' : '&#xe06d;',
			'icomoon16-globe-2' : '&#xe06e;',
			'icomoon16-notebook' : '&#xe06f;',
			'icomoon16-bookmark' : '&#xe070;',
			'icomoon16-notebook-2' : '&#xe071;',
			'icomoon16-book' : '&#xe072;',
			'icomoon16-list-2' : '&#xe073;',
			'icomoon16-paperclip-2' : '&#xe066;',
			'icomoon16-marker' : '&#xe074;',
			'icomoon16-pin' : '&#xe075;',
			'icomoon16-pin-2' : '&#xe076;',
			'icomoon16-pin-alt' : '&#xe077;',
			'icomoon16-location' : '&#xe078;',
			'icomoon16-location-2' : '&#xe079;',
			'icomoon16-minus-3' : '&#xe07a;',
			'icomoon16-plus-3' : '&#xe07b;',
			'icomoon16-map-2' : '&#xe04c;',
			'icomoon16-user' : '&#xe07c;',
			'icomoon16-profile' : '&#xe07d;',
			'icomoon16-expand' : '&#xe07e;',
			'icomoon16-move' : '&#xe07f;',
			'icomoon16-notes' : '&#xe081;',
			'icomoon16-cog' : '&#xe080;',
			'icomoon16-cogs' : '&#xe082;',
			'icomoon16-inbox' : '&#xe083;',
			'icomoon16-signup' : '&#xe084;',
			'icomoon16-add' : '&#xe085;',
			'icomoon16-plus-4' : '&#xe086;',
			'icomoon16-iconmonstr-save-9-icon' : '&#xe088;',
			'icomoon16-download-2' : '&#xe089;',
			'icomoon16-download-3' : '&#xe08a;',
			'icomoon16-download-4' : '&#xe08b;',
			'icomoon16-monstr-save-7' : '&#xe087;',
			'icomoon16-plus-5' : '&#xe08c;',
			'icomoon16-checkmark-3' : '&#xe08d;',
			'icomoon16-checkmark-4' : '&#xe08e;',
			'icomoon16-checkmark-5' : '&#xe08f;',
			'icomoon16-pencil-2' : '&#xe090;',
			'icomoon16-cancel-4' : '&#xe091;',
			'icomoon16-pencil-3' : '&#xe092;',
			'icomoon16-pencil-4' : '&#xe093;',
			'icomoon16-pen-alt-stroke' : '&#xe094;',
			'icomoon16-pencil-5' : '&#xe095;',
			'icomoon16-in-alt' : '&#xe096;',
			'icomoon16-question' : '&#xe097;',
			'icomoon16-question-2' : '&#xe098;',
			'icomoon16-info-3' : '&#xe099;',
			'icomoon16-bullhorn' : '&#xe09a;',
			'icomoon16-phone' : '&#xe09b;',
			'icomoon16-phone-hang-up' : '&#xe09c;',
			'icomoon16-support' : '&#xe09d;',
			'icomoon16-paragraph-left' : '&#xe09e;',
			'icomoon16-read-more' : '&#xe09f;',
			'icomoon16-type' : '&#xe0a0;',
			'icomoon16-grid' : '&#xe0a1;',
			'icomoon16-direction' : '&#xe0a2;',
			'icomoon16-location-3' : '&#xe0a3;',
			'icomoon16-analytics' : '&#xe0a4;',
			'icomoon16-grid-2' : '&#xe0a5;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icomoon16-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};