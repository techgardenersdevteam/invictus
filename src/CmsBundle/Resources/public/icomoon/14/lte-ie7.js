/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon14\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icomoon14-heart' : '&#xe000;',
			'icomoon14-star' : '&#xe001;',
			'icomoon14-star-empty' : '&#xe002;',
			'icomoon14-search' : '&#xe003;',
			'icomoon14-repeat' : '&#xe004;',
			'icomoon14-refresh' : '&#xe005;',
			'icomoon14-ok' : '&#xe006;',
			'icomoon14-remove' : '&#xe007;',
			'icomoon14-off' : '&#xe008;',
			'icomoon14-cog' : '&#xe009;',
			'icomoon14-home' : '&#xe00a;',
			'icomoon14-tag' : '&#xe00b;',
			'icomoon14-align-justify' : '&#xe00c;',
			'icomoon14-list' : '&#xe00d;',
			'icomoon14-map-marker' : '&#xe00e;',
			'icomoon14-check' : '&#xe00f;',
			'icomoon14-chevron-left' : '&#xe010;',
			'icomoon14-chevron-right' : '&#xe011;',
			'icomoon14-plus-sign' : '&#xe012;',
			'icomoon14-minus-sign' : '&#xe013;',
			'icomoon14-remove-sign' : '&#xe014;',
			'icomoon14-ok-sign' : '&#xe015;',
			'icomoon14-question-sign' : '&#xe016;',
			'icomoon14-info-sign' : '&#xe017;',
			'icomoon14-plus' : '&#xe018;',
			'icomoon14-minus' : '&#xe019;',
			'icomoon14-exclamation-sign' : '&#xe01a;',
			'icomoon14-eye-open' : '&#xe01b;',
			'icomoon14-eye-close' : '&#xe01c;',
			'icomoon14-warning-sign' : '&#xe01d;',
			'icomoon14-heart-empty' : '&#xe01e;',
			'icomoon14-comments' : '&#xe01f;',
			'icomoon14-facebook-sign' : '&#xe020;',
			'icomoon14-twitter-sign' : '&#xe021;',
			'icomoon14-bar-chart' : '&#xe022;',
			'icomoon14-chevron-down' : '&#xe023;',
			'icomoon14-chevron-up' : '&#xe024;',
			'icomoon14-comment' : '&#xe025;',
			'icomoon14-twitter' : '&#xe026;',
			'icomoon14-facebook' : '&#xe027;',
			'icomoon14-rss' : '&#xe028;',
			'icomoon14-reorder' : '&#xe029;',
			'icomoon14-paper-clip' : '&#xe02a;',
			'icomoon14-globe' : '&#xe02b;',
			'icomoon14-wrench' : '&#xe02c;',
			'icomoon14-caret-down' : '&#xe02d;',
			'icomoon14-caret-up' : '&#xe02e;',
			'icomoon14-caret-left' : '&#xe02f;',
			'icomoon14-caret-right' : '&#xe030;',
			'icomoon14-sort-down' : '&#xe031;',
			'icomoon14-sort-up' : '&#xe032;',
			'icomoon14-sitemap' : '&#xe033;',
			'icomoon14-bolt' : '&#xe034;',
			'icomoon14-angle-left' : '&#xe035;',
			'icomoon14-angle-right' : '&#xe036;',
			'icomoon14-angle-up' : '&#xe037;',
			'icomoon14-angle-down' : '&#xe038;',
			'icomoon14-spinner' : '&#xe039;',
			'icomoon14-save' : '&#xf0c7;',
			'icomoon14-pencil' : '&#xf040;',
			'icomoon14-download-alt' : '&#xf019;',
			'icomoon14-download' : '&#xf01a;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icomoon14-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};