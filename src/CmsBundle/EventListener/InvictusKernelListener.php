<?php
/*
 * This file is part of the INVICTUS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace Invictus\CmsBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Invictus\CmsBundle\Extensions\InvictusKernel;

class InvictusKernelListener
{
    protected $InvictusKernel;

    public function __construct(InvictusKernel $InvictusKernel)
    {
        $this->InvictusKernel = $InvictusKernel;
    }


    public function onRequestEvent(Event $event)
    {
        $request = $event->getRequest();

        // Viene richiamato SOLO nelle richieste principali
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType() ) {
            return;
        }

        /**
         * List of routes to be skipped
         *
         * @var array
         */
        $skip_routes = array(
            "_wdt",
            "_profiler",
            "_assetic_",
            "fos_js_routing",
            "bazinga_exposetranslation_js",
            "invictus_login",
            "invictus_login_redirect",
            "invictus_add_to_session",
            "invictus_set_bool",
            "liip_imagine_filter_runtime",
            "liip_imagine_filter",
            "invictus_habitat_import",
            "n2ktag_api_img_generator"
        );

        // Viene richiamato SOLO nelle rotte che NON matchano l'array $skip_routes
        if (preg_match('/^('.implode('|', $skip_routes).')/', $request->get('_route'))) {
            return;
        }

        // Evita che InvictusKernel venga inizializzato se la richiesta riguarda un asset.
        // L'htacess standard di Symfony fa servire direttamente ad Apache,
        // ma se per caso un file non esiste viene cmq fatta una richiesta al frontcontroller
        // e quindi viene richiamato anche questo listener
        if (preg_match('%\.(css|js|gif|jpe?g|png)$%i', $request->server->get('REQUEST_URI'))) {
            $notFoundHttpException = new NotFoundHttpException($request->server->get('REQUEST_URI').' not found');
            throw $notFoundHttpException;
        }

        // Richiamo l'init di InvictusKernel per iniettargli il kernel di Symfony2
        $this->InvictusKernel->init($event);
    
    }

}