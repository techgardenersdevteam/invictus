<?php
  
namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AttachmentTypologyTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'label',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                ),
                'translation_domain' => 'attachment'

            )
        );

        $builder->add('abstract', 'text', array(
                'label' => 'abstract',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'body',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'mce'
                )
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\AttachmentTypology'
            )
        );

    }

    public function getName()
    {
        return 'attachment_typology_translation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\AttachmentTypologyTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
