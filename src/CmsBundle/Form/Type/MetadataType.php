<?php
  
namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MetadataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('slug', 'text', array(
                'label' => 'slug',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('path', 'text', array(
                'label' => 'path',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('pageTitle', 'text', array(
                'label' => 'pageTitle',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('metaKeywords', 'textarea', array(
                'label' => 'metaKeywords',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('metaDescription', 'textarea', array(
                'label' => 'metaDescription',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('customMeta', 'textarea', array(
                'label' => 'customMeta',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('customVars', 'textarea', array(
                'label' => 'customVars',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('redirectUrl', 'text', array(
                'label' => 'redirectUrl',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('redirectCode', 'choice', array(
                'label' => 'redirectCode',
                //'empty_value' => 'undefined',
                //'empty_data'  => null,
                'required' => false,
                'choices'=>array(
                    '' => 'undefined',
                    300 => '300 Multiple Choices',
                    301 => '301 Moved Permanently',
                    302 => '302 Found',
                    303 => '303 See Other',
                    304 => '304 Not Modified',
                    305 => '305 Use Proxy',
                    307 => '307 Temporary Redirect'
                ),
                'attr'   =>  array(
                    'class' => 'select2',
                    'style' => 'width:100%' //necessario per ridimensionare select2
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('changeFreq', 'choice', array(
                'label' => 'changeFreq',
                //'empty_value' => 'undefined',
                //'empty_data'  => null,
                'required' => false,
                'choices'=>array(
                    '' => 'undefined',
                    "always" => "always",
                    "hourly" => "hourly",
                    "daily" => "daily",
                    "weekly" => "weekly",
                    "monthly" => "monthly",
                    "yearly" => "yearly",
                    "never" => "never"
                ),
                'attr'   =>  array(
                    'class' => 'select2',
                    'style' => 'width:100%' //necessario per ridimensionare select2
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('priority', 'choice', array(
                'label' => 'priority',
                //'empty_value' => 'undefined',
                //'empty_data'  => null,
                'required' => false,
                'choices'=>array(
                    '' => 'undefined',
                    '0.0' => '0.0',
                    '0.1' => '0.1',
                    '0.2' => '0.2',
                    '0.3' => '0.3',
                    '0.4' => '0.4',
                    '0.5' => '0.5',
                    '0.6' => '0.6',
                    '0.7' => '0.7',
                    '0.8' => '0.8',
                    '0.9' => '0.9',
                    '1.0' => '1.0',
                ),
                'attr'   =>  array(
                    'class' => 'select2',
                    'style' => 'width:100%' //necessario per ridimensionare select2
                ),
                'translation_domain' => 'metadata'
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkModule', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Module'
            )
        );

        $builder->add('fkItemId', 'hidden', array(
            )
        );

    }

    public function getName()
    {
        return 'metadata';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\Metadata',
            'invictusKernel' => null
        ));
    }
    
}
