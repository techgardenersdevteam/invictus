<?php
  
namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ModuleTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'label',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('abstract', 'text', array(
                'label' => 'abstract',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'body',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                )
            )
        );

        $builder->add('path', 'text', array(
                'label' => 'path',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Module'
            )
        );

    }

    public function getName()
    {
        return 'moduletranslation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\ModuleTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
