<?php
  
namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AttachmentTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'label',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                ),
                'translation_domain' => 'attachment'

            )
        );

        $builder->add('alt', 'text', array(
                'label' => 'alt',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'attachment'

            )
        );

        $builder->add('title', 'text', array(
                'label' => 'title',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'attachment'

            )
        );

        $builder->add('abstract', 'text', array(
                'label' => 'abstract',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'body',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'mce'
                )
            )
        );

        $builder->add('path', 'text', array(
                'label' => 'path',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'attachment'
            )
        );

        $builder->add('link', 'text', array(
                'label' => 'link',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'attachment'
            )
        );

        $builder->add('code', 'textarea', array(
                'label' => 'code',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'attachment'
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Attachment'
            )
        );

    }

    public function getName()
    {
        return 'attachmenttranslation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\AttachmentTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
