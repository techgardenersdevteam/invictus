<?php

namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;


class ModuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
            'label' => 'enabledDisabled',
            'required' => false,
            'attr'   =>  array(
                    'class' => 'checkbox'
            )                                        )
        );

        $builder->add('tag', 'text', array(
            'label' => 'tag',
            'attr'   =>  array(
                )
            )
        );

        /*
        $builder->add('fkTemplate', 'entity' , array(
                'class' => 'InvictusTemplateBundle:Template',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('m')
                        ->innerJoin('m.translations', 'trans', 'WITH', 'trans.fkLanguage = :locale')
                        ->innerJoin('trans.fkLanguage', 'lang', 'WITH', 'lang.id = :locale')
                        ->andWhere('m.deleted = 0')
                        ->orderBy('m.id', 'ASC')
                        ->setParameter('locale', 'it_IT');
                    return $qb;
                },
                'empty_value' => 'none',
                'empty_data' => null,
                'label' => 'template',
                'required' => false,
                'translation_domain' => 'module',
                'attr'   =>  array(
                    'class' => 'select2',
                )
            )
        );
        */

        $builder->add('icon', 'text', array(
                'label' => 'icon',
                'attr'   =>  array(
                )
            )
        );

        $builder->add('dbTable', 'text', array(
                'label' => 'dbTable',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('listRows', 'text', array(
                'label' => 'listRows',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('listType', 'choice', array(
                'label' => 'listType',
                //'empty_value' => 'undefined',
                //'empty_data'  => null,
                'required' => true,
                'choices'=>array(
                    'table' => 'Table',
                    'tree' => 'Tree'
                ),
                'attr'   =>  array(
                    'class' => 'select2',
                    'style' => 'width:100%' //necessario per ridimensionare select2
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('config', 'textarea', array(
                'label' => 'config',
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('tabsConfig', 'textarea', array(
                'label' => 'tabsConfig',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('bundle', 'text', array(
                'label' => 'bundle',
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('controller', 'text', array(
                'label' => 'controller',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('views', 'text', array(
                'label' => 'views',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('entity', 'text', array(
                'label' => 'entity',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('formType', 'text', array(
                'label' => 'formType',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('css', 'text', array(
                'label' => 'css',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('js', 'text', array(
                'label' => 'js',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'module'
            )
        );

        $builder->add('deleted', 'hidden', array(
                                        'label' => 'deleted',
                                        'required' => false,
                                        'attr' =>   array(
                                                        'value' => 0
                                                    )
                                        )
        );

        $builder->add('translations', 'collection', array('type' => new ModuleTranslationType() ));
    }

    public function getName()
    {
        return 'module';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\Module',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
