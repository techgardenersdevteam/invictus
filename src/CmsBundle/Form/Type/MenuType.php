<?php

namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                        'class' => 'checkbox'
                )
            )
        );

        /*
        $builder->add('parent', 'entity' , array(
                'required' => false,
                'class' => 'InvictusCmsBundle:Menu',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) use($options){
                    $qb = $er->createQueryBuilder('m')
                        ->addSelect('trans')
                        ->innerJoin('m.translations', 'trans')
                        ->innerJoin('trans.fkLanguage', 'lang')
                        ->andWhere('m.enabled = 1')
                        ->andWhere('m.deleted = 0')
                        ->andWhere('trans.fkLanguage = :locale')
                        ->orderBy('trans.label', 'ASC')
                        ->setParameter('locale', $options['UILanguageId']);
                    //echo "<br /><br />".$qb->getDQL();
                    //echo "<br /><br />".$qb->getQuery()->getSQL();
                    return $qb;
                },
                //'empty_value' => 'newMenu',
                //'empty_data' => null,
                'label' => 'parent',
                'translation_domain' => 'menu',
                'attr'   =>  array(
                    'class' => 'select2',
                )
            )
        );
        */


        $builder->add('parent', 'entity' , array(
                'required' => false,
                'class' => 'InvictusCmsBundle:Menu',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) use($options){
                    $qb = $er->createQueryBuilder('m')
                        ->addSelect('trans')
                        ->innerJoin('m.translations', 'trans')
                        ->innerJoin('trans.fkLanguage', 'lang')
                        //->andWhere('m.enabled = 1')
                        ->andWhere('m.deleted = 0')
                        //->andWhere('trans.fkLanguage = :locale')
                        ->orderBy('trans.label', 'ASC');
                        //->setParameter('locale', $options['UILanguageId']);
                    //echo "<br /><br />".$qb->getDQL();
                    //echo "<br /><br />".$qb->getQuery()->getSQL();
                    return $qb;
                },
                'empty_value' => 'newMenu',
                'empty_data' => null,
                'label' => 'parent',
                'translation_domain' => 'menu',
                'attr'   =>  array(
                    'class' => 'select2',
                )
            )
        );

        $builder->add('tag', 'text', array(
            'label' => 'tag',
            'attr'   =>  array(
                )
            )
        );

        $builder->add('itemAttributes', 'text', array(
                'label' => 'itemAttributes',
                'required' => false,
                'attr'   =>  array(
                ),
                'label_attr' => array(
                    'raw' => 'true'
                ),
                'translation_domain' => 'menu'
            )
        );

        $builder->add('linkAttributes', 'text', array(
                'label' => 'linkAttributes',
                'required' => false,
                'attr'   =>  array(
                ),
                'label_attr' => array(
                    'raw' => 'true'
                ),
                'translation_domain' => 'menu'
            )
        );

        $builder->add('childrenAttributes', 'text', array(
                'label' => 'childrenAttributes',
                'required' => false,
                'attr'   =>  array(
                ),
                'label_attr' => array(
                    'raw' => 'true'
                ),
                'translation_domain' => 'menu'
            )
        );

        $builder->add('deleted', 'hidden', array(
            'label' => 'deleted',
            'required' => false,
            'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new MenuTranslationType() ));
    }

    public function getName()
    {
        return 'menu';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\Menu',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
