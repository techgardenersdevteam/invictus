<?php

namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AttachmentTypologyType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('tag', 'text', array(
                'label' => 'tag',
                'required' => false,
                'attr'   =>  array(
                    //'disabled' => true
                )
            )
        );

        $builder->add('icon', 'text', array(
                'label' => 'icon',
                'required' => false,
                'translation_domain' => 'attachment_typology',
                'attr'   =>  array(
                    //'disabled' => true
                )
            )
        );

        $builder->add('type', 'choice', array(
                'label' => 'type',
                'required' => true,
                'translation_domain' => 'attachment_typology',
                'attr'   =>  array(
                   'class' => 'select2'
                ),
                'choices'=>array(
                    'uncategorized' => 'Uncategorized',
                    'file' => 'File',
                    'image' => 'Image',
                    'link' => 'Link',
                    'video' => 'Video',
                    'zip' => 'Zip',
                    'code' => 'Code',
                    'music' => 'Music',
                    'panorama' => 'Panorama'
                ),
            )
        );

        $builder->add('allowedTypes', 'text', array(
                'label' => 'allowedTypes',
                'required' => false,
                'translation_domain' => 'attachment_typology',
                'attr'   =>  array(
                    //'disabled' => true
                )
            )
        );

        $builder->add('customJs', 'text', array(
                'label' => 'customJs',
                'required' => false,
                'translation_domain' => 'attachment_typology',
                'attr'   =>  array(
                    //'disabled' => true
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
            'label' => 'deleted',
            'required' => false,
            'attr' =>   array(
                'value' => 0
                )
            )
        );

        $builder->add('translations', 'collection', array('type' => new AttachmentTypologyTranslationType() ));
    }

    public function getName()
    {
        return 'attachment_typology';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\AttachmentTypology',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
