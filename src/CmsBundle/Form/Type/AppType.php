<?php
  
namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AppType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
            'label' => 'enabledDisabled',
            'required' => false,
            'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('id', 'text', array(
                'label' => 'id'
            )
        );

        $builder->add('label', 'text', array(
            'label' => 'label',
            'attr'   =>  array(
                'class' => 'modifying-text'
                )
            )
        );

        $builder->add('position', 'text', array(
                'label' => 'position',
                'attr'   =>  array(
                ),
                'translation_domain' => 'app'
            )
        );

        $builder->add('icon', 'text', array(
            'label' => 'icon',
            'required' => true,
            'attr'   =>  array(
                )
            )
        );

        $builder->add('logo', 'text', array(
            'label' => 'logo',
            'required' => true,
            'attr'   =>  array(
                ),
            'translation_domain' => 'app'
            )
        );
        
        $builder->add('highlighted', 'checkbox', array(
            'label' => 'default',
            'required' => false,
            'attr'   =>  array(
                    'class' => 'checkbox star'
                )
            )
        );
        
        $builder->add('deleted', 'hidden', array(
            'label' => 'deleted',
            'required' => false,
            'attr' =>   array(
                            'value' => 0
                        )
            )
        );
    }

    public function getName()
    {
        return 'app';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\App',
            'validation_groups' => array(),
            'invictusKernel' => null
        ));
    }
    
}
