<?php

namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Finder\Finder;

class AttachmentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('fkAttachmentTypology', 'entity' , array(
                /*
                'class' => 'InvictusCmsBundle:AttachmentTypologyTranslation',
                'property' => 'fkAttachmentTypology.icon',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('trans')
                        ->innerJoin('trans.fkAttachmentTypology', 'at')
                        ->where('trans.fkLanguage = :locale')
                        ->andwhere('at.enabled = 1')
                        ->andwhere('at.deleted = 0')
                        ->orderBy('at.id', 'ASC')
                        ->setParameter('locale', 'it_IT');
                },
                */
                'class' => 'InvictusCmsBundle:AttachmentTypology',
                'property' => 'translations[1].label',
                'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('at')
                        ->innerJoin('at.translations', 'trans', 'WITH', 'trans.fkLanguage = :locale')
                        ->innerJoin('trans.fkLanguage', 'lang', 'WITH', 'lang.id = :locale')
                        ->andWhere('at.enabled = 1')
                        ->andWhere('at.deleted = 0')
                        ->orderBy('at.id', 'ASC')
                        ->setParameter('locale', 'it_IT');
                    //echo "<br /><br />".$qb->getDQL();
                    //echo "<br /><br />".$qb->getQuery()->getSQL();
                    return $qb;
                },
                'empty_value' => 'choose',
                'label' => 'typology',
                'translation_domain' => 'attachment',
                'attr'   =>  array(
                    'class' => 'select2',
                )

                /*
                'choices' => function(){
                    return array(
                        'test' => 'pippo',
                        'ss' => 'pdddippo'
                    );
                }
                */
            )
        );


        $builder->add('fkModule', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Module'
            )
        );


        $builder->add('fkItemId', 'hidden', array(
            )
        );


        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('file', 'file', array(
                'label' => 'file',
                'translation_domain' => 'attachment',
                'required' => false,
                'attr'   =>  array(

                )
            )
        );

        $builder->add('oldFilename', 'hidden', array(
                'label' => 'oldFilename',
                'required' => false,
                'attr'   =>  array(
                    //'disabled' => true
                ),
                'translation_domain' => 'attachment'
            )
        );

        $builder->add('filename', 'hidden', array(
                'label' => 'filename',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'attachment'
            )
        );

        $builder->add('size', 'hidden', array(
                'label' => 'size',
                'required' => false,
                'attr'   =>  array(
                    //'disabled' => true
                ),
                'translation_domain' => 'attachment'
            )
        );

        $builder->add('ext', 'hidden', array(
                'label' => 'ext',
                'required' => false,
                'attr'   =>  array(
                    //'disabled' => true
                ),
                'translation_domain' => 'attachment'
            )
        );

        $builder->add('mime', 'hidden', array(
                'label' => 'mime',
                'required' => false,
                'translation_domain' => 'attachment',
                'attr'   =>  array(
                    //'disabled' => true
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
            'label' => 'deleted',
            'required' => false,
            'attr' =>   array(
                'value' => 0
                )
            )
        );

        $builder->add('translations', 'collection', array('type' => new AttachmentTranslationType() ));
    }

    public function getName()
    {
        return 'attachment';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\Attachment',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
