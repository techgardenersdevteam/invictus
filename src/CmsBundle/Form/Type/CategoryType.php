<?php

namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                        'class' => 'checkbox'
                )
            )
        );

        $builder->add('parent', 'entity' , array(
                'required' => false,
                'class' => 'InvictusCmsBundle:Category',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) use($options){
                    $qb = $er->createQueryBuilder('m')
                        ->addSelect('trans')
                        ->innerJoin('m.translations', 'trans')
                        ->innerJoin('trans.fkLanguage', 'lang')
                        //->andWhere('m.enabled = 1')
                        ->andWhere('m.deleted = 0')
                        //->andWhere('trans.fkLanguage = :locale')
                        ->orderBy('trans.label', 'ASC');
                        //->setParameter('locale', $options['UILanguageId']);
                    //echo "<br /><br />".$qb->getDQL();
                    //echo "<br /><br />".$qb->getQuery()->getSQL();
                    return $qb;
                },
                'empty_value' => 'newCategory',
                'empty_data' => null,
                'label' => 'parent',
                'translation_domain' => 'category',
                'attr'   =>  array(
                    'class' => 'select2',
                )
            )
        );

        $builder->add('tag', 'text', array(
            'label' => 'tag',
            'attr'   =>  array(
                )
            )
        );

        $builder->add('customVars', 'text', array(
                'label' => 'customVars',
                'required' => false,
                'attr'   =>  array(
                ),
                'label_attr' => array(
                    'raw' => 'true'
                ),
                'translation_domain' => 'category'
            )
        );

        $builder->add('deleted', 'hidden', array(
            'label' => 'deleted',
            'required' => false,
            'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new CategoryTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'category';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\Category',
            'invictusKernel' => null
        ));
    }

}
