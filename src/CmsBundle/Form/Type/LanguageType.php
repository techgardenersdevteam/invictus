<?php
  
namespace Invictus\CmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LanguageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
            'label' => 'enabledDisabled',
            'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );
        $builder->add('label', 'text', array(
            'label' => 'language',
            'attr'   =>  array(
                'class' => 'modifying-text'
                )
            )
        );
        $builder->add('id', 'text', array(
            'label' => 'id'
            )
        );
        $builder->add('icon', 'text', array(
            'label' => 'icon',
            'required' => false,
            'attr'   =>  array(
                )
            )
        );

        $builder->add('country', 'text', array(
                'label' => 'country',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('ISO639', 'text', array(
                'label' => 'ISO639',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('ISO3166', 'text', array(
                'label' => 'ISO3166',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
            'label' => 'deleted',
            'required' => false,
            'attr' =>   array(
                            'value' => 0
                        )
            )
        );
    }

    public function getName()
    {
        return 'language';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\CmsBundle\Entity\Language',
            'invictusKernel' => null
        ));
    }
    
}
