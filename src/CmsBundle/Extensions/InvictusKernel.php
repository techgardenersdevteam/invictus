<?php
/**
* This file is part of the INVICTUS project
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @copyright 2012-2013 techgardeners | devsign
* @author Matteo Poile <matteo.poile@devsign.it>
* @author Roberto Beccaceci <roberto@beccaceci.it>
* @package InvictusCmsBundle\Extensions\InvictusKernel.php
*/
 
namespace Invictus\CmsBundle\Extensions;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class InvictusKernel
{

    private $container = null;
    private $session = null;
    private $em = null;
    private $router = null;
    public $request = null;

    //private $isInit = false;            // Indica se la classe è stata inizializzata
    //private $metaParams = null;         // Contiene un oggetto che incapsula i parametri
    //private $metadata = null;           // Contiene l'oggetto Metatada per la pagina corrente

    private $requestAttributes = null;  // Contiene un oggetto con gli attributi della request, serve per recuperare dati dopo un forward che crea una subrequest
    private $GETParams = null;          // Contiene un oggetto idratato con $_GET[], serve per recuperare dati dopo un forward che crea una subrequest
    private $POSTParams = null;         // Contiene un oggetto idratato con $_POST[], serve per recuperare dati dopo un forward che crea una subrequest


    public $invictusFolder = '';
    public $requestedUri = null;                // Contiene l'uri della pagina richiamata
    public $uri = null;                // Contiene l'uri della pagina richiamata
    public $isDebug = false;
    public $environment = null;
    public $isXmlHttpRequest = false;  // Indica se la richiesta è stata fatta via ajax (non è attendibile al 100% se non si usa jquery o qualche altra libreria famosa)

    public $userALMTree = array();
    public $userMALTree = array();
    public $userLanguages = array();

    public $app = null;                // l'oggetto App corrente
    public $language = null;           // l'oggetto Language corrente
    public $UILanguage = null;         // l'oggetto UILanguage scelto dall'utente loggato
    public $module = null;             // l'oggetto Module per la pagina corrente
    public $moduleTranslation = null;  // l'oggetto Module Translation per la pagina corrente
    public $user = null;               // l'oggetto User corrente
    public $item = null;               // l'oggetto Item corrente
    public $template = null;           // l'oggetto Template corrente
    public $metadata = null;           // l'oggetto Metadata corrente

    public $appId = null;               // stringa contenente l'id dell'app corrente
    public $languageId = null;          // stringa contenente l'id del language corrente
    public $UILanguageId = null;        // stringa contenente l'id del UILanguage dell'utente loggato
    public $moduleId = null;            // stringa contenente l'id del language corrente
    public $moduleTag = null;           // stringa contenente l'id del language corrente
    public $modulePath = null;          // stringa contenente stringa del tipo /invictus/app/locale/module dove app e locale sono opzionali
    public $itemId = false;             // l'id dell'item corrente
    public $parentModuleId = false;     // l'id del modulo padre
    public $parentModuleTag = false;    // il tag del modulo padre
    public $parentItemId = false;       // l'id dell'item padre
    public $layout = 'standard';        // tipo di layout della pagina
    public $invictusSessionLifetime = null;     // indica la durata della sessione del BE in secondi. Viene settato in initBE(). default 3600s
    public $side = null;                // stringa 'BE' o 'FE' per capire se sono nel front-end o nel backend


    
    /**
    * Costruttore del metodo a cui viene passato l'intero contenitore dei servizi da cui recuperare request e routing
    * 
    * @param ContainerInterface $container
    * @return CMSManager
    */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
    }

    
    /**
    *  Inizializza l'oggetto
    *  viene richiamato dall'evento Invictus\CmsBundle\Listener
    * 
    * @param \Symfony\Component\HttpFoundation\Request $request
    */
    public function init(Event $event)
    {

        $this->request = $event->getRequest();
        $this->router = $this->container->get('router'); // Instanzio l'oggetto per la gestione delle rotte
        $this->session = $this->container->get('session');
        $this->isDebug = $this->container->getParameter('kernel.debug');
        $this->environment = $this->container->getParameter('kernel.environment');


        $this->invictusFolder = $this->container->getParameter('invictus_folder');
        $this->isXmlHttpRequest = $this->request->isXmlHttpRequest();
        $this->requestedUri = $this->request->getRequestUri();
        $this->uri = $this->request->getUri();
        $this->side = (stristr($this->requestedUri, $this->invictusFolder) == false) ? 'FE' : 'BE';
        if($this->side == 'BE'){
            $this->initBE();
        }else{
            $this->initFE();
        }
        //$this->dump($this->container->parameters, false, true );
     }    
     
     
    public function initFE()
    {

        $this->setRequestAttributes($this->request->attributes->all());

        $this->setGETParams($this->request->query->all());

        $this->setPOSTParams($this->request->request->all());


        $defaultApp = 'web';

        $this->setRequestAttribute('app', $defaultApp);

        //$languageId = $this->getRequestAttribute('_locale', 'it_IT');
        /**
         * Se nella richiesta è specificata un app ricavo i suoi dati facendo filtro con il campo enabled e deleted
         */
        $this->app = $this->getApp(array('id' => $this->getRequestAttribute('app', null), 'enabled' => true, 'deleted' => false));
        $this->appId = ($this->app) ? $this->app->getId() : null;

        /**
         * Se nella richiesta è specificato un locale ricavo i suoi dati facendo filtro con il campo enabled e deleted
         *
         */
        $this->language = $this->getLanguage(array('id' => $this->getRequestAttribute('_locale', null), 'enabled' => true, 'deleted' => false));
        $this->languageId = ($this->language) ? $this->language->getId() : null;

        // basandosi su $this->appId, $this->languageId e path_and_slug setta $this->moduleId e $this->itemId
        //$this->dump($this->getRequestAttributes());
        //die();
        $this->setModuleIdAndItemIdFromPathAndSlug($this->getRequestAttribute('path_and_slug', null));

        $this->module = $this->getModule(array('id' => $this->moduleId, 'enabled' => true, 'deleted' => false));

        $this->item = $this->getItem(array('id' => $this->itemId, 'enabled' => true, 'deleted' => false));

        if(!is_null($this->item)){
            $this->template = $this->getTemplate(array('id' => $this->item->getFkTemplate(), 'enabled' => true, 'deleted' => false));
        }

     }
     
     
    /**
    * Initialize invictusKernel for the back-end
    *
    */
    private function initBE()
    {
        $session_lifetime = $this->container->getParameter("invictus_session_lifetime");

        $this->invictusSessionLifetime = (isset($session_lifetime))
                                        ? $this->container->getParameter("invictus_session_lifetime")
                                        : 3600;
        $this->checkSessionExpiration();

        $this->setRequestAttributes($this->request->attributes->all());

        $this->setGETParams($this->request->query->all());

        $this->setPOSTParams($this->request->request->all());


        $this->parentModuleTag = $this->getRequestAttribute('parentModuleTag', false);
        if($this->parentModuleTag){
            $parentModule = $this->em
                ->getRepository('InvictusCmsBundle:Module')
                ->findOneBy(
                    array(
                        'tag' => $this->parentModuleTag,
                        'enabled' => true,
                        'deleted' => false
                    )
                );
            $this->parentModuleId = $parentModule->getId();
        }
        $this->parentItemId = $this->getRequestAttribute('parentItemId', null);

        $this->layout = $this->getRequestAttribute('layout', 'standard');

        /**
         * Recupero l'utente loggato
         */
        $this->user = $this->container->get('security.context')->getToken()->getUser();


        $this->userALMTree = $this->em->getRepository('InvictusCmsBundle:Setting')
            ->getALMTree(array(
                    //'app' => 'vs',
                    //'locale' => 'it_IT',
                    //'module' => $this->getRequestAttribute('module'),
                    'user' => $this->user->getId(),
                    //'uiLocale' => 'en_GB' //$this->getRequestAttribute('_locale')
                ),
                'ALM'
            );

        $this->userMALTree = $this->em->getRepository('InvictusCmsBundle:Setting')
            ->getMALTree(array(
                    //'app' => 'vs',
                    //'locale' => 'it_IT',
                    //'module' => $this->getRequestAttribute('module'),
                    'user' => $this->user->getId(),
                    //'uiLocale' => 'en_GB' //$this->getRequestAttribute('_locale')
                ),
                'MAL'
            );

        $this->userLanguages = $this->getUserLanguages();
        //$this->dump($this->userMALTree['news']['apps']['op']['languages'], true, true);

        if( !isset($this->userMALTree[$this->getRequestAttribute('moduleTag')]) ){
            $this->setRequestAttribute('moduleTag', 'dashboard');
        }

        /**
         * Salvo in sessione l'ultima e l'ultimo language usati
         */
        if($this->getRequestAttribute('app')){
            $this->session->set('lastUsedApp', $this->getRequestAttribute('app'));
        }else{
            $app = ($this->session->has('lastUsedApp')) ? $this->session->get('lastUsedApp') : $this->user->getDefaultApp()->getId();
            $this->session->set('lastUsedApp', $app);
        }

        if($this->getRequestAttribute('locale')){
            $this->session->set('lastUsedLocale', $this->getRequestAttribute('locale'));
        }else{
            $locale = ($this->session->has('locale')) ? $this->session->get('locale') : $this->user->getDefaultLanguage()->getId();
            $this->session->set('lastUsedLocale', $locale);
        }



        /**
        * Se nella richiesta è specificata un app ricavo i suoi dati facendo filtro con il campo enabled e deleted
        */
        $this->app = $this->getApp(array('id' => $this->getRequestAttribute('app', null), 'enabled' => true, 'deleted' => false));
        $this->appId = ($this->app) ? $this->app->getId() : null;


        
        /**
        * Se nella richiesta è specificato un locale ricavo i suoi dati facendo filtro con il campo enabled e deleted
        *
        */
        $this->language = $this->getLanguage(array('id' => $this->getRequestAttribute('locale', null), 'enabled' => true, 'deleted' => false));
        $this->languageId = ($this->language) ? $this->language->getId() : null;


        /**
         * ricavo i dati del UILanguage dalle preferenze dell'utente loggato
         *
         */
        $this->UILanguage = $this->getLanguage(array('id' => $this->user->getUILanguage()->getId(), 'enabled' => true, 'deleted' => false));
        $this->UILanguageId = $this->UILanguage->getId();

        
        /**
        * ricavo i dati del module della richiesta solo se è attivo e non eliminato
        * 
        */
        $this->module = $this->getModule(array('tag' => $this->getRequestAttribute('moduleTag'), 'enabled' => true, 'deleted' => false));
        $this->moduleId = $this->module->getId();
        $this->moduleTag = $this->module->getTag();
        $this->modulePath = $this->generateModulePath();
        /*
        foreach($this->userMALTree[$this->moduleTag]['apps'] AS $key => $value){
            foreach($value['languages'] AS $language ){
                echo $value['id'].' - '.$language['id'].'<br>';
            }
        }
        die();
        */

        //$this->dump($this->getRequestAttributes(), false, true);
        //$this->dump($this->userMALTree, false, true);
        //$this->dump($this->getRequestAttribute('app', null), true, false);
        //$this->dump($this->appId, true, false);
        //$this->dump($this->languageId, true, true);
        //$this->dump($this->getRequestAttribute('module'), false, false);
        //$this->dump($this->module->getTag(), true, true);


        if(!$this->isGrantedUserAppLanguageModule($this->getRequestAttribute('app', null), $this->getRequestAttribute('locale', null), $this->getRequestAttribute('moduleTag') ))
        {
            $redirectUrl = $this->suggestRedirectUrl($this->getRequestAttribute('app', null), $this->getRequestAttribute('locale', null), $this->getRequestAttribute('moduleTag'));
            //die($redirectUrl);
            $this->redirect($redirectUrl);
        }
        $this->setModuleTranslation($this->module->getId(),  $this->user->getUILanguage()->getId());
    }


    public function getEnvironment()
    {
        return $this->environment;
    }


    public function isDebug()
    {
        return $this->isDebug;
    }


    public function getSide()
    {
        return $this->side;
    }


    public function getUserALMTree($json = false)
    {
        if($json)
        {
            return json_encode($this->userALMTree);
        }
        return $this->userALMTree;
    }


    public function getUserMALTree($json = false)
    {
        if($json)
        {
            return json_encode($this->userMALTree);
        }
        return $this->userMALTree;
    }


    public function getUserLanguages($moduleTag = false, $appId = false, $json = false)
    {
        if(!$moduleTag and $appId){
            die('InvictusKernel->getUserLanguages: if you set $appId you must set also $moduleTag');
        }

        $languages = array();
        //$this->dump($this->userMALTree, true, true);
        if($moduleTag){
            if($appId){
                foreach($this->userMALTree[$moduleTag]['apps'][$appId]['languages'] as $key => $language){
                    $languages[$key] = $language;
                }
            }else{
                foreach($this->userMALTree[$moduleTag]['apps'] as $key => $app){
                    foreach($app['languages'] as $key => $language){
                        $languages[$key] = $language;
                    }
                }
            }
        }else{
            if($this->userALMTree){
                foreach($this->userALMTree as $app){
                    foreach($app['languages'] as $key => $language){
                        unset($language['modules']);
                        $languages[$key] = $language;
                    }
                }
            }
        }

        if($json)
        {
            return json_encode($languages);
        }
        return $languages;
    }


    public function getUserModules($json = false)
    {
        $modules = array();

        foreach($this->userMALTree as $key => $module){
            unset($module['apps']);
            $modules[$key] = $module;
        }

        if($json)
        {
            return json_encode($modules);
        }
        return $modules;
    }


    public function getUserApps($json = false)
    {
        $apps = array();

        foreach($this->userALMTree as $key => $app){
            unset($app['languages']);
            $apps[$key] = $app;
        }

        if($json)
        {
            return json_encode($apps);
        }
        return $apps;
    }


    public function setRequestAttributes($attributes)
    {   
        $this->requestAttributes = new \stdClass();
        foreach($attributes as $key => $value){
            $this->requestAttributes->$key = $value;
        }

        if(!isset($this->requestAttributes->layout) OR empty($this->requestAttributes->layout)){
            $this->requestAttributes->layout = 'standard';
        }
    }


    public function getRequestAttributes()
    {
        return $this->requestAttributes;         
    }


    public function setRequestAttribute($key, $value)
    {
        $this->requestAttributes->$key = $value;
    }


    public function getRequestAttribute($attribute, $default = false)
    {
        return (isset($this->requestAttributes->$attribute) AND !empty($this->requestAttributes->$attribute)) ? $this->requestAttributes->$attribute : $default;
    }


    public function setGETParams($params)
    {   
        $this->GETParams = new \stdClass();
        foreach($params as $key => $value){
            $this->GETParams->$key = $value;
        }
    }


    public function getGETParams()
    {
        return $this->GETParams;
    }


    public function getGETParam($param, $default = false)
    {
        return (isset($this->GETParams->$param) AND !empty($this->GETParams->$param)) ? $this->GETParams->$param : $default;
    }


    public function setPOSTParams($params)
    {   
        $this->POSTParams = new \stdClass();
        foreach($params as $key => $value){
            $this->POSTParams->$key = $value;
        }
    }


    public function getPOSTParams()
    {
        return $this->POSTParams;         
    }


    public function getPOSTParam($param, $default = false)
    {
        return (isset($this->POSTParams->$param) AND !empty($this->POSTParams->$param)) ? $this->POSTParams->$param : $default;
    }


    public function getCurrentApp($json = false)
    {
        if($json)
        {
            return json_encode($this->app);
        }
        
        return $this->app;
    }


    public function getApp($fieldsValuesArray = array())
    {
        return $this->em
            ->getRepository('InvictusCmsBundle:App')
            ->findOneBy($fieldsValuesArray);
    }


    public function getCurrentLanguage($json = false)
    {
        if($json)
        {
            return json_encode($this->language);
        }
        
        return $this->language;
    }


    public function getLanguage($fieldsValuesArray = array())
    {

        return $this->em
            ->getRepository('InvictusCmsBundle:Language')
            ->findOneBy($fieldsValuesArray);
    }

    /*
    public function getUILanguage($json = false)
    {
        if($json)
        {
            return json_encode($this->UILanguage);
        }

        return $this->UILanguage;
    }
    */

  
    public function getCurrentModule($json = false)
    {
        if($json)
        {
            return json_encode($this->module);
        }
        
        return $this->module;
    }


    public function setCurrentModule($fieldsValuesArray)
    {
        $this->module = $this->getModule($fieldsValuesArray);
    }


    public function getModule($fieldsValuesArray)
    {
        return $this->em
            ->getRepository('InvictusCmsBundle:Module')
            ->findOneBy($fieldsValuesArray);
    }


    public function getMetadata($fieldsValuesArray)
    {
        return $this->em
            ->getRepository('InvictusCmsBundle:Metadata')
            ->findOneBy($fieldsValuesArray);
    }


    public function getItem($fieldsValuesArray)
    {
        return $this->em
            ->getRepository('InvictusPageBundle:Page')
            ->findOneBy($fieldsValuesArray);
    }


    public function getTemplate($fieldsValuesArray)
    {
        return $this->em
            ->getRepository('InvictusTemplateBundle:Template')
            ->findOneBy($fieldsValuesArray);
    }


    public function getModuleByTag($moduleTag, $enabled = true, $deleted = false )
    {
        return $this->em
            ->getRepository('InvictusCmsBundle:Module')
            ->findOneBy(
                array(
                    'tag' => $moduleTag,
                    'enabled' => $enabled,
                    'deleted' => $deleted
                )
            );
    }


    public function setModuleTranslation($moduleId,  $locale)
    {
        $this->moduleTranslation = $this->em
                        ->getRepository('InvictusCmsBundle:ModuleTranslation')
                        ->findOneBy(array('fkBase' => $moduleId, 'fkLanguage' => $locale ));
    }


    public function getModuleTranslation()
    {
        return $this->moduleTranslation;
    }


    /**
     * Restituisce una stringa del tipo /invictus/app/locale/module dove app e locale sono opzionali tenendo conto della configurazione MA/ML del modulo
     * Se alla funzione non vengono passati app, locale e module attraverso il parametro $ALM vengono presi di default dalla richiesta
     *
     * @param array $ALM
     *
     * @return string
     */
    public function generateModulePath($ALM = array()){

        $defaultALM = array(
            'app' => $this->getRequestAttribute('app'),
            'locale' => $this->getRequestAttribute('locale'),
            'moduleTag' => $this->getRequestAttribute('moduleTag'),
            'action' => false,
            'layout' => $this->getRequestAttribute('layout', ''),
            'parentModuleTag' => $this->parentModuleTag,
            'parentItemId' => $this->parentItemId
        );

        $ALM = array_merge($defaultALM, $ALM);

        //$this->dump($ALM, true, true);
        if(isset($this->userMALTree[$ALM['moduleTag']])){
            $config = $this->module->getConfig(false);
            $config = $config['module'];
        }else{
            $module = $this->em
                ->getRepository('InvictusCmsBundle:Module')
                ->findOneBy(
                    array(
                        'tag' => $ALM['moduleTag']
                    )
                );
            if($module){
                $config = $module->getConfig(false);
                $config = $config['module'];
            }else{
                $config = array('MA' => false, 'ML' => false);
                $ALM['moduleTag'] = 'module-tag-not-found';
            }
        }

        if($ALM['layout'] == 'nested'){

            $path = '/'.$this->invictusFolder.'/'.$ALM['parentModuleTag'].'/'.$ALM['parentItemId'].'/nested/'.$ALM['locale'].'/'.$ALM['moduleTag'];

        }else{

            $path= '/'.$this->invictusFolder;

            if($config['MA']){
                $path .= "/";
                if($ALM['app']){
                    $path .= $ALM['app'];
                }else{
                    $path .= ($this->getRequestAttribute('app')) ? $this->getRequestAttribute('app') : $this->session->get('lastUsedApp');
                }
            }

            if($config['ML']){
                $path .= "/";
                if($ALM['locale']){
                    $path .= $ALM['locale'];
                }else{
                    $path .= ($this->getRequestAttribute('locale')) ? $this->getRequestAttribute('locale') : $this->session->get('lastUsedLocale');
                }
            }

            // Deve sempre esserci un module settato nella request
            $path .= '/'.$ALM['moduleTag'];

            $path .= ($ALM['action']) ? "/".$ALM['action'] : '';
        }

        return $path;
    }


    private function checkSessionExpiration()
    {
        $tt = time() - $this->session->getMetadataBag()->getLastUsed();
        $tr = $this->invictusSessionLifetime - $tt;
        /*
        echo "<div id='debug'>";
        echo "Sessione creata: ". $this->session->getMetadataBag()->getCreated()."<br/>";
        echo "Sessione ultimo utilizzo: ". $this->session->getMetadataBag()->getLastUsed()."<br/>";
        echo "Cookie lifetime: ". $this->session->getMetadataBag()->getLifetime()."<br/>";
        echo "Invictus session lifetime: ". $this->invictusSessionLifetime."<br/>";
        echo "Tempo trascorso da ultimo utilizzo: $tt s <br/> ";
        echo "Tempo rimasto prima del logout: $tr s <br/> ";
        echo "</div>";
        */
        if( $tr < 0 ){
            $this->session->invalidate();
            $this->redirect($this->requestedUri);
        }
    }


    /**
     * Check if user is allowed to access to current uri
     *
     * @param string $app
     * @param string $locale
     * @param string $moduleTag
     * Check if user can access to the app, language and module mapped in the uri.
     *
     * @return bool
     */
    private function isGrantedUserAppLanguageModule($app, $locale, $moduleTag)
    {
        $config = $this->module->getConfig(false);
        $config = $config['module'];

        // Garantisco sempre l'accesso al modulo dashboard
        if($moduleTag == 'dashboard'){
            return true;
        }

        // Il modulo è MA e ML
        if( $config['MA'] AND $config['ML'] AND isset($this->userMALTree[$moduleTag]['apps'][$app]['languages'][$locale]))
        {
            return true;
        }

        // Il modulo è MA e non ML
        if( $config['MA'] AND !$config['ML'] AND !$locale AND isset($this->userMALTree[$moduleTag]['apps'][$app]))
        {
            return true;
        }

        // Il modulo non è MA ma è ML
        $locales = array();
        foreach($this->userLanguages as $language){
            $locales[] = $language['id'];
        }
        if( !$config['MA'] AND !$app AND $config['ML'] AND in_array($locale, $locales))
        {
            return true;
        }

        // Il modulo non è ne MA ne ML
        if( !$config['MA'] AND !$app AND !$config['ML'] AND !$locale AND isset($this->userMALTree[$moduleTag])){
            return true;
        }
        return false;
    }
    
    
    /**
     * Suggest a redirectUrl if the user has not access to current $app/$locale/$moduleTag
     *
     * @param string $app
     * @param string $locale
     * @param string $module
     *
     * @return string
     */
    private function suggestRedirectUrl($app, $locale, $moduleTag)
    {

        $config = $this->module->getConfig(false);
        $config = $config['module'];

        $querystring = "?redirectFrom=/".$this->invictusFolder;
        $querystring.= ($app) ? "/$app" : '';
        $querystring.= ($locale) ? "/$locale" : '';
        $querystring.= ($moduleTag) ? "/$moduleTag" : '';

        $redirectUrl = array('', $this->invictusFolder);
        /*
        echo "<pre>";
        echo "<b>Config $moduleTag:</b><br/>";
        var_dump($config);
        echo " - - - - - - <br/>";
        echo "<br/><br/><b>Session:</b><br/>";
        var_dump($this->session->get('lastUsedApp'));
        var_dump($this->session->get('lastUsedLocale'));
        echo " - - - - - - <br/>";
        echo "<br/><br/><b>Input:</b><br/>";
        var_dump($app);
        var_dump($locale);
        echo " - - - - - - <br/>";
        echo "<br/><br/><b>Output:</b><br/>";
        */

        $fallbackModule = 'dashboard';
        if(isset($this->userMALTree[$moduleTag])){

            $fallbackModule = $moduleTag;

            if($config['MA']){
                if(isset($this->userMALTree[$fallbackModule]['apps'][$app])){
                    $fallbackApp = $this->userMALTree[$fallbackModule]['apps'][$app]['id'];
                }else{
                    $firstApp = array_keys($this->userMALTree[$fallbackModule]['apps']);
                    $firstApp = $firstApp[0];
                    $fallbackApp = isset($this->userMALTree[$fallbackModule]['apps'][$this->session->get('lastUsedApp')])
                                    ? $this->session->get('lastUsedApp')
                                    : $this->userMALTree[$fallbackModule]['apps'][$firstApp]['id'];
                }

                $redirectUrl[] = $fallbackApp;
                //var_dump($fallbackApp);
            }




            if($config['ML']){
                //die('ML');
                $locales = array();
                if(!$config['MA']){
                    foreach($this->userLanguages as $language){
                        $locales[] = $language['id'];
                    }
                    if((in_array($locale, $locales))){
                        $fallbackLocale = $locale;
                    }else{
                        $fallbackLocale = (in_array($this->user->getDefaultLanguage(), $locales))
                                            ? $this->user->getDefaultLanguage()
                                            : $locales[0];
                    }
                    $redirectUrl[] = $fallbackLocale;
                }else{
                    $firstLocale = array_keys($this->userMALTree[$fallbackModule]['apps'][$fallbackApp]['languages']);
                    $firstLocale = $firstLocale[0];
                    if(isset($this->userMALTree[$fallbackModule]['apps'][$fallbackApp]['languages'][$locale])){
                        $fallbackLocale = $this->userMALTree[$fallbackModule]['apps'][$fallbackApp]['languages'][$locale]['id'];
                    }else{
                        $fallbackLocale = isset($this->userMALTree[$fallbackModule]['apps'][$fallbackApp]['languages'][$this->session->get('lastUsedLocale')])
                            ? $this->session->get('lastUsedLocale')
                            : $this->userMALTree[$fallbackModule]['apps'][$fallbackApp]['languages'][$firstLocale]['id'];
                    }
                    $redirectUrl[] = $fallbackLocale;
                    //var_dump($fallbackLocale);
                }
            }

            //echo " - - - - - - <br/><br/>";
        }


        //$this->dump($this->userMALTree[$fallbackModule]['apps'], false, true);
        //$this->dump(array_keys($this->userMALTree[$fallbackModule]['apps']), false, true);
        //$this->dump($locales, false, false);
        //$this->dump($locale, true, true);


        $redirectUrl[] = $fallbackModule;

        $url = implode('/', $redirectUrl);

        return $url.$querystring;
    }
    
    
    /**
    * Redirect to $url param
    * 
    * @param string $url
    * @return void
    * @todo capire perchè non funziona con new RedirectResponse($url, 302);
    */
    private function redirect($url)
    {
        header("location: $url");
        exit();
        //$response = new RedirectResponse($url, 302);
        //return $response;
    }


    public function dump($e, $var_dump = false, $exit = false)
    {
        echo " - start - - - - - - - - - - - - start - \n";
        echo "<pre>";
        ($var_dump) ? var_dump($e) : print_r($e);
        echo "</pre>";
        echo "\n - end - - - - - - - - - - - - - end - <br /><br />";
        if($exit){
            exit();
        }
    }


    public function getFileTypes()
    {
        return $this->em
            ->getRepository('InvictusCmsBundle:AttachmentTypology')
            ->setInvictusKernel($this)
            ->getFileTypes();
    }


    public function getIdentifier($moduleId, $itemId, $locale = false)
    {
        $module = $this->em
            ->getRepository('InvictusCmsBundle:Module')
            ->findOneById($moduleId);
        if($locale){
            $entity = $this->em
                ->getRepository($module->getBundleEntity().'Translation')
                ->findOneBy(array('fkBase' => $itemId, 'fkLanguage' => $locale));
            //print_r($entity);
        }else{
            $entity =  $this->em
                ->getRepository($module->getBundleEntity())
                ->findOneById($moduleId);
        }

        //echo $module->getBundleEntity()." $moduleId $itemId $locale";
        if($entity){
            $identifier = $entity->getIdentifier();
        }else{
            $identifier = '...';
        }

        return $identifier;
    }


    public function getCheckedVisibilities($moduleId = false, $itemId = false)
    {
        $moduleId = $moduleId ? $moduleId : $this->moduleId;
        $itemId = $itemId ? $itemId : $this->itemId;
        return $this->em
            ->getRepository('InvictusCmsBundle:Visibility')
            ->setInvictusKernel($this)
            ->getCheckedVisibilities($moduleId, $itemId);
    }


    public function getPositions($moduleTag = false, $moduleId = false, $app = null, $locale = null, $label = false, $filters = false)
    {
        $moduleTag = $moduleTag ? $moduleTag : $this->moduleTag;
        $moduleId = $moduleId ? $moduleId : $this->moduleId;
        $label = $label ? $label : $moduleTag.$label;
        return $this->em
            ->getRepository('InvictusCmsBundle:Position')
            ->setInvictusKernel($this)
            ->getPositions($moduleTag, $moduleId, $app, $locale, $label, $filters);
    }


    public function getRelations($side, $appId, $languageId, $owningModuleId, $inverseModuleId, $knownItemId, $slot, $filters = array(), $label = false)
    {

        return $this->em
            ->getRepository('InvictusCmsBundle:Relation')
            ->setInvictusKernel($this)
            ->getRelations($side, $appId, $languageId, $owningModuleId, $inverseModuleId, $knownItemId, $slot, $filters, $label);
    }


    public function getAppLanguageRelations($appId, $filters = array(), $label = false)
    {

        return $this->em
            ->getRepository('InvictusCmsBundle:Relation')
            ->setInvictusKernel($this)
            ->getAppLanguageRelations($appId, $filters, $label);
    }


    public function repositoryProxy($bundle, $entity, $method, $arguments)
    {
        $bundle = str_replace('Bundle', '', $bundle);
        $repository = $this->em
            ->getRepository($bundle.'Bundle:'.$entity)
            ->setInvictusKernel($this);
        return call_user_func_array(array($repository, $method), $arguments);
    }


    public function serviceProxy($service, $method, $arguments)
    {
        //$service = $this->get($service);
        $service = $this->container->get($service);
        return call_user_func_array(array($service, $method), $arguments);
    }


    public function getData($arguments)
    {
        $service = $this->container->get('invictus.contentManager');
        return call_user_func_array(array($service, 'getData'), $arguments);
    }


    public function getCurrentItem($arguments)
    {
        $service = $this->container->get('invictus.contentManager');
        return call_user_func_array(array($service, 'getCurrentItem'), $arguments);
    }


    public function getAttachments($arguments)
    {
        $service = $this->container->get('invictus.contentManager');
        return call_user_func_array(array($service, 'getAttachments'), $arguments);
    }


    public function getRelatedIdsFromConfig($arguments)
    {
        $service = $this->container->get('invictus.contentManager');
        return call_user_func_array(array($service, 'getRelatedIdsFromConfig'), $arguments);
    }


    public function getRelatedElementsFromConfig($arguments)
    {
        $service = $this->container->get('invictus.contentManager');
        return call_user_func_array(array($service, 'getRelatedElementsFromConfig'), $arguments);
    }


    public function doctrineDump($data)
    {
        echo "<pre style='background:#FFF'>";
        \Doctrine\Common\Util\Debug::dump($data);
        echo "</pre>";
    }


    public function setModuleIdAndItemIdFromPathAndSlug($pathAndSlug)
    {
        $segments = explode('/', trim($pathAndSlug, '/'));
        $slug = array_pop($segments);
        $path = implode('/', $segments);

        $conditions = array(
            'slug' => $slug
        );

        if(!empty($path)){
            $conditions['path'] = $path;
        }

        if(!is_null($this->languageId)){
            $conditions['fkLanguage'] = $this->languageId;
        }

        if(!is_null($this->appId)){
            $conditions['fkApp'] = $this->appId;
        }

        // A questo punto non posso conoscere le configurazioni MA/ML del modulo quindi :
        //Provo a cercare un metadata come se il modulo fosse MA/ML
        $metadata = $this->em
            ->getRepository('InvictusCmsBundle:Metadata')
            ->findOneBy($conditions);

        //Se non esiste un metadata provo a cercare un metadata come se il modulo fosse ML
        if(!$metadata){
            unset($conditions['fkApp']);
            $metadata = $this->em
                ->getRepository('InvictusCmsBundle:Metadata')
                ->findOneBy($conditions);
        }

        //Se non esiste un metadata provo a cercare un metadata come se il modulo fosse MA
        if(!$metadata){
            unset($conditions['fkLanguage']);
            $conditions['fkApp'] = $this->appId;
            $metadata = $this->em
                ->getRepository('InvictusCmsBundle:Metadata')
                ->findOneBy($conditions);
        }

        //Se non esiste un metadata provo a cercare un metadata come se il modulo non fosse ne MA ne ML
        if(!$metadata){
            unset($conditions['fkApp']);
            $metadata = $this->em
                ->getRepository('InvictusCmsBundle:Metadata')
                ->findOneBy($conditions);
        }

        if($metadata){
            $this->metadata = $metadata;
            $this->moduleId = $metadata->getFkModule()->getId();
            $this->itemId = $metadata->getFkItemId();
        }else{
            // setto di default l'id del modulo pagine
            $this->moduleId = 11;
            // Commentato perchè a volte ho bisogno di richiedere pagine che non hanno metadata
            //throw new NotFoundHttpException('La pagina richiesta non esiste o è stata rimossa');
        }
    }


    public function customPath($path, $newPath)
    {
        $path = explode('/', $path);
        $filename = array_pop($path);
        return '/'.trim($newPath, '/').'/'.$filename;
    }


    public function jsonDecode($json)
    {
        return json_decode($json);
    }


    public function getItemByModuleIdAndItemId($moduleId, $itemId)
    {
        $module = $this->getModule(array('id' => $moduleId));

        $item = $this->em
            ->getRepository($module->getBundleEntity())
            ->findOneById($itemId);

        return $item;
    }


    public function generateUrl($config = array(), $absolute = false)
    {
        if(!isset($config['moduleId']) or !isset($config['itemId'])){
            return "#missing-params";
        }

        $defaults = array(
            'appId' => $this->appId,
            'languageId' => $this->languageId,
            'moduleId' => $this->moduleId,
            'itemId' => $this->itemId
        );
        $config = array_merge($defaults, $config);

        if($config['moduleId'] == $this->moduleId){
            $moduleConfig = json_decode($this->module->getConfig());
        }else{
            /**
             * todo
             * caricare le configurazioni del modulo per capire com'è configurato metadata
             * fare la query su metadata per ricavare path e slug
             * Se per caso non c'è metadata slug = id
             */
            $moduleConfig = json_decode($this->getModule(array('id' => $config['moduleId']))->getConfig());
        }

        $metaConfig = array(
            'fkModule' => $config['moduleId'],
            'fkItemId' => $config['itemId']
        );

        if($moduleConfig->module->MA and $moduleConfig->module->ML){
            $route = 'invictus_app_locale_path';
            $metaConfig['fkApp'] = $config['appId'];
            $metaConfig['fkLanguage'] = $config['languageId'];
        }elseif(!$moduleConfig->module->MA and $moduleConfig->module->ML){
            $route = 'invictus_locale_path';
            $metaConfig['fkLanguage'] = $config['languageId'];
        }else{
            $route = 'invictus_path';
        }

        $metadata = $this->getMetadata($metaConfig);
        $routeConfig = array(
            'path_and_slug' => trim($metadata->getUrl(), '/'),
            '_locale' => $config['languageId'],
        );

        return $this->container->get('router')->generate($route, $routeConfig, $absolute);
    }


    public function death()
    {
        die('Death!!!');
    }



















    // Metodo per la generazione di uno slug

    public static function slugify($title) {

        if(empty($title)){
            $title = "slug-".date('Y-m-d-H-i-s');
        }

        $title = strip_tags($title);

        $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);

        $title = str_replace('%', '', $title);

        $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

        $title = self::remove_accents($title);
        if (self::seems_utf8($title)) {
            if (function_exists('mb_strtolower')) {
                $title = mb_strtolower($title, 'UTF-8');
            }
            //$title = $this->utf8_uri_encode($title);
        }

        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title);
        $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');

        return $title;
    }

    /* ---------------------------------------------- */
    /*              METODI PRIVATI                    */
    /* ---------------------------------------------- */

    private static function seems_utf8($Str)
    {
        for ($i=0; $i<strlen($Str); $i++) {
            if (ord($Str[$i]) < 0x80) continue;
            elseif ((ord($Str[$i]) & 0xE0) == 0xC0) $n=1;
            elseif ((ord($Str[$i]) & 0xF0) == 0xE0) $n=2;
            elseif ((ord($Str[$i]) & 0xF8) == 0xF0) $n=3;
            elseif ((ord($Str[$i]) & 0xFC) == 0xF8) $n=4;
            elseif ((ord($Str[$i]) & 0xFE) == 0xFC) $n=5;
            else return false;
            for ($j=0; $j<$n; $j++) {
                if ((++$i == strlen($Str)) || ((ord($Str[$i]) & 0xC0) != 0x80))
                    return false;
            }
        }
        return true;
    }

    private static function utf8_uri_encode( $utf8_string )
    {
        $unicode = '';
        $values = array();
        $num_octets = 1;

        for ($i = 0; $i < strlen( $utf8_string ); $i++ ) {

            $value = ord( $utf8_string[ $i ] );

            if ( $value < 128 ) {
                $unicode .= chr($value);
            } else {
                if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

                $values[] = $value;

                if ( count( $values ) == $num_octets ) {
                    if ($num_octets == 3) {
                        $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
                    } else {
                        $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
                    }

                    $values = array();
                    $num_octets = 1;
                }
            }
        }

        return $unicode;
    }

    private static function remove_accents($string)
    {
        if (self::seems_utf8($string)) {
            $chars = array(
                // Latin-1
                chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
                chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
                chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
                chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
                chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
                chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
                chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
                chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
                chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
                chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
                chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
                chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
                chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
                chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
                chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
                chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
                chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
                chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
                chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
                chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
                chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
                chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
                chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
                chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
                chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
                chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
                chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
                chr(195).chr(191) => 'y',
                // Latin A
                chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
                chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
                chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
                chr(196).chr(134) => 'C', chr(196).chr(134) => 'c',
                chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
                chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
                chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
                chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
                chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
                chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
                chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
                chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
                chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
                chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
                chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
                chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
                chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
                chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
                chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
                chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
                chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
                chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
                chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
                chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
                chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
                chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
                chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
                chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
                chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
                chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
                chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
                chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
                chr(197).chr(128) => 'l', chr(196).chr(129) => 'L',
                chr(197).chr(130) => 'l', chr(196).chr(131) => 'N',
                chr(197).chr(132) => 'n', chr(196).chr(133) => 'N',
                chr(197).chr(134) => 'n', chr(196).chr(135) => 'N',
                chr(197).chr(136) => 'n', chr(196).chr(137) => 'N',
                chr(197).chr(138) => 'n', chr(196).chr(139) => 'N',
                chr(197).chr(140) => 'O', chr(196).chr(141) => 'o',
                chr(197).chr(142) => 'O', chr(196).chr(143) => 'o',
                chr(197).chr(144) => 'O', chr(196).chr(145) => 'o',
                chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
                chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
                chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
                chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
                chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
                chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
                chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
                chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
                chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
                chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
                chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
                chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
                chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
                chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
                chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
                chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
                chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
                chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
                chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
                chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
                chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
                chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
                chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
                // Euro
                chr(226).chr(130).chr(172) => 'E');

            $string = strtr($string, $chars);
        } else {

            $chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
                .chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
                .chr(195).chr(197).chr(199).chr(200).chr(201).chr(202)
                .chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
                .chr(211).chr(212).chr(213).chr(216).chr(217).chr(218)
                .chr(219).chr(221).chr(224).chr(225).chr(226).chr(227)
                .chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
                .chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
                .chr(244).chr(245).chr(248).chr(249).chr(250).chr(251)
                .chr(253).chr(255);

            $chars['out'] = "EfSZszYcYuAAAAACEEEEIIIINOOOOOUUUYaaaaaceeeeiiiinooooouuuyy";

            $string = strtr($string, $chars['in'], $chars['out']);
            $double_chars['in'] = array(chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254), chr(196), chr(220), chr(214), chr(228), chr(252), chr(246));
            $double_chars['out'] = array('OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th', 'Ae', 'Ue', 'Oe', 'ae', 'ue', 'oe');
            $string = str_replace($double_chars['in'], $double_chars['out'], $string);
        }

        return $string;
    }


}









    /* ---------------------------------------------- */
/*              METODI PUBBLICI                   */
/* ---------------------------------------------- */
    

    /**
    * Ritorna una oggetto metadata partendo da un id del modulo e dell' Item
    * 
    * @param mixed $moduleId
    * @param mixed $item_id
    */
    /*
    public function getMetadataByModuleAndItemId($moduleId, $itemId)
    {
    
          return CMSMetadata::getMetadataByModuleAndItemId($this->em, $moduleId, $itemId, $this->metaParams['app'], $this->metaParams['_locale']);
    }   
*/
    
    // GENERAZIONE URI
    /*
    public function getUriByModuleAndItemId($moduleId, $itemId)
    {
        return $this->getUriByMetadata($this->getMetadataByModuleAndItemId($moduleId, $itemId));
    }

    public function getUriByMetadata(Metadata $metadata)
    {
        
        return CMSMetadata::generateUriByMetadata($metadata, $this->metaParams['_locale'], $this->router);
    }
    
    public function getUriByMetadataId($metadata_id)
    {
        return CMSMetadata::generateUriByMetadataId($metadata_id, $this->metaParams['_locale'], $this->router, $this->em);
    }
    
    public function getUriForCMSPageById($cmsPageId)
    {
        $metadata = CMSNode::getMetadataFromItemId($this->em,  $this->metaParams['app'], $this->metaParams['_locale'], $cmsPageId);
        return ($metadata) ? CMSMetadata::generateUriByMetadata($metadata, $this->metaParams['_locale'], $this->router) : '/';        
    }
    
    public function getUriForCMSPageByConfConst($confConst)
    {
        return $this->getUriForCMSPageById(CMSKernel::getConfConst($confConst));
    }
    */
    /**
    * Ritorna l'uri della pagina corrente 
    * partendo da quella corrente ma modificando il locale
    * 
    * @param mixed $locale
    */
    /*
    public function getLocalizedUri($locale)
    {
        
        $localized_metadata = CMSMetadata::getLocalizedMetadata($this->metadata, $locale, $this->em);
        
        if (CMSMetadata::isValidMetadata($localized_metadata)) {
            return CMSMetadata::generateUriByMetadata($localized_metadata,
                                                      $this->router, $this->metaParams['app']);
        } else {
            return CMSMetadata::generateHomeUri($this->metaParams['app'],
                                                $locale,
                                                $this->router);
        }
        
    }
    
    */    

    

