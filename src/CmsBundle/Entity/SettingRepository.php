<?php

/**
* This file is part of the INVICTUS project
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @copyright 2012-2013 devsign
* @author Matteo Poile <matteo.poile@devsign.it>
* @package InvictusCmsBundle\Entity\InvictusRepository.php
*/

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Invictus\CmsBundle\Entity\ItemRepository;

/**
 * InvictusRepository
 *
 * Main INVICTUS repository
 * 
 * It manages all INVICTUS queries not directly related to a single entity/item
 */
class SettingRepository extends ItemRepository
{
    
    /**
    * Retrieve a structure based on users, apps, languages and modules in an associative multidimensional array.
    * If filters $app or $locale are set the function filters out results with modules not associated to $app and $locale filter.
    * 
    * @example 
    * $this->em->getRepository('InvictusCmsBundle:Setting')
    *   ->getAppsLanguagesModulesTree(array( 
    *                                   'app' => array('vs'), // app id string | array of app id string
    *                                   'locale' => 'it_IT', // locale string | array of locale
    *                                   'module' => array('app', 11), // module tag string | module id int or string | mixed array of module tag and id
    *                                   'user' => 1 // user id int or array
    *                                   'uiLocale' => $this->getRequestAttribute('_locale') // ui_locale used only for module translation
    *                                 ),
    *                                 'MAL' // empty or 'ALM' for a app->language->module structure, 'MAL' for a module->app->language structure
    *                               );
    * 
    * @param mixed $filters
    * @param string $type
    * 
    * @return array
    * @todo bisogna aggiungere una inner join che controlli che i moduli siano direttamenti associati a qualche app e lingua,
    * non basta usare la relazione fra moduli e app/user basata sugli utenti (fk_module_id_a = 10) 
    */
    public function getStructureTree($filters = array(), $type = 'ALM')
    {
        
        foreach($filters as $key => $value)
        {
            if(!is_array($value)){
                $filters[$key] = array($value);
            }
        }
        
        $appFilter = (!isset($filters['app'])) ? '' : " AND app.id IN ('".implode("','", $filters['app'])."') " ;
        $localeFilter = (!isset($filters['locale'])) ? '' : " AND language.id IN ('".implode("','", $filters['locale'])."') " ;
        $moduleFilter = (!isset($filters['module'])) ? '' : " AND ( module.id IN ('".implode("','", $filters['module'])."') OR module.tag IN('".implode("','", $filters['module'])."') ) " ;
        $userFilter = (!isset($filters['user'])) ? '' : " AND relation.fk_item_id_a IN ('".implode("','", $filters['user'])."') " ;
        $uiLocaleFilter = (!isset($filters['uiLocale'])) ? '' : " AND module_translation.fk_language_id IN ('".implode("','", $filters['uiLocale'])."') " ;

        $sql = "SELECT
                    CONCAT(
                        app.id, ' (', app.position, ') - ',
                        language.id, ' (', language_position.position, ') - '
                        -- , module.tag, ' (', user_module_position.position, ')'  
                    ) AS debug,
                    app.id AS appId,
                    app.label AS appLabel,
                    app.logo AS appLogo,
                    app.icon AS appIcon,
                    app.enabled AS appHighlighted,
                    app.enabled AS appEnabled,
                    app.deleted AS appDeleted,
                    app.position AS appPosition,
                    language.id AS languageId,
                    language.label AS languageLabel,
                    language.icon AS languageIcon,
                    language.country AS languageCountry,
                    language.ISO639 AS languageISO639,
                    language.ISO3166 AS languageISO3166,
                    language.enabled AS languageEnabled,
                    language.deleted AS languageDeleted,
                    language_position.position AS languagePosition,
                    module.id AS moduleId,
                    module.parent_id AS moduleParentId,
                    module.tag AS moduleTag,
                    module.fk_template_id AS moduleFkTemplateId,
                    module.bundle AS moduleBundle,
                    module.controller AS moduleController,
                    module.views AS moduleViews,
                    module.entity AS moduleEntity,
                    module.db_table AS moduleDbTable,
                    module.icon AS moduleIcon,
                    module.list_rows AS moduleListRows,
                    module.config AS moduleConfig,
                    module_translation.fk_language_id AS moduleLocale,
                    module_translation.label AS moduleLabel,
                    module_translation.abstract AS moduleAbstract,
                    module_translation.body AS moduleBody";
                    
        if(isset($filters['user']))
        {
            $sql .= ",
                    user_module_position.position AS userModulePosition ";
        }  
              
        $sql .= " 
                FROM
                    relation
                
                INNER JOIN app 
                    ON relation.fk_app_id = app.id
                    $appFilter
                    AND app.enabled = 1
                    AND app.deleted = 0
                    AND relation.fk_module_id_a = 10
                    $userFilter
                    AND relation.fk_module_id_b = 4

                INNER JOIN language 
                    ON relation.fk_language_id = language.id
                    $localeFilter
                    AND language.enabled = 1
                    AND language.deleted = 0
                    AND relation.fk_module_id_a = 10
                    $userFilter
                    AND relation.fk_module_id_b = 4
                
                INNER JOIN module 
                    ON relation.fk_item_id_b = module.id
                    $moduleFilter
                    AND module.enabled = 1
                    AND module.deleted = 0
                    AND relation.fk_module_id_a = 10
                    $userFilter
                    AND relation.fk_module_id_b = 4

                INNER JOIN relation AS enabled_app
                    ON enabled_app.fk_app_id = app.id
                    AND enabled_app.fk_language_id = language.id
                    AND enabled_app.fk_module_id_a IS NULL
                    AND enabled_app.fk_item_id_a IS NULL
                    AND enabled_app.fk_item_id_b IS NULL
                    AND enabled_app.fk_item_id_b IS NULL

                INNER JOIN visibility AS enabled_module
                    ON enabled_module.fk_item_id = module.id
                    AND enabled_module.fk_module_id = 4
                    AND enabled_module.fk_app_id = app.id
                    AND enabled_module.fk_language_id = language.id

                -- INNER JOIN relation AS enabled_module
                -- ON enabled_module.fk_item_id_a = module.id
                --    AND enabled_module.fk_app_id = app.id
                --    AND enabled_module.fk_language_id = language.id
                --    AND enabled_module.fk_module_id_a = 4
                --    AND enabled_module.fk_module_id_b IS NULL
                --    AND enabled_module.fk_item_id_b IS NULL
                        
                LEFT OUTER JOIN module_translation
                    ON module.id = module_translation.fk_module_id
                    $uiLocaleFilter
                
                LEFT OUTER JOIN relation AS language_position
                ON language_position.fk_language_id = language.id
                    AND language_position.fk_app_id = app.id
                    AND language_position.fk_module_id_a IS NULL
                    AND language_position.fk_item_id_a IS NULL
                    AND language_position.fk_module_id_b IS NULL
                    AND language_position.fk_item_id_b IS NULL";
                    
        if(isset($filters['user']))
        {
            $sql .= " 
                INNER JOIN relation AS user_module_position
                    ON user_module_position.fk_language_id = language.id
                        AND user_module_position.fk_app_id = app.id
                        AND user_module_position.fk_module_id_a = 10
                        AND user_module_position.fk_item_id_a IN ('".implode('\',\'', $filters['user'])."')
                        AND user_module_position.fk_module_id_b = 4
                        AND user_module_position.fk_item_id_b = module.id";
        }

        $modulePosition = ($type == 'MAL') ? ", user_module_position.position ASC" : "" ;
                

        if(isset($filters['user']))
        {
            $sql .= "
                ORDER BY
                userModulePosition ASC";
        }else{
            $sql .= "
                ORDER BY
                    appPosition ASC,
                    languagePosition ASC
                    $modulePosition";
        }
        
        //if($type == 'MAL'){echo "<pre>$sql</pre>";die();}

        $result = $this->getEntityManager()->getConnection()->fetchAll($sql);
        
        if(!$result)
        {
            return false;
        }
        
        $data = array();
        
        
        switch($type)
        {
            case 'MAL':
                //echo "<pre>";die($sql);

                foreach($result as $item)
                {
                    $data[$item['moduleTag']]['id'] = $item['moduleId'];
                    $data[$item['moduleTag']]['parent_id'] = $item['moduleParentId'];
                    $data[$item['moduleTag']]['tag'] = $item['moduleTag'];
                    $data[$item['moduleTag']]['fk_template_id'] = $item['moduleFkTemplateId'];
                    $data[$item['moduleTag']]['bundle'] = $item['moduleBundle'];
                    $data[$item['moduleTag']]['controller'] = $item['moduleController'];
                    $data[$item['moduleTag']]['views'] = $item['moduleViews'];
                    $data[$item['moduleTag']]['entity'] = $item['moduleEntity'];
                    $data[$item['moduleTag']]['dbTable'] = $item['moduleDbTable'];
                    $data[$item['moduleTag']]['icon'] = $item['moduleIcon'];
                    $data[$item['moduleTag']]['list_rows'] = $item['moduleListRows'];
                    $data[$item['moduleTag']]['assets_folder'] = '/bundles/'.strtolower(str_replace('Bundle', '', $item['moduleBundle'] ));
                    $data[$item['moduleTag']]['config'] = json_decode($item['moduleConfig']);
                    $data[$item['moduleTag']]['translations'][$item['moduleLocale']]['label'] = $item['moduleLabel'];
                    $data[$item['moduleTag']]['translations'][$item['moduleLocale']]['abstract'] = $item['moduleAbstract'];
                    $data[$item['moduleTag']]['translations'][$item['moduleLocale']]['body'] = $item['moduleBody'];
                    if(isset($filters['user']))
                    {
                        $data[$item['moduleTag']]['translations'][$item['moduleLocale']]['userModulePosition'] = $item['userModulePosition'];
                    }
                    
                    $data[$item['moduleTag']]['apps'][$item['appId']]['id'] = $item['appId'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['label'] = $item['appLabel'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['logo'] = $item['appLogo']; 
                    $data[$item['moduleTag']]['apps'][$item['appId']]['icon'] = $item['appIcon']; 
                    $data[$item['moduleTag']]['apps'][$item['appId']]['highlighted'] = $item['appHighlighted'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['position'] = $item['appPosition'];
                        
                    $data[$item['moduleTag']]['apps'][$item['appId']]['languages'][$item['languageId']]['id'] = $item['languageId'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['languages'][$item['languageId']]['label'] = $item['languageLabel'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['languages'][$item['languageId']]['icon'] = $item['languageIcon'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['languages'][$item['languageId']]['country'] = $item['languageCountry'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['languages'][$item['languageId']]['ISO639'] = $item['languageISO639'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['languages'][$item['languageId']]['ISO3166'] = $item['languageISO3166'];
                    $data[$item['moduleTag']]['apps'][$item['appId']]['languages'][$item['languageId']]['position'] = $item['languagePosition'];
                }
            
            break;
            
            
            default:
            
                foreach($result as $item)
                {
                    $data[$item['appId']]['id'] = $item['appId'];
                    $data[$item['appId']]['label'] = $item['appLabel'];
                    $data[$item['appId']]['logo'] = $item['appLogo']; 
                    $data[$item['appId']]['icon'] = $item['appIcon']; 
                    $data[$item['appId']]['highlighted'] = $item['appHighlighted'];
                    $data[$item['appId']]['position'] = $item['appPosition'];
                        
                    $data[$item['appId']]['languages'][$item['languageId']]['id'] = $item['languageId'];
                    $data[$item['appId']]['languages'][$item['languageId']]['label'] = $item['languageLabel'];
                    $data[$item['appId']]['languages'][$item['languageId']]['icon'] = $item['languageIcon'];
                    $data[$item['appId']]['languages'][$item['languageId']]['country'] = $item['languageCountry'];
                    $data[$item['appId']]['languages'][$item['languageId']]['ISO639'] = $item['languageISO639'];
                    $data[$item['appId']]['languages'][$item['languageId']]['ISO3166'] = $item['languageISO3166'];
                    $data[$item['appId']]['languages'][$item['languageId']]['position'] = $item['languagePosition'];
                        
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['id'] = $item['moduleId'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['parent_id'] = $item['moduleParentId'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['tag'] = $item['moduleTag'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['fk_template_id'] = $item['moduleFkTemplateId'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['bundle'] = $item['moduleBundle'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['controller'] = $item['moduleController'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['entity'] = $item['moduleEntity'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['views'] = $item['moduleViews'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['dbTable'] = $item['moduleDbTable'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['icon'] = $item['moduleIcon'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['list_rows'] = $item['moduleListRows'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['assets_folder'] = '/bundles/'.strtolower(str_replace('Bundle', '', $item['moduleBundle'] ));
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['config'] = json_decode($item['moduleConfig']);
                    if(isset($filters['user']))
                    {
                        $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['userPosition'] = $item['userModulePosition'];
                    }
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['translations'][$item['moduleLocale']]['label'] = $item['moduleLabel'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['translations'][$item['moduleLocale']]['abstract'] = $item['moduleAbstract'];
                    $data[$item['appId']]['languages'][$item['languageId']]['modules'][$item['moduleTag']]['translations'][$item['moduleLocale']]['body'] = $item['moduleBody'];
                    
                    
                }
        }
        
        return $data;
        
    }


    public function getALMTree($filters = array())
    {
        return $this->getStructureTree($filters, 'ALM');
    }


    public function getMALTree($filters = array())
    {
        return $this->getStructureTree($filters, 'MAL');
    }


    public function getFullALTree()
    {
        $sql = "SELECT
                    app.id AS appId,
                    language.id AS languageId,
                    app.label AS appLabel,
                    app.logo AS appLogo,
                    app.icon AS appIcon,
                    app.enabled AS appHighlighted,
                    app.enabled AS appEnabled,
                    app.deleted AS appDeleted,
                    app.position AS appPosition,
                    language.label AS languageLabel,
                    language.icon AS languageIcon,
                    language.country AS languageCountry,
                    language.ISO639 AS languageISO639,
                    language.ISO3166 AS languageISO3166,
                    language.enabled AS languageEnabled,
                    language.deleted AS languageDeleted,
										relation.position

                    FROM
                        relation

                    INNER JOIN app
                        ON relation.fk_app_id = app.id
                        AND app.enabled = 1
                        AND app.deleted = 0

                    INNER JOIN language
                        ON relation.fk_language_id = language.id
                        AND language.enabled = 1
                        AND language.deleted = 0

                    WHERE
                        fk_module_id_a IS NULL
                        AND fk_item_id_a IS NULL
                        AND fk_module_id_b IS NULL
                        AND fk_item_id_b IS NULL

                    ORDER BY
                        appPosition ASC,
                        relation.position ASC";

        $result = $this->getEntityManager()->getConnection()->fetchAll($sql);

        if(!$result)
        {
            return array();
        }

        $data = array();

        foreach($result as $item)
        {
            $data[$item['appId']]['id'] = $item['appId'];
            $data[$item['appId']]['label'] = $item['appLabel'];
            $data[$item['appId']]['logo'] = $item['appLogo'];
            $data[$item['appId']]['icon'] = $item['appIcon'];
            $data[$item['appId']]['highlighted'] = $item['appHighlighted'];
            $data[$item['appId']]['position'] = $item['appPosition'];

            $data[$item['appId']]['languages'][$item['languageId']]['id'] = $item['languageId'];
            $data[$item['appId']]['languages'][$item['languageId']]['label'] = $item['languageLabel'];
            $data[$item['appId']]['languages'][$item['languageId']]['icon'] = $item['languageIcon'];
            $data[$item['appId']]['languages'][$item['languageId']]['country'] = $item['languageCountry'];
            $data[$item['appId']]['languages'][$item['languageId']]['ISO639'] = $item['languageISO639'];
            $data[$item['appId']]['languages'][$item['languageId']]['ISO3166'] = $item['languageISO3166'];
            $data[$item['appId']]['languages'][$item['languageId']]['position'] = $item['position'];
        }

        return $data;
    }

}