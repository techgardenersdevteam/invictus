<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Module
 *
 * @ORM\Table(name="module", uniqueConstraints={@ORM\UniqueConstraint(columns={"tag"})})
 * @UniqueEntity(fields={"tag"})
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\ModuleRepository")
 */
class Module extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag = '...';

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="bundle", type="string", length=255, nullable=true)
     */
    private $bundle = '...';
    
    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=255, nullable=true)
     */
    private $controller = '...';

    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255, nullable=true)
     */
    private $entity = '...';

    /**
     * @var string
     *
     * @ORM\Column(name="form_type", type="string", length=255, nullable=true)
     */
    private $formType = '...';

    /**
     * @var string
     *
     * @ORM\Column(name="views", type="string", length=255, nullable=true)
     */
    private $views = '...';

    /**
     * @var string
     *
     * @ORM\Column(name="css", type="string", length=255, nullable=true)
     */
    private $css = '...';

    /**
     * @var string
     *
     * @ORM\Column(name="js", type="string", length=255, nullable=true)
     */
    private $js = '...';

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon = '...';

    /**
     * @var string
     *
     * @ORM\Column(name="list_type", type="string", length=255, nullable=true)
     */
    private $listType;

    /**
     * @var string
     *
     * @ORM\Column(name="db_table", type="text", nullable=true)
     */
    private $dbTable;

    /**
     * @var integer
     *
     * @ORM\Column(name="list_rows", type="integer", nullable=false)
     */
    private $listRows = 10;

    /**
     * @var string
     *
     * @ORM\Column(name="config", type="text", nullable=true)
     */
    private $config = '{}';
    
    /**
     * @var string
     *
     * @ORM\Column(name="tabs_config", type="text", nullable=true)
     */
    private $tabsConfig = '{}';

    /**
     * @var \Template
     *
     * @ORM\ManyToOne(targetEntity="\Invictus\TemplateBundle\Entity\Template")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_template_id", referencedColumnName="id")
     * })
     */
    private $fkTemplate;

    /**
     * @ORM\OneToMany(targetEntity="ModuleTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Visibility", mappedBy="fkModuleItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Position", mappedBy="fkModuleItem")
     */
    protected $positions;


    // ***********************************************************************
    // ***********************************************************************

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->positions = new ArrayCollection();
    }


    // ***********************************************************************
    // ***********************************************************************
    // ******************** CUSTOM FUNCTIONS *********************************
    // ***********************************************************************
    // ***********************************************************************


    public function getModuleEntity()
    {
        $base = $this->getEntity();
        return new $base();
    }

    public function getModuleEntityTranslation()
    {
        $base = $this->getEntity().'Translation';
        $item = new $base();

        return $item;
    }


    /**
     * Get bundleController
     *
     * @return string
     */
    public function getBundleController()
    {

      $controller_parts = explode('\\',$this->controller);
      if ($controller_parts) {
        $controller = $controller_parts[count($controller_parts)-1];
      }

      $controller = str_replace('Controller','',$controller);

        return $this->bundle.':'.$controller;
    }

    /**
     * Get bundleEntity
     *
     * @return string
     */
    public function getBundleEntity()
    {
        $entity_parts = explode('\\',$this->entity);
        if ($entity_parts) {
          $entity = $entity_parts[count($entity_parts)-1];
        }

        return $this->bundle.':'.$entity;
    }

    /**
     * Get getBundleEntityTranslation
     *
     * @return string
     */
    public function getBundleEntityTranslation()
    {
        return $this->getBundleEntity().'Translation';
    }

    /**
     * Get bundleView
     *
     * @return string
     */
    public function getBundleView()
    {
        return $this->bundle.':'.$this->views;
    }

    /**
     * Get bundleNameSpacePath
     *
     * @return string
     */
    public function getBundleNameSpacePath()
    {
        return preg_replace('/(?<!\ )[A-Z]/', '\\\\$0', $this->getBundle(), 2);
    }

    /**
     * Get assetsFolder
     *
     * @return string
     */
    public function getAssetsFolder()
    {
        return '/bundles/'.strtolower(str_replace('Bundle', '', $this->bundle ));
    }
    // ***********************************************************************
    // ***********************************************************************
    // ******************** GETTER / SETTER **********************************
    // ***********************************************************************
    // ***********************************************************************


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return Module
     */
    public function setTag($tag)
    {
        $this->tag = strtolower($tag);

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Module
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    
        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set bundle
     *
     * @param string $bundle
     * @return Module
     */
    public function setBundle($bundle)
    {
        $this->bundle = (empty($bundle)) ? 'InvictusCmsBundle' : $bundle;
    
        return $this;
    }

    /**
     * Get bundle
     *
     * @return string 
     */
    public function getBundle()
    {
        return $this->bundle;
    }

    /**
     * Set controller
     *
     * @param string $controller
     * @return Module
     */
    public function setController($controller)
    {
        $this->controller = (empty($controller)) ? ucfirst($this->tag) : $controller;
    
        return $this;
    }

    /**
     * Get controller
     *
     * @return string 
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Set entity
     *
     * @param string $entity
     * @return Module
     */
    public function setEntity($entity)
    {
        $this->entity = (empty($entity)) ? ucfirst($this->tag) : $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set views
     *
     * @param string $views
     * @return Module
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return string
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Set listType
     *
     * @param string $listType
     * @return Module
     */
    public function setListType($listType)
    {
        $this->listType = $listType;

        return $this;
    }

    /**
     * Get listType
     *
     * @return string
     */
    public function getListType()
    {
        return $this->listType;
    }

    /**
     * Set listRows
     *
     * @param integer $listRows
     * @return Module
     */
    public function setListRows($listRows)
    {
        $this->listRows = $listRows;
    
        return $this;
    }

    /**
     * Get listRows
     *
     * @return integer 
     */
    public function getListRows()
    {
        return $this->listRows;
    }

    /**
     * Set config
     *
     * @param string $config
     * @return Module
     */
    public function setConfig($config)
    {
        $this->config = (empty($config)) ? '{}' : $config;
    
        return $this;
    }

    /**
     * Get config
     *
     * @return string
     * @param boolean $json
     */
    public function getConfig($json = true)
    {
        if(!$json){
            return json_decode($this->config, true);
        }
        
        return $this->config;
    }
    
    /**
     * Set tabsConfig
     *
     * @param string $tabsConfig
     * @return Module
     */
    public function setTabsConfig($tabsConfig)
    {
        $this->tabsConfig = (empty($tabsConfig)) ? '{}' : $tabsConfig;
    
        return $this;
    }

    /**
     * Get tabsConfig
     *
     * @return string
     * @param boolean $json
     */
    public function getTabsConfig($json = true)
    {
        if(!$json){
            return json_decode($this->tabsConfig, true);
        }
        
        return $this->tabsConfig;
    }

    /**
     * Set fkTemplate
     *
     * @param \Invictus\TemplateBundle\Entity\Template $fkTemplate
     * @return Module
     */
    public function setFkTemplate(\Invictus\TemplateBundle\Entity\Template $fkTemplate = null)
    {
        $this->fkTemplate = $fkTemplate;
    
        return $this;
    }

    /**
     * Get fkTemplate
     *
     * @return \Invictus\TemplateBundle\Entity\Template
     */
    public function getFkTemplate()
    {
        return $this->fkTemplate;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Module
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Add translations
     *
     * @param \Invictus\CmsBundle\Entity\ModuleTranslation $translations
     * @return Module
     */
    public function addTranslation(\Invictus\CmsBundle\Entity\ModuleTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;

        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\CmsBundle\Entity\ModuleTranslation $translations
     */
    public function removeTranslation(\Invictus\CmsBundle\Entity\ModuleTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Set dbTable
     *
     * @param string $dbTable
     * @return Module
     */
    public function setDbTable($dbTable)
    {
        $this->dbTable = (empty($dbTable)) ? strtolower($this->tag) : strtolower($dbTable);

        return $this;
    }

    /**
     * Get dbTable
     *
     * @return string 
     */
    public function getDbTable()
    {
        return $this->dbTable;
    }

    /**
     * Set fkLanguage
     *
     * @param \Invictus\CmsBundle\Entity\App $fkLanguage
     * @return Module
     */
    public function setFkLanguage(\Invictus\CmsBundle\Entity\App $fkLanguage = null)
    {
        $this->fkLanguage = $fkLanguage;

        return $this;
    }

    /**
     * Get fkLanguage
     *
     * @return \Invictus\CmsBundle\Entity\App 
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Module
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $visibilities->setFkModuleItem($this);
        $this->visibilities[] = $visibilities;

        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Set formType
     *
     * @param string $formType
     * @return Module
     */
    public function setFormType($formType)
    {
        $this->formType = (empty($formType)) ? ucfirst($this->tag) : $formType;
    
        return $this;
    }

    /**
     * Get formType
     *
     * @return string 
     */
    public function getFormType()
    {
        return $this->formType;
    }

    /**
     * Set css
     *
     * @param string $css
     * @return Module
     */
    public function setCss($css)
    {
        $this->css = (empty($css)) ? strtolower($this->tag) : strtolower($css);
    
        return $this;
    }

    /**
     * Get css
     *
     * @return string 
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set js
     *
     * @param string $js
     * @return Module
     */
    public function setJs($js)
    {
        $this->js = (empty($js)) ? strtolower($this->tag) : strtolower($js);
    
        return $this;
    }

    /**
     * Get js
     *
     * @return string 
     */
    public function getJs()
    {
        return $this->js;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Module
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Module
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }
}