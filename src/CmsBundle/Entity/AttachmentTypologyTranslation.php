<?php
/*
 * This file is part of the STS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invictus\CmsBundle\Entity\AttachmentTypologyTranslation
 *
 * @ORM\Table(name="attachment_typology_translation")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\AttachmentTypologyTranslationRepository")
 */
class AttachmentTypologyTranslation extends ItemTranslation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var AttachmentTypology
     *
     * @ORM\ManyToOne(targetEntity="AttachmentTypology", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_attachment_typology_id", referencedColumnName="id")
     * })
     */
    private $fkBase;


    /**
     * Set fkBase
     *
     * @param \Invictus\CmsBundle\Entity\AttachmentTypology $fkBase
     * @return AttachmentTypologyTranslation
     */
    public function setFkBase(\Invictus\CmsBundle\Entity\AttachmentTypology $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\CmsBundle\Entity\AttachmentTypology 
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}