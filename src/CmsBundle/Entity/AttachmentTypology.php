<?php
/*
 * This file is part of the STS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\Item as Item;

/**
 * Invictus\CmsBundle\Entity\AttachmentTypology
 *
 * @ORM\Table(name="attachment_typology")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\AttachmentTypologyRepository")
 */
class AttachmentTypology extends Item
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $tag
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var string $icon
     *
     * @ORM\Column(name="icon", type="string", length=50, nullable=true)
     */
    private $icon;

    /**
     * @var string $allowedTypes
     *
     * @ORM\Column(name="allowed_types", type="text", nullable=true)
     */
    private $allowedTypes;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="text", nullable=true)
     */
    private $type;

    /**
     * @var string $customJs
     *
     * @ORM\Column(name="custom_js", type="text", nullable=true)
     */
    private $customJs;

    /**
     * @ORM\OneToMany(targetEntity="AttachmentTypologyTranslation", mappedBy="fkBase")
     */
    protected $translations;



    public function __construct()
    {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set tag
     *
     * @param string $tag
     * @return AttachmentTypology
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return AttachmentTypology
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    
        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return AttachmentTypology
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set customJs
     *
     * @param string $customjs
     * @return AttachmentTypology
     */
    public function setCustomJs($customJs)
    {
        $this->customJs = $customJs;
    
        return $this;
    }

    /**
     * Get customjs
     *
     * @return string 
     */
    public function getCustomJs()
    {
        return $this->customJs;
    }

    /**
     * Add translations
     *
     * @param \Invictus\CmsBundle\Entity\AttachmentTypologyTranslation $translations
     * @return AttachmentTypology
     */
    public function addTranslation(\Invictus\CmsBundle\Entity\AttachmentTypologyTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\CmsBundle\Entity\AttachmentTypologyTranslation $translations
     */
    public function removeTranslation(\Invictus\CmsBundle\Entity\AttachmentTypologyTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set allowedTypes
     *
     * @param string $allowedTypes
     * @return AttachmentTypology
     */
    public function setAllowedTypes($allowedTypes)
    {
        $this->allowedTypes = $allowedTypes;
    
        return $this;
    }

    /**
     * Get allowedTypes
     *
     * @return string 
     */
    public function getAllowedTypes()
    {
        return $this->allowedTypes;
    }
}