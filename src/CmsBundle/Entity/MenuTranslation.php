<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MenuTranslation
 *
 * @ORM\Entity
 * @ORM\Table(name="menu_translation")
 */
class MenuTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_menu_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return MenuTranslation
     */
    public function setLink($link)
    {
        $newLink = (stristr($link, 'http://')) ? $link : 'http://'.$link;
        $newLink = (empty($link)) ? null : $newLink;
        $this->link = $newLink;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\CmsBundle\Entity\Menu $fkBase
     * @return MenuTranslation
     */
    public function setFkBase(\Invictus\CmsBundle\Entity\Menu $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\CmsBundle\Entity\Menu
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}