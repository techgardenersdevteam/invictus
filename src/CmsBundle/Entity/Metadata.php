<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Metadata
 *
 * @ORM\Table(name="metadata", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id", "slug"})})
 * @UniqueEntity(fields={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id", "slug"})
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\MetadataRepository")
 */
class Metadata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="page_title", type="string", length=255, nullable=true)
     */
    private $pageTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text", nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_meta", type="text", nullable=true)
     */
    private $customMeta;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_vars", type="text", nullable=true)
     */
    private $customVars;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=100, nullable=true)
     */
    private $path = '';

    /**
     * @var string
     *
     * @ORM\Column(name="change_freq", type="string", length=20, nullable=true)
     */
    private $changeFreq;

    /**
     * @var string
     *
     * @ORM\Column(name="priority", type="string", length=4, nullable=true)
     */
    private $priority;

    /**
     * @var string
     *
     * @ORM\Column(name="redirect_url", type="string", length=255, nullable=true)
     */
    private $redirectUrl;

    /**
     * @var integer
     *
     * @ORM\Column(name="redirect_code", type="smallint", nullable=true)
     */
    private $redirectCode;

    /**
     * @var \App
     *
     * @ORM\ManyToOne(targetEntity="App")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_app_id", referencedColumnName="id")
     * })
     */
    private $fkApp;

    /**
     * @var \Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_module_id", referencedColumnName="id")
     * })
     */
    private $fkModule;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_language_id", referencedColumnName="id")
     * })
     */
    private $fkLanguage;


    /**
     * @var integer
     *
     * @ORM\Column(name="fk_item_id", type="integer", nullable=true)
     */
    private $fkItemId = 0;

    /* Relazioni verso le item dei vari moduli */

    /**
     * @var News
     *
     * @ORM\ManyToOne(targetEntity="Invictus\NewsBundle\Entity\News", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkNewsItem;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Invictus\ProjectBundle\Entity\Project", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkProjectItem;

    /**
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="Invictus\PageBundle\Entity\Page", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkPageItem;

    /**
     * @var Template
     *
     * @ORM\ManyToOne(targetEntity="Invictus\TemplateBundle\Entity\Template", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkTemplateItem;

    /**
     * @var Catgeory
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Category", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkCategoryItem;

    /**
     * @var Service
     *
     * @ORM\ManyToOne(targetEntity="Invictus\ServiceBundle\Entity\Service", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkServiceItem;

    /**
     * @var Faq
     *
     * @ORM\ManyToOne(targetEntity="Invictus\FaqBundle\Entity\Faq", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkFaqItem;

    /**
     * @var Guide
     *
     * @ORM\ManyToOne(targetEntity="Invictus\GuideBundle\Entity\Guide", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkGuideItem;

    /**
     * @var Testimonial
     *
     * @ORM\ManyToOne(targetEntity="Invictus\TestimonialBundle\Entity\Testimonial", inversedBy="metadatas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkTestimonialItem;

    /* Fine relazioni verso le item dei vari moduli */


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Metadata
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set pageTitle
     *
     * @param string $pageTitle
     * @return Metadata
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    
        return $this;
    }

    /**
     * Get pageTitle
     *
     * @return string 
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Metadata
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    
        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Metadata
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set customMeta
     *
     * @param string $customMeta
     * @return Metadata
     */
    public function setCustomMeta($customMeta)
    {
        $this->customMeta = $customMeta;
    
        return $this;
    }

    /**
     * Get customMeta
     *
     * @return string 
     */
    public function getCustomMeta()
    {
        return $this->customMeta;
    }

    /**
     * Set customVars
     *
     * @param string $customVars
     * @return Metadata
     */
    public function setCustomVars($customVars)
    {
        $this->customVars = $customVars;
    
        return $this;
    }

    /**
     * Get customVars
     *
     * @return string 
     */
    public function getCustomVars($asArray = false)
    {
        return ($asArray) ? json_decode($this->customVars, true) : $this->customVars ;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Metadata
     */
    public function setPath($path)
    {
        $this->path = strtolower(trim($path, '/'));
        $this->path = str_replace(' ', '-', $this->path);
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set changeFreq
     *
     * @param string $changeFreq
     * @return Metadata
     */
    public function setChangeFreq($changeFreq)
    {
        $this->changeFreq = $changeFreq;
    
        return $this;
    }

    /**
     * Get changeFreq
     *
     * @return string 
     */
    public function getChangeFreq()
    {
        return $this->changeFreq;
    }

    /**
     * Set priority
     *
     * @param string $priority
     * @return Metadata
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    
        return $this;
    }

    /**
     * Get priority
     *
     * @return string 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set redirectUrl
     *
     * @param string $redirectUrl
     * @return Metadata
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    
        return $this;
    }

    /**
     * Get redirectUrl
     *
     * @return string 
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * Set redirectCode
     *
     * @param integer $redirectCode
     * @return Metadata
     */
    public function setRedirectCode($redirectCode)
    {
        $this->redirectCode = $redirectCode;
    
        return $this;
    }

    /**
     * Get redirectCode
     *
     * @return integer 
     */
    public function getRedirectCode()
    {
        return $this->redirectCode;
    }

    /**
     * Set fkApp
     *
     * @param \Invictus\CmsBundle\Entity\App $fkApp
     * @return Metadata
     */
    public function setFkApp(\Invictus\CmsBundle\Entity\App $fkApp = null)
    {
        $this->fkApp = $fkApp;
    
        return $this;
    }

    /**
     * Get fkApp
     *
     * @return \Invictus\CmsBundle\Entity\App 
     */
    public function getFkApp()
    {
        return $this->fkApp;
    }

    /**
     * Set fkModule
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkModule
     * @return Metadata
     */
    public function setFkModule(\Invictus\CmsBundle\Entity\Module $fkModule = null)
    {
        $this->fkModule = $fkModule;
    
        return $this;
    }

    /**
     * Get fkModule
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkModule()
    {
        return $this->fkModule;
    }

    /**
     * Set fkLanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $fkLanguage
     * @return Metadata
     */
    public function setFkLanguage(\Invictus\CmsBundle\Entity\Language $fkLanguage = null)
    {
        $this->fkLanguage = $fkLanguage;
    
        return $this;
    }

    /**
     * Get fkLanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }

    /**
     * Set fkNewsItem
     *
     * @param \Invictus\NewsBundle\Entity\News $fkNewsItem
     * @return Metadata
     */
    public function setFkNewsItem(\Invictus\NewsBundle\Entity\News $fkNewsItem = null)
    {
        //$this->fkNewsItem = $fkNewsItem;

        return $this;
    }

    /**
     * Get fkNewsItem
     *
     * @return \Invictus\NewsBundle\Entity\News
     */
    public function getFkNewsItem()
    {
        return $this->fkNewsItem;
    }

    /**
     * Set fkProjectItem
     *
     * @param \Invictus\ProjectBundle\Entity\Project $fkProjectItem
     * @return Metadata
     */
    public function setFkProjectItem(\Invictus\ProjectBundle\Entity\Project $fkProjectItem = null)
    {
        //$this->fkProjectItem = $fkProjectItem;

        return $this;
    }

    /**
     * Get fkProjectItem
     *
     * @return \Invictus\ProjectBundle\Entity\Project
     */
    public function getFkProjectItem()
    {
        return $this->fkProjectItem;
    }

    /**
     * Set fkPageItem
     *
     * @param \Invictus\PageBundle\Entity\Page $fkPageItem
     * @return Metadata
     */
    public function setFkPageItem(\Invictus\PageBundle\Entity\Page $fkPageItem = null)
    {
        //$this->fkPageItem = $fkPageItem;

        return $this;
    }

    /**
     * Get fkPageItem
     *
     * @return \Invictus\PageBundle\Entity\Page
     */
    public function getFkPageItem()
    {
        return $this->fkPageItem;
    }

    /**
     * Set fkItemId
     *
     * @param integer $fkItemId
     * @return Metadata
     */
    public function setFkItemId($fkItemId)
    {
        $this->fkItemId = $fkItemId;
    
        return $this;
    }

    /**
     * Get fkItemId
     *
     * @return integer 
     */
    public function getFkItemId()
    {
        return $this->fkItemId;
    }

    public function getURL(){
        $url = array();
        $url[] = $this->getPath();
        $url[] = $this->getSlug();
        $url = array_filter($url);

        return '/'.implode('/', $url);
    }
}