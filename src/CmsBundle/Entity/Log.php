<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Log
 *
 * @ORM\Table(name="log")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\LogRepository")
 */
class Log extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="integer", length=255, nullable=false)
     */
    private $level = 1;

    /**
     * @var datetime $datetime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="user_agent", type="string", length=255, nullable=true)
     */
    private $user_agent;

    /**
     * @var string
     *
     * @ORM\Column(name="fk_item_id", type="string", length=255, nullable=true)
     */
    private $item_id;

    /**
     * @var \Module
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_module_id", referencedColumnName="id")
     * })
     */
    private $fkModule = NULL;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_language_id", referencedColumnName="id")
     * })
     */
    private $fkLanguage = NULL;

    /**
     * @var \Admin
     *
     * @ORM\ManyToOne(targetEntity="Invictus\AdminBundle\Entity\Admin")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_admin_id", referencedColumnName="id")
     * })
     */
    private $fkAdmin = NULL;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->metadatas = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->tag;
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Log
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Log
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Log
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Log
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set user_agent
     *
     * @param string $userAgent
     * @return Log
     */
    public function setUserAgent($userAgent)
    {
        $this->user_agent = $userAgent;
    
        return $this;
    }

    /**
     * Get user_agent
     *
     * @return string 
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * Set item_id
     *
     * @param string $itemId
     * @return Log
     */
    public function setItemId($itemId)
    {
        $this->item_id = $itemId;
    
        return $this;
    }

    /**
     * Get item_id
     *
     * @return string 
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * Set fkModule
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkModule
     * @return Log
     */
    public function setFkModule(\Invictus\CmsBundle\Entity\Module $fkModule = null)
    {
        $this->fkModule = $fkModule;
    
        return $this;
    }

    /**
     * Get fkModule
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkModule()
    {
        return $this->fkModule;
    }

    /**
     * Set fkLanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $fkLanguage
     * @return Log
     */
    public function setFkLanguage(\Invictus\CmsBundle\Entity\Language $fkLanguage = null)
    {
        $this->fkLanguage = $fkLanguage;
    
        return $this;
    }

    /**
     * Get fkLanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }

    /**
     * Set fkAdmin
     *
     * @param \Invictus\AdminBundle\Entity\Admin $fkAdmin
     * @return Log
     */
    public function setFkAdmin(\Invictus\AdminBundle\Entity\Admin $fkAdmin = null)
    {
        $this->fkAdmin = $fkAdmin;
    
        return $this;
    }

    /**
     * Get fkAdmin
     *
     * @return \Invictus\AdminBundle\Entity\Admin 
     */
    public function getFkAdmin()
    {
        return $this->fkAdmin;
    }
}