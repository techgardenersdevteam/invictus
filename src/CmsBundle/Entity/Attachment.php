<?php
/*
 * This file is part of the STS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Invictus\CmsBundle\Entity\Item as Item;


/**
 * Invictus\CmsBundle\Entity\Attachment
 *
 * @ORM\Table(name="attachment")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\AttachmentRepository")
 */
class Attachment extends Item
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_module_id", referencedColumnName="id")
     * })
     */
    private $fkModule;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @var string $oldFilename
     *
     * @ORM\Column(name="old_filename", type="string", length=255, nullable=true)
     */
    private $oldFilename;

    /**
     * @var string $filename
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @var string $ext
     *
     * @ORM\Column(name="ext", type="string", length=5, nullable=true)
     */
    private $ext;

    /**
     * @var integer $size
     *
     * @ORM\Column(name="size", type="decimal", nullable=true)
     */
    private $size;

    /**
     * @var string $mime
     *
     * @ORM\Column(name="mime", type="string", length=50, nullable=true)
     */
    private $mime;

    /**
     * @var string $panoramaPath
     *
     * @ORM\Column(name="panorama_path", type="string", length=255, nullable=true)
     */
    private $panoramaPath;

    /**
     * @var \AttachmentTypology
     *
     * @ORM\ManyToOne(targetEntity="AttachmentTypology")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_attachment_typology_id", referencedColumnName="id")
     * })
     */
    private $fkAttachmentTypology;
    
    /**
     * @ORM\OneToMany(targetEntity="AttachmentTranslation", mappedBy="fkBase")
     *
     * set to protected to work with resetTranslations in Item.php
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Visibility", mappedBy="fkAttachmentItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Position", mappedBy="fkPageItem")
     */
    protected $positions;

    /* Relazioni verso i vari tipi di item */


    /*
     * @var News
     *
     * @ORM\ManyToOne(targetEntity="Invictus\NewsBundle\Entity\News", inversedBy="attachments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    //private $fkNewsItem;

    /*
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Invictus\ProjectBundle\Entity\Project", inversedBy="attachments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    //private $fkProjectItem;

    /*
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="Invictus\PageBundle\Entity\Page", inversedBy="attachments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    //private $fkPageItem;


    /* Fine relazioni verso i vari tipi di item */


    /**
     * @var integer $size
     *
     * @ORM\Column(name="fk_item_id", type="integer", nullable=true)
     */
    private $fkItemId;



    public function __clone() {
        if ($this->id) {
            $this->id = null;
        }
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set oldFilename
     *
     * @param string $oldFilename
     * @return Attachment
     */
    public function setOldFilename($oldFilename)
    {
        $this->oldFilename = $oldFilename;
    
        return $this;
    }

    /**
     * Get oldFilename
     *
     * @return string 
     */
    public function getOldFilename()
    {
        return $this->oldFilename;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return Attachment
     */
    public function setFilename($filename)
    {
        $this->filename = $this->sanitizeFilename($filename);
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename($token = false)
    {
        if(!$token){
            return $this->filename;
        }else{
            return $this->filename.$token;
        }
    }

    /**
     * Set ext
     *
     * @param string $ext
     * @return Attachment
     */
    public function setExt($ext)
    {
        $this->ext = $ext;
    
        return $this;
    }

    /**
     * Get ext
     *
     * @return string 
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return Attachment
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mime
     *
     * @param string $mime
     * @return Attachment
     */
    public function setMime($mime)
    {
        $this->mime = $mime;
    
        return $this;
    }

    /**
     * Get mime
     *
     * @return string 
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * Set fkAttachmentTypology
     *
     * @param \Invictus\CmsBundle\Entity\AttachmentTypology $fkAttachmentTypology
     * @return Attachment
     */
    public function setFkAttachmentTypology(\Invictus\CmsBundle\Entity\AttachmentTypology $fkAttachmentTypology = NULL)
    {
        $this->fkAttachmentTypology = $fkAttachmentTypology;
    
        return $this;
    }

    /**
     * Get fkAttachmentTypology
     *
     * @return \Invictus\CmsBundle\Entity\AttachmentTypology 
     */
    public function getFkAttachmentTypology()
    {
        return $this->fkAttachmentTypology;
    }

    /**
     * Add translations
     *
     * @param \Invictus\CmsBundle\Entity\AttachmentTranslation $translations
     * @return Attachment
     */
    public function addTranslation(\Invictus\CmsBundle\Entity\AttachmentTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\CmsBundle\Entity\AttachmentTranslation $translations
     */
    public function removeTranslation(\Invictus\CmsBundle\Entity\AttachmentTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Attachment
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibilities[] = $visibility;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibilities->removeElement($visibility);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }


    /**
     * Set fkModule
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkModule
     * @return Attachment
     */
    public function setFkModule(\Invictus\CmsBundle\Entity\Module $fkModule = null)
    {
        $this->fkModule = $fkModule;
    
        return $this;
    }

    /**
     * Get fkModule
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkModule()
    {
        return $this->fkModule;
    }

    /**
     * Set fkNewsItem
     *
     * @param \Invictus\NewsBundle\Entity\News $fkNewsItem
     * @return Attachment
     */
    public function setFkNewsItem(\Invictus\NewsBundle\Entity\News $fkNewsItem = null)
    {
        //$this->fkNewsItem = $fkNewsItem;

        return $this;
    }

    /**
     * Get fkNewsItem
     *
     * @return \Invictus\NewsBundle\Entity\News
     */
    public function getFkNewsItem()
    {
        return $this->fkNewsItem;
    }

    /**
     * Set fkProjectItem
     *
     * @param \Invictus\ProjectBundle\Entity\Project $fkProjectItem
     * @return Attachment
     */
    public function setFkProjectItem(\Invictus\ProjectBundle\Entity\Project $fkProjectItem = null)
    {
        //$this->fkProjectItem = $fkProjectItem;

        return $this;
    }

    /**
     * Get fkProjectItem
     *
     * @return \Invictus\ProjectBundle\Entity\Project
     */
    public function getFkProjectItem()
    {
        return $this->fkProjectItem;
    }

    /**
     * Set fkPageItem
     *
     * @param \Invictus\PageBundle\Entity\Page $fkPageItem
     * @return Attachment
     */
    public function setFkPageItem(\Invictus\PageBundle\Entity\Page $fkPageItem = null)
    {
        //$this->fkPageItem = $fkPageItem;

        return $this;
    }

    /**
     * Get fkPageItem
     *
     * @return \Invictus\PageBundle\Entity\Page
     */
    public function getFkPageItem()
    {
        return $this->fkPageItem;
    }




    public function getUploadDir()
    {
        return 'uploads';
    }

    public function getUploadRootDir()
    {
        // il percorso assoluto della cartella dove i
        // documenti caricati verranno salvati
        return __DIR__.'/../../../../../../web/'.$this->getUploadDir();
    }

    public function upload($path = false, $token = false)
    {
        // la proprietà file può essere vuota se il campo non è obbligatorio
        if (NULL === $this->file){
            return;
        }

        $uploadDir = $path ? $this->sanitizePath($path) : $this->getUploadRootDir() ;

        $originalFilename = $this->sanitizeFilename($this->file->getClientOriginalName());

        $filename = $this->getUniqueFilename($uploadDir, $originalFilename, $token);

        // move accetta come parametri la cartella di destinazione
        // e il nome del file di destinazione
        $this->file->move(
            $uploadDir,
            $this->sanitizeFilename($filename)
        );

        return $filename;
    }

    public function sanitizeFilename($filename, $debug = false)
    {
        $search  = array('è','à©','à.','ò','ù','ì','à §');
        $replace = array('e','e' ,'a' ,'o','u','i','c');

        $s1 = str_replace(' ', '_', $filename);
        //Sostituisce i gli indici in $search con gli indici in $replace nella stringa $S
        $s2 = str_replace(
            $search,
            $replace,
            $s1);
        //Rimuove tutti i caratteri non compresi fra 'a' e 'z', fra 0 e 9, diversi da _, da - e da .
        $s3 = preg_replace('/[^a-z0-9-_\.]/i', '', strtolower($s2));
        //Rimuove i doppi _ e li sostituisce con un singolo _
        $s4 = preg_replace("/(_{2,})+/", '_', $s3);
        // elimina - e _ dalle estremità della stringa
        $s5 = trim($s4, '_');
        $s6 = trim($s5, '-');
        // Elimina l'undescore o il meno che si possono trovare appena prima dell'estensione
        $s7 = str_replace(
            array('_.', '-.'),
            array('.', '.'),
            $s6);

        if($debug){
            echo "s0:$s<br />
				s1: $s1<br />
				s2: $s2<br />
				s3: $s3<br />
				s4: $s4<br />
				s5: $s5<br />
				s6: $s6<br />
				s7: $s7<br />";
        }

        return $s7;
    }

    /**
     * @param bool $path
     * @param bool $debug
     * @return null|string
     *
     * Funzione duplicata anche in AttachmentTranslation, non cancellare serve in entrambe le classe
     */
    function sanitizePath($path, $debug = false)
    {
        $search  = array('è','à©','à.','ò','ù','ì','à §');
        $replace = array('e','e' ,'a' ,'o','u','i','c');

        $s1 = str_replace(' ', '_', $path);
        //Sostituisce i gli indici in $search con gli indici in $replace nella stringa $S
        $s2 = str_replace(
            $search,
            $replace,
            $s1);
        //Rimuove tutti i caratteri non compresi fra 'a' e 'z', fra 0 e 9, diversi da _, da - e da .
        $s3 = preg_replace('/[^a-z0-9-_\/]/i', '', strtolower($s2));
        //Rimuove i doppi _ e i doppi - un singolo _ e -
        $s4 = preg_replace("/(_{2,})+/", '_', $s3);
        $s4 = preg_replace("/(-{2,})+/", '-', $s4);
        // elimina - e _ dalle estremità della stringa
        $s5 = trim($s4, '_');
        $s6 = trim($s5, '-');
        //Rimuove i doppi / e li sostituisce con un singolo / alla stringa a cui è stato aggiunto un / all'inizio e uno alla fine
        $s7 = preg_replace("/(\/{2,})+/", '/','/'.$s6.'/');
        $s8 = ltrim($s7, '/');

        if($debug){
            echo "s0:$s<br />
                    s1: $s1<br />
                    s2: $s2<br />
                    s3: $s3<br />
                    s4: $s4<br />
                    s5: $s5<br />
                    s6: $s6<br />
                    s7: $s7<br />
                    s8: $s8<br />";
        }

        return $s8;
    }


    public function getUniqueFilename($path, $basename, $token = false)
    {
        //echo "$path, $basename, $token : risultato ";
        $filePath = $path.'/'.$basename;
        $filenameInfo = pathinfo($filePath);
        $filename = $filenameInfo['filename'];
        $ext = $filenameInfo['extension'];

        if(file_exists($filePath)){
            if($token){
                $newFilename = $filename."_"."$token.$ext";
                if(file_exists($path.'/'.$newFilename)){
                    $time = time();
                    $newFilename = $filename."_"."$time.$ext";
                }
            }else{
                $time = time();
                $newFilename = $filename."_"."$time.$ext";
            }
        }else{
            $newFilename = $filename.".".$ext;
        }

        //echo "$newFilename";die();
        return $newFilename;
    }




    /**
     * Set fkItemId
     *
     * @param integer $fkItemId
     * @return Attachment
     */
    public function setFkItemId($fkItemId)
    {
        $this->fkItemId = $fkItemId;
    
        return $this;
    }

    /**
     * Get fkItemId
     *
     * @return integer 
     */
    public function getFkItemId()
    {
        return $this->fkItemId;
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Attachment
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }


    /**
     * Set panoramaPath
     *
     * @param string $panoramaPath
     * @return Attachment
     */
    public function setPanoramaPath($panoramaPath)
    {
        $this->panoramaPath = $panoramaPath;
    
        return $this;
    }

    /**
     * Get panoramaPath
     *
     * @return string 
     */
    public function getPanoramaPath()
    {
        return $this->panoramaPath;
    }
}