<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * ItemTranslation object
 * @ORM\MappedSuperclass
 */
abstract class ItemTranslation
{

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="abstract", type="text", nullable=true)
     */
    private $abstract;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     */
    private $body;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Language", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_language_id", referencedColumnName="id")
     * })
     */
    private $fkLanguage;



    public function getIdentifier()
    {
        return $this->label;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return ItemTranslation
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set abstract
     *
     * @param string $abstract
     * @return ItemTranslation
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;
    
        return $this;
    }

    /**
     * Get abstract
     *
     * @return string 
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return ItemTranslation
     */
    public function setBody($body)
    {
        $this->body = $body;
    
        return $this;
    }

    /**
     * Get body
     *
     * @return string 
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set fkLanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $fkLanguage
     * @return ItemTranslation
     */
    public function setFkLanguage(\Invictus\CmsBundle\Entity\Language $fkLanguage = null)
    {
        $this->fkLanguage = $fkLanguage;
    
        return $this;
    }

    /**
     * Get fkLanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }
}