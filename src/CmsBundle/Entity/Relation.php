<?php
/*
 * This file is part of the STS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Invictus\CmsBundle\Entity\Relation
 *
 * @ORM\Table(name="relation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_app_id", "fk_language_id", "fk_module_id_a", "fk_item_id_a", "fk_module_id_b", "fk_item_id_b", "slot" })})
 * @UniqueEntity(fields={"fk_app_id", "fk_language_id", "fk_module_id_a", "fk_item_id_a", "fk_module_id_b", "fk_item_id_b", "slot"})
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\RelationRepository")
 */
class Relation
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var App
     *
     * @ORM\ManyToOne(targetEntity="App")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_app_id", referencedColumnName="id")
     * })
     */
    private $fkApp;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_language_id", referencedColumnName="id")
     * })
     */
    private $fkLanguage;

    /**
     * @var Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_module_id_a", referencedColumnName="id")
     * })
     */
    private $fkModuleA;
    
    /**
     * @var integer $fkItemIdA
     *
     * @ORM\Column(name="fk_item_id_a", type="integer", nullable=true)
     */
    private $fkItemIdA;

    /**
     * @var Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_module_id_b", referencedColumnName="id")
     * })
     */
    private $fkModuleB;

    /**
     * @var integer $fkItemIdB
     *
     * @ORM\Column(name="fk_item_id_b", type="integer", nullable=true)
     */
    private $fkItemIdB;

    /**
     * @var integer $position
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var integer $slot
     *
     * @ORM\Column(name="slot", type="integer", nullable=true)
     */
    private $slot = 1;
    
    /**
     * @var string $extra
     *
     * @ORM\Column(name="extra", type="text", nullable=true)
     */
    private $extra;
    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkItemIdA
     *
     * @param integer $fkItemIdA
     * @return Relation
     */
    public function setFkItemIdA($fkItemIdA)
    {
        $this->fkItemIdA = $fkItemIdA;
    
        return $this;
    }

    /**
     * Get fkItemIdA
     *
     * @return integer 
     */
    public function getFkItemIdA()
    {
        return $this->fkItemIdA;
    }

    /**
     * Set fkItemIdB
     *
     * @param integer $fkItemIdB
     * @return Relation
     */
    public function setFkItemIdB($fkItemIdB)
    {
        $this->fkItemIdB = $fkItemIdB;
    
        return $this;
    }

    /**
     * Get fkItemIdB
     *
     * @return integer 
     */
    public function getFkItemIdB()
    {
        return $this->fkItemIdB;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Relation
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set slot
     *
     * @param integer $slot
     * @return Relation
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;
    
        return $this;
    }

    /**
     * Get slot
     *
     * @return integer 
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * Set extra
     *
     * @param string $extra
     * @return Relation
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;
    
        return $this;
    }

    /**
     * Get extra
     *
     * @return string 
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set fkApp
     *
     * @param \Invictus\CmsBundle\Entity\App $fkApp
     * @return Relation
     */
    public function setFkApp(\Invictus\CmsBundle\Entity\App $fkApp = null)
    {
        $this->fkApp = $fkApp;
    
        return $this;
    }

    /**
     * Get fkApp
     *
     * @return \Invictus\CmsBundle\Entity\App 
     */
    public function getFkApp()
    {
        return $this->fkApp;
    }

    /**
     * Set fkLanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $fkLanguage
     * @return Relation
     */
    public function setFkLanguage(\Invictus\CmsBundle\Entity\Language $fkLanguage = null)
    {
        $this->fkLanguage = $fkLanguage;
    
        return $this;
    }

    /**
     * Get fkLanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }

    /**
     * Set fkModuleA
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkModuleA
     * @return Relation
     */
    public function setFkModuleA(\Invictus\CmsBundle\Entity\Module $fkModuleA = null)
    {
        $this->fkModuleA = $fkModuleA;
    
        return $this;
    }

    /**
     * Get fkModuleA
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkModuleA()
    {
        return $this->fkModuleA;
    }

    /**
     * Set fkModuleB
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkModuleB
     * @return Relation
     */
    public function setFkModuleB(\Invictus\CmsBundle\Entity\Module $fkModuleB = null)
    {
        $this->fkModuleB = $fkModuleB;
    
        return $this;
    }

    /**
     * Get fkModuleB
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkModuleB()
    {
        return $this->fkModuleB;
    }
}