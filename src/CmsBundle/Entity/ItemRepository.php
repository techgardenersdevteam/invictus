<?php
/**
* This file is part of the INVICTUS project
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*
* @copyright 2012-2013 devsign
* @author Matteo Poile <matteo.poile@devsign.it>
* @package InvictusCmsBundle\Entity\ItemRepository.php
*/

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * ItemRepository
 *
 * INVICTUS item repository that extends Doctrine EntityRepository
 * 
 * Every INVICTUS related repository should extend this repository 
 * to have access to INVICTUS specific item repository functions like findOneItemBy and findItemsBy
 */
class ItemRepository extends EntityRepository
{

    protected $invictusKernel = NULL;

    protected $app = null;
    protected $language = null;
    protected $module = null;
    protected $moduleConfig = null;
    protected $UILanguage = null;

    public $debug = false;


    /**
     * @param $invictusKernel
     * @return $this
     *
     * todo: try to override EntityRepository constructor
     *
     * public function __construct($em, $class, $invictusKernel){
     *     parent::__construct($em, $class);
     *     $this->invictusKernel = $invictusKernel;
     * }
     */
    public function setInvictusKernel($invictusKernel = false)
    {
        if($invictusKernel){
            $this->invictusKernel = $invictusKernel;

            $this->app = $this->invictusKernel->getCurrentApp();
            $this->language = $this->invictusKernel->getCurrentLanguage();
            $this->module = $this->invictusKernel->getCurrentModule();
            if($this->module){
                $this->moduleConfig = json_decode($this->module->getConfig());
            }
            $this->UILanguage = $this->invictusKernel->UILanguageId;
        }

        return $this;
    }
    
    
    public function setDebug($debug = false){
        $this->debug($debug);
        return $this;
    }


    public function getData($config = array(), $debug = false)
    {

        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );

        if(is_array($config)){
            $this->configureDefaults($config);
            $config = array_replace_recursive($defaults, $config);
        }

        //echo "<pre>";print_r($config);echo "</pre>";die();

        foreach($config['where'] as $key => $value){
            if(is_null($value)){
                unset($config['where'][$key]);
            }
        }

        foreach($config['params'] as $key => $value){
            if(is_null($value)){
                unset($config['params'][$key]);
            }
        }

        $translation = $this->buildTranslation($config);
        $config = $translation['config'];
        $translationDQL = $translation['DQL'];

        $visibility = $this->buildVisibility($config);
        $config = $visibility['config'];
        $visibilityDQL = $visibility['DQL'];

        $position = $this->buildPosition($config);
        $config = $position['config'];
        $positionDQL = $position['DQL'];

        $metadata = $this->buildMetadata($config);
        $config = $metadata['config'];
        $metadataDQL = $metadata['DQL'];

        $select = "SELECT ".implode(', ', $config['select']);

        $where = $this->buildWhere($config);
        $config = $where['config'];
        $whereDQL = $where['DQL'];

        $groupByDQL = '';

        $orderByDQL = (!empty($config['orderBy'])) ? " ORDER BY ".implode(', ', $config['orderBy']) : '';

        $DQL = "$select

                FROM
                  {$this->module->getBundleEntity()} base

                $translationDQL

                $visibilityDQL

                $positionDQL

                $metadataDQL

                $whereDQL

                $groupByDQL

                $orderByDQL";


        $query = $this->getEntityManager()
            ->createQuery($DQL);


        if(is_array($config['params'])){
            foreach($config['params'] as $param => $value){
                // Se è un array viene gestito senza bind dirretamente dove serve
                if(!is_array($value)){
                    $query->setParameter($param, $value);
                }
            }
        }

        $SQL = $query->getSQL();

       //echo "<pre>";print_r($SQL);print_r($DQL);print_r($config);echo "</pre>";die();


        $result = array(
            'data' => null,
            'info' => null,
            'DQL' => $DQL,
            'SQL' => $SQL
        );


        if(isset($config['itemId']) AND !is_array($config['itemId'])){

            try {
                $result['data'] = $query->getSingleResult();
            } catch (\Doctrine\ORM\NoResultException $e){

                if($debug){
                    /**
                     * Verificare cos'è andato storto:
                     * esiste un item con quell'id?
                     * è abilitato e non cancellato?
                     * è visibile nell'app e nel locale corrente?
                     * ha un metadata nell'app e nel locale corrente?
                     * Se $debug è true stampare i problemi.
                     */
                }else{
                    /**
                     * Studiare come lanciare eccezione con messaggio personalizzato
                     */
                }
            }
        }else{

            if($config['paginator']){

                $currentPage = (isset($config['paginator']['currentPage']) AND $config['paginator']['currentPage'] > 0 ) ? $config['paginator']['currentPage'] : 1 ;
                $pageSize = (isset($config['paginator']['pageSize']) AND $currentPage > 0 ) ? $config['paginator']['pageSize'] : 999999 ;

                $query->setFirstResult($pageSize * ($currentPage - 1));

                if($pageSize){
                    $query->setMaxResults($pageSize);
                }

                try{
                    $result['data']  = new Paginator($query, $fetchJoinCollection = true);
                    $totalItems = count($result['data']);
                    $totalPages = ceil($totalItems / $pageSize) ? ceil($totalItems / $pageSize) : 1;

                    if($currentPage > $totalPages){
                        $currentPage = $totalPages;
                        $query->setFirstResult($pageSize * ($currentPage - 1));
                        if($pageSize){
                            $query->setMaxResults($pageSize);
                        }

                        $result['data'] = new Paginator($query, $fetchJoinCollection = true);
                    }

                    $result['info'] = array(
                        'currentPage' => $currentPage,
                        'pageSize' => $pageSize,
                        'totalItems' => $totalItems,
                        'totalPages' => $totalPages
                    );
                }catch (Exception $e) {
                    $result['data'] = array();
                }
            }else{

                try{
                    $result['data']  = $query->getResult();
                }catch (\Doctrine\ORM\NoResultException $e) {
                    $result['data'] = array();
                }
            }

        }


        if($debug){
            $this->config = $config;
            $this->debug($query, $result['data']);
            die();
        }


        if(!count($result['data'])){
            $result['data'] = array();
        }


        return $result;

    }


    public function getCurrentItem($config = null, $debug = false)
    {
        $config['itemId'] = $this->invictusKernel->itemId;

        return $this->getData($config, $debug);
    }


    public function getAttachments($config = null, $debug = false)
    {
        $config['moduleId'] = 6;
        $config['joins']['metadata'] = false;
        $fkModuleId = (isset($config['fkModuleId'])) ? $config['fkModuleId'] : $this->module->getId();
        $config['where']['auto_base.fkModule'] = 'base.fkModule = :fkModule';
        $config['params']['fkModule'] = $fkModuleId;
        if((isset($config['fkItemId']))){
            $config['where']['auto_base.fkItemId'] = 'base.fkItemId = :fkItemId';
            $config['params']['fkItemId'] = $config['fkItemId'];
        }

        if((isset($config['typology']))){
            if(is_array($config['typology'])){
                $config['where']['auto_base.fkAttachmentTypology'] = 'base.fkAttachmentTypology IN('.implode(',', $config['typology']).')';
            }else{
                $config['where']['auto_base.fkAttachmentTypology'] = 'base.fkAttachmentTypology = :fkAttachmentTypology';
                $config['params']['fkAttachmentTypology'] = $config['typology'];
            }
        }
        return $this->getData($config, $debug);
    }


    public function getItemIdentifier($id, $locale = false)
    {
        $locale = ($locale) ? $locale : $this->locale;

        if($this->config->module->ML){
            $trans = $this->getEntityManager()
                ->createQuery("
                    SELECT trans

                    FROM {$this->bundle}:{$this->entity}Translation trans

                    WHERE
                        trans.fk{$this->entity} = :id
                        trans.fkLanguage = :locale"
                )
                ->setParameter('id', $id)
                ->setParameter('locale', $locale);
            var_dump($trans);
            $identifier = $trans->getLabel();
        }else{
            $base = $this->getEntityManager()
                ->createQuery("
                    SELECT base

                    FROM {$this->bundle}:{$this->entity} base

                    WHERE
                        base.id = :id"
                )
                ->setParameter('id', $id);

            $identifier = $base->getLabel();
        }
        return $identifier;
    }


    public function buildTranslation($config)
    {
        $DQL = '';
        if($this->moduleConfig->module->ML){
            $config['select']['translation'] = 'translation';
            $DQL = $config['joins']['translation']['type']." JOIN base.translations translation WITH translation.fkLanguage = :languageId ";
            $config['params']['languageId'] = $this->language->getId();
        }

        $data = array(
            'config' => $config,
            'DQL' => $DQL
        );

        return $data;
    }


    private function buildVisibility($config)
    {
        $DQL = '';
        if(isset($this->moduleConfig->visibility) AND $this->moduleConfig->visibility AND $config['joins']['visibility']){

            $config['select']['visibility'] = 'visibility';

            if($config['joins']['visibility'] === true){
                $config['joins']['visibility'] = array('type' => 'INNER');
            }

            if($this->moduleConfig->visibility->MA AND $this->moduleConfig->visibility->ML){
                $DQL = $config['joins']['visibility']['type']." JOIN base.visibilities visibility WITH visibility.fkApp = :visibilityAppId
                                        AND visibility.fkLanguage = :visibilityLanguageId";
                $config['params']['visibilityAppId'] = $this->app->getId();
                $config['params']['visibilityLanguageId'] = $this->language->getId();
            }else if($this->moduleConfig->visibility->MA AND !$this->moduleConfig->visibility->ML){
                $DQL = $config['joins']['visibility']['type']." JOIN base.visibilities visibility WITH visibility.fkApp = :visibilityAppId
                                        AND visibility.fkLanguage IS NULL";
                $config['params']['visibilityAppId'] = $this->app->getId();
            }else if(!$this->moduleConfig->visibility->MA AND $this->moduleConfig->visibility->ML){
                $DQL = $config['joins']['visibility']['type']." JOIN base.visibilities visibility WITH visibility.fkApp IS NULL
                                        AND visibility.fkLanguage = :visibilityLanguageId";
                $config['params']['visibilityLanguageId'] = $this->language->getId();
            }

            $DQL .= " AND visibility.fkModule = :visibilityModuleId";
            $config['params']['visibilityModuleId'] = $this->module->getId();

            if(is_array($config) AND isset($config['itemId'])){
                if(!is_array($config['itemId'])){
                    $DQL .= " AND visibility.fkItemId = :visibilityItemId";
                    $config['params']['visibilityItemId'] = $config['itemId'];
                }else{
                    $DQL .= " AND visibility.fkItemId IN(".implode(',',$config['itemId']).")";
                }
            }


        }


        $data = array(
            'config' => $config,
            'DQL' => $DQL
        );

        return $data;
    }


    private function buildPosition($config)
    {
        $DQL = '';

        if(isset($this->moduleConfig->position) AND $this->moduleConfig->position AND $config['joins']['position']){

            $config['select']['position'] = 'position';

            if($config['joins']['position'] === true){
                $config['joins']['position'] = array('type' => 'LEFT');
            }

            if($this->moduleConfig->position->MA AND $this->moduleConfig->position->ML){
                $DQL = $config['joins']['position']['type']." JOIN base.positions position WITH position.fkApp = :positionAppId AND position.fkLanguage = :positionLanguageId";
                $config['params']['positionAppId'] = $this->app->getId();
                $config['params']['positionLanguageId'] = $this->language->getId();
            }else if($this->moduleConfig->position->MA AND !$this->moduleConfig->position->ML){
                $DQL = $config['joins']['position']['type']." JOIN base.positions position WITH position.fkApp = :positionAppId AND position.fkLanguage IS NULL";
                $config['params']['positionAppId'] = $this->app->getId();
            }else if(!$this->moduleConfig->position->MA AND $this->moduleConfig->position->ML){
                $DQL = $config['joins']['position']['type']." JOIN base.positions position WITH position.fkApp IS NULL AND position.fkLanguage = :positionLanguageId";
                if(!isset($config['params']['positionLanguageId'])){
                    $config['params']['positionLanguageId'] = $this->language->getId();
                }
            }else{
                $DQL = $config['joins']['position']['type']." JOIN base.positions position WITH position.fkApp IS NULL AND position.fkLanguage IS NULL";
            }

            $DQL .= " AND position.fkModule = :positionModuleId";
            $config['params']['positionModuleId'] = $this->module->getId();

            if(is_array($config) AND isset($config['itemId'])){
                if(!is_array($config['itemId'])){
                    $DQL .= " AND position.fkItemId = :positionItemId";
                    $config['params']['positionItemId'] = $config['itemId'];
                }else{
                    $DQL .= " AND position.fkItemId IN(".implode(',',$config['itemId']).")";
                }
            }

            $config['orderBy']['first'] = 'position.position ASC';
        }

        $data = array(
            'config' => $config,
            'DQL' => $DQL
        );

        return $data;
    }


    private function buildMetadata($config)
    {
        $DQL = '';

        if(isset($this->moduleConfig->metadata) AND $this->moduleConfig->metadata AND $config['joins']['metadata']){

            $config['select']['metadata'] = 'metadata';

            if($this->moduleConfig->metadata->MA AND $this->moduleConfig->metadata->ML){
                $DQL = $config['joins']['metadata']['type']." JOIN base.metadatas metadata WITH metadata.fkApp = :metadataAppId AND metadata.fkLanguage = :metadataLanguageId";
                $config['params']['metadataAppId'] = $this->app->getId();
                $config['params']['metadataLanguageId'] = $this->language->getId();
            }else if($this->moduleConfig->metadata->MA AND !$this->moduleConfig->metadata->ML){
                $DQL = $config['joins']['metadata']['type']." JOIN base.metadatas metadata WITH metadata.fkApp = :metadataAppId AND metadata.fkLanguage IS NULL";
                $config['params']['metadataAppId'] = $this->app->getId();
            }else if(!$this->moduleConfig->metadata->MA AND $this->moduleConfig->metadata->ML){
                $DQL = $config['joins']['metadata']['type']." JOIN base.metadatas metadata WITH metadata.fkApp IS NULL AND metadata.fkLanguage = :metadataLanguageId";
                $config['params']['metadataLanguageId'] = $this->language->getId();
            }else if(!$this->moduleConfig->metadata->MA AND !$this->moduleConfig->metadata->ML){
                $DQL = $config['joins']['metadata']['type']." JOIN base.metadatas metadata WITH metadata.fkApp IS NULL AND metadata.fkLanguage IS NULL";
            }

            $DQL .= " AND metadata.fkModule = :metadataModuleId";
            $config['params']['metadataModuleId'] = $this->module->getId();

            if(is_array($config) AND isset($config['itemId'])){
                if(!is_array($config['itemId'])){
                    $DQL .= " AND metadata.fkItemId = :metadataItemId";
                    $config['params']['metadataItemId'] = $config['itemId'];
                }else{
                    $DQL .= " AND metadata.fkItemId IN(".implode(',',$config['itemId']).")";
                }
            }
        }

        $data = array(
            'config' => $config,
            'DQL' => $DQL
        );

        return $data;
    }


    private function buildWhere($config)
    {
        $DQL = '';

        if($this->moduleConfig->module->MA){
            $config['where']['base.fkApp'] = 'base.fkApp = :appId';
            $config['params']['appId'] = $this->app->getId();
        }

        /**
         * todo
         *
         * rendere dinamico n2ktag
         *
         */

        if($this->invictusKernel AND $this->invictusKernel->getGetParam('preview', false) == 'n2ktag'){
            unset($config['where']['base.enabled']);
            unset($config['params']['enabled']);
        }

        if(is_array($config) AND isset($config['itemId'])){

            if(!is_array($config['itemId'])){
                $config['where']['base.id'] = 'base.id = :id';
                $config['params']['id'] = $config['itemId'];
            }else{
                $in = " base.id IN(".implode(',',$config['itemId']).")";
            }
        }

        if(is_array($config['where'])){
            foreach($config['where'] as $key => $value){
                if(is_null($value)){
                    unset($config['where'][$key]);
                }
            }
            $DQL = ' WHERE '.implode(' AND ', $config['where']);
            $DQL .= (isset($config['itemId']) AND is_array($config['itemId'])) ? " AND $in" : '';
        }else{
            $DQL = (isset($config['itemId']) AND is_array($config['itemId'])) ? " WHERE $in" : '';
        }

        $data = array(
            'config' => $config,
            'DQL' => $DQL
        );

        return $data;
    }


    protected function configureDefaults($config)
    {
        //print_r($config);
        if(is_array($config)){

            if(isset($config['appId'])){
                if(is_null($this->app)){
                    $this->app = $this->getEntityManager()
                        ->getRepository('InvictusCmsBundle:App')
                        ->find($config['appId']);
                }else if($config['appId'] != $this->app->getId()){
                    $this->app = $this->getEntityManager()
                        ->getRepository('InvictusCmsBundle:Language')
                        ->find($config['appId']);
                }
                //$this->app = $this->invictusKernel->getApp(array('id' => $config['appId']));
            }

            if(isset($config['languageId'])){
                if(is_null($this->language)){
                    $this->language = $this->getEntityManager()
                        ->getRepository('InvictusCmsBundle:Language')
                        ->find($config['languageId']);
                }else if($config['languageId'] != $this->language->getId()){
                    $this->language = $this->getEntityManager()
                        ->getRepository('InvictusCmsBundle:Language')
                        ->find($config['languageId']);
                }
                //$this->language = $this->invictusKernel->getLanguage(array('id' => $config['languageId']));
                //echo $this->language->getId();
            }

            if(isset($config['moduleId'])){
                //$this->module = $this->invictusKernel->getModule(array('id' => $config['moduleId']));
                if(is_null($this->module)){
                    $this->module = $this->getEntityManager()
                        ->getRepository('InvictusCmsBundle:Module')
                        ->find($config['moduleId']);
                }else if($config['moduleId'] != $this->module->getId()){
                    $this->module = $this->getEntityManager()
                        ->getRepository('InvictusCmsBundle:Module')
                        ->find($config['moduleId']);
                }
                $this->moduleConfig = json_decode($this->module->getConfig());
                //var_dump(json_decode($this->module->getConfig()));die();
            }

        }
    }


    private function debug($query, $result = null)
    {
        echo "<pre style='background:#FFF'>";
        if($this->app) echo "appId: ".$this->app->getId()."<br>";
        echo "languageId: ".$this->language->getId()."<br>";
        echo "moduleId: ".$this->module->getId()."<br>";
        echo "moduleTag: ".$this->module->getTag()."<br>";
        echo "moduleConfig: <br>".$this->module->getConfig()."<br><br>";
        var_dump($this->config);
        echo $query->getSQL()."<br><br>";
        echo $query->getDQL()."<br><br>";
        \Doctrine\Common\Util\Debug::dump($query)."<br><br>";
        print_r($query->getParameters());
        \Doctrine\Common\Util\Debug::dump($result)."<br><br>";
        echo "</pre>";
    }

}
