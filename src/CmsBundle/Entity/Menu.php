<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Menu
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\MenuRepository")
 */
class Menu extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag = '...';

    /**
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="item_attributes", type="text", nullable=true)
     */
    private $itemAttributes = "{}";

    /**
     * @var string
     *
     * @ORM\Column(name="link_attributes", type="text", nullable=true)
     */
    private $linkAttributes = "{}";

    /**
     * @var string
     *
     * @ORM\Column(name="children_attributes", type="text", nullable=true)
     */
    private $childrenAttributes = "{}";

    /**
     * @ORM\OneToMany(targetEntity="Invictus\PageBundle\Entity\Page", mappedBy="fkMenu")
     */
    private $pages;

    /**
     * @ORM\OneToMany(targetEntity="MenuTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Visibility", mappedBy="fkModuleItem")
     */
    protected $visibilities;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return Menu
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set itemAttributes
     *
     * @param string $itemAttributes
     * @return Menu
     */
    public function setItemAttributes($itemAttributes)
    {
        $itemAttributes = (strlen($itemAttributes) < 3)? '{}' : $itemAttributes;
        $this->itemAttributes = $itemAttributes;

        return $this;
    }

    /**
     * Get itemAttributes
     *
     * @return string 
     */
    public function getItemAttributes()
    {
        return $this->itemAttributes;
    }

    /**
     * Set linkAttributes
     *
     * @param string $linkAttributes
     * @return Menu
     */
    public function setLinkAttributes($linkAttributes)
    {
        $linkAttributes = (strlen($linkAttributes) < 3)? '{}' : $linkAttributes;
        $this->linkAttributes = $linkAttributes;
    
        return $this;
    }

    /**
     * Get linkAttributes
     *
     * @return string 
     */
    public function getLinkAttributes()
    {
        return $this->linkAttributes;
    }

    /**
     * Add translation
     *
     * @param \Invictus\CmsBundle\Entity\MenuTranslation $translation
     * @return Menu
     */
    public function addTranslation(\Invictus\CmsBundle\Entity\MenuTranslation $translation)
    {
        $translation->setFkBase($this);
        $this->translations[] = $translation;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\CmsBundle\Entity\MenuTranslation $translations
     */
    public function removeTranslation(\Invictus\CmsBundle\Entity\MenuTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Menu
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Set parent
     *
     * @param \Invictus\CmsBundle\Entity\Menu $parent
     * @return Menu
     */
    public function setParent(\Invictus\CmsBundle\Entity\Menu $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Invictus\CmsBundle\Entity\Menu 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set childrenAttributes
     *
     * @param string $childrenAttributes
     * @return Menu
     */
    public function setChildrenAttributes($childrenAttributes)
    {
        $childrenAttributes = (strlen($childrenAttributes) < 3)? '{}' : $childrenAttributes;
        $this->childrenAttributes = $childrenAttributes;
    
        return $this;
    }

    /**
     * Get childrenAttributes
     *
     * @return string 
     */
    public function getChildrenAttributes()
    {
        return $this->childrenAttributes;
    }

    /**
     * Add pages
     *
     * @param \Invictus\PageBundle\Entity\Page $pages
     * @return Menu
     */
    public function addPage(\Invictus\PageBundle\Entity\Page $pages)
    {
        $this->pages[] = $pages;
    
        return $this;
    }

    /**
     * Remove pages
     *
     * @param \Invictus\PageBundle\Entity\Page $pages
     */
    public function removePage(\Invictus\PageBundle\Entity\Page $pages)
    {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages()
    {
        return $this->pages;
    }
}