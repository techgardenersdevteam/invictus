<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvictusUser
 *
 * @ORM\Table(name="invictus_user")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\InvictusUserRepository")
 */
class InvictusUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     */
    private $enabled;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean", nullable=false)
     */
    private $locked;

    /**
     * @var string
     *
     * @ORM\Column(name="roles", type="text", nullable=false)
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted;

    /**
     * @var \App
     *
     * @ORM\ManyToOne(targetEntity="App")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_app_id", referencedColumnName="id")
     * })
     */
    private $fkApp;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_language_id", referencedColumnName="id")
     * })
     */
    private $fkLanguage;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_language_id_ui", referencedColumnName="id")
     * })
     */
    private $fkLanguageUi;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return InvictusUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return InvictusUser
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return InvictusUser
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return InvictusUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return InvictusUser
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    
        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set roles
     *
     * @param string $roles
     * @return InvictusUser
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    
        return $this;
    }

    /**
     * Get roles
     *
     * @return string 
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InvictusUser
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return InvictusUser
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return InvictusUser
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set fkApp
     *
     * @param \Invictus\CmsBundle\Entity\App $fkApp
     * @return InvictusUser
     */
    public function setFkApp(\Invictus\CmsBundle\Entity\App $fkApp = null)
    {
        $this->fkApp = $fkApp;
    
        return $this;
    }

    /**
     * Get fkApp
     *
     * @return \Invictus\CmsBundle\Entity\App 
     */
    public function getFkApp()
    {
        return $this->fkApp;
    }

    /**
     * Set fkLanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $fkLanguage
     * @return InvictusUser
     */
    public function setFkLanguage(\Invictus\CmsBundle\Entity\Language $fkLanguage = null)
    {
        $this->fkLanguage = $fkLanguage;
    
        return $this;
    }

    /**
     * Get fkLanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }

    /**
     * Set fkLanguageUi
     *
     * @param \Invictus\CmsBundle\Entity\Language $fkLanguageUi
     * @return InvictusUser
     */
    public function setFkLanguageUi(\Invictus\CmsBundle\Entity\Language $fkLanguageUi = null)
    {
        $this->fkLanguageUi = $fkLanguageUi;
    
        return $this;
    }

    /**
     * Get fkLanguageUi
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getFkLanguageUi()
    {
        return $this->fkLanguageUi;
    }
}