<?php
/*
 * This file is part of the STS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Invictus\CmsBundle\Entity\Visibility
 *
 * @ORM\Table(name="visibility", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id"})
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\VisibilityRepository")
 */
class Visibility
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Module
     *
     * @ORM\ManyToOne(targetEntity="Module")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_module_id", referencedColumnName="id")
     * })
     */
    private $fkModule;

    /**
     * @var integer
     *
     * @ORM\Column(name="fk_item_id", type="integer", nullable=false)
     */
    private $fkItemId;

    /**
     * @var App
     *
     * @ORM\ManyToOne(targetEntity="App")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_app_id", referencedColumnName="id")
     * })
     */
    private $fkApp;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_language_id", referencedColumnName="id")
     * })
     */
    private $fkLanguage;


    public function __clone() {
        if ($this->id) {
            $this->id = null;
        }
    }

    /* Relazioni verso i vari tipi di item */


    /**
     * @var Module
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Module", inversedBy="visibilities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkModuleItem;


    /**
     * @var Template
     *
     * @ORM\ManyToOne(targetEntity="Invictus\TemplateBundle\Entity\Template", inversedBy="visibilities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkTemplateItem;

    /**
     * @var News
     *
     * @ORM\ManyToOne(targetEntity="Invictus\NewsBundle\Entity\News", inversedBy="visibilities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkNewsItem;

    /**
     * @var Attachment
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Attachment", inversedBy="visibilities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkAttachmentItem;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Invictus\ProjectBundle\Entity\Project", inversedBy="visibilities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkProjectItem;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Category", inversedBy="visibilities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
     * })
     */
    private $fkCategoryItem;


    /**
    * @var Page
    *
    * @ORM\ManyToOne(targetEntity="Invictus\PageBundle\Entity\Page", inversedBy="visibilities")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="fk_item_id", referencedColumnName="id")
    * })
    */

    private $fkPageItem;


    /* Fine relazioni verso i vari tipi di item */





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkModule
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkModule
     * @return Visibility
     */
    public function setFkModule(\Invictus\CmsBundle\Entity\Module $fkModule = null)
    {
        $this->fkModule = $fkModule;
    
        return $this;
    }

    /**
     * Get fkModule
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkModule()
    {
        return $this->fkModule;
    }

    /**
     * Set fkApp
     *
     * @param \Invictus\CmsBundle\Entity\App $fkApp
     * @return Visibility
     */
    public function setFkApp(\Invictus\CmsBundle\Entity\App $fkApp = null)
    {
        $this->fkApp = $fkApp;
    
        return $this;
    }

    /**
     * Get fkApp
     *
     * @return \Invictus\CmsBundle\Entity\App 
     */
    public function getFkApp()
    {
        return $this->fkApp;
    }

    /**
     * Set fkLanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $fkLanguage
     * @return Visibility
     */
    public function setFkLanguage(\Invictus\CmsBundle\Entity\Language $fkLanguage = null)
    {
        $this->fkLanguage = $fkLanguage;
    
        return $this;
    }

    /**
     * Get fkLanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }

    /**
     * Set fkNewsItem
     *
     * @param \Invictus\NewsBundle\Entity\News $fkNewsItem
     * @return Visibility
     */
    public function setFkNewsItem(\Invictus\NewsBundle\Entity\News $fkNewsItem = null)
    {
        //$this->fkNewsItem = $fkNewsItem;
    
        return $this;
    }

    /**
     * Get fkNewsItem
     *
     * @return \Invictus\NewsBundle\Entity\News 
     */
    public function getFkNewsItem()
    {
        return $this->fkNewsItem;
    }

    /**
     * Set fk(ModuleTag)Item
     *
     * Funzione custom di Invictus per gestire dinamicamente il campo fk_item_id
     *
     * @param $moduleTag
     * @param $item
     * @return Metadata
     */
    public function setFkItem($moduleTag, $item){
        $method = 'setFk'.ucfirst($moduleTag).'Item';
        $this->$method($item);
    }


    /**
     * Set fkModuleItem
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkModuleItem
     * @return Visibility
     */
    public function setFkModuleItem(\Invictus\CmsBundle\Entity\Module $fkModuleItem = null)
    {
        //$this->fkModuleItem = $fkModuleItem;
    
        return $this;
    }

    /**
     * Get fkModuleItem
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkModuleItem()
    {
        return $this->fkModuleItem;
    }

    /**
     * Set fkAttachmentItem
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $fkAttachmentItem
     * @return Visibility
     */
    public function setFkAttachmentItem(\Invictus\CmsBundle\Entity\Attachment $fkAttachmentItem = null)
    {
        //$this->fkAttachmentItem = $fkAttachmentItem;
    
        return $this;
    }

    /**
     * Get fkAttachmentItem
     *
     * @return \Invictus\CmsBundle\Entity\Attachment 
     */
    public function getFkAttachmentItem()
    {
        return $this->fkAttachmentItem;
    }

    /**
     * Set fkProjectItem
     *
     * @param \Invictus\ProjectBundle\Entity\Project $fkProjectItem
     * @return Visibility
     */
    public function setFkProjectItem(\Invictus\ProjectBundle\Entity\Project $fkProjectItem = null)
    {
        //$this->fkProjectItem = $fkProjectItem;
    
        return $this;
    }

    /**
     * Get fkProjectItem
     *
     * @return \Invictus\ProjectBundle\Entity\Project 
     */
    public function getFkProjectItem()
    {
        return $this->fkProjectItem;
    }

    /**
     * Set fkCategoryItem
     *
     * @param \Invictus\CmsBundle\Entity\Category $fkCategoryItem
     * @return Visibility
     */
    public function setFkCategoryItem(\Invictus\CmsBundle\Entity\Category $fkCategoryItem = null)
    {
        //$this->fkCategoryItem = $fkCategoryItem;

        return $this;
    }

    /**
     * Get fkCategoryItem
     *
     * @return \Invictus\CmsBundle\Entity\Category
     */
    public function getFkCategoryItem()
    {
        return $this->fkCategoryItem;
    }




    /**
     * Set fkSchedaItem
     *
     * @param \Invictus\SchedaBundle\Entity\Scheda $fkSchedaItem
     * @return Visibility
     */
    public function setFkSchedaItem(\Invictus\SchedaBundle\Entity\Scheda $fkSchedaItem = null)
    {
        //$this->fkPathItem = $fkPathItem;

        return $this;
    }

    /**
     * Get fkSchedaItem
     *
     * @return \Invictus\SchedaBundle\Entity\Scheda
     */
    public function getFkSchedaItem()
    {
        return $this->fkSchedaItem;
    }





    /**
     * Set fkPathItem
     *
     * @param \Invictus\PathBundle\Entity\Path $fkPathItem
     * @return Visibility
     */
    public function setFkPathItem(\Invictus\PathBundle\Entity\Path $fkPathItem = null)
    {
        //$this->fkPathItem = $fkPathItem;

        return $this;
    }

    /**
     * Get fkPathItem
     *
     * @return \Invictus\PathBundle\Entity\Path
     */
    public function getFkPathItem()
    {
        return $this->fkPathItem;
    }

    /**
     * Set fkPageItem
     *
     * @param \Invictus\PageBundle\Entity\Page $fkPageItem
     * @return Visibility
     */
    public function setFkPageItem(\Invictus\PageBundle\Entity\Page $fkPageItem = null)
    {
        //$this->fkPageItem = $fkPageItem;
    
        return $this;
    }

    /**
     * Get fkPageItem
     *
     * @return \Invictus\PageBundle\Entity\Page 
     */
    public function getFkPageItem()
    {
        return $this->fkPageItem;
    }

    /**
     * Set fkItemId
     *
     * @param integer $fkItemId
     * @return Visibility
     */
    public function setFkItemId($fkItemId)
    {
        $this->fkItemId = $fkItemId;
    
        return $this;
    }

    /**
     * Get fkItemId
     *
     * @return integer 
     */
    public function getFkItemId()
    {
        return $this->fkItemId;
    }

    /**
     * Set fkGeoPointItem
     *
     * @param \Invictus\WidgetBundle\Entity\GeoPoint $fkGeoPointItem
     * @return Visibility
     */
    public function setFkGeoPointItem(\Invictus\WidgetBundle\Entity\GeoPoint $fkGeoPointItem = null)
    {
        $this->fkGeoPointItem = $fkGeoPointItem;
    
        return $this;
    }

    /**
     * Get fkGeoPointItem
     *
     * @return \Invictus\WidgetBundle\Entity\GeoPoint 
     */
    public function getFkGeoPointItem()
    {
        return $this->fkGeoPointItem;
    }

    /**
     * Set fkTemplateItem
     *
     * @param \Invictus\TemplateBundle\Entity\Template $fkTemplateItem
     * @return Visibility
     */
    public function setFkTemplateItem(\Invictus\TemplateBundle\Entity\Template $fkTemplateItem = null)
    {
        $this->fkTemplateItem = $fkTemplateItem;
    
        return $this;
    }

    /**
     * Get fkTemplateItem
     *
     * @return \Invictus\TemplateBundle\Entity\Template
     */
    public function getFkTemplateItem()
    {
        return $this->fkTemplateItem;
    }
}