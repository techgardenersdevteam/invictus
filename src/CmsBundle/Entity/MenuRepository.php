<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * MenuRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MenuRepository extends ItemRepository
{

    public function findAllOrdered($where = array(), $parameters = array(), $fields = array(), $sorts = array(), $page = 1, $rpp = 10)
    {

        if(!is_array($fields)){
            $fields = array($fields) ;
        }
        if(!is_array($sorts)){
            $sorts = array($sorts) ;
        }
        $i = 0;
        $orderClauses = array();
        foreach($fields as $field){
            $sort = (isset($sorts[$i])) ? $sorts[$i] : 'asc';
            $orderClauses[] = $field.' '.$sort;
            $i++;
        }
        $orderBy = implode(', ', $orderClauses);

        $where = implode(' ', $where);
        $offset = ($page - 1) * $rpp;
        $sql = "SELECT
                    menu.*,
                    menu_translation.*,
                    menu.id,
                    parent.label AS parent_label

                    FROM menu

                    INNER JOIN menu_translation
                    ON menu_translation.fk_menu_id = menu.id
                    AND menu_translation.fk_language_id = '".$this->language->getId()."'

                    LEFT OUTER JOIN menu_translation as parent
                    ON menu.parent_id = parent.fk_menu_id
                    AND parent.fk_language_id = '".$this->language->getId()."'

                    WHERE
                    1 = 1
                    $where

                    ORDER BY
                    $orderBy

                    LIMIT $offset, $rpp";
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        foreach($parameters AS $key => $value){
            $stmt->bindValue($key, $value);
        }

        $stmt->execute();
        $data = $stmt->fetchAll();

        return $data;

    }


    public function getTotalRecords()
    {
        $sql = "SELECT count(menu.id)

                    FROM menu

                    WHERE

                    deleted = 0";
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        $stmt->execute();
        return $stmt->fetchColumn();
    }


    public function getParents()
    {
        $sql = "SELECT
                    menu.id,
                    menu_translation.label

                    FROM
                    menu

                    LEFT OUTER JOIN menu_translation
                    ON menu_translation.fk_menu_id = menu.id

                    WHERE
                    menu.deleted = 0
                    AND menu_translation.fk_language_id = :UILocale

                    ORDER BY
                    menu_translation.label";

        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        $stmt->bindValue('UILocale', $this->UILocale);
        $stmt->execute();
        return $stmt->fetchAll();
    }


    public function getJsTreeChildren($parentId = null, $appId = null, $languageId = null, $json = true)
    {

        $module = $this->invictusKernel->getModule(array('id' => 50));
        $moduleConfig = $module->getConfig(false);

        $positionAppConstraint = $moduleConfig['position']['MA'] ? " = '$appId' " : ' IS NULL';
        $positionLanguageConstraint = $moduleConfig['position']['ML'] ? " = '$languageId' " : ' IS NULL';

        $visibilityAppConstraint = $moduleConfig['visibility']['MA'] ? " = '$appId' " : ' IS NULL';
        $visibilityLanguageConstraint = $moduleConfig['visibility']['ML'] ? " = '$languageId' " : ' IS NULL';

        $parentConstraint = is_null($parentId) ? 'IS NULL' : "= $parentId";
        $appConstraint = is_null($appId) ? 'IS NULL' : "= '$appId'";

        $sql = "SELECT
                    menu.id,
                    menu.parent_id,
                    menu.enabled,
                    menu_translation.label,
                    children.id AS hasChild,
                    position.position,
                    visibility.id AS visibility

                    FROM
                    menu

                    LEFT OUTER JOIN menu_translation
                    ON menu_translation.fk_menu_id = menu.id

                    LEFT OUTER JOIN menu AS children
                    ON children.parent_id = menu.id
                    AND children.deleted = 0

                    LEFT OUTER JOIN position
                    ON menu.id = position.fk_item_id
                    AND position.fk_module_id = 50
                    AND position.fk_app_id $positionAppConstraint
                    AND position.fk_language_id $positionLanguageConstraint

                    LEFT OUTER JOIN visibility
                    ON menu.id = visibility.fk_item_id
                    AND visibility.fk_module_id = 50
                    AND visibility.fk_app_id $visibilityAppConstraint
                    AND visibility.fk_language_id $visibilityLanguageConstraint

                    WHERE
                    menu.deleted = 0
                    AND menu_translation.fk_language_id = :languageId
                    AND menu.parent_id $parentConstraint
                    AND menu.fk_app_id $appConstraint

                    GROUP BY
                    menu.id

                    ORDER BY
                    position.position,
                    menu.id";
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        $stmt->bindValue('languageId', $languageId);
        $stmt->execute();
        $data = $stmt->fetchAll();

        $children = array();
        foreach($data as $child){

            $rel = is_null($child['parent_id']) ? 'root' : 'page';
            $rel = $child['enabled'] ? $rel : $rel.'_disabled';
            $child['label'] = empty($child['label']) ? '...' : $child['label'];
            $label = (stristr($rel, 'page') and is_null($child['visibility'])) ? "<i>{$child['label']}</i>" : $child['label'];
            $class = is_null($child['hasChild']) ? 'jstree-leaf' : '';
            $children[] = array(
                'attr' => array(
                    'id' => 'm-'.$child['id'],
                    'rel' => $rel,
                    'data-id' => $child['id'],
                    'title' => $child['id'] ."|". $child['position'],
                    'class' => $class
                ),
                'data' => $label,
                'state' => 'closed'
            );
        }

        if($json){
            $children = json_encode($children);
        }

        return $children;
    }


    public function orderJsTreeChildren($parentId = null, $ids = null, $appId = null, $languageId = null)
    {
        $module = $this->invictusKernel->getModule(array('id' => 50));
        $moduleConfig = $module->getConfig(false);

        $positionApp = $moduleConfig['position']['MA'] ? "'$appId'" : 'NULL';
        $positionAppConstraint = $moduleConfig['position']['MA'] ? " = '$appId' " : ' IS NULL';

        $positionLanguage = $moduleConfig['position']['ML'] ? "'$languageId'" : 'NULL';
        $positionLanguageConstraint = $moduleConfig['position']['ML'] ? " = '$languageId' " : ' IS NULL';

        $ids = explode('|', $ids);

        $i = 0;
        foreach($ids as $id){

            $i++;

            $sql = "DELETE FROM
                      position
                    WHERE
                        position.fk_module_id = 50
                        AND position.fk_item_id = $id
                        AND position.fk_app_id $positionAppConstraint
                        AND position.fk_language_id $positionLanguageConstraint";
            $stmt = $this->getEntityManager()
                ->getConnection()
                ->prepare($sql);
            $stmt->execute();
            //echo $sql;

            $sql = "INSERT INTO position
                      (fk_module_id, fk_item_id, fk_app_id, fk_language_id, position)
                      VALUES(50, $id, $positionApp, $positionLanguage, $i)";
            $stmt = $this->getEntityManager()
                ->getConnection()
                ->prepare($sql);
            $stmt->execute();
            //echo $sql;
        }

        $sql = "UPDATE menu

                SET
                  menu.parent_id = $parentId

                WHERE
                menu.id IN (".implode(',', $ids).")";
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        $stmt->execute();

        $result = array('success' => $i);

        return $result;
    }


    public function getMenuRows($appId, $languageId, $selId = 0)
    {
        $sql = "SELECT
                        menu.*,
                        menu_translation.label,
                        menu_translation.link,
                        metadata.path,
                        metadata.slug,
                        metadata.fk_item_id AS itemId

                    FROM
                        menu

                    LEFT JOIN menu_translation
                        ON menu_translation.fk_menu_id = menu.id
                        AND menu_translation.fk_language_id = '$languageId'

                    LEFT JOIN visibility AS menu_visibility
                        ON menu_visibility.fk_item_id = menu.id
                        AND menu_visibility.fk_module_id = 50
                        AND menu_visibility.fk_app_id IS NULL
                        AND menu_visibility.fk_language_id = '$languageId'

                    LEFT OUTER JOIN relation
                        ON menu.id = relation.fk_item_id_a
                        AND relation.fk_module_id_a = 50
                        AND relation.fk_module_id_b = 11
                        AND relation.slot =	1
                        AND relation.fk_app_id = '$appId'
                        AND relation.fk_language_id IS NULL

                    LEFT OUTER JOIN position
                        ON menu.id = position.fk_item_id
                        AND position.fk_module_id = 50
                        AND position.fk_app_id IS NULL
                        AND position.fk_language_id = '$languageId'

                    LEFT OUTER JOIN metadata
                        ON relation.fk_item_id_b = metadata.fk_item_id
                        AND metadata.fk_module_id = 11
                        AND metadata.fk_app_id IS NULL
                        AND metadata.fk_language_id = '$languageId'

                    WHERE
                        menu.deleted = 0

                    ORDER BY
                    position.position,
                    menu.id";

        //echo $sql;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        //$stmt->bindValue('locale', $locale);
        //$stmt->bindValue('root', $root);
        $stmt->execute();

        $rows = $stmt->fetchAll();

        $data = array();
        foreach($rows as $row){
            $row['sel'] = false;
            $row['ancestor_sel'] = false;
            $data[$row['id']] = $row;
        }
        $parents = array();
        if(isset($data[$selId]['parent_id'])){
            $data[$selId]['sel'] = true;
            $parentId = $data[$selId]['parent_id'];
            while($parentId) {
                if(isset($data[$parentId])){
                    $data[$parentId]['ancestor_sel'] = true;
                    $parentId = $data[$parentId]['parent_id'];
                    $parents[] = $parentId;
                }else{
                    $parentId = false;
                }
            }
        }
        $result = array(
            'data' => $data,
            'parents' => array_reverse($parents)
        );

        return $result;
    }


    public function getFEMenuRows($appId, $languageId, $selId = 0)
    {
        $sql = "SELECT
                        menu.*,
                        menu.id as menuId,
                        menu_translation.label,
                        menu_translation.link,
                        metadata.path,
                        metadata.slug,
                        metadata.fk_item_id AS itemId

                    FROM
                        menu

                    LEFT JOIN menu_translation
                        ON menu_translation.fk_menu_id = menu.id
                        AND menu_translation.fk_language_id = '$languageId'

                    INNER JOIN visibility AS menu_visibility
                        ON menu_visibility.fk_item_id = menu.id
                        AND menu_visibility.fk_module_id = 50
                        AND menu_visibility.fk_app_id IS NULL
                        AND menu_visibility.fk_language_id = '$languageId'

                    LEFT OUTER JOIN relation
                        ON menu.id = relation.fk_item_id_a
                        AND relation.fk_module_id_a = 50
                        AND relation.fk_module_id_b = 11
                        AND relation.slot =	1
                        AND relation.fk_app_id = '$appId'
                        AND relation.fk_language_id IS NULL

                    LEFT OUTER JOIN position
                        ON menu.id = position.fk_item_id
                        AND position.fk_module_id = 50
                        AND position.fk_app_id IS NULL
                        AND position.fk_language_id = '$languageId'

                    LEFT OUTER JOIN metadata
                        ON relation.fk_item_id_b = metadata.fk_item_id
                        AND metadata.fk_module_id = 11
                        AND metadata.fk_app_id IS NULL
                        AND metadata.fk_language_id = '$languageId'

                    WHERE
                        menu.deleted = 0
                        AND menu.enabled = 1

                    ORDER BY
                    position.position,
                    menu.id";

        //echo $sql;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        //$stmt->bindValue('locale', $locale);
        //$stmt->bindValue('root', $root);
        $stmt->execute();

        $rows = $stmt->fetchAll();

        $data = array();
        foreach($rows as $row){
            $row['sel'] = false;
            $row['ancestor_sel'] = false;
            $data[$row['id']] = $row;
        }
        $parents = array();
        if(isset($data[$selId]['parent_id'])){
            $data[$selId]['sel'] = true;
            $parentId = $data[$selId]['parent_id'];
            while($parentId) {
                if(isset($data[$parentId])){
                    $data[$parentId]['ancestor_sel'] = true;
                    $parentId = $data[$parentId]['parent_id'];
                    $parents[] = $parentId;
                }else{
                    $parentId = false;
                }
            }
        }
        $result = array(
            'data' => $data,
            'parents' => array_reverse($parents)
        );

        return $result;
    }


    public function nestRows(array $rows, $parentId = null, $level = 0)
    {

        $branch = array();
        foreach ($rows as $row) {
            if ($row['parent_id'] == $parentId) {
                $row['level'] = ++$level;
                $children = $this->nestRows($rows, $row['id'], $row['level']);
                if ($children) {
                    $row['children'] = $children;

                }
                $level--;
                $branch[] = $row;
            }
        }
        return $branch;
    }


    public function buildListTree(array $array, array $options = array())
    {
        $childrenAttributes = $this->arrayToAttributes($options);
        $html = array();
        $html[] = "<ul $childrenAttributes>";
        foreach ($array as $item)
        {
            $itemArrayAttributes = json_decode($item['item_attributes'], true) ? json_decode($item['item_attributes'], true) : array();

            $linkArrayAttributes = json_decode($item['link_attributes'], true) ? json_decode($item['link_attributes'], true) : array();
            $linkAttributes = $this->arrayToAttributes($linkArrayAttributes);
            $path = empty($item['path']) ? '' : $item['path'].'/';
            $pathSlug = '/'.$this->language->getId() .'/'. $path .$item['slug'];
            //$pathSlug = '/'. $path .$item['slug'];
            $pathSlug = is_null($item['slug']) ? '/' : $pathSlug;

            if($item['ancestor_sel']){
                isset($itemArrayAttributes['class']) ? $itemArrayAttributes['class'] = $itemArrayAttributes['class'].' ancestor_sel': $itemArrayAttributes['class'] = 'ancestor_sel' ;
            }
            if($item['sel']){
                isset($itemArrayAttributes['class']) ? $itemArrayAttributes['class'] = $itemArrayAttributes['class'].' sel': $itemArrayAttributes['class'] = 'sel' ;
            }
            $itemAttributes = $this->arrayToAttributes($itemArrayAttributes);

            $href = is_null($item['link']) ? $pathSlug : $item['link'];
            $href = empty($href) ? '/' : $href;
            $html[] = "<li $itemAttributes>";
            $html[] = "<a href='$href' data-id='".$item['menuId']."' $linkAttributes>{$item['label']}</a>";
            if(isset($item['children'])){
                $options = (!is_null($item['children_attributes'])) ? json_decode($item['children_attributes'], true) : array();
                //$html[] = $this->buildListTree($item['children'], $options);
                $html = array_merge($html, $this->buildListTree($item['children'], $options));
            }
            $html[] = '</li>';
        }
        $html[] = '</ul>';

        return $html;
    }


    public function buildOptionsTree(array $array, array $options = array())
    {
        //echo "<pre style='background:#FFF'>";print_r($array);echo "</pre>";
        $html = array();
        foreach ($array as $item)
        {
            //echo $item['label'];
            $selected = $item['id'] == $options['selId'] ? 'selected="selected"' : '';
            $indent = '';
            if($item['level'] > 1){
                $indent .= str_repeat(" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ", $item['level'] - 2).' &nbsp;&nbsp; └ ';
            }

            $html[] = "<option value='{$item['id']}' data-sel='".$item['sel']."'$selected data-level='{$item['level']}'>";
            if(is_null($item['parent_id'])){
                $html[] = "<b>[ {$item['label']} ]</b>";
            }else{
                $html[] = "$indent {$item['label']}";
            }

            $html[] = '</option>';

            if(isset($item['children'])){
                $html = array_merge($html, $this->buildOptionsTree($item['children'], $options));
            }
        }
        //echo "<pre style='background:#FFF'>";print_r($html);echo "</pre>";

        return $html;
    }


    public function renderTree($root, $appId, $languageId, array $options = array())
    {
        $selId = (isset($options['selId'])) ? $options['selId'] : 0;
        $results = $this->getMenuRows($appId, $languageId, $selId);
        //echo "<pre style='background:#FFF'>";print_r($results['data']);echo "</pre>";

        $array = $this->nestRows($results['data'], $root);
        //echo "<pre style='background:#FFF'>";var_dump($array);echo "</pre>";

        $listTree = $this->buildListTree($array, $options);

        return implode('', $listTree);

    }


    public function renderFETree($root, $appId, $languageId, array $options = array())
    {
        $selId = (isset($options['selId'])) ? $options['selId'] : 0;
        $results = $this->getFEMenuRows($appId, $languageId, $selId);
        //echo "<pre style='background:#FFF'>";print_r($results['data']);echo "</pre>";

        $array = $this->nestRows($results['data'], $root);
        //echo "<pre style='background:#FFF'>";var_dump($array);echo "</pre>";

        $listTree = $this->buildListTree($array, $options);

        return implode('', $listTree);

    }


    public function renderOptionsTree($root, $appId, $languageId, array $options = array())
    {
        $selId = isset($options['selId']) ? $options['selId'] : 0;
        $results = $this->getMenuRows($appId, $languageId, $selId);
        //echo "<pre style='background:#FFF'>";print_r($results['data']);echo "</pre>";

        $nestedArray = $this->nestRows($results['data'], $root);
        //echo "<pre style='background:#FFF'>";var_dump($nestedArray);echo "</pre>";

        $optionsTree = $this->buildOptionsTree($nestedArray, $options);
        //echo "<pre style='background:#FFF'>";print_r($optionsTree);echo "</pre>";

        return implode('', $optionsTree);

    }


    private function arrayToAttributes(array $array)
    {
        $string = '';
        if(count($array)){
            $keys = array_keys($array);
            $values = array_values($array);
            $string = '';
            for ($i = 0; $i < count($array); $i++) {
                $string .= "{$keys[$i]}=\"{$values[$i]}\" ";
            }
        }
        return $string;
    }


    public function getSelectedMenuItemId($itemId)
    {
        $sql = "SELECT
                    relation.fk_item_id_a

                    FROM
                    relation

                    WHERE
                    relation.fk_app_id = '".$this->invictusKernel->appId."' AND
                    relation.fk_language_id IS NULL AND
                    relation.fk_module_id_a = 50 AND
                    relation.fk_module_id_b = 11 AND
                    relation.fk_item_id_b = $itemId AND
                    relation.slot = 1";

        //echo $sql;
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        //$stmt->bindValue('locale', $locale);
        //$stmt->bindValue('root', $root);
        $stmt->execute();

        $id = $stmt->fetchColumn();

        return $id;
    }

}
