<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModuleTranslation
 *
 * @ORM\Table(name="module_translation")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\ModuleTranslationRepository")
 */
class ModuleTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_module_id", referencedColumnName="id")
     * })
     */
    private $fkBase;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return ModuleTranslation
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\CmsBundle\Entity\Module $fkBase
     * @return ModuleTranslation
     */
    public function setFkBase(\Invictus\CmsBundle\Entity\Module $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\CmsBundle\Entity\Module 
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

}
