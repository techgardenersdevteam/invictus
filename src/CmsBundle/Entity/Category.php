<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\CategoryRepository")
 */
class Category extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag = '...';

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_vars", type="text", nullable=false)
     */
    private $customVars = '{"color":"rgba(255,255,255,.65)"}';

    /**
     * @ORM\OneToMany(targetEntity="CategoryTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Visibility", mappedBy="fkCategoryItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Position", mappedBy="fkCategoryItem")
     */
    protected $positions;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkCategoryItem")
     */
    protected $metadatas;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return Category
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    
        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set customVars
     *
     * @param string $customVars
     * @return Category
     */
    public function setCustomVars($customVars)
    {
        $this->customVars = $customVars;
    
        return $this;
    }

    /**
     * Get customVars
     *
     * @return string 
     */
    public function getCustomVars()
    {
        return $this->customVars;
    }

    /**
     * Get customVarsAsArray
     *
     * @return string
     */
    public function getCustomVarsAsArray()
    {
        return json_decode($this->customVars, true);
    }

    /**
     * Set parent
     *
     * @param \Invictus\CmsBundle\Entity\Category $parent
     * @return Category
     */
    public function setParent(\Invictus\CmsBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Invictus\CmsBundle\Entity\Category 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add translations
     *
     * @param \Invictus\CmsBundle\Entity\CategoryTranslation $translations
     * @return Category
     */
    public function addTranslation(\Invictus\CmsBundle\Entity\CategoryTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\CmsBundle\Entity\CategoryTranslation $translations
     */
    public function removeTranslation(\Invictus\CmsBundle\Entity\CategoryTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Category
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     * @return Category
     */
    public function addMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $metadata->setFkItemId($this->getId());
        $this->metadatas[] = $metadata;

        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     */
    public function removeMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $this->metadatas->removeElement($metadata);
    }

    /**
     * Get metadatas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }
}