<?php
/*
 * This file is part of the STS project
 *
 * (c) Roberto Beccaceci <roberto@beccaceci.it>
 * (c) Matteo Poile <matteo.poile@devsign.it>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AttachmentTranslation
 *
 * @ORM\Table(name="attachment_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_attachment_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_attachment_id", "fk_language_id"})
 * @ORM\Entity(repositoryClass="Invictus\CmsBundle\Entity\AttachmentTranslationRepository")
 */
class AttachmentTranslation extends ItemTranslation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $alt
     *
     * @ORM\Column(name="alt", type="string", length=255, nullable=true)
     */
    private $alt;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path = '/media/';

    /**
     * @var string $link
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="text", nullable=true)
     */
    private $code;

    /**
     * @var Attachment
     *
     * @ORM\ManyToOne(targetEntity="Attachment", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_attachment_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    public function __clone() {
        if($this->id) {
            $this->id = null;
        }
    }

    /**
     * Set alt
     *
     * @param string $alt
     * @return AttachmentTranslation
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    
        return $this;
    }

    /**
     * Get alt
     *
     * @return string 
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return AttachmentTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return AttachmentTranslation
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return AttachmentTranslation
     */
    public function setLink($link)
    {
        $this->link = $link;
    
        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return AttachmentTranslation
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $fkAttachment
     * @return AttachmentTranslation
     */
    public function setFkBase(\Invictus\CmsBundle\Entity\Attachment $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\CmsBundle\Entity\Attachment 
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param bool $path
     * @param bool $debug
     * @return null|string
     *
     * Funzione duplicata anche in Attachment, non cancellare serve in entrambe le classe
     */
    function sanitizePath($path = false, $debug = false)
    {
        if(!$path){
            return null;
        }

        $search  = array('è','à©','à.','ò','ù','ì','à §');
        $replace = array('e','e' ,'a' ,'o','u','i','c');

        $s1 = str_replace(' ', '_', $path);
        //Sostituisce i gli indici in $search con gli indici in $replace nella stringa $S
        $s2 = str_replace(
            $search,
            $replace,
            $s1);
        //Rimuove tutti i caratteri non compresi fra 'a' e 'z', fra 0 e 9, diversi da _, da - e da .
        $s3 = preg_replace('/[^a-z0-9-_\/]/i', '', strtolower($s2));
        //Rimuove i doppi _ e i doppi - un singolo _ e -
        $s4 = preg_replace("/(_{2,})+/", '_', $s3);
        $s4 = preg_replace("/(-{2,})+/", '-', $s4);
        // elimina - e _ dalle estremità della stringa
        $s5 = trim($s4, '_');
        $s6 = trim($s5, '-');
        //Rimuove i doppi / e li sostituisce con un singolo / alla stringa a cui è stato aggiunto un / all'inizio e uno alla fine
        $s7 = preg_replace("/(\/{2,})+/", '/','/'.$s6.'/');
        $s8 = ltrim($s7, '/');

        if($debug){
            echo "
                    s1: $s1<br />
                    s2: $s2<br />
                    s3: $s3<br />
                    s4: $s4<br />
                    s5: $s5<br />
                    s6: $s6<br />
                    s7: $s7<br />
                    s8: $s8<br />";
        }

        return $s8;
    }

    public function getYoutubeImgPreview($previewName = 'hqdefault', $code = false)
    {
        $code = $code ? $code : $this->code;
        if(stristr('/', $code)){
            $pattern = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";
            preg_match($pattern, $code, $matches);
            $code = $matches[1];
        }

        return "http://img.youtube.com/vi/".$code."/$previewName.jpg";
    }

    public function getVimeoImgPreview($previewName = 'thumbnail_large', $code = false)
    {
        $code = $code ? $code : $this->code;

        if( false === file_get_contents("http://vimeo.com/api/v2/video/".$code.".json") ){
            return "error reading http://vimeo.com/api/v2/video/".$code.".json";
        }else{
            $data = json_decode(file_get_contents("http://vimeo.com/api/v2/video/".$code.".json"));
            return $data[0]->$previewName;
        }
    }

}