<?php

namespace Invictus\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryTranslation
 *
 * @ORM\Table(name="category_translation")
 * @ORM\Entity
 */
class CategoryTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\CmsBundle\Entity\Category $fkBase
     * @return CategoryTranslation
     */
    public function setFkBase(\Invictus\CmsBundle\Entity\Category $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\CmsBundle\Entity\Category
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}