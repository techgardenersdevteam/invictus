<?php
  
namespace Invictus\NewsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'title',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('abstract', 'text', array(
                'label' => 'abstract',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'content',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'mce'
                )
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\NewsBundle\Entity\News'
            )
        );

    }

    public function getName()
    {
        return 'newstranslation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\NewsBundle\Entity\NewsTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
