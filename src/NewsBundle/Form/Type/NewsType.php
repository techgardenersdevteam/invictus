<?php

namespace Invictus\NewsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox',
                     'autocomplete' => 'off'
                )
            )
        );

        $builder->add('visibleDate', 'datetime', array(
                'widget' => 'single_text',
                'with_seconds' => true,
                'input' => 'datetime',
                'format' => 'y/MM/dd', // HH:mm:ss',
                'label' => 'visibleDate',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'datepicker',
                    'autocomplete' => 'off'
                ),
                'translation_domain' => 'blog'
            )
        );

        $builder->add('fromDate', 'datetime', array(
                'widget' => 'single_text',
                'with_seconds' => true,
                'format' => 'y/MM/dd', // HH:mm:ss',
                'input' => 'datetime',
                'label' => 'fromDate',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'datepicker',
                    'autocomplete' => 'off'
                ),
                'translation_domain' => 'news'
            )
        );

        $builder->add('toDate', 'datetime', array(
                'widget' => 'single_text',
                'with_seconds' => true,
                'format' => 'y/MM/dd', // HH:mm:ss',
                'input' => 'datetime',
                'label' => 'toDate',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'datepicker'
                ),
                'translation_domain' => 'news'
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkCategory', 'entity_hidden', array(
                'required' => false,
                'class' => 'Invictus\CmsBundle\Entity\Category'
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new NewsTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'news';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\NewsBundle\Entity\News',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
