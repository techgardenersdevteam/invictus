<?php

namespace Invictus\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="Invictus\NewsBundle\Entity\NewsRepository")
 */
class News extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime $visbleDate
     *
     * @ORM\Column(name="visible_date", type="datetime", nullable=false)
     */
    private $visibleDate;

    /**
     * @var datetime $fromDate
     *
     * @ORM\Column(name="from_date", type="datetime", nullable=true)
     */
    private $fromDate;

    /**
     * @var datetime $toDate
     *
     * @ORM\Column(name="to_date", type="datetime", nullable=true)
     */
    private $toDate;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category_id", referencedColumnName="id")
     * })
     */
    private $fkCategory;

    /**
     * @ORM\OneToMany(targetEntity="NewsTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkNewsItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkNewsItem")
     */
    protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkNewsItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->metadatas = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->tag;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add translations
     *
     * @param \Invictus\NewsBundle\Entity\NewsTranslation $translations
     * @return News
     */
    public function addTranslation(\Invictus\NewsBundle\Entity\NewsTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;

        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\NewsBundle\Entity\NewsTranslation $translations
     */
    public function removeTranslation(\Invictus\NewsBundle\Entity\NewsTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Set visibleDate
     *
     * @param \DateTime $visibleDate
     * @return News
     */
    public function setVisibleDate($visibleDate)
    {
        $this->visibleDate = $visibleDate;

        return $this;
    }

    /**
     * Get visibleDate
     *
     * @return \DateTime 
     */
    public function getVisibleDate()
    {

        return $this->visibleDate;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return News
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return News
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Add visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     * @return News
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility[] = $visibility;

        return $this;
    }

    /**
     * Remove visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility->removeElement($visibility);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     * @return News
     */
    public function addAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    
        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     * @return News
     */
    public function addMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $metadata->setFkItemId($this->getId());
        $this->metadatas[] = $metadata;
    
        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     */
    public function removeMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $this->metadatas->removeElement($metadata);
    }

    /**
     * Get metadatas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * Set fkCategory
     *
     * @param \Invictus\CmsBundle\Entity\Category $fkCategory
     * @return Habitat
     */
    public function setFkCategory(\Invictus\CmsBundle\Entity\Category $fkCategory = null)
    {
        $this->fkCategory = $fkCategory;

        return $this;
    }

    /**
     * Get fkCategory
     *
     * @return \Invictus\CmsBundle\Entity\Category
     */
    public function getFkCategory()
    {
        return $this->fkCategory;
    }
}