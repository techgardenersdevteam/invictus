<?php

namespace Invictus\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * NewsTranslation
 *
 * @ORM\Table(name="news_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_news_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id"})
 * @ORM\Entity(repositoryClass="Invictus\NewsBundle\Entity\NewsTranslationRepository")
 */
class NewsTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \News
     *
     * @ORM\ManyToOne(targetEntity="News", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_news_id", referencedColumnName="id")
     * })
     */
    private $fkBase;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\NewsBundle\Entity\News $fkBase
     * @return NewsTranslation
     */
    public function setFkBase(\Invictus\NewsBundle\Entity\News $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\NewsBundle\Entity\News 
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}