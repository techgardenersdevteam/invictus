Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('visibleDate', {}, 'blog'),
    Translator.trans('label'),
    Translator.trans('category'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:80, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:45, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'date', index:'base.visibleDate', width:100, fixed: true, sortable: true, search: true, align: 'left'},
    {name:'label', index:'translation.label', width:40, search:true, align: 'left'},
  {name:'category', index:'base.fkCategory', width:30, search:false, align: 'left'},
    {name:'enabled', index:'base.enabled', fixed: true, width:90, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'news_label': "Translation.validation.required"
		};
}

//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'date', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'label', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'category', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'enabled', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});