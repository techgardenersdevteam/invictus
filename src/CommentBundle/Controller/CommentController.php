<?php

namespace Invictus\CommentBundle\Controller;

use Invictus\CommentBundle\Entity\Comment;
use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        //$config['appId'] = 'vs';
        //$config['languageId'] = 'en_GB';
        //$config['moduleId'] = 4;
        //$config['orderBy'] = array(
        //   'date' => 'base.visibleDate DESC'
        //);
        $config['joins'] = array(
            'position' => false
        );
        $config['orderBy'] = array(
            'enabled' => 'base.enabled DESC',
            'date' => 'base.date DESC'
        );
        $config['where'] = array(
            'type' => 'base.type > 1'
        );
        $this->tableManager->addConfig($config);
        $this->tableManager->enablePredefinedToggle('enabled');

        //$this->tableManager->disableAction('edit');
        $this->tableManager->disableAction('attach');
    }


    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $userId = $user ? $user->getId() : $request->request->get('userId', 0);

        $update = $request->request->get('update', 0);

        try {

            if($update){

                $comment = $em->getRepository('InvictusCommentBundle:Comment')
                    ->findOneBy(array('id' => $update, 'userId' => $userId));

                $comment->setBody($request->request->get('text'));

            }else{

                $comment = new Comment();
                $comment->setUserId($userId);
                $comment->setName($request->request->get('name',''));
                $comment->setBody($request->request->get('body', $request->request->get('text')));
                $comment->setEmail($request->request->get('email',''));
                $comment->setLanguage($request->request->get('language',''));
                $comment->setType($request->request->get('type'));
                $comment->setModuleId($request->request->get('moduleId'));
                $comment->setItemId($request->request->get('itemId'));
                $comment->setEnabled(true);
            }

            $em->persist($comment);
            $em->flush();

            $result = array(
                'error' => 0,
                'commentId' => $comment->getId()
            );

        } catch (\Doctrine\DBAL\DBALException $e) {
            $result = array(
                'error' => 1
            );
        }

        return new JsonResponse($result);

    }


    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $userId = $user ? $user->getId() : $request->request->get('userId', 0);


        $result = array(
            'error' => 1
        );

        try{

            $comment = $em->getRepository('InvictusCommentBundle:Comment')
                ->findOneBy(array('id' => $id));

            if($comment and $comment->getUserId() == $userId){

                $comment->setDeleted(true);
                $em->persist($comment);
                $em->flush();

                $result = array(
                    'error' => 0
                );
            }

        }catch (\Doctrine\DBAL\DBALException $e) {
            $result = array(
                'error' => 1
            );
        }

        return new JsonResponse($result);

    }


    public function listAction($moduleId, $itemId)
    {
        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository('InvictusCommentBundle:Comment')
        ->getArrayComments($moduleId, $itemId);

        $safeComments = array();
        foreach($comments as $comment){
            $date = $comment['date'];

            $comment['date'] = array('date' => $date->format('Y-m-d H:i:s'));
            $safeComments[] = $comment;
        }



        return new JsonResponse($safeComments);

    }


    public function reloadListAction($moduleId, $itemId)
    {
        $user = $this->getUser();
        $userId = $user ? $user->getId() : 0;

        $personalComments = $this->getDoctrine()
            ->getRepository('InvictusCommentBundle:Comment')
            ->findBy(array('userId' => $userId, 'moduleId' => $moduleId, 'itemId' => $itemId, 'type' => 1, 'enabled' => 1, 'deleted' => 0), array('date' => 'DESC'));
        $socialComments = $this->getDoctrine()
            ->getRepository('InvictusCommentBundle:Comment')
            ->findBy(array('moduleId' => $moduleId, 'itemId' => $itemId, 'enabled' => 1, 'deleted' => 0), array('date' => 'DESC'));

        return $this->render('N2ktagWebappBundle::reloadList.html.twig', array(
                'personalComments' => $personalComments,
                'socialComments' => $socialComments
            )
        );

    }

}
