Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('date'),
    //Translator.trans('name'),
    Translator.trans('text'),
    Translator.trans('social'),
    //Translator.trans('language'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:80, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:45, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'date', index:'base.date', fixed: true, width:130, search:true, align: 'left'},
    //{name:'name', index:'base.name', width:20, search:true, align: 'left'},
    {name:'body', index:'base.body', width:70, search:true, align: 'left'},
    {name:'type', index:'base.type', width:10, search:true, align: 'left'},
    //{name:'language', index:'base.language', fixed: true, width:110, search:true, align: 'left'},
    {name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'news_label': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'name', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'body', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'date', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'type', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'language', '', {'text-align':'left'});
    }
});