<?php
  
namespace Invictus\TestimonialBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TestimonialTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'title',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('abstract', 'text', array(
                'label' => 'FBid',
                'required' => false,
                'attr'   =>  array(
                ),
                'label_attr' => array(
                    'raw' => true
                ),
                'translation_domain' => 'testimonial'
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'content',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                )
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\TestimonialBundle\Entity\Testimonial'
            )
        );

    }

    public function getName()
    {
        return 'testimonial_translation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\TestimonialBundle\Entity\TestimonialTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
