<?php

namespace Invictus\TestimonialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TestimonialTranslation
 *
 * @ORM\Table(name="testimonial_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_testimonial_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_testimonial_id", "fk_language_id"})
 * @ORM\Entity()
 */
class TestimonialTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Testimonial
     *
     * @ORM\ManyToOne(targetEntity="Testimonial", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_testimonial_id", referencedColumnName="id")
     * })
     */
    private $fkBase;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\TestimonialBundle\Entity\Testimonial $fkBase
     * @return TestimonialTranslation
     */
    public function setFkBase(\Invictus\TestimonialBundle\Entity\Testimonial $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\TestimonialBundle\Entity\Testimonial
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}