<?php

namespace Invictus\TestimonialBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Invictus\CmsBundle\Controller\InvictusController;


class TestimonialController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        //$config['appId'] = 'vs';
        //$config['languageId'] = 'en_GB';
        //$config['moduleId'] = 4;
        $config['orderBy'] = array(
            'date' => 'translation.label ASC'
        );
        $config['joins'] = array(
            'position' => false
        );
        $this->tableManager->addConfig($config);
        $this->tableManager->enablePredefinedToggle('enabled');

        //$this->tableManager->enableAction('edit');
        //$this->tableManager->disableAction('delete');
    }

}
