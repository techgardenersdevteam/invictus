<?php

namespace Invictus\GuideBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * GuideTranslation
 *
 * @ORM\Table(name="guide_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_guide_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_guide_id", "fk_language_id"})
 * @ORM\Entity()
 */
class GuideTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Guide
     *
     * @ORM\ManyToOne(targetEntity="Guide", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_guide_id", referencedColumnName="id")
     * })
     */
    private $fkBase;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\GuideBundle\Entity\Guide $fkBase
     * @return GuideTranslation
     */
    public function setFkBase(\Invictus\GuideBundle\Entity\Guide $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\GuideBundle\Entity\Guide
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}