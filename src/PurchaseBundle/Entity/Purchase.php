<?php

namespace Invictus\PurchaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Purchase
 *
 * @ORM\Table(name="purchase")
 * @ORM\Entity(repositoryClass="Invictus\PurchaseBundle\Entity\PurchaseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Purchase extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $email
     *     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string $shipping
     *     * @ORM\Column(name="shipping", type="text", nullable=true)
     */
    private $shipping;

    /**
     * @var string $billing
     *     * @ORM\Column(name="billing", type="text", nullable=true)
     */
    private $billing;

    /**
     * @var datetime $date
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string $total
     *
     * @ORM\Column(name="total", type="string", nullable=true)
     */
    private $total;

    /**
     * @var string $paypalId
     *
     * @ORM\Column(name="paypal_id", type="string", nullable=true)
     */
    private $paypalId;

    /**
     * @var string $invoiceNumber
     *
     * @ORM\Column(name="invoice_number", type="string", nullable=true)
     */
    private $invoiceNumber;

    /**
     * @var string $tracking
     *
     * @ORM\Column(name="tracking", type="string", nullable=true)
     */
    private $tracking;

    /**
     * @var text $ipn
     *
     * @ORM\Column(name="ipn", type="text", nullable=true)
     */
    private $ipn;

    /**
     * @var string $userAgent
     *
     * @ORM\Column(name="userAgent", type="string", nullable=true)
     */
    private $userAgent;



    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Hideandjack\AuthBundle\Entity\AuthUser", inversedBy="purchases")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     * })
     */
    private $fkAuthUser;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\PurchaseBundle\Entity\PurchaseItem", mappedBy="fkPurchase")
     */
    protected $purchaseItems;



    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return Purchase
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    
        return $this;
    }

    /**
     * Get identifier
     *
     * @return string 
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Purchase
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set total
     *
     * @param string $total
     * @return Purchase
     */
    public function setTotal($total)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total
     *
     * @return string 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set fkAuthUser
     *
     * @param \Hideandjack\AuthBundle\Entity\AuthUser $fkAuthUser
     * @return Purchase
     */
    public function setFkAuthUser(\Hideandjack\AuthBundle\Entity\AuthUser $fkAuthUser = null)
    {
        $this->fkAuthUser = $fkAuthUser;
    
        return $this;
    }

    /**
     * Get fkAuthUser
     *
     * @return \Hideandjack\AuthBundle\Entity\AuthUser 
     */
    public function getFkAuthUser()
    {
        return $this->fkAuthUser;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->date = new \DateTime();
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Purchase
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add purchaseItems
     *
     * @param \Invictus\PurchaseBundle\Entity\PurchaseItem $purchaseItems
     * @return Purchase
     */
    public function addPurchaseItem(\Invictus\PurchaseBundle\Entity\PurchaseItem $purchaseItems)
    {
        $this->purchaseItems[] = $purchaseItems;
    
        return $this;
    }

    /**
     * Remove purchaseItems
     *
     * @param \Invictus\PurchaseBundle\Entity\PurchaseItem $purchaseItems
     */
    public function removePurchaseItem(\Invictus\PurchaseBundle\Entity\PurchaseItem $purchaseItems)
    {
        $this->purchaseItems->removeElement($purchaseItems);
    }

    /**
     * Get purchaseItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurchaseItems()
    {
        return $this->purchaseItems;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Purchase
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set paypalId
     *
     * @param string $paypalId
     * @return Purchase
     */
    public function setPaypalId($paypalId)
    {
        $this->paypalId = $paypalId;
    
        return $this;
    }

    /**
     * Get paypalId
     *
     * @return string 
     */
    public function getPaypalId()
    {
        return $this->paypalId;
    }

    /**
     * Set shipping
     *
     * @param string $shipping
     * @return Purchase
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    
        return $this;
    }

    /**
     * Get shipping
     *
     * @return string 
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * Set billing
     *
     * @param string $billing
     * @return Purchase
     */
    public function setBilling($billing)
    {
        $this->billing = $billing;
    
        return $this;
    }

    /**
     * Get billing
     *
     * @return string 
     */
    public function getBilling()
    {
        return $this->billing;
    }

    /**
     * Set invoiceNumber
     *
     * @param string $invoiceNumber
     * @return Purchase
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;
    
        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return string 
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set tracking
     *
     * @param string $tracking
     * @return Purchase
     */
    public function setTracking($tracking)
    {
        $this->tracking = $tracking;
    
        return $this;
    }

    /**
     * Get tracking
     *
     * @return string 
     */
    public function getTracking()
    {
        return $this->tracking;
    }

    /**
     * Set ipn
     *
     * @param string $ipn
     * @return Purchase
     */
    public function setIpn($ipn)
    {
        $this->ipn = $ipn;
    
        return $this;
    }

    /**
     * Get ipn
     *
     * @return string 
     */
    public function getIpn()
    {
        return $this->ipn;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     * @return Purchase
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    
        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string 
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }
}