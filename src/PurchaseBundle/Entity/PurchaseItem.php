<?php

namespace Invictus\PurchaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * PurchaseItem
 *
 * @ORM\Table(name="purchase_item")
 * @ORM\Entity(repositoryClass="Invictus\PurchaseBundle\Entity\PurchaseItemRepository")
 */
class PurchaseItem extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $productId
     *
     * @ORM\Column(name="product_id", type="integer", nullable=true)
     */
    private $productId;

    /**
     * @var string $code
     *
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", scale=2, length=255, nullable=true)
     */
    private $price = '0.00';

    /**
     * @var integer $quantity
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Invictus\PurchaseBundle\Entity\Purchase", inversedBy="purchaseItems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_purchase_id", referencedColumnName="id")
     * })
     */
    private $fkPurchase;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return PurchaseItem
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return PurchaseItem
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return PurchaseItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set fkPurchase
     *
     * @param \Invictus\PurchaseBundle\Entity\Purchase $fkPurchase
     * @return PurchaseItem
     */
    public function setFkPurchase(\Invictus\PurchaseBundle\Entity\Purchase $fkPurchase = null)
    {
        $this->fkPurchase = $fkPurchase;
    
        return $this;
    }

    /**
     * Get fkPurchase
     *
     * @return \Invictus\PurchaseBundle\Entity\Purchase 
     */
    public function getFkPurchase()
    {
        return $this->fkPurchase;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return PurchaseItem
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    
        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }
}