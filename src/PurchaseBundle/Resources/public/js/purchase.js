Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('date'),
    Translator.trans('orderNumber'),
    Translator.trans('buyer', {}, 'purchase'),
    Translator.trans('PayPal Id'),
    Translator.trans('total', {}, 'purchase'),
    Translator.trans('status'),
    //Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:80, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:45, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'date', index:'base.date', fixed: true, width:140, search:true, align: 'left'},
    {name:'orderNumber', index:'base.label', fixed: true, width:160, search:true, align: 'left'},
    {name:'buyer', index:'base.buyer', width:40, search:true, sortable: false, align: 'left'},
    {name:'paypalId', index:'base.paypalId', fixed: true, width:200, search:true, align: 'left'},
    {name:'total', index:'base.total', fixed: true, width:100, search:true, align: 'left'},
    {name:'status', index:'base.status', fixed: true, width:120, search:true, align: 'left'},
    //{name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'news_label': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'orderNumber', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'buyer', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'paypalId', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'total', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'date', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'status', '', {'text-align':'left'});
    }
});