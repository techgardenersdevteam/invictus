<?php

namespace Invictus\PurchaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class PurchaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('status', 'choice', array(
                'label' => 'status',
                'required' => false,
                'choices'   => array(
                    '0' => 'notPaid',
                    '1' => 'paid',
                    '2' => 'preparation',
                    '3' => 'sent',
                    '4' => 'completed',
                    '5' => 'aborted',
                    '6' => 'refunded',
                    '7' => 'partial'
                ),
                'attr' =>   array(
                    'class' => 'select2'
                ),
                'translation_domain' => 'purchase',
                'empty_value' => false,
            )
        );

        $builder->add('invoiceNumber', 'text', array(
                'label' => 'invoiceNumber',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('tracking', 'text', array(
                'label' => 'trackingCode',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );
    }

    public function getName()
    {
        return 'purchase';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\PurchaseBundle\Entity\Purchase',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
