<?php

namespace Invictus\PurchaseBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Invictus\CmsBundle\Controller\InvictusController;


class PurchaseController extends InvictusController
{

    protected function configureTableManager()
    {

        $config = array();
        $config['orderBy'] = array(
            'base.status' => 'base.status ASC',
            'base.date' => 'base.date DESC'
        );
        $config['joins'] = array(
            'position' => false,
            'metadata' => false,
        );
        $this->tableManager->addConfig($config);

        $this->tableManager->disableAction('attach');
        $this->tableManager->disableAction('delete');
        //$this->tableManager->enablePredefinedToggle('enabled');
    }


    public function invoiceAction(Request $request, $nr){

        $em = $this->get('doctrine');
        $purchase = $em->getRepository('InvictusPurchaseBundle:Purchase')
                ->getPurchaseData($nr);

        $filePath = $this->get('kernel')->getRootDir().'/../web/invoices/'.$nr.'.pdf';



        if(!$purchase){
            die('The order with number '.$nr.' doesn\'t exists');
        }
        //var_dump($purchase);die();

        if(is_null($purchase->getInvoiceNumber())){
            die('You have to set an InvoiceNumber on order '.$purchase->getLabel().' to generate an invoice');
        }

        $pdf = $request->query->get('pdf', false);

        if(!$pdf){
            return $this->render('InvictusPurchaseBundle::invoice.html.twig', array(
                'purchase' => $purchase
            ));
        }

        $pageUrl = $this->generateUrl('invictus_purchase_invoice', array('_locale' => 'en_GB', 'nr' => $nr), true); // use absolute path!

        return new Response(
            $this->get('knp_snappy.pdf')->getOutput($pageUrl),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="'.$nr.'.pdf"'
            )
        );
    }

}
