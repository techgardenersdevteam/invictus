<?php

namespace Invictus\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity(repositoryClass="Invictus\ServiceBundle\Entity\ServiceRepository")
 */
class Service extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Service")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="installation", type="boolean", nullable=false)
     */
    private $installation = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="maintenance", type="boolean", nullable=false)
     */
    private $mainteinance = false;

    /**
     * @var smallint
     *
     * @ORM\Column(name="northPrice", type="smallint", nullable=true)
     */
    private $northPrice;

    /**
     * @var smallint
     *
     * @ORM\Column(name="centralPrice", type="smallint", nullable=true)
     */
    private $centralPrice;

    /**
     * @var smallint
     *
     * @ORM\Column(name="southPrice", type="smallint", nullable=true)
     */
    private $southPrice;

    /**
     * @var smallint
     *
     * @ORM\Column(name="extensionPrice", type="smallint", nullable=true)
     */
    private $extensionPrice;

    /**
     * @var smallint
     *
     * @ORM\Column(name="northDiscount", type="smallint", nullable=true)
     */
    private $northDiscount;

    /**
     * @var smallint
     *
     * @ORM\Column(name="centralDiscount", type="smallint", nullable=true)
     */
    private $centralDiscount;

    /**
     * @var smallint
     *
     * @ORM\Column(name="southDiscount", type="smallint", nullable=true)
     */
    private $southDiscount;

    /**
     * @var smallint
     *
     * @ORM\Column(name="northCCDiscount", type="smallint", nullable=true)
     */
    private $northCCDiscount;

    /**
     * @var smallint
     *
     * @ORM\Column(name="centralCCDiscount", type="smallint", nullable=true)
     */
    private $centralCCDiscount;

    /**
     * @var smallint
     *
     * @ORM\Column(name="southCCDiscount", type="smallint", nullable=true)
     */
    private $southCCDiscount;

    /**
     * @var string
     *
     * @ORM\Column(name="questions", type="string", nullable=true)
     */
    private $questions = false;

    /*
     * @var \Template
     *
     * @ORM\ManyToOne(targetEntity="Invictus\TemplateBundle\Entity\Template", inversedBy="services")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_template_id", referencedColumnName="id")
     * })
     */
    //protected $fkTemplate = 5;

    /**
     * @ORM\OneToMany(targetEntity="ServiceTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkServiceItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Position", mappedBy="fkServiceItem")
     */
    protected $positions;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkServiceItem")
     */
    protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkServiceItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->metadatas = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->tag;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add translation
     *
     * @param \Invictus\ServiceBundle\Entity\ServiceTranslation $translation
     * @return Service
     */
    public function addTranslation(\Invictus\ServiceBundle\Entity\ServiceTranslation $translation)
    {
        $translation->setFkBase($this);
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \Invictus\ServiceBundle\Entity\ServiceTranslation $translation
     */
    public function removeTranslation(\Invictus\ServiceBundle\Entity\ServiceTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }


    /**
     * Add visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     * @return Service
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility[] = $visibility;

        return $this;
    }

    /**
     * Remove visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility->removeElement($visibility);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     * @return Service
     */
    public function addAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    
        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     * @return Service
     */
    public function addMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $metadata->setFkItemId($this->getId());
        $this->metadatas[] = $metadata;
    
        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     */
    public function removeMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $this->metadatas->removeElement($metadata);
    }

    /**
     * Get metadatas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Service
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Service
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Set type
     *
     * @param boolean $type
     * @return Service
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return boolean 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set fkTemplate
     *
     * @param \Invictus\TemplateBundle\Entity\Template $fkTemplate
     * @return Service
     */
    public function setFkTemplate(\Invictus\TemplateBundle\Entity\Template $fkTemplate = null)
    {
        $this->fkTemplate = $fkTemplate;
    
        return $this;
    }

    /**
     * Get fkTemplate
     *
     * @return \Invictus\TemplateBundle\Entity\Template 
     */
    public function getFkTemplate()
    {
        return false;
        return $this->fkTemplate;
    }


    /**
     * Set northPrice
     *
     * @param integer $northPrice
     * @return Service
     */
    public function setNorthPrice($northPrice)
    {
        $this->northPrice = $northPrice;
    
        return $this;
    }

    /**
     * Get northPrice
     *
     * @return integer 
     */
    public function getNorthPrice()
    {
        return $this->northPrice;
    }

    /**
     * Set centralPrice
     *
     * @param integer $centralPrice
     * @return Service
     */
    public function setCentralPrice($centralPrice)
    {
        $this->centralPrice = $centralPrice;
    
        return $this;
    }

    /**
     * Get centralPrice
     *
     * @return integer 
     */
    public function getCentralPrice()
    {
        return $this->centralPrice;
    }

    /**
     * Set southPrice
     *
     * @param integer $southPrice
     * @return Service
     */
    public function setSouthPrice($southPrice)
    {
        $this->southPrice = $southPrice;
    
        return $this;
    }

    /**
     * Get southPrice
     *
     * @return integer 
     */
    public function getSouthPrice()
    {
        return $this->southPrice;
    }

    /**
     * Set installation
     *
     * @param boolean $installation
     * @return Service
     */
    public function setInstallation($installation)
    {
        $this->installation = $installation;
    
        return $this;
    }

    /**
     * Get installation
     *
     * @return boolean 
     */
    public function getInstallation()
    {
        return $this->installation;
    }

    /**
     * Set mainteinance
     *
     * @param boolean $mainteinance
     * @return Service
     */
    public function setMainteinance($mainteinance)
    {
        $this->mainteinance = $mainteinance;
    
        return $this;
    }

    /**
     * Get mainteinance
     *
     * @return boolean 
     */
    public function getMainteinance()
    {
        return $this->mainteinance;
    }

    /**
     * Set parent
     *
     * @param \Invictus\ServiceBundle\Entity\Service $parent
     * @return Service
     */
    public function setParent(\Invictus\ServiceBundle\Entity\Service $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Invictus\ServiceBundle\Entity\Service 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set extensionPrice
     *
     * @param integer $extensionPrice
     * @return Service
     */
    public function setExtensionPrice($extensionPrice)
    {
        $this->extensionPrice = $extensionPrice;
    
        return $this;
    }

    /**
     * Get extensionPrice
     *
     * @return integer 
     */
    public function getExtensionPrice()
    {
        return $this->extensionPrice;
    }

    /**
     * Set northDiscount
     *
     * @param integer $northDiscount
     * @return Service
     */
    public function setNorthDiscount($northDiscount)
    {
        $this->northDiscount = $northDiscount;
    
        return $this;
    }

    /**
     * Get northDiscount
     *
     * @return integer 
     */
    public function getNorthDiscount()
    {
        return $this->northDiscount;
    }

    /**
     * Set centralDiscount
     *
     * @param integer $centralDiscount
     * @return Service
     */
    public function setCentralDiscount($centralDiscount)
    {
        $this->centralDiscount = $centralDiscount;
    
        return $this;
    }

    /**
     * Get centralDiscount
     *
     * @return integer 
     */
    public function getCentralDiscount()
    {
        return $this->centralDiscount;
    }

    /**
     * Set southDiscount
     *
     * @param integer $southDiscount
     * @return Service
     */
    public function setSouthDiscount($southDiscount)
    {
        $this->southDiscount = $southDiscount;
    
        return $this;
    }

    /**
     * Get southDiscount
     *
     * @return integer 
     */
    public function getSouthDiscount()
    {
        return $this->southDiscount;
    }

    /**
     * Set questions
     *
     * @param string $questions
     * @return Service
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    
        return $this;
    }

    /**
     * Get questions
     *
     * @return string 
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set northCCDiscount
     *
     * @param integer $northCCDiscount
     * @return Service
     */
    public function setNorthCCDiscount($northCCDiscount)
    {
        $this->northCCDiscount = $northCCDiscount;
    
        return $this;
    }

    /**
     * Get northCCDiscount
     *
     * @return integer 
     */
    public function getNorthCCDiscount()
    {
        return $this->northCCDiscount;
    }

    /**
     * Set centralCCDiscount
     *
     * @param integer $centralCCDiscount
     * @return Service
     */
    public function setCentralCCDiscount($centralCCDiscount)
    {
        $this->centralCCDiscount = $centralCCDiscount;
    
        return $this;
    }

    /**
     * Get centralCCDiscount
     *
     * @return integer 
     */
    public function getCentralCCDiscount()
    {
        return $this->centralCCDiscount;
    }

    /**
     * Set southCCDiscount
     *
     * @param integer $southCCDiscount
     * @return Service
     */
    public function setSouthCCDiscount($southCCDiscount)
    {
        $this->southCCDiscount = $southCCDiscount;
    
        return $this;
    }

    /**
     * Get southCCDiscount
     *
     * @return integer 
     */
    public function getSouthCCDiscount()
    {
        return $this->southCCDiscount;
    }
}
