<?php

namespace Invictus\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ServiceTranslation
 *
 * @ORM\Table(name="service_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_service_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_service_id", "fk_language_id"})
 * @ORM\Entity()
 */
class ServiceTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Service
     *
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_service_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * @var string
     *
     * @ORM\Column(name="what", type="text", nullable=true)
     */
    private $what;

    /**
     * @var string
     *
     * @ORM\Column(name="abstract2", type="text", nullable=true)
     */
    private $abstract2;

    /**
     * @var string
     *
     * @ORM\Column(name="body2", type="text", nullable=true)
     */
    private $body2;

    /**
     * @var string
     *
     * @ORM\Column(name="prices", type="text", nullable=true)
     */
    private $prices;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\ServiceBundle\Entity\Service $fkBase
     * @return ServiceTranslation
     */
    public function setFkBase(\Invictus\ServiceBundle\Entity\Service $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\ServiceBundle\Entity\Service
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Set abstract2
     *
     * @param string $abstract2
     * @return ServiceTranslation
     */
    public function setAbstract2($abstract2)
    {
        $this->abstract2 = $abstract2;
    
        return $this;
    }

    /**
     * Get abstract2
     *
     * @return string 
     */
    public function getAbstract2()
    {
        return $this->abstract2;
    }

    /**
     * Set body2
     *
     * @param string $body2
     * @return ServiceTranslation
     */
    public function setBody2($body2)
    {
        $this->body2 = $body2;
    
        return $this;
    }

    /**
     * Get body2
     *
     * @return string 
     */
    public function getBody2()
    {
        return $this->body2;
    }

    /**
     * Set what
     *
     * @param string $what
     * @return ServiceTranslation
     */
    public function setWhat($what)
    {
        $this->what = $what;
    
        return $this;
    }

    /**
     * Get what
     *
     * @return string 
     */
    public function getWhat()
    {
        return $this->what;
    }

    /**
     * Set prices
     *
     * @param string $prices
     * @return ServiceTranslation
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    
        return $this;
    }

    /**
     * Get prices
     *
     * @return string 
     */
    public function getPrices()
    {
        return $this->prices;
    }
}
