<?php

namespace Invictus\ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Invictus\CmsBundle\Controller\InvictusController;


class ServiceController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        $this->tableManager->addConfig($config);
        $this->tableManager->enablePredefinedToggle('enabled');

        //$this->tableManager->enableAction('edit');
        //$this->tableManager->disableAction('delete');
    }

}
