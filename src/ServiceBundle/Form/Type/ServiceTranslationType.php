<?php
  
namespace Invictus\ServiceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'title',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('what', 'textarea', array(
                'label' => 'what',
                'required' => false,
                'attr'   =>  array(
                    'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('abstract', 'text', array(
                'label' => 'abstract',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'content',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('abstract2', 'text', array(
                'label' => 'abstract2',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('body2', 'textarea', array(
                'label' => 'content2',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('prices', 'textarea', array(
                'label' => 'prices',
                'required' => false,
                'attr'   =>  array(
                    'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\ServiceBundle\Entity\Service'
            )
        );

    }

    public function getName()
    {
        return 'service_translation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\ServiceBundle\Entity\ServiceTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
