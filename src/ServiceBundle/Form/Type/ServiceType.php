<?php

namespace Invictus\ServiceBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;
use Doctrine\ORM\EntityRepository;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );


        $builder->add('parent', 'entity' , array(
                'required' => false,
                'class' => 'InvictusServiceBundle:Service',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) use($options){
                        $qb = $er->createQueryBuilder('s')
                            ->addSelect('trans')
                            ->innerJoin('s.translations', 'trans')
                            ->innerJoin('trans.fkLanguage', 'lang')
                            //->andWhere('m.enabled = 1')
                            ->andWhere('s.deleted = 0')
                            //->andWhere('trans.fkLanguage = :locale')
                            ->orderBy('trans.label', 'ASC');
                        //->setParameter('locale', $options['UILanguageId']);
                        //echo "<br /><br />".$qb->getDQL();
                        //echo "<br /><br />".$qb->getQuery()->getSQL();
                        return $qb;
                    },
                'empty_value' => 'newService',
                'empty_data' => null,
                'label' => 'parent',
                'translation_domain' => 'service',
                'attr'   =>  array(
                    'class' => 'select2',
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('type', 'checkbox', array(
                'label' => 'primaryComplementary',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('installation', 'checkbox', array(
                'label' => 'installation',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('mainteinance', 'checkbox', array(
                'label' => 'mainteinance',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('northPrice', 'text', array(
                'label' => 'northPrice',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('centralPrice', 'text', array(
                'label' => 'centralPrice',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('southPrice', 'text', array(
                'label' => 'southPrice',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('extensionPrice', 'text', array(
                'label' => 'extensionPrice',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );




        $builder->add('northDiscount', 'text', array(
                'label' => 'northDiscount',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('centralDiscount', 'text', array(
                'label' => 'centralDiscount',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('southDiscount', 'text', array(
                'label' => 'southDiscount',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );






        $builder->add('northCCDiscount', 'text', array(
                'label' => 'northCCDiscount',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('centralCCDiscount', 'text', array(
                'label' => 'centralCCDiscount',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('southCCDiscount', 'text', array(
                'label' => 'southCCDiscount',
                'required' => false,
                'attr'   =>  array(
                    //'class' => ''
                ),
                'translation_domain' => 'service'
            )
        );

        $builder->add('questions', 'choice', array(
                'label' => 'questions',
                'choices' => array(
                    'none'   => 'Nessuna',
                    'a'   => 'Pulizia canna fumaria',
                    'b'   => 'Manutenzione stufa a pellet/legna',
                    'c'   => 'Installazione elettrodomestico ad incasso',
                    'd'   => 'Manutenzione climatizzatore',
                    'e'   => 'Installazione climatizzatore',
                    'f'   => 'Installazione stufa a pellet'
                ),
                'required' => false,
                'attr'   =>  array(
                    'class' => 'select2',
                ),
                'translation_domain' => 'service'
            )
        );



        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new ServiceTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'service';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\ServiceBundle\Entity\Service',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
