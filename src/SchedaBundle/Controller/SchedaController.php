<?php

namespace Invictus\SchedaBundle\Controller;

use Invictus\CmsBundle\Controller\InvictusController;

class SchedaController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        //$config['appId'] = 'vs';
        //$config['languageId'] = 'en_GB';
        //$config['moduleId'] = 4;
        //$config['orderBy'] = array(
        //   'date' => 'base.visibleDate DESC'
        //);
        $config['joins'] = array(
            'position' => false
        );
        $this->tableManager->addConfig($config);
        $this->tableManager->enablePredefinedToggle('enabled');

        //$this->tableManager->enableAction('edit');
        //$this->tableManager->disableAction('delete');
    }


    protected function configureTableList(){

        $this->fields[] = 'scheda.label';
        $this->sorts[] = 'asc';

        if($this->invictusKernel->getGETParam('id')){
            $this->where[] = ' AND id LIKE :id';
            $this->parameters['id'] = $this->request->query->get('id');
        }

        if($this->invictusKernel->getGETParam('label')){
            $this->where[] = ' AND label LIKE :label';
            $this->parameters['label'] = '%'.$this->request->query->get('label').'%';
        }

        $this->where[] = ' AND deleted = :deleted';
        $this->parameters['deleted'] = '0';

        $this->tableListConfig['bools'] = array(
            /*
            array(
                'field' => 'highlighted',
                'icons' => array(
                    0 => 'icomoon16-star',
                    1 => 'icomoon16-star-3'
                ),
                'translation' => array(
                    0 => 'notHighlighted',
                    1 => 'highlighted'
                ),
                'unique' => 0
            ),
            */
            array(
                'field' => 'enabled',
                'icons' => array(
                    0 => 'icomoon16-minus-3',
                    1 => 'icomoon16-checkmark-3'
                ),
                'translation' => array(
                    0 => 'notPublished',
                    1 => 'published'
                ),
                'css' => array(
                    0 => 'disabled',
                    1 => 'enabled'
                )
            )
        );

        $this->tableListConfig['actions'] = array(
            'attachments' => true,
            'edit' => true,
            'delete' => true
        );
    }

    protected function preUpdateItem($item){
        try{
            $length = rand(1,15);
            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
            $item->setAbstract($randomString);
        }catch (Exception $e){

        }

        return $item;
    }

}
