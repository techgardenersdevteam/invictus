Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('comName', {}, 'scheda'),
    Translator.trans('scName', {}, 'scheda'),
    Translator.trans('famiglia', {}, 'scheda'),
    Translator.trans('ordine', {}, 'scheda'),
    Translator.trans('regno', {}, 'scheda'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:100, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:80, fixed: true, sortable: true, search: true, align: 'center'},
    {name:'comName', index:'translation.comName', width:20, sortable: true, search: true, align: 'left'},
    {name:'scName', index:'translation.scName', width:40, search:true, align: 'left'},
    {name:'famiglia', index:'base.famiglia', width:20, search:true, align: 'left'},
    {name:'ordine', index:'base.ordine', width:20, search:true, align: 'left'},
    {name:'regno', index:'base.regno', width:20, search:true, align: 'left'},
    {name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'news_label': "Translation.validation.required"
		};
}

//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            //.jqGrid ('setLabel', 'id', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'comName', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'scName', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'famiglia', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'ordine', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'regno', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});

var gmapConfig = {
    map:{
        options: {
            zoom: 11,
            center: new google.maps.LatLng(45.379161,12.458496),
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            },
            navigationControl: true,
            scrollwheel: true,
            streetViewControl: false
        }
    },
    "marker":{
        "values":[
            {"latLng":[45.372161,12.458996], "data":"Punto a caso vicino al centro della mappa !"},
            {"address":"31100 Treviso, Italia", "data":"<b>Treviso</b> <br>great city !"},
            {"address":"30121 Venezia, Italia", "data":"Venezia ! GO ASAP !", "options":{"draggable": false, "icon": "http://maps.google.com/mapfiles/marker_green.png"}}
        ],
        "options":{
            "draggable": true
        },
        events:{
            click: function(marker, event, context){
                var map = $(this).gmap3("get"),
                    infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                    infowindow.open(map, marker);
                    infowindow.setContent(context.data);
                } else {
                    $(this).gmap3({
                        infowindow:{
                            anchor:marker,
                            options:{content: context.data}
                        }
                    });
                }
            }
            /*
            ,
            mouseout: function(){
                var infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                    infowindow.close();
                }
            }
            */
        }
    },
    autofit:{}
};