<?php

namespace Invictus\SchedaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class SchedaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox',
                    'autocomplete' => 'off'
                )
            )
        );

        $builder->add('cod', 'text', array(
                'label' => 'cod',
                'required' => false,
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('sicZps', 'choice', array(
                'label' => 'sicZps',
                'required' => false,
                'attr' => array(
                    'class' => 'select2'
                ),
                'translation_domain' => 'scheda',
                'choices' => array(
                    'IT3250003' => 'IT3250003',
                    'IT3250023' => 'IT3250023',
                    'IT3250030' => 'IT3250030',
                    'IT3250031' => 'IT3250031',
                    'IT3250046' => 'IT3250046',
                    'IT3250048' => 'IT3250048',
                    'IT3250031 - IT3250046' => 'IT3250031 - IT3250046',
                    'IT3250030 - IT3250046' => 'IT3250030 - IT3250046',
                    'IT3250030 - IT3250031 - IT3250046' => 'IT3250030 - IT3250031 - IT3250046',
                    'IT3250003 - IT3250023' => 'IT3250003 - IT3250023',
                    'IT3250030 - IT3250031 - IT3250046 - IT3250003 - IT3250023' => 'IT3250030 - IT3250031 - IT3250046 - IT3250003 - IT3250023',
                    'IT3250030 - IT3250031 - IT3250046 - IT3250003' => 'IT3250030 - IT3250031 - IT3250046 - IT3250003',
                    'IT3250030 - IT3250031 - IT3250046 - IT3250023' => 'IT3250030 - IT3250031 - IT3250046 - IT3250023',
                    'IT3250048 - IT3250031 - IT3250030 - IT3250046' => 'IT3250048 - IT3250031 - IT3250030 - IT3250046',
                    'IT3250048 - IT3250030 - IT3250046' => 'IT3250048 - IT3250030 - IT3250046'
                )
            )
        );

        $builder->add('codiceN2K', 'text', array(
                'label' => 'codiceN2K',
                'required' => false,
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('famiglia', 'text', array(
                'label' => 'famiglia',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('ordine', 'text', array(
                'label' => 'ordine',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('classe', 'text', array(
                'label' => 'classe',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('divisione', 'text', array(
                'label' => 'divisione',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('superdivisione', 'text', array(
                'label' => 'superdivisione',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('regno', 'text', array(
                'label' => 'regno',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('sottoregno', 'text', array(
                'label' => 'sottoregno',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('descLevelSc', 'textarea', array(
                'label' => 'descLevelSc',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkCategory', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Category'
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new SchedaTranslationType() ));

        //$builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'scheda';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\SchedaBundle\Entity\Scheda',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
