<?php

namespace Invictus\SchedaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SchedaTranslation
 *
 * @ORM\Table(name="scheda_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_scheda_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id"})
 * @ORM\Entity(repositoryClass="Invictus\SchedaBundle\Entity\SchedaTranslationRepository")
 */
class SchedaTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_n2k", type="text", nullable=true)
     */
    private $descN2K;

    /**
     * @var string
     *
     * @ORM\Column(name="com_name", type="string", nullable=true)
     */
    private $comName;

    /**
     * @var string
     *
     * @ORM\Column(name="sc_name", type="string", nullable=true)
     */
    private $scName;

    /**
     * @var string
     *
     * @ORM\Column(name="prot_level_n2k", type="string", nullable=true)
     */
    private $protLevelN2K;

    /**
     * @var string
     *
     * @ORM\Column(name="add_prot_level", type="string", nullable=true)
     */
    private $addProtLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_level1", type="text", nullable=true)
     */
    private $descLevel1;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_level2", type="text", nullable=true)
     */
    private $descLevel2;

    /**
     * @var \Scheda
     *
     * @ORM\ManyToOne(targetEntity="Scheda", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_scheda_id", referencedColumnName="id")
     * })
     */
    private $fkBase;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descN2K
     *
     * @param string $descN2K
     * @return SchedaTranslation
     */
    public function setDescN2K($descN2K)
    {
        $this->descN2K = $descN2K;
    
        return $this;
    }

    /**
     * Get descN2K
     *
     * @return string 
     */
    public function getDescN2K()
    {
        return $this->descN2K;
    }

    /**
     * Set comName
     *
     * @param string $comName
     * @return SchedaTranslation
     */
    public function setComName($comName)
    {
        $this->comName = $comName;
        $this->setLabel($comName);

        return $this;
    }

    /**
     * Get comName
     *
     * @return string 
     */
    public function getComName()
    {
        return $this->comName;
    }

    /**
     * Set scName
     *
     * @param string $scName
     * @return SchedaTranslation
     */
    public function setScName($scName)
    {
        $this->scName = $scName;
    
        return $this;
    }

    /**
     * Get scName
     *
     * @return string 
     */
    public function getScName()
    {
        return $this->scName;
    }

    /**
     * Set descLevel1
     *
     * @param string $descLevel1
     * @return SchedaTranslation
     */
    public function setDescLevel1($descLevel1)
    {
        $this->descLevel1 = $descLevel1;
    
        return $this;
    }

    /**
     * Get descLevel1
     *
     * @return string 
     */
    public function getDescLevel1()
    {
        return $this->descLevel1;
    }

    /**
     * Set descLevel2
     *
     * @param string $descLevel2
     * @return SchedaTranslation
     */
    public function setDescLevel2($descLevel2)
    {
        $this->descLevel2 = $descLevel2;
    
        return $this;
    }

    /**
     * Get descLevel2
     *
     * @return string 
     */
    public function getDescLevel2()
    {
        return $this->descLevel2;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\SchedaBundle\Entity\Scheda $fkBase
     * @return SchedaTranslation
     */
    public function setFkBase(\Invictus\SchedaBundle\Entity\Scheda $fkBase = null)
    {
        $this->fkBase = $fkBase;
    
        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\SchedaBundle\Entity\Scheda 
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Set protLevelN2K
     *
     * @param string $protLevelN2K
     * @return SchedaTranslation
     */
    public function setProtLevelN2K($protLevelN2K)
    {
        $this->protLevelN2K = $protLevelN2K;
    
        return $this;
    }

    /**
     * Get protLevelN2K
     *
     * @return string 
     */
    public function getProtLevelN2K()
    {
        return $this->protLevelN2K;
    }

    /**
     * Set addProtLevel
     *
     * @param string $addProtLevel
     * @return SchedaTranslation
     */
    public function setAddProtLevel($addProtLevel)
    {
        $this->addProtLevel = $addProtLevel;
    
        return $this;
    }

    /**
     * Get addProtLevel
     *
     * @return string 
     */
    public function getAddProtLevel()
    {
        return $this->addProtLevel;
    }
}