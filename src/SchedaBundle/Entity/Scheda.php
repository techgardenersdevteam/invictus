<?php

namespace Invictus\SchedaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Scheda
 *
 * @ORM\Table(name="scheda")
 * @ORM\Entity(repositoryClass="Invictus\SchedaBundle\Entity\SchedaRepository")
 */
class Scheda extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cod", type="string", nullable=true)
     */
    private $cod;

    /**
     * @var string
     *
     * @ORM\Column(name="sic_zps", type="string", nullable=true)
     */
    private $sicZps;

    /**
     * @var string
     *
     * @ORM\Column(name="codice_n2k", type="string", nullable=true)
     */
    private $codiceN2K;

    /**
     * @var string
     *
     * @ORM\Column(name="famiglia", type="string", nullable=true)
     */
    private $famiglia;

    /**
     * @var string
     *
     * @ORM\Column(name="ordine", type="string", nullable=true)
     */
    private $ordine;

    /**
     * @var string
     *
     * @ORM\Column(name="classe", type="string", nullable=true)
     */
    private $classe;

    /**
     * @var string
     *
     * @ORM\Column(name="divisione", type="string", nullable=true)
     */
    private $divisione;

    /**
     * @var string
     *
     * @ORM\Column(name="superdivisione", type="string", nullable=true)
     */
    private $superdivisione;

    /**
     * @var string
     *
     * @ORM\Column(name="sottoregno", type="string", nullable=true)
     */
    private $sottoregno;

    /**
     * @var string
     *
     * @ORM\Column(name="regno", type="string", nullable=true)
     */
    private $regno;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_level_sc", type="text", nullable=true)
     */
    private $descLevelSc;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category_id", referencedColumnName="id")
     * })
     */
    private $fkCategory;

    /**
     * @ORM\OneToMany(targetEntity="SchedaTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkSchedaItem")
     */
    protected $visibilities;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkSchedaItem")
     */
    //protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkSchedaItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        //$this->metadatas = new ArrayCollection();
        //$this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->code;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cod
     *
     * @param string $cod
     * @return Scheda
     */
    public function setCod($cod)
    {
        $this->cod = $cod;
    
        return $this;
    }

    /**
     * Get cod
     *
     * @return string 
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * Set codiceN2K
     *
     * @param string $codiceN2K
     * @return Scheda
     */
    public function setCodiceN2K($codiceN2K)
    {
        $this->codiceN2K = $codiceN2K;
    
        return $this;
    }

    /**
     * Get codiceN2K
     *
     * @return string 
     */
    public function getCodiceN2K()
    {
        return $this->codiceN2K;
    }

    /**
     * Add translations
     *
     * @param \Invictus\SchedaBundle\Entity\SchedaTranslation $translations
     * @return Scheda
     */
    public function addTranslation(\Invictus\SchedaBundle\Entity\SchedaTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\SchedaBundle\Entity\SchedaTranslation $translations
     */
    public function removeTranslation(\Invictus\SchedaBundle\Entity\SchedaTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Scheda
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Set fkCategory
     *
     * @param \Invictus\CmsBundle\Entity\Category $fkCategory
     * @return Scheda
     */
    public function setFkCategory(\Invictus\CmsBundle\Entity\Category $fkCategory = null)
    {
        $this->fkCategory = $fkCategory;
    
        return $this;
    }

    /**
     * Get fkCategory
     *
     * @return \Invictus\CmsBundle\Entity\Category 
     */
    public function getFkCategory()
    {
        return $this->fkCategory;
    }

    /**
     * Set famiglia
     *
     * @param string $famiglia
     * @return Scheda
     */
    public function setFamiglia($famiglia)
    {
        $this->famiglia = $famiglia;
    
        return $this;
    }

    /**
     * Get famiglia
     *
     * @return string 
     */
    public function getFamiglia()
    {
        return $this->famiglia;
    }

    /**
     * Set ordine
     *
     * @param string $ordine
     * @return Scheda
     */
    public function setOrdine($ordine)
    {
        $this->ordine = $ordine;
    
        return $this;
    }

    /**
     * Get ordine
     *
     * @return string 
     */
    public function getOrdine()
    {
        return $this->ordine;
    }

    /**
     * Set classe
     *
     * @param string $classe
     * @return Scheda
     */
    public function setClasse($classe)
    {
        $this->classe = $classe;
    
        return $this;
    }

    /**
     * Get classe
     *
     * @return string 
     */
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * Set divisione
     *
     * @param string $divisione
     * @return Scheda
     */
    public function setDivisione($divisione)
    {
        $this->divisione = $divisione;
    
        return $this;
    }

    /**
     * Get divisione
     *
     * @return string 
     */
    public function getDivisione()
    {
        return $this->divisione;
    }

    /**
     * Set superdivisione
     *
     * @param string $superdivisione
     * @return Scheda
     */
    public function setSuperdivisione($superdivisione)
    {
        $this->superdivisione = $superdivisione;
    
        return $this;
    }

    /**
     * Get superdivisione
     *
     * @return string 
     */
    public function getSuperdivisione()
    {
        return $this->superdivisione;
    }

    /**
     * Set sottoregno
     *
     * @param string $sottoregno
     * @return Scheda
     */
    public function setSottoregno($sottoregno)
    {
        $this->sottoregno = $sottoregno;
    
        return $this;
    }

    /**
     * Get sottoregno
     *
     * @return string 
     */
    public function getSottoregno()
    {
        return $this->sottoregno;
    }

    /**
     * Set regno
     *
     * @param string $regno
     * @return Scheda
     */
    public function setRegno($regno)
    {
        $this->regno = $regno;
    
        return $this;
    }

    /**
     * Get regno
     *
     * @return string 
     */
    public function getRegno()
    {
        return $this->regno;
    }

    /**
     * Set descLevelSc
     *
     * @param string $descLevelSc
     * @return Scheda
     */
    public function setDescLevelSc($descLevelSc)
    {
        $this->descLevelSc = $descLevelSc;
    
        return $this;
    }

    /**
     * Get descLevelSc
     *
     * @return string 
     */
    public function getDescLevelSc()
    {
        return $this->descLevelSc;
    }

    /**
     * Set sicZps
     *
     * @param string $sicZps
     * @return Scheda
     */
    public function setSicZps($sicZps)
    {
        $this->sicZps = $sicZps;
    
        return $this;
    }

    /**
     * Get sicZps
     *
     * @return string 
     */
    public function getSicZps()
    {
        return $this->sicZps;
    }
}