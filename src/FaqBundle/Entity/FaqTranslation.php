<?php

namespace Invictus\FaqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * FaqTranslation
 *
 * @ORM\Table(name="faq_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_faq_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_faq_id", "fk_language_id"})
 * @ORM\Entity()
 */
class FaqTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Faq
     *
     * @ORM\ManyToOne(targetEntity="Faq", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_faq_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * @var string
     *
     * @ORM\Column(name="what", type="text", nullable=true)
     */
    private $what;

    /**
     * @var string
     *
     * @ORM\Column(name="abstract2", type="text", nullable=true)
     */
    private $abstract2;

    /**
     * @var string
     *
     * @ORM\Column(name="body2", type="text", nullable=true)
     */
    private $body2;

    /**
     * @var string
     *
     * @ORM\Column(name="prices", type="text", nullable=true)
     */
    private $prices;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\FaqBundle\Entity\Faq $fkBase
     * @return FaqTranslation
     */
    public function setFkBase(\Invictus\FaqBundle\Entity\Faq $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\FaqBundle\Entity\Faq
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Set abstract2
     *
     * @param string $abstract2
     * @return FaqTranslation
     */
    public function setAbstract2($abstract2)
    {
        $this->abstract2 = $abstract2;
    
        return $this;
    }

    /**
     * Get abstract2
     *
     * @return string 
     */
    public function getAbstract2()
    {
        return $this->abstract2;
    }

    /**
     * Set body2
     *
     * @param string $body2
     * @return FaqTranslation
     */
    public function setBody2($body2)
    {
        $this->body2 = $body2;
    
        return $this;
    }

    /**
     * Get body2
     *
     * @return string 
     */
    public function getBody2()
    {
        return $this->body2;
    }

    /**
     * Set what
     *
     * @param string $what
     * @return FaqTranslation
     */
    public function setWhat($what)
    {
        $this->what = $what;
    
        return $this;
    }

    /**
     * Get what
     *
     * @return string 
     */
    public function getWhat()
    {
        return $this->what;
    }

    /**
     * Set prices
     *
     * @param string $prices
     * @return FaqTranslation
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
    
        return $this;
    }

    /**
     * Get prices
     *
     * @return string 
     */
    public function getPrices()
    {
        return $this->prices;
    }
}
