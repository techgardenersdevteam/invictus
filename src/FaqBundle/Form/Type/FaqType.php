<?php

namespace Invictus\FaqBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class FaqType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new FaqTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'faq';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\FaqBundle\Entity\Faq',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
