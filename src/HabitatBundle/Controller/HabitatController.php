<?php

namespace Invictus\HabitatBundle\Controller;

use Invictus\CmsBundle\Controller\InvictusController;
use Invictus\HabitatBundle\Entity\PolygonPoint;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Finder\Finder;

class HabitatController extends InvictusController
{

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        //$config['appId'] = 'vs';
        //$config['languageId'] = 'en_GB';
        //$config['moduleId'] = 4;
        //$config['orderBy'] = array(
        //   'date' => 'base.visibleDate DESC'
        //);
        $config['joins'] = array(
            'position' => false
        );
        $this->tableManager->addConfig($config);
        $this->tableManager->enablePredefinedToggle('enabled');

        //$this->tableManager->enableAction('edit');
        //$this->tableManager->disableAction('delete');
    }


    public function getPolygonsInRadiusAction($lat, $lng, $radius, $zoom, $discriminator, $itemId)
    {
        $this->em = $this->getDoctrine()->getManager();

        $polygons = $this->getDoctrine()
            ->getRepository('InvictusHabitatBundle:PolygonPoint')
            ->getPolygonsInRadius($lat, $lng, $radius, $zoom, $discriminator, $itemId);

        return new JsonResponse($polygons);
    }


    public function getPolygonsByThemeAction($_locale, $theme, $lat, $lng, $radius, $zoom, $discriminator)
    {

        $cm = $this->get('invictus.contentManager');

        $habitatsConfig = array(
            'moduleId' => 51,
            'languageId' => $_locale,
            'joins' => array(
                'visibility' => false
            ),
            'where' => array(
                'base.fkCategory' => 'base.fkCategory IN('.$theme.')',
            )
        );
        $habitats = $cm->getData($habitatsConfig)['data'];


        $data = array();

        $habitatIds = array();
        foreach($habitats as $habitat){
            $data[$habitat->getId()]['id'] = $habitat->getId();
            //$data[$habitat->getId()]['codice'] = $habitat->getCod();
            //$data[$habitat->getId()]['idCategoria'] = $habitat->getFkCategory()->getId();
            //$data[$habitat->getId()]['categoria'] = $habitat->getFkCategory()->getTranslations()[0]->getLabel();
            $data[$habitat->getId()]['label'] = $habitat->getTranslations()[0]->getDescN2K();
            $data[$habitat->getId()]['color'] = $habitat->getColor();
            $data[$habitat->getId()]['alpha'] = '0.45';
            $data[$habitat->getId()]['zIndex'] = $habitat->getZIndex() ? $habitat->getZIndex() : 1;
            $data[$habitat->getId()]['polygons'] = array();

            $habitatIds[] = $habitat->getId();
        }

        //var_dump($habitatIds);die();

        $polygons = $this->getDoctrine()
            ->getRepository('InvictusHabitatBundle:PolygonPoint')
            ->getAllItemsPolygonsInRadius($lat, $lng, $radius, $zoom, $discriminator, $habitatIds);

        //print_r($polygons);die();
        foreach($polygons as $key => $polygons){
            $data[$key]['polygons'] = $polygons;
        }

        //echo "<pre>";print_r($data);die();

        return new JsonResponse(array_values($data));
    }


    public function getMarkersByThemeAction($_locale, $theme, $lat, $lng, $radius)
    {

        $limit = isset($_GET['limit']) ? $_GET['limit'] : 9999;
        $filter = isset($_GET['filter']) ? $_GET['filter'] : 0;
        $cm = $this->get('invictus.contentManager');

        $data = array();

        $habitatsConfig = array(
            'moduleId' => 51,
            'languageId' => $_locale,
            'joins' => array(
                'visibility' => false
            ),
            'where' => array(
                'base.fkCategory' => 'base.fkCategory IN('.$theme.')',
            )
        );
        $habitats = $cm->getData($habitatsConfig)['data'];

        $habitatIds = array();
        foreach($habitats as $habitat){
            $data['51'.$habitat->getId()]['id'] = $habitat->getId();
            //$data['51'.$habitat->getId()]['codice'] = $habitat->getCod();
            //$data['51'.$habitat->getId()]['idCategoria'] = $habitat->getFkCategory()->getId();
            //$data['51'.$habitat->getId()]['categoria'] = $habitat->getFkCategory()->getTranslations()[0]->getLabel();
            $data['51'.$habitat->getId()]['typeLabel'] = 'habitat';
            $data['51'.$habitat->getId()]['label'] = $habitat->getTranslations()[0]->getComName();
            $data['51'.$habitat->getId()]['label2'] = $habitat->getTranslations()[0]->getScName();
            $data['51'.$habitat->getId()]['type'] = 'habitat';
            $data['51'.$habitat->getId()]['markers'] = array();

            $habitatIds[] = $habitat->getId();
        }

        //var_dump($habitatIds);die();

        if(count($habitatIds)){
            $hMarkers = $this->getDoctrine()
                ->getRepository('InvictusWidgetBundle:GeoPoint')
                ->getAllItemsMarkersInRadius($lat, $lng, $radius, 51, $habitatIds);

            //print_r($polygons);die();
            foreach($hMarkers as $key => $markers){

                if($filter){
                    $tmpMarkers = array();
                    $i = 0;
                    $itemId = 0;
                    foreach($markers as $marker){
                        $i++;
                        if($itemId != $marker['itemId']){
                            $itemId = $marker['itemId'];
                            $i = 0;
                        }
                        if($i){
                            if($i % $filter != 0){
                                continue;
                            }
                        }
                        $tmpMarkers[] = $marker;
                    }
                    $markers = $tmpMarkers;
                }
                $markers = array_slice($markers, 0, $limit);
                $data['51'.$key]['markers'] = $markers;
            }
        }


        $datasheetsConfig = array(
            'moduleId' => 52,
            'languageId' => $_locale,
            'joins' => array(
                'visibility' => false
            ),
            'where' => array(
                'base.fkCategory' => 'base.fkCategory IN('.$theme.')',
            )
        );
        $datasheets = $cm->getData($datasheetsConfig)['data'];

        $datasheetIds = array();
        foreach($datasheets as $datasheet){
            $data['52'.$datasheet->getId()]['id'] = $datasheet->getId();
            //$data['52'.$datasheet->getId()]['codice'] = $datasheet->getCod();
            //$data['52'.$datasheet->getId()]['idCategoria'] = $datasheet->getFkCategory()->getId();
            //$data['52'.$datasheet->getId()]['categoria'] = $datasheet->getFkCategory()->getTranslations()[0]->getLabel();
            $data['52'.$datasheet->getId()]['typeLabel'] = 'specie';
            $data['52'.$datasheet->getId()]['label'] = $datasheet->getTranslations()[0]->getComName();
            $data['52'.$datasheet->getId()]['label2'] = $datasheet->getTranslations()[0]->getScName();
            $data['52'.$datasheet->getId()]['type'] = 'datasheet';
            $data['52'.$datasheet->getId()]['markers'] = array();

            $datasheetIds[] = $datasheet->getId();
        }

        //var_dump($habitatIds);die();
        if(count($datasheetIds)){
            $dMarkers = $this->getDoctrine()
                ->getRepository('InvictusWidgetBundle:GeoPoint')
                ->getAllItemsMarkersInRadius($lat, $lng, $radius, 52, $datasheetIds);

            //print_r($polygons);die();
            foreach($dMarkers as $key => $markers){
                $markers = array_slice($markers, 0, $limit);
                $data['52'.$key]['markers'] = $markers;
            }
        }

        //echo "<pre>";print_r($data);die();

        return new JsonResponse(array_values($data));
    }


    public function importAction()
    {
        $kmlDir = $this->get('kernel')->getRootDir().'/../src/Invictus/HabitatBundle/Resources/kml/';

        $finder = new Finder();
        $finder->files()->in($kmlDir.'poligoni')->name('*.kml')->sortByName();
        $polygons = array();
        foreach ($finder as $file) {
            $path = $kmlDir.'poligoni/'.$file->getRelativePathname();
            $polygons[$file->getRelativePathname()] = $path;
            /*
            // Print the absolute path
            print $file->getRealpath()."\n";

            // Print the relative path to the file, omitting the filename
            print $file->getRelativePath()."\n";

            // Print the relative path to the file
            print $file->getRelativePathname()."\n";
            */
        }

        $finder = new Finder();
        $finder->files()->in($kmlDir.'punti')->name('*.kml')->sortByName();
        $markers = array();
        foreach ($finder as $file) {
            $path = $kmlDir.'punti/'.$file->getRelativePathname();
            $markers[$file->getRelativePathname()] = $path;
            /*
            // Print the absolute path
            print $file->getRealpath()."\n";

            // Print the relative path to the file, omitting the filename
            print $file->getRelativePath()."\n";

            // Print the relative path to the file
            print $file->getRelativePathname()."\n";
            */
        }

        return $this->render('InvictusHabitatBundle:Import:habitat.html.twig', array(
                'polygons' => $polygons,
                'markers' => $markers
            ));

    }


    public function importPolygonsAction($habitatId, $zoom, $filename)
    {

        set_time_limit(7200);

        $dbHost = $this->container->getParameter('database_host');
        $dbName = $this->container->getParameter('database_name');
        $dbUser = $this->container->getParameter('database_user');
        $dbPassword = $this->container->getParameter('database_password');

        $conn = new \PDO("mysql:host=$dbHost;dbname=$dbName;charset=utf8", $dbUser, $dbPassword);

        $filePath = $this->get('kernel')->getRootDir().'/../src/Invictus/HabitatBundle/Resources/kml/poligoni/'.$filename;

        if(file_exists($filePath)){

            $kml = simplexml_load_file($filePath);

            if ($kml === false) {
                echo "Failed loading KML\n";
                foreach(libxml_get_errors() as $error) {
                    echo "\t", $error->message;
                }
                die();
            }

            $query = $conn->prepare("DELETE FROM polygon_point WHERE habitat_id = $habitatId and type = $zoom");
            if(!$query->execute()){
                echo "Errore durante l'eliminazione dei record con habitat_id = $habitatId e type = $zoom, forse non ci sono record con quell'habitat_id e type";
            }

            $i = 0;
            $j = 0;
            $errors = 0;
            foreach($kml->Document->Folder->Placemark as $placemark){

                foreach($placemark->MultiGeometry->Polygon as $polygon){

                    $points = explode(' ', $polygon->outerBoundaryIs->LinearRing->coordinates);
                    array_shift($points);
                    $j += count($points);
                    $i++;

                    $conn->beginTransaction(); //this speeds up a lot

                    foreach($points as $point){

                        $coordinates = explode(',', $point);

                        $polygonPoint = array();
                        $polygonPoint['polygon_id'] = $habitatId.$zoom.$i;
                        $polygonPoint['latitude'] = $coordinates[1];
                        $polygonPoint['longitude'] = $coordinates[0];
                        //$polygonPoint['elevation'] = $coordinates[2];
                        //$polygonPoint['coordinates'] = $coordinates[0].', '.$coordinates[1].', 0';
                        $polygonPoint['habitat_id'] = $habitatId;
                        $polygonPoint['type'] = $zoom;

                        $sql = "INSERT INTO polygon_point( polygon_id, latitude, longitude, habitat_id, type )
                                        values ( :polygon_id, :latitude, :longitude, :habitat_id, :type )";
                        $query = $conn->prepare($sql);
                        $success = $query->execute($polygonPoint);

                        if(!$success){
                            $errors++;
                            echo "<pre>";
                            print_r($query->errorInfo());
                            echo "</pre>";
                            echo "<br><br>";

                           //echo "{$coordinates[0]} - {$coordinates[1]} - $habitatCode - ".date('is').$i." <br>";
                        }
                    }

                    $conn->commit();

                    //echo "<pre>";print_r($points);echo "</pre>";
                }
            }

            echo " - - - - - [ $j punti ] - - - - - -<br>";
            echo " - - - - - [ $i poligoni ] - - - - - -<br>";
            echo " - - - - - [ $errors errori ] - - - - - -";

            die();

        }else{
            echo "$filename non esiste";
        }

        die(' - - - ');
    }


    public function importMarkersAction($habitatId, $filename)
    {

        set_time_limit(7200);

        $dbHost = $this->container->getParameter('database_host');
        $dbName = $this->container->getParameter('database_name');
        $dbUser = $this->container->getParameter('database_user');
        $dbPassword = $this->container->getParameter('database_password');

        $conn = new \PDO("mysql:host=$dbHost;dbname=$dbName;charset=utf8", $dbUser, $dbPassword);

        $filePath = $this->get('kernel')->getRootDir().'/../src/Invictus/HabitatBundle/Resources/kml/punti/'.$filename;

        if(file_exists($filePath)){

            $kml = simplexml_load_file($filePath);

            if ($kml === false) {
                echo "Failed loading KML\n";
                foreach(libxml_get_errors() as $error) {
                    echo "\t", $error->message;
                }
                die();
            }

            $query = $conn->prepare("
            DELETE geo_point FROM geo_point
            INNER JOIN relation ON
            geo_point.id = relation.fk_item_id_b
            AND relation.fk_module_id_a = 51
            AND relation.fk_item_id_a = $habitatId
            AND relation.fk_module_id_b = 53");
            $query->execute();

            $query = $conn->prepare("
            DELETE FROM relation
            WHERE
            fk_module_id_a = 51
            AND fk_item_id_a = $habitatId
            AND fk_module_id_b = 53");
            if(!$query->execute()){
                die("Errore durante l'eliminazione dei record con habitat_code '$habitatId'");
            }


            $i = 0;
            $errors = 0;
            foreach($kml->Document->Folder->Placemark as $placemark){

                foreach($placemark->Point as $point){

                    $coordinates = explode(',', $point->coordinates);
                    $i++;

                    $point = array();
                    $point['latitude'] = $coordinates[1];
                    $point['longitude'] = $coordinates[0];
                    $point['enabled'] = true;
                    $point['deleted'] = false;

                    /*
                    echo "<pre>";
                    echo $i;
                    print_r($point);
                    echo "</pre>";
                    echo "<br><br>";
                    */

                    $sql = "INSERT INTO geo_point( lat, lng, enabled, deleted )
                                    values ( :latitude, :longitude, :enabled, :deleted )";
                    $query = $conn->prepare($sql);
                    $success = $query->execute($point);

                    if(!$success)
                    {
                        $errors++;
                        echo "<pre>";
                        print_r($query->errorInfo());
                        echo "</pre>";
                        echo "<br><br>";
                    }else{
                        $lastInsertId = $conn->lastInsertId();
                        $sql = "INSERT INTO relation( fk_module_id_a, fk_item_id_a, fk_module_id_b, fk_item_id_b, slot )
                                    values ( 51, $habitatId, 53, $lastInsertId, 1)";
                        $query = $conn->prepare($sql);
                        if(!$query->execute()){
                            echo '<br>'.$sql;
                        }
                    }

                }

            }

            echo " - - - - - [ $i punti ] - - - - - -<br>";

            echo " - - - - - [ $errors errori ] - - - - - -";

            die();

        }else{
            echo "$filename non esiste";
        }

        die(' - - - ');
    }


    public function getHabitatMarkersByIdsAction($ids, $_locale, $lat, $lng, $radius){

        $markers = $this->getDoctrine()
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->getGeoPointsByHabitatIds($ids, $_locale, $lat, $lng, $radius);

        return new JsonResponse($markers);
    }


    public function getDatasheetMarkersByIdsAction($ids, $_locale, $lat, $lng, $radius){

        $markers = $this->getDoctrine()
            ->getRepository('InvictusWidgetBundle:GeoPoint')
            ->getGeoPointsByDatasheetIds($ids, $_locale, $lat, $lng, $radius);

        return new JsonResponse($markers);
    }


    protected function preUpdateItem($item){
        try{
            $length = rand(1,15);
            $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
            $item->setAbstract($randomString);
        }catch (Exception $e){

        }

        return $item;
    }

}
