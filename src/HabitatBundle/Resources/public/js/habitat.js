Table.jqGridOptions.colNames =[
    Translator.trans('actions'),
    Translator.trans('id'),
    Translator.trans('cod', {}, 'habitat'),
    Translator.trans('descN2K', {}, 'habitat'),
    Translator.trans('category'),
    Translator.trans('colorIndex', {}, 'habitat'),
    Translator.trans('enabled')
];
Table.jqGridOptions.colModel = [
    {name:'actions', index:'actions', fixed: true, width:80, sortable: false, search: false, align: 'center'},
    {name:'id', index:'base.id', width:45, fixed: true, sortable: false, search: true, align: 'center'},
    {name:'habitatCode', index:'base.cod', width:10, search:true, align: 'left'},
    {name:'siteCode', index:'translation.descN2K', width:20, search:true, align: 'left'},
    {name:'category', index:'base.fkCategory', width:20, search:true, align: 'left'},
    {name:'colorIndex', index:'base.colorIndex', fixed: true, width:160, search:true, sortable: false, align: 'center'},
    {name:'enabled', index:'base.enabled', fixed: true, width:110, sortable: true, search: false, align: 'center'}
];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'news_label': "Translation.validation.required"
		};
}

//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            //.jqGrid ('setLabel', 'id', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'body', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'source', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'siteCode', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'habitatCode', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'category', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'area', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});