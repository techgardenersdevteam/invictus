<?php

namespace Invictus\HabitatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PolygonPoint
 *
 * @ORM\Table(name="polygon_point")
 * @ORM\Entity(repositoryClass="Invictus\HabitatBundle\Entity\PolygonPointRepository")
 */
class PolygonPoint
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var decimal
     *
     * @ORM\Column(name="latitude", type="decimal", precision=13, scale=10, nullable=true)
     */
    private $latitude;

    /**
     * @var decimal
     *
     * @ORM\Column(name="longitude", type="decimal", precision=13, scale=10, nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="habitat_id", type="integer", nullable=true)
     */
    private $habitatId;

    /**
     * @var integer
     *
     * @ORM\Column(name="polygon_id", type="integer", nullable=false)
     */
    private $polygonId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type = 1;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return PolygonPoint
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    
        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return PolygonPoint
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    
        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set habitatId
     *
     * @param string $habitatId
     * @return PolygonPoint
     */
    public function setHabitatId($habitatId)
    {
        $this->habitatId = $habitatId;
    
        return $this;
    }

    /**
     * Get habitatId
     *
     * @return string 
     */
    public function getHabitatId()
    {
        return $this->habitatId;
    }

    /**
     * Set polygonId
     *
     * @param integer $polygonId
     * @return PolygonPoint
     */
    public function setPolygonId($polygonId)
    {
        $this->polygonId = $polygonId;
    
        return $this;
    }

    /**
     * Get polygonId
     *
     * @return integer 
     */
    public function getPolygonId()
    {
        return $this->polygonId;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PolygonPoint
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
}