<?php

namespace Invictus\HabitatBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Habitat
 *
 * @ORM\Table(name="habitat")
 * @ORM\Entity(repositoryClass="Invictus\HabitatBundle\Entity\HabitatRepository")
 */
class Habitat extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cod", type="string", nullable=true)
     */
    private $cod;

    /**
     * @var string
     *
     * @ORM\Column(name="codice_n2k", type="string", nullable=true)
     */
    private $codiceN2K;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", nullable=true)
     */
    private $color = 'FFFFFF';

    /**
     * @var integer
     *
     * @ORM\Column(name="z_index", type="integer", nullable=true)
     */
    private $zIndex = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="sup_lag", type="string", nullable=true)
     */
    private $supLag;

    /**
     * @var string
     *
     * @ORM\Column(name="sup_liito", type="string", nullable=true)
     */
    private $supLiito;

    /**
     * @var string
     *
     * @ORM\Column(name="sup_sito", type="string", nullable=true)
     */
    private $supSito;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_level_sc", type="text", nullable=true)
     */
    private $descLevelSc;

    /**
     * @var \Category
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Category")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_category_id", referencedColumnName="id")
     * })
     */
    private $fkCategory;

    /**
     * @ORM\OneToMany(targetEntity="HabitatTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkHabitatItem")
     */
    protected $visibilities;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkHabitatItem")
     */
    //protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkHabitatItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        //$this->metadatas = new ArrayCollection();
        //$this->attachments = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cod
     *
     * @param string $cod
     * @return Habitat
     */
    public function setCod($cod)
    {
        $this->cod = $cod;
    
        return $this;
    }

    /**
     * Get cod
     *
     * @return string 
     */
    public function getCod()
    {
        return $this->cod;
    }

    /**
     * Set codiceN2K
     *
     * @param string $codiceN2K
     * @return Habitat
     */
    public function setCodiceN2K($codiceN2K)
    {
        $this->codiceN2K = $codiceN2K;
    
        return $this;
    }

    /**
     * Get codiceN2K
     *
     * @return string 
     */
    public function getCodiceN2K()
    {
        return $this->codiceN2K;
    }

    /**
     * Set supLag
     *
     * @param string $supLag
     * @return Habitat
     */
    public function setSupLag($supLag)
    {
        $this->supLag = $supLag;
    
        return $this;
    }

    /**
     * Get supLag
     *
     * @return string 
     */
    public function getSupLag()
    {
        return $this->supLag;
    }

    /**
     * Set supLiito
     *
     * @param string $supLiito
     * @return Habitat
     */
    public function setSupLiito($supLiito)
    {
        $this->supLiito = $supLiito;
    
        return $this;
    }

    /**
     * Get supLiito
     *
     * @return string 
     */
    public function getSupLiito()
    {
        return $this->supLiito;
    }

    /**
     * Set supSito
     *
     * @param string $supSito
     * @return Habitat
     */
    public function setSupSito($supSito)
    {
        $this->supSito = $supSito;
    
        return $this;
    }

    /**
     * Get supSito
     *
     * @return string 
     */
    public function getSupSito()
    {
        return $this->supSito;
    }

    /**
     * Set descLevelSc
     *
     * @param string $descLevelSc
     * @return Habitat
     */
    public function setDescLevelSc($descLevelSc)
    {
        $this->descLevelSc = $descLevelSc;
    
        return $this;
    }

    /**
     * Get descLevelSc
     *
     * @return string 
     */
    public function getDescLevelSc()
    {
        return $this->descLevelSc;
    }

    /**
     * Add translations
     *
     * @param \Invictus\HabitatBundle\Entity\HabitatTranslation $translations
     * @return Habitat
     */
    public function addTranslation(\Invictus\HabitatBundle\Entity\HabitatTranslation $translations)
    {
        $translations->setFkBase($this);
        $this->translations[] = $translations;
    
        return $this;
    }

    /**
     * Remove translations
     *
     * @param \Invictus\HabitatBundle\Entity\HabitatTranslation $translations
     */
    public function removeTranslation(\Invictus\HabitatBundle\Entity\HabitatTranslation $translations)
    {
        $this->translations->removeElement($translations);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Habitat
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Habitat
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set fkCategory
     *
     * @param \Invictus\CmsBundle\Entity\Category $fkCategory
     * @return Habitat
     */
    public function setFkCategory(\Invictus\CmsBundle\Entity\Category $fkCategory = null)
    {
        $this->fkCategory = $fkCategory;
    
        return $this;
    }

    /**
     * Get fkCategory
     *
     * @return \Invictus\CmsBundle\Entity\Category 
     */
    public function getFkCategory()
    {
        return $this->fkCategory;
    }

    /**
     * Set zIndex
     *
     * @param integer $zIndex
     * @return Habitat
     */
    public function setZIndex($zIndex)
    {
        $this->zIndex = $zIndex;
    
        return $this;
    }

    /**
     * Get zIndex
     *
     * @return integer 
     */
    public function getZIndex()
    {
        return $this->zIndex;
    }
}