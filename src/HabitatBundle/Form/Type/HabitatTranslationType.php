<?php
  
namespace Invictus\HabitatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HabitatTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('descN2K', 'text', array(
                'label' => 'descN2K',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'single-line-mce'
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('scName', 'text', array(
                'label' => 'scName',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'single-line-mce'
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('comName', 'text', array(
                'label' => 'comName',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('protLevelN2K', 'choice', array(
                'label' => 'protLevelN2K',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'select2',
                ),
                'choices'   => array('1' => '1 - Prioritario', '2' => '2 - Comunitario', '3' => '3 - Non comunitario'),
                'empty_value' => false,
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('descLevel1', 'textarea', array(
                'label' => 'descLevel1',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('descLevel2', 'textarea', array(
                'label' => 'descLevel2',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                ),
                'translation_domain' => 'scheda'
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\HabitatBundle\Entity\Habitat'
            )
        );

    }

    public function getName()
    {
        return 'habitat_translation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\HabitatBundle\Entity\HabitatTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
