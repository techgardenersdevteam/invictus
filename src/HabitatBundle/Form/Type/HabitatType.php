<?php

namespace Invictus\HabitatBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class HabitatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox',
                     'autocomplete' => 'off'
                )
            )
        );

        $builder->add('cod', 'text', array(
                'label' => 'cod',
                'required' => false,
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('zIndex', 'text', array(
                'label' => 'zIndex',
                'required' => false,
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('codiceN2K', 'text', array(
                'label' => 'codiceN2K',
                'required' => false,
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('color', 'text', array(
                'label' => 'color',
                'required' => false,
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('supLag', 'text', array(
                'label' => 'supSito',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('supLiito', 'text', array(
                'label' => 'supLiito',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('supSito', 'text', array(
                'label' => 'supSito',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('descLevelSc', 'textarea', array(
                'label' => 'descLevelSc',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                ),
                'translation_domain' => 'habitat'
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkCategory', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Category'
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new HabitatTranslationType() ));

        //$builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'habitat';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\HabitatBundle\Entity\Habitat',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
