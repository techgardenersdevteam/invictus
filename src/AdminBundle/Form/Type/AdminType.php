<?php

namespace Invictus\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('isActive', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('firstName', 'text', array(
                'translation_domain' => 'admin',
                'label' => 'firstName',
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('lastName', 'text', array(
                'label' => 'lastName',
                'translation_domain' => 'admin',
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('email', 'email', array(
                'label' => 'email',
                'attr'   =>  array(
                )
            )
        );

        $builder->add('username', 'text', array(
                'label' => 'username',
                'attr'   =>  array(
                )
            )
        );

        $builder->add('password', 'password', array(
                'label' => 'password',
                'required' => (in_array('update', $options['validation_groups'])) ? false : true,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('defaultApp', 'entity', array(
                'class' => 'InvictusCmsBundle:App',
                'property' => 'label',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('app')
                        ->where('app.enabled = 1')
                        ->andwhere('app.deleted = 0')
                        ->orderBy('app.label', 'ASC');
                },
                'label' => 'mainApp',
                'translation_domain' => 'admin',
                'attr'   =>  array(
                    'class' => 'select2'
                )
            )
        );

        $builder->add('defaultLanguage', 'entity', array(
                'class' => 'InvictusCmsBundle:Language',
                'property' => 'label',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('language')
                        ->where('language.enabled = 1')
                        ->andwhere('language.deleted = 0')
                        ->orderBy('language.label', 'ASC');
                },
                'label' => 'mainLanguage',
                'translation_domain' => 'admin',
                'attr'   =>  array(
                    'class' => 'select2',
                    'style' => 'width:100%' //necessario per ridimensionare select2
                )
            )
        );

        $builder->add('UILanguage', 'entity', array(
                'class' => 'InvictusCmsBundle:Language',
                'property' => 'label',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('language')
                        ->where('language.enabled = 1')
                        ->andwhere('language.deleted = 0')
                        ->orderBy('language.label', 'ASC');
                },
                'label' => 'UILanguage',
                'translation_domain' => 'admin',
                'attr'   =>  array(
                    'class' => 'select2',
                    'style' => 'width:100%' //necessario per ridimensionare select2
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'attr'   =>  array(
                )
            )
        );

    }

    public function getName()
    {
        return 'admin';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\AdminBundle\Entity\Admin',
            'validation_groups' => array(),
            'invictusKernel' => null
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('')
        );
    }

}