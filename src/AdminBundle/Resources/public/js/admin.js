Table.jqGridOptions.colNames =[
        Translator.trans('actions'),
        Translator.trans('id'),
        Translator.trans('lastName', {}, 'admin'),
        Translator.trans('firstName', {}, 'admin'),
        Translator.trans('email'),
        Translator.trans('username'),
        Translator.trans('enabled')
    ];
Table.jqGridOptions.colModel = [
        {name:'actions', index:'actions', fixed: true, width:70, sortable: false, search: false, align: 'center'},
        {name:'id', index:'base.id', fixed: true, width:50, sortable: true, search: true, align: 'center'},
        {name:'lastName', index:'base.lastName', width:20, sortable: true, search: true, align: 'left'},
        {name:'firstName', index:'base.firstName', width:20, sortable: true, search: true, align: 'left'},
        {name:'email', index:'base.email', width:20, sortable: true, search: true, align: 'left'},
        {name:'username', index:'base.username', width:20, sortable: true, search: true, align: 'left'},
	    {name:'enabled', index:'base.isActive', fixed: true, width:90, sortable: true, search: false, align: 'center'}
	];

Form.getValidateRules = function(){
    console.info('Form.getValidateRules');
    /*
    return {
        'app_label': 'required',
        'app_path': 'required'
    };*/
}

Form.getValidateMessages = function(){
    console.info('Form.getValidateMessages');
    return {
	        'admin_email': "Translation.validation.required",
            'admin_username': "Translation.validation.required"
		};
}
//$("#password").rules("remove", "required");


$(document).ready(function(){
    if($('body').hasClass('table')){
        $("#jqGrid")
            .jqGrid ('setLabel', 'id', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'lastName', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'firstName', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'email', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'username', '', {'text-align':'left'})
            .jqGrid ('setLabel', 'enabled', '', {'text-align':'left'});
        //$("#jqGrid").hideCol("id");
        //$(document).trigger('setItemListAutoWidth');
    }
});