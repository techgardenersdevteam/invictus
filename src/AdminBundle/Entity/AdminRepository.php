<?php

namespace Invictus\AdminBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Invictus\CmsBundle\Entity\ItemRepository;
use Doctrine\ORM\NoResultException;

/**
 * AgentUserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AdminRepository extends ItemRepository implements UserProviderInterface
{

    public function findAllOrdered($where = array(), $parameters = array(), $fields = array(), $sorts = array(), $page = 1, $rpp = 10)
    {

        if(!is_array($fields)){
            $fields = array($fields) ;
        }
        if(!is_array($sorts)){
            $sorts = array($sorts) ;
        }
        $i = 0;
        $orderClauses = array();
        foreach($fields as $field){
            $sort = (isset($sorts[$i])) ? $sorts[$i] : 'asc';
            $orderClauses[] = $field.' '.$sort;
            $i++;
        }
        $orderBy = implode(', ', $orderClauses);

        $where = implode(' ', $where);
        $offset = ($page - 1) * $rpp;
        $sql = "SELECT
                    admin.*

                    FROM admin

                    WHERE
                    1 = 1
                    $where

                    ORDER BY
                    $orderBy

                    LIMIT $offset, $rpp";
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        foreach($parameters AS $key => $value){
            $stmt->bindValue($key, $value);
        }

        $stmt->execute();
        $data = $stmt->fetchAll();

        return $data;

    }

    public function getTotalRecords()
    {
        $sql = "SELECT count(admin.id)

                    FROM admin

                    WHERE

                    deleted = 0";
        $stmt = $this->getEntityManager()
            ->getConnection()
            ->prepare($sql);
        $stmt->execute();
        return $stmt->fetchColumn();
    }

    public function loadUserByUsername($username)
    {
        $q = $this
            ->createQueryBuilder('a')
            ->where('a.username = :username OR a.email = :email')
            ->andwhere('a.deleted = 0')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
        ;

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin InvictusAdminBundle:Admin object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {

        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }

}