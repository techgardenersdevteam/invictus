<?php 

namespace Invictus\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="admin")
 * @ORM\Entity(repositoryClass="Invictus\AdminBundle\Entity\AdminRepository")
 */
class Admin implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name",type="string", nullable=true)
     */
    protected $firstName;

    /**
     * @ORM\Column(name="last_name",type="string", nullable=true)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @var \App
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\App", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_app_id", referencedColumnName="id")
     * })
     */
    private $fkApp;

    /**
     * @var \App
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\App", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="default_app_id", referencedColumnName="id")
     * })
     */
    private $defaultApp;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Language", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="default_language_id", referencedColumnName="id")
     * })
     */
    private $defaultLanguage;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Invictus\CmsBundle\Entity\Language", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ui_language", referencedColumnName="id")
     * })
     */
    private $UILanguage;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted = 0;




    public function __construct()
    {
        $this->isActive = false;
        $this->salt = md5(uniqid(null, true));
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return array('ROLE_ADMIN');
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * @inheritDoc
     */
    public function isEqualTo(UserInterface $user)
    {
        return $this->id === $user->getId();
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id/*,
            $this->username*/
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id/*,
            $this->username*/
            ) = unserialize($serialized);
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        if(!empty($password)){
            $this->password = $password;
        }
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }



    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Admin
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Admin
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Admin
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }



    /**
     * Set fkApp
     *
     * @param \Invictus\CmsBundle\Entity\App $fkApp
     * @return Admin
     */
    public function setFkApp(\Invictus\CmsBundle\Entity\App $fkApp = null)
    {
        $this->fkApp = $fkApp;

        return $this;
    }

    /**
     * Get fkApp
     *
     * @return \Invictus\CmsBundle\Entity\App 
     */
    public function getFkApp()
    {
        return $this->fkApp;
    }

    /**
     * Set defaultApp
     *
     * @param \Invictus\CmsBundle\Entity\App $defaultApp
     * @return Admin
     */
    public function setDefaultApp(\Invictus\CmsBundle\Entity\App $defaultApp = null)
    {
        $this->defaultApp = $defaultApp;

        return $this;
    }

    /**
     * Get defaultApp
     *
     * @return \Invictus\CmsBundle\Entity\App 
     */
    public function getDefaultApp()
    {
        return $this->defaultApp;
    }

    /**
     * Set defaultLanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $defaultLanguage
     * @return Admin
     */
    public function setDefaultLanguage(\Invictus\CmsBundle\Entity\Language $defaultLanguage = null)
    {
        $this->defaultLanguage = $defaultLanguage;

        return $this;
    }

    /**
     * Get defaultLanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getDefaultLanguage()
    {
        return $this->defaultLanguage;
    }

    /**
     * Set UILanguage
     *
     * @param \Invictus\CmsBundle\Entity\Language $uILanguage
     * @return Admin
     */
    public function setUILanguage(\Invictus\CmsBundle\Entity\Language $UILanguage = null)
    {
        $this->UILanguage = $UILanguage;

        return $this;
    }

    /**
     * Get UILanguage
     *
     * @return \Invictus\CmsBundle\Entity\Language 
     */
    public function getUILanguage()
    {
        return $this->UILanguage;
    }
}
