<?php

namespace Invictus\AdminBundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;

use Invictus\CmsBundle\Controller\InvictusController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends InvictusController
{

    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        /* prendo il locale dal browser utente, in seguito verrà preso dalle preferenze dell'utente loggato */
        $preferredLanguage = $request->getPreferredLanguage();
        $request->setLocale($preferredLanguage);
        $this->get('session')->set('_locale', $preferredLanguage);

        /* Passare in GET una Pwd per generare al volo salt e password codificata */
        //$factory = $this->get('security.encoder_factory');
        //$user = new \Invictus\AdminBundle\Entity\Admin();
        //$encoder = $factory->getEncoder($user);
        //$salt = $user->getSalt();
        //$password = "GrabVS15";
        //$hashedPassword = $encoder->encodePassword($password, $salt);
        //echo "salt: $salt <br />password: $password <br />hash: $hashedPassword <br />";
        //exit();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        $response = new Response();
        $response->headers->set('INVICTUS-page', 'Login');

        return $this->render(
            'InvictusAdminBundle:Admin:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            ),
            $response
        );
    }


    protected function configureTableList(){

        $this->fields[] = 'admin.last_name';
        $this->sorts[] = 'asc';

        $this->fields[] = 'admin.first_name';
        $this->sorts[] = 'asc';

        if($this->invictusKernel->getGETParam('id')){
            $this->where[] = ' AND id LIKE :id';
            $this->parameters['id'] = $this->request->query->get('id');
        }

        if($this->invictusKernel->getGETParam('label')){
            $this->where[] = ' AND label LIKE :label';
            $this->parameters['label'] = '%'.$this->request->query->get('label').'%';
        }

        $this->where[] = ' AND deleted = :deleted';
        $this->parameters['deleted'] = '0';

        $this->tableListConfig['bools'] = array(
            array(
                'table' => 'admin',
                'field' => 'is_active',
                'icons' => array(
                    0 => 'icomoon16-minus-3',
                    1 => 'icomoon16-checkmark-3'
                ),
                'translation' => array(
                    0 => 'disabled',
                    1 => 'enabled'
                ),
                'css' => array(
                    0 => 'disabled',
                    1 => 'enabled'
                )

            )
        );

        $this->tableListConfig['actions'] = array(
            'attachments' => false,
            'edit' => true,
            'delete' => true
        );
    }

    protected function configureTableManager()
    {
        /*
        $defaults = array(
            'select' => array(
                'base' => 'base'
            ),
            'joins' => array(
                'translation' => array(
                    'type' => 'LEFT'
                ),
                'visibility' => array(
                    'type' => 'INNER'
                ),
                'position' => array(
                    'type' => 'LEFT'
                ),
                'metadata' => array(
                    'type' => 'INNER'
                ),
            ),
            'where' => array(
                'base.enabled' => 'base.enabled = :enabled',
                'base.deleted' => 'base.deleted = :deleted',
                'base.fkApp' => 'base.fkApp IS NULL'
            ),
            'groupBy' => array(
            ),
            'orderBy' => array(
            ),
            'params' => array(
                'enabled' => 1,
                'deleted' => 0
            ),
            'paginator' => false
        );
        */

        $config = array();
        $config['orderBy'] = array(
            'base.lastName' => 'base.lastName ASC',
            'base.firstName' => 'base.firstName ASC'
        );
        $config['joins'] = array(
            'position' => false
        );
        $this->tableManager->addConfig($config);

        $this->tableManager->disableAction('attach');
        $this->tableManager->addToggle(array(
            'field' => 'isActive',
            'icons' => array(
                0 => 'icomoon16-minus-3',
                1 => 'icomoon16-checkmark-3'
            ),
            'translation' => array(
                0 => 'notPublished',
                1 => 'published'
            ),
            'css' => array(
                0 => 'disabled',
                1 => 'enabled'
            )
        ));
    }


    protected function preInsertItem($item){

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($item);
        $hashedPassword = $encoder->encodePassword($item->getPassword(), $item->getSalt());
        $item->setPassword($hashedPassword);
        return $item;
    }


    protected function preUpdateItem($item){
        // prendo la password da $_POST.
        // Se usassi $item->getPassword() riceverei la vecchia password già hashata
        $password = $this->invictusKernel->getPOSTParam('admin')['password'];
        if(!empty($password)){
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($item);
            $hashedPassword = $encoder->encodePassword($password, $item->getSalt());
            $item->setPassword($hashedPassword);
        }
        return $item;
    }

}
