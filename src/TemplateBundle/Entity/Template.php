<?php

namespace Invictus\TemplateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Template
 *
 * @ORM\Table(name="template")
 * @ORM\Entity(repositoryClass="Invictus\TemplateBundle\Entity\TemplateRepository")
 */
class Template extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="bundle_views_twig", type="string", nullable=false)
     */
    private $bundleViewsTwig = 'FooBundle:Templates:bar.html.twig';

    /**
     * @var string
     *
     * @ORM\Column(name="bundle_controller_action", type="string", nullable=false)
     */
    private $bundleControllerAction= 'FooBundle:Default:foo';

    /**
     * @ORM\OneToMany(targetEntity="Invictus\PageBundle\Entity\Page", mappedBy="fkTemplate")
     */
    private $pages;


    /**
     * @ORM\OneToMany(targetEntity="TemplateTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkTemplateItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Position", mappedBy="fkTemplateItem")
     */
    protected $positions;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkTemplateItem")
     */
    protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkTemplateItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->metadatas = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->tag;
    }

    public function __toString(){
        return $this->translations[0]->getLabel();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add translation
     *
     * @param \Invictus\TemplateBundle\Entity\TemplateTranslation $translation
     * @return Template
     */
    public function addTranslation(\Invictus\TemplateBundle\Entity\TemplateTranslation $translation)
    {
        $translation->setFkBase($this);
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \Invictus\TemplateBundle\Entity\TemplateTranslation $translation
     */
    public function removeTranslation(\Invictus\TemplateBundle\Entity\TemplateTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }


    /**
     * Add visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     * @return Template
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility[] = $visibility;

        return $this;
    }

    /**
     * Remove visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility->removeElement($visibility);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     * @return Template
     */
    public function addAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    
        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     * @return Template
     */
    public function addMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $metadata->setFkItemId($this->getId());
        $this->metadatas[] = $metadata;

        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     */
    public function removeMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $this->metadatas->removeElement($metadata);
    }

    /**
     * Get metadatas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Template
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Template
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Set bundleViewsTwig
     *
     * @param string $bundleViewsTwig
     * @return Template
     */
    public function setBundleViewsTwig($bundleViewsTwig)
    {
        $this->bundleViewsTwig = $bundleViewsTwig;
    
        return $this;
    }

    /**
     * Get bundleViewsTwig
     *
     * @return string 
     */
    public function getBundleViewsTwig()
    {
        return $this->bundleViewsTwig;
    }

    /**
     * Set fkPageItem
     *
     * @param \Invictus\PageBundle\Entity\Page $fkPageItem
     * @return Template
     */
    public function setFkPageItem(\Invictus\PageBundle\Entity\Page $fkPageItem = null)
    {
        $this->fkPageItem = $fkPageItem;
    
        return $this;
    }

    /**
     * Get fkPageItem
     *
     * @return \Invictus\PageBundle\Entity\Page 
     */
    public function getFkPageItem()
    {
        return $this->fkPageItem;
    }

    /**
     * Set fkServiceItem
     *
     * @param \Invictus\ServiceBundle\Entity\Service $fkServiceItem
     * @return Template
     */
    public function setFkServiceItem(\Invictus\ServiceBundle\Entity\Service $fkServiceItem = null)
    {
        $this->fkServiceItem = $fkServiceItem;

        return $this;
    }

    /**
     * Get fkServiceItem
     *
     * @return \Invictus\ServiceBundle\Entity\Service
     */
    public function getFkServiceItem()
    {
        return $this->fkServiceItem;
    }

    /**
     * Add pages
     *
     * @param \Invictus\PageBundle\Entity\Page $pages
     * @return Template
     */
    public function addPage(\Invictus\PageBundle\Entity\Page $pages)
    {
        $this->pages[] = $pages;
    
        return $this;
    }

    /**
     * Remove pages
     *
     * @param \Invictus\PageBundle\Entity\Page $pages
     */
    public function removePage(\Invictus\PageBundle\Entity\Page $pages)
    {
        $this->pages->removeElement($pages);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add services
     *
     * @param \Invictus\PageBundle\Entity\Service $services
     * @return Template
     */
    public function addService(\Invictus\ServiceBundle\Entity\Service $services)
    {
        $this->services[] = $services;

        return $this;
    }

    /**
     * Remove services
     *
     * @param \Invictus\PageBundle\Entity\Service $services
     */
    public function removeService(\Invictus\ServiceBundle\Entity\Service $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getService()
    {
        return $this->services;
    }

    /**
     * Set bundleControllerAction
     *
     * @param string $bundleControllerAction
     * @return Template
     */
    public function setBundleControllerAction($bundleControllerAction)
    {
        $this->bundleControllerAction = $bundleControllerAction;
    
        return $this;
    }

    /**
     * Get bundleControllerAction
     *
     * @return string 
     */
    public function getBundleControllerAction()
    {
        return $this->bundleControllerAction;
    }
}
