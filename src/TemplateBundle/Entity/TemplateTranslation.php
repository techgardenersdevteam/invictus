<?php

namespace Invictus\TemplateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * TemplateTranslation
 *
 * @ORM\Table(name="template_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_template_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_template_id", "fk_language_id"})
 * @ORM\Entity()
 */
class TemplateTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Template
     *
     * @ORM\ManyToOne(targetEntity="Template", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_template_id", referencedColumnName="id")
     * })
     */
    private $fkBase;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\SchedaBundle\Entity\Template $fkBase
     * @return TemplateTranslation
     */
    public function setFkBase(\Invictus\TemplateBundle\Entity\Template $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\TemplateBundle\Entity\Template
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}