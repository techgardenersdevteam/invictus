<?php

namespace Invictus\TemplateBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class TemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('bundleViewsTwig', 'text', array(
                'label' => 'bundleViewsTwig',
                'required' => true,
                'attr'   =>  array(
                ),
                'translation_domain' => 'template'
            )
        );

        $builder->add('bundleControllerAction', 'text', array(
                'label' => 'bundleControllerAction',
                'required' => true,
                'attr'   =>  array(
                ),
                'translation_domain' => 'template'
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new TemplateTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'template';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\TemplateBundle\Entity\Template',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
