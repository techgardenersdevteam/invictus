<?php

namespace Invictus\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PageTranslation
 *
 * @ORM\Table(name="page_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_page_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_module_id", "fk_item_id", "fk_app_id", "fk_language_id"})
 * @ORM\Entity(repositoryClass="Invictus\PageBundle\Entity\PageTranslationRepository")
 */
class PageTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Page
     *
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_page_id", referencedColumnName="id")
     * })
     */
    private $fkBase;

    /**
     * @var string
     *
     * @ORM\Column(name="home_category", type="string", length=255, nullable=true)
     */
    private $homeCategory;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\PageBundle\Entity\Page $fkBase
     * @return PageTranslation
     */
    public function setFkBase(\Invictus\PageBundle\Entity\Page $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\PageBundle\Entity\Page
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }

    /**
     * Set homeCategory
     *
     * @param string $homeCategory
     * @return PageTranslation
     */
    public function setHomeCategory($homeCategory)
    {
        $this->homeCategory = $homeCategory;
    
        return $this;
    }

    /**
     * Get homeCategory
     *
     * @return string 
     */
    public function getHomeCategory()
    {
        return $this->homeCategory;
    }
}