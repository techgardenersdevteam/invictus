<?php

namespace Invictus\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Invictus\CmsBundle\Entity\Item;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Invictus\PageBundle\Entity\PageRepository")
 */
class Page extends Item
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text_position", type="string", length=255, nullable=true)
     */
    private $textPosition = 'left';

    /**
     * @var string
     *
     * @ORM\Column(name="text_color", type="string", length=255, nullable=true)
     */
    private $textColor = 'white';

    /**
     * @var string
     *
     * @ORM\Column(name="icons_color", type="string", length=255, nullable=true)
     */
    private $iconsColor = 'white';

    /**
     * @var string
     *
     * @ORM\Column(name="footer_color", type="string", length=255, nullable=true)
     */
    private $footerColor = 'white';

    /**
     * @var \Template
     *
     * @ORM\ManyToOne(targetEntity="Invictus\TemplateBundle\Entity\Template", inversedBy="pages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_template_id", referencedColumnName="id")
     * })
     */
    protected $fkTemplate = null;

    /**
     * @ORM\OneToMany(targetEntity="PageTranslation", mappedBy="fkBase")
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Visibility", mappedBy="fkPageItem")
     */
    protected $visibilities;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Position", mappedBy="fkPageItem")
     */
    protected $positions;

    /**
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Metadata", mappedBy="fkPageItem")
     */
    protected $metadatas;

    /*
     * @ORM\OneToMany(targetEntity="Invictus\CmsBundle\Entity\Attachment", mappedBy="fkPageItem")
     */
    //protected $attachments;



    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->visibilities = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->metadatas = new ArrayCollection();
        $this->attachments = new ArrayCollection();
    }

    public function getIdentifier()
    {
        return $this->tag;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add translation
     *
     * @param \Invictus\PageBundle\Entity\PageTranslation $translation
     * @return Page
     */
    public function addTranslation(\Invictus\PageBundle\Entity\PageTranslation $translation)
    {
        $translation->setFkBase($this);
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \Invictus\PageBundle\Entity\PageTranslation $translation
     */
    public function removeTranslation(\Invictus\PageBundle\Entity\PageTranslation $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTranslations()
    {
        return $this->translations;
    }


    /**
     * Add visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     * @return Page
     */
    public function addVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility[] = $visibility;

        return $this;
    }

    /**
     * Remove visibility
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibility
     */
    public function removeVisibility(\Invictus\CmsBundle\Entity\Visibility $visibility)
    {
        $this->visibility->removeElement($visibility);
    }

    /**
     * Get visibilities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVisibilities()
    {
        return $this->visibilities;
    }

    /**
     * Add attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     * @return Page
     */
    public function addAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    
        return $this;
    }

    /**
     * Remove attachment
     *
     * @param \Invictus\CmsBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\Invictus\CmsBundle\Entity\Attachment $attachment)
    {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     * @return Page
     */
    public function addMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $metadata->setFkItemId($this->getId());
        $this->metadatas[] = $metadata;
    
        return $this;
    }

    /**
     * Remove metadata
     *
     * @param \Invictus\CmsBundle\Entity\Metadata $metadata
     */
    public function removeMetadata(\Invictus\CmsBundle\Entity\Metadata $metadata)
    {
        $this->metadatas->removeElement($metadata);
    }

    /**
     * Get metadatas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * Add visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     * @return Page
     */
    public function addVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities[] = $visibilities;
    
        return $this;
    }

    /**
     * Remove visibilities
     *
     * @param \Invictus\CmsBundle\Entity\Visibility $visibilities
     */
    public function removeVisibilitie(\Invictus\CmsBundle\Entity\Visibility $visibilities)
    {
        $this->visibilities->removeElement($visibilities);
    }

    /**
     * Add positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     * @return Page
     */
    public function addPosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \Invictus\CmsBundle\Entity\Position $positions
     */
    public function removePosition(\Invictus\CmsBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Add fkTemplate
     *
     * @param \Invictus\TemplateBundle\Entity\Template $fkTemplate
     * @return Page
     */
    public function addFkTemplate(\Invictus\TemplateBundle\Entity\Template $fkTemplate)
    {
        $this->fkTemplate[] = $fkTemplate;
    
        return $this;
    }

    /**
     * Remove fkTemplate
     *
     * @param \Invictus\TemplateBundle\Entity\Template $fkTemplate
     */
    public function removeFkTemplate(\Invictus\TemplateBundle\Entity\Template $fkTemplate)
    {
        $this->fkTemplate->removeElement($fkTemplate);
    }

    /**
     * Get fkTemplate
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFkTemplate()
    {
        return $this->fkTemplate;
    }

    /**
     * Set fkTemplate
     *
     * @param \Invictus\TemplateBundle\Entity\Template $fkTemplate
     * @return Page
     */
    public function setFkTemplate(\Invictus\TemplateBundle\Entity\Template $fkTemplate = null)
    {
        $this->fkTemplate = $fkTemplate;
    
        return $this;
    }


    /**
     * Set textPosition
     *
     * @param string $textPosition
     * @return Page
     */
    public function setTextPosition($textPosition)
    {
        $this->textPosition = $textPosition;
    
        return $this;
    }

    /**
     * Get textPosition
     *
     * @return string 
     */
    public function getTextPosition()
    {
        return $this->textPosition;
    }

    /**
     * Set textColor
     *
     * @param string $textColor
     * @return Page
     */
    public function setTextColor($textColor)
    {
        $this->textColor = $textColor;
    
        return $this;
    }

    /**
     * Get textColor
     *
     * @return string 
     */
    public function getTextColor()
    {
        return $this->textColor;
    }

    /**
     * Set iconsColor
     *
     * @param string $iconsColor
     * @return Page
     */
    public function setIconsColor($iconsColor)
    {
        $this->iconsColor = $iconsColor;
    
        return $this;
    }

    /**
     * Get iconsColor
     *
     * @return string 
     */
    public function getIconsColor()
    {
        return $this->iconsColor;
    }

    /**
     * Set footerColor
     *
     * @param string $footerColor
     * @return Page
     */
    public function setFooterColor($footerColor)
    {
        $this->footerColor = $footerColor;
    
        return $this;
    }

    /**
     * Get footerColor
     *
     * @return string 
     */
    public function getFooterColor()
    {
        return $this->footerColor;
    }
}