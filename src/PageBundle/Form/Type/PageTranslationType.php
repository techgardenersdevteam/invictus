<?php
  
namespace Invictus\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'title',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('homeCategory', 'text', array(
                'label' => 'homeCategory',
                'required' => false
            )
        );

        $builder->add('abstract', 'text', array(
                'label' => 'abstract',
                'required' => false,
                'attr'   =>  array(
                )
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'content',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'mce'
                )
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\PageBundle\Entity\Page'
            )
        );

    }

    public function getName()
    {
        return 'pagetranslation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\PageBundle\Entity\PageTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
