<?php

namespace Invictus\PageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;
use Doctrine\ORM\EntityRepository;

class PageType extends AbstractType
{

    private function getTemplateChoices($options)
    {
        $iK = $options['invictusKernel'];

        $choices = $iK->getRelatedElementsFromConfig(
            array(
                array(
                    'moduleId' => 4,
                    'itemId' => $iK->moduleId,
                    'relationIndex' => 'template-owning-1'
                ),
                array(
                    'moduleId' => 12
                ),
                true
            )
        )['data'];

        return $choices;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('textPosition', 'choice', array(
                'label' => 'textPosition',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'select2'
                ),
                'choices' => array(
                    'left' => 'left',
                    'right' => 'right',
                    'middle' => 'center',
                ),
                'translation_domain' => 'page'
            )
        );

        $builder->add('textColor', 'choice', array(
                'label' => 'textColor',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'select2'
                ),
                'choices' => array(
                    'white' => 'white',
                    'black' => 'black'
                ),
                'translation_domain' => 'page'
            )
        );

        $builder->add('iconsColor', 'choice', array(
                'label' => 'iconsColor',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'select2'
                ),
                'choices' => array(
                    'white' => 'white',
                    'black' => 'black'
                ),
                'translation_domain' => 'page'
            )
        );

        $builder->add('footerColor', 'choice', array(
                'label' => 'footerColor',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'select2'
                ),
                'choices' => array(
                    'white' => 'white',
                    'black' => 'black'
                ),
                'translation_domain' => 'page'
            )
        );

        $builder->add('fkTemplate', 'entity' , array(
                'class' => 'InvictusTemplateBundle:Template',
                'property' => 'translations[0].label',
                'query_builder' => function(EntityRepository $er) use ($options) {
                    $iK = $options['invictusKernel'];

                    $qb = $er->createQueryBuilder('t')
                        ->select('t, trans')
                        ->innerJoin('t.translations', 'trans', 'WITH', 'trans.fkLanguage = :locale')
                        ->andWhere('t.deleted = 0')
                        ->orderBy('t.id', 'ASC')
                        ->setParameter('locale', $iK->UILanguageId);

                    /*
                    $qb = $iK->getRelatedElementsFromConfig(
                        array(
                            array(
                                'moduleId' => 4,
                                'itemId' => $iK->moduleId,
                                'relationIndex' => 'template-owning-1'
                            ),
                            array(
                                'moduleId' => 12
                            ),
                            false
                        )
                    )['data'];
                    */
                    return $qb;
                },
                'choices' => $this->getTemplateChoices($options),
                /*
                'empty_value' => 'none',
                'empty_data' => null,
                */
                'label' => 'template',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'select2',
                )
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new PageTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'page';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\PageBundle\Entity\Page',
            'allow_add' => true,
            'invictusKernel' => null
        ));
    }

}
