<?php

namespace Invictus\ProjectBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Invictus\CmsBundle\Form\Type\MetadataType;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('enabled', 'checkbox', array(
                'label' => 'enabledDisabled',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                )
            )
        );

        $builder->add('newProd', 'checkbox', array(
                'label' => 'newNotNew',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'checkbox'
                ),
                'translation_domain' => 'product'
            )
        );

        $builder->add('visibleDate', 'datetime', array(
                'widget' => 'single_text',
                'with_seconds' => true,
                'input' => 'datetime',
                'format' => 'y/MM/dd', // HH:mm:ss',
                'label' => 'date',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'datepicker'
                )
            )
        );

        $builder->add('code', 'text', array(
                'label' => 'code',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                ),
                'translation_domain' => 'product'
            )
        );

        $builder->add('icons', 'text', array(
                'label' => 'icons',
                'required' => false,
                'attr'   =>  array(
                ),
                'translation_domain' => 'product'
            )
        );

        $builder->add('deleted', 'hidden', array(
                'label' => 'deleted',
                'required' => false,
                'attr' =>   array(
                    'value' => 0
                )
            )
        );

        $builder->add('fkCategory', 'entity_hidden', array(
                'required' => false,
                'class' => 'Invictus\CmsBundle\Entity\Category'
            )
        );

        $builder->add('fkApp', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\App'
            )
        );

        $builder->add('translations', 'collection', array('type' => new ProjectTranslationType() ));

        $builder->add('metadatas', 'collection', array('type' => new MetadataType() ));
    }

    public function getName()
    {
        return 'project';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\ProjectBundle\Entity\Project',
            'allow_add'    => true,
            'invictusKernel' => null
        ));
    }

}
