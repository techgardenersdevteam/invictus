<?php
  
namespace Invictus\ProjectBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('label', 'text', array(
                'label' => 'title',
                'required' => true,
                'attr'   =>  array(
                    'class' => 'modifying-text'
                )
            )
        );

        $builder->add('abstract', 'textarea', array(
                'label' => 'abstract',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                )
            )
        );

        $builder->add('body', 'textarea', array(
                'label' => 'content',
                'required' => false,
                'attr'   =>  array(
                    'class' => 'simple-mce'
                )
            )
        );

        $builder->add('fkLanguage', 'entity_hidden', array(
                'class' => 'Invictus\CmsBundle\Entity\Language'
            )
        );

        $builder->add('fkBase', 'entity_hidden', array(
                'class' => 'Invictus\ProjectBundle\Entity\Project'
            )
        );

    }

    public function getName()
    {
        return 'projecttranslation';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Invictus\ProjectBundle\Entity\ProjectTranslation',
            'appId' => null,
            'languageId' => null,
            'UILanguageId' => null
        ));
    }
    
}
