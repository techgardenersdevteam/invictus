<?php

namespace Invictus\FrontEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Invictus\CmsBundle\Model\MobileDetect;

class MainController extends Controller
{
    private $device = false;

    public function templatesAction($templateName)
    {
        $twig = ':Templates:'.$templateName.'.html.twig';

        return $this->render($twig, array());
    }

    public function indexAction()
    {
        //die($this->generateUrl('page', array('_locale' => 'it_IT')));
        return $this->redirect('/it_IT', 301);
    }

    public function routerAction(Request $request, $_locale = 'it_IT', $path_and_slug = 'home')
    {
        $ik = $this->get('invictus.kernel');
        $cm = $this->get('invictus.contentManager');

        $folders = explode('/', trim($path_and_slug, '/'));
        $slug = array_pop($folders);
        $path = implode('/', $folders);

        // Retrieve metadata information
        $metadatas = $this->getDoctrine()
            ->getRepository('InvictusCmsBundle:Metadata')
            ->findBy(
                array(
                    'path' => $path,
                    'slug' => $slug
                )
            );

        //var_dump($path, $slug, count($metadatas));
        //$ik->doctrineDump($metadata);die();

        // There could be more then one metadata matching given $path and $slug due to MA ML configurations
        // If more than one metadata were found only the first one is considered even if it could be the wrong one.
        // Todo: alert for duplicated path/slug
        if(count($metadatas) > 1){

            $to = $this->container->getParameter('invictus_superadmin_mail');
            $subject = "Duplicated path_and_slug: $path_and_slug - ". $this->getRequest()->getHost();

            $message = '';
            foreach($metadatas as $metadata){
                $message .= $metadata->getId().' - ';
                $message .= $metadata->getFkModule()->getId().' - ';
                $message .= $metadata->getFkItemId().' \n\r ';
            }

            //mail($to, $subject, $message);


        }elseif(count($metadatas) == 0){

            $html = $this->renderView(':Templates:404.html.twig',
                    array(
                        'title' => 'Oops!',
                        'message' => 'Il contenuto che stavi cercando non è più qui.'
                    )
                );
            $response = new Response($html);
            $response->setStatusCode(Response::HTTP_NOT_FOUND);

            return $response;
        }

        $module = $metadatas[0]->getFkModule();
        $itemId = $metadatas[0]->getFkItemId();

        $config = array(
            'moduleId' => $module->getId(),
            'itemId' => $itemId,
            'languageId' => $_locale
        );

        //var_dump($config);die();

        $content = $cm->getData($config);

        if($content['data']){

            $item = $content['data'];
            $template = $item->getFkTemplate();

            // Se l'item non ha un template associato prendo il primo template associato al suo modulo
            if(!$item->getFkTemplate()){

                $relTemplatesConfig = array(
                    'itemId' => $module->getId(),
                    'moduleId' => 4,
                    'relationIndex' => 'template-owning-1'
                );

                $relTemplatesIds = $ik->getRelatedIdsFromConfig(array($relTemplatesConfig));

                $relTemplates = array();
                if(count($relTemplatesIds)){
                    $relFaqsConfig = array(
                        'moduleId' => 12,
                        'appId' => null,
                        'languageId' => null,
                        'joins' => array(
                            'metadata' => false
                        ),
                        'where' => array(
                            'base.id' => 'base.id IN('.implode(',', $relTemplatesIds).')'
                        )
                    );
                    $relTemplates = $cm->getData($relFaqsConfig)['data'];
                    $template = $relTemplates[0];
                }
            }

            $attachmetsConfig = array(
                'fkModuleId' => $module->getId(),
                'fkItemId' => $item->getId(),
                'appId' => null,
                'languageId' => $_locale,
                //'joins' => array(
                //    'visibility' => true
                //),
                //'typology' => 19
            );
            $attachments = $cm->getAttachments($attachmetsConfig);

        }else{
            return $this->redirect("/", 302);
        }

        $data = array(
            'template' => $template,
            'module' => $module,
            'item' => $item,
            'attachments' => $attachments['data'],
            'device' => $this->getDeviceInfo(),
            '_locale'  => $_locale);

        $action = $template->getBundleControllerAction();

        $use_forward = true;
        if ($use_forward) {

            $response = $this->forward(
              $action,
              $data,
              $request->query->all() // needed to propagate request query parameters to subrequest
            );

        } else {

            $subRequest = $request->duplicate(null, null, $data);

            $resolver = $this->get("jms_di_extra.controller_resolver");

            $controller = $resolver->getController($subRequest);
            $arguments = $resolver->getArguments($subRequest, $controller);

            $response = call_user_func_array($controller, $arguments);
        }

        return $response;
    }

    // This is the default action and every template could point to this action
    // so you are not forced to create an action for each template if no custom logic is required
    public function defaultAction($template, $module, $item, $attachments, $device_info, $_locale = 'it_IT')
    {
        return $this->render($template->getBundleViewsTwig(), array(
                'content' => $item,
                'attachments' => $attachments,
                'device' => $device_info
            ));
    }


    private function getDeviceInfo()
    {
        if(!$this->device){
            $detect = new MobileDetect();
            $this->device = array(
                'isPhone' => $detect->isMobile() && !$detect->isTablet(),
                'isTablet' => $detect->isTablet(),
                'isMobile' => $detect->isMobile(),
                'isDesktop' => !$detect->isMobile()
            );
        }

        return $this->device;
    }


}
