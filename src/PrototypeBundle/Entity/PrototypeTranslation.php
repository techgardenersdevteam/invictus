<?php

namespace Invictus\PrototypeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Invictus\CmsBundle\Entity\ItemTranslation;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PrototypeTranslation
 *
 * @ORM\Table(name="prototype_translation", uniqueConstraints={@ORM\UniqueConstraint(columns={"fk_prototype_id", "fk_language_id"})})
 * @UniqueEntity(fields={"fk_prototype_id", "fk_language_id"})
 * @ORM\Entity()
 */
class PrototypeTranslation extends ItemTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Prototype
     *
     * @ORM\ManyToOne(targetEntity="Prototype", inversedBy="translations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_prototype_id", referencedColumnName="id")
     * })
     */
    private $fkBase;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fkBase
     *
     * @param \Invictus\PrototypeBundle\Entity\Prototype $fkBase
     * @return PrototypeTranslation
     */
    public function setFkBase(\Invictus\PrototypeBundle\Entity\Prototype $fkBase = NULL)
    {
        $this->fkBase = $fkBase;

        return $this;
    }

    /**
     * Get fkBase
     *
     * @return \Invictus\PrototypeBundle\Entity\Prototype
     */
    public function getFkBase()
    {
        return $this->fkBase;
    }
}